$(document).ready(function () {
    $.fn.extend({
        animateCss: function (animationName) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            $(this).addClass('animated ' + animationName).one(animationEnd, function () {
                $(this).removeClass('animated ' + animationName);
            });
        }
    });


    $('.fromm-slider').flexslider({
        animation: "fade",
        controlNav: false,
        directionNav: true,
        prevText: "",
        nextText: "",
    });
//    $(".nav-partable-left").find("li").last().hover(function(){
//        $(this).find("resize-left-width").addClass("add-background");
//    })
    $(".multilanguage-ul").click(function () {
        $(".sub-language").slideToggle();
    });

    $(".five-parts").hover(function () {
        $(this).animateCss("flipInX");
    });

    var height_content = $('.get-height').height();
    $('.product-details-box').css("min-height", height_content);


    $('.menu-moblie-product').click(function(){

        if (!$(this).hasClass('active')) {
            $(this).addClass('active');
            $('.menu-for-product').stop().slideDown();
        } else {
            $(this).removeClass('active');
            $('.menu-for-product').stop().slideUp();
        }
    
    });
});


