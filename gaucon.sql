/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50051
Source Host           : localhost:3306
Source Database       : gaucon

Target Server Type    : MYSQL
Target Server Version : 50051
File Encoding         : 65001

Date: 2016-04-22 14:01:18
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `gaucon_age`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_age`;
CREATE TABLE `gaucon_age` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_age
-- ----------------------------
INSERT INTO `gaucon_age` VALUES ('1', 'Trẻ từ 0-3 tháng', 'tre-tu-03-thang', '1', '1');
INSERT INTO `gaucon_age` VALUES ('2', 'Trẻ từ 0-6 tháng', 'tre-tu-06-thang', '2', '1');
INSERT INTO `gaucon_age` VALUES ('3', 'Trẻ từ 3-6 tháng', 'tre-tu-36-thang', '3', '1');
INSERT INTO `gaucon_age` VALUES ('4', 'Trẻ từ 6-12 tháng', 'tre-tu-612-thang', '4', '1');
INSERT INTO `gaucon_age` VALUES ('5', 'Trẻ từ 12-24 tháng', 'tre-tu-1224-thang', '5', '1');
INSERT INTO `gaucon_age` VALUES ('6', 'Trẻ từ 12-36 tháng', 'tre-tu-1236-thang', '6', '1');
INSERT INTO `gaucon_age` VALUES ('7', 'Trẻ từ 2-15 tuổi', 'tre-tu-215-tuoi', '7', '1');
INSERT INTO `gaucon_age` VALUES ('10', 'Mang thai & cho con bú', 'mang-thai-cho-con-bu', '8', '1');
INSERT INTO `gaucon_age` VALUES ('11', 'Phục hồi sức khỏe', 'phuc-hoi-suc-khoe', '9', '1');
INSERT INTO `gaucon_age` VALUES ('12', '', '', '0', '0');

-- ----------------------------
-- Table structure for `gaucon_albums`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_albums`;
CREATE TABLE `gaucon_albums` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `ordering` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=103 DEFAULT CHARSET=utf8 COMMENT='gaucon_products';

-- ----------------------------
-- Records of gaucon_albums
-- ----------------------------
INSERT INTO `gaucon_albums` VALUES ('98', 'TIỆC CƯỚI THÙY LINH & HOÀNG HUY', 'tiec-cuoi-thuy-linh-hoang-huy', 'upload/album/thumb/IMG_1596.JPG', '0', '7', '1');
INSERT INTO `gaucon_albums` VALUES ('93', 'Khai trương Nha Khoa Xuân Thảo', 'khai-truong-nha-khoa-xuan-thao', 'upload/album/thumb/10868261_1007243332637363_6146230794139246899_n.jpg', '1', '2', '1');
INSERT INTO `gaucon_albums` VALUES ('94', 'Khai trương Gấu Con- 139 BÀN CỜ ,P3,Q3', 'khai-truong-gau-con-139-ban-co-p3q3', 'upload/album/thumb/10422218_992945794067117_3067540866703984171_n.jpg', '1', '3', '1');
INSERT INTO `gaucon_albums` VALUES ('96', 'Sheraton Saigon Hotel & Towers', 'sheraton-saigon-hotel-towers', 'upload/album/thumb/10897942_1064469080248121_3219171960172781643_n.jpg', '0', '5', '1');
INSERT INTO `gaucon_albums` VALUES ('97', 'Sự Kiện Cưới', 'su-kien-cuoi', 'upload/album/thumb/1511356_845932068768491_200653950_n.jpg', '0', '6', '1');
INSERT INTO `gaucon_albums` VALUES ('99', 'TIỆC CƯỚI TẠI KHÁCH SẠN REX 06/2015', 'tiec-cuoi-tai-khach-san-rex-062015', 'upload/album/thumb/IMG_3102.JPG', '0', '8', '1');
INSERT INTO `gaucon_albums` VALUES ('100', 'KHAI TRƯƠNG CHI NHÁNH 2 - 582 QUANG TRUNG ,GÒ VẤP', 'khai-truong-chi-nhanh-2-582-quang-trung-go-vap', 'upload/album/thumb/VPP_27661.JPG', '1', '9', '1');
INSERT INTO `gaucon_albums` VALUES ('101', 'MỘT NGÀY CỦA GẤU CON', 'mot-ngay-cua-gau-con', 'upload/album/thumb/IMG_5431.JPG', '1', '10', '1');
INSERT INTO `gaucon_albums` VALUES ('102', 'HOA VÀ EM', 'hoa-va-em', 'upload/album/thumb/IMG_6205.JPG', '1', '11', '1');

-- ----------------------------
-- Table structure for `gaucon_albums_image`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_albums_image`;
CREATE TABLE `gaucon_albums_image` (
  `id` int(11) NOT NULL auto_increment,
  `id_albums` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_albums_image
-- ----------------------------
INSERT INTO `gaucon_albums_image` VALUES ('4', '93', '10888424_1007243272637369_6695153188084600869_n.jpg', '0');
INSERT INTO `gaucon_albums_image` VALUES ('26', '92', '10387478_966708080024222_3166415725192937708_n.jpg', '0');
INSERT INTO `gaucon_albums_image` VALUES ('5', '93', '10891706_1007243389304024_6777014470309798005_n.jpg', '1');
INSERT INTO `gaucon_albums_image` VALUES ('27', '92', '10430822_966777920017238_4744130687309526069_n.jpg', '1');
INSERT INTO `gaucon_albums_image` VALUES ('73', '94', '10501815_992945754067121_7528243022484378408_n.jpg', '2');
INSERT INTO `gaucon_albums_image` VALUES ('72', '94', '10422218_992945794067117_3067540866703984171_n1.jpg', '1');
INSERT INTO `gaucon_albums_image` VALUES ('71', '94', '10325657_992945457400484_43377585550768988_n.jpg', '0');
INSERT INTO `gaucon_albums_image` VALUES ('25', '95', '10305970_985516704810026_9181621230608899972_n2.jpg', '1');
INSERT INTO `gaucon_albums_image` VALUES ('24', '95', '10385595_985516641476699_2807453414357352864_n2.jpg', '0');
INSERT INTO `gaucon_albums_image` VALUES ('28', '92', '10441365_967404563287907_5744175074846275276_n.jpg', '2');
INSERT INTO `gaucon_albums_image` VALUES ('29', '92', '1959447_967404576621239_6563614970465474061_n.jpg', '3');
INSERT INTO `gaucon_albums_image` VALUES ('30', '92', '10435983_967404593287904_4432957389135003105_n1.jpg', '4');
INSERT INTO `gaucon_albums_image` VALUES ('31', '96', '10897942_1064469080248121_3219171960172781643_n1.jpg', '0');
INSERT INTO `gaucon_albums_image` VALUES ('32', '96', '10354237_1064468903581472_2834380341330470523_n.jpg', '1');
INSERT INTO `gaucon_albums_image` VALUES ('33', '96', '11076222_1064468690248160_772154751443384925_n.jpg', '2');
INSERT INTO `gaucon_albums_image` VALUES ('34', '96', '1531533_1064467993581563_8789642069449956851_n.jpg', '3');
INSERT INTO `gaucon_albums_image` VALUES ('35', '96', '12476_1064467750248254_6522463975322764326_n.jpg', '4');
INSERT INTO `gaucon_albums_image` VALUES ('36', '96', '20818_1064467703581592_3733657676969353224_n.jpg', '5');
INSERT INTO `gaucon_albums_image` VALUES ('37', '97', '1964874_845932155435149_1997650923_n.jpg', '0');
INSERT INTO `gaucon_albums_image` VALUES ('38', '97', '150092_845932128768485_653241246_n.jpg', '1');
INSERT INTO `gaucon_albums_image` VALUES ('39', '97', '1779110_845932115435153_46832103_n.jpg', '2');
INSERT INTO `gaucon_albums_image` VALUES ('40', '97', '10155814_845932092101822_418378945_n.jpg', '3');
INSERT INTO `gaucon_albums_image` VALUES ('41', '97', '1779110_845932085435156_6706563_n.jpg', '4');
INSERT INTO `gaucon_albums_image` VALUES ('42', '97', '10153770_845932038768494_604241086_n.jpg', '5');
INSERT INTO `gaucon_albums_image` VALUES ('43', '98', 'IMG_1580.JPG', '0');
INSERT INTO `gaucon_albums_image` VALUES ('44', '98', 'IMG_15961.JPG', '1');
INSERT INTO `gaucon_albums_image` VALUES ('45', '98', 'IMG_1619.JPG', '2');
INSERT INTO `gaucon_albums_image` VALUES ('46', '98', 'IMG_1630.JPG', '3');
INSERT INTO `gaucon_albums_image` VALUES ('47', '98', 'IMG_1632.JPG', '4');
INSERT INTO `gaucon_albums_image` VALUES ('48', '98', 'IMG_1626.JPG', '5');
INSERT INTO `gaucon_albums_image` VALUES ('49', '99', 'IMG_3035.JPG', '0');
INSERT INTO `gaucon_albums_image` VALUES ('50', '99', 'IMG_3043.JPG', '1');
INSERT INTO `gaucon_albums_image` VALUES ('51', '99', 'IMG_3067.JPG', '2');
INSERT INTO `gaucon_albums_image` VALUES ('52', '99', 'IMG_3087.JPG', '3');
INSERT INTO `gaucon_albums_image` VALUES ('53', '99', 'IMG_3036.JPG', '4');
INSERT INTO `gaucon_albums_image` VALUES ('54', '99', 'IMG_3092.JPG', '5');
INSERT INTO `gaucon_albums_image` VALUES ('55', '99', 'IMG_3110.JPG', '6');
INSERT INTO `gaucon_albums_image` VALUES ('56', '99', 'IMG_3114.JPG', '7');
INSERT INTO `gaucon_albums_image` VALUES ('57', '99', 'IMG_3099.JPG', '8');
INSERT INTO `gaucon_albums_image` VALUES ('58', '99', 'IMG_3034.JPG', '9');
INSERT INTO `gaucon_albums_image` VALUES ('59', '99', 'IMG_3108.JPG', '10');
INSERT INTO `gaucon_albums_image` VALUES ('60', '100', 'VPP_2797.JPG', '0');
INSERT INTO `gaucon_albums_image` VALUES ('61', '100', 'VPP_27662.JPG', '1');
INSERT INTO `gaucon_albums_image` VALUES ('62', '100', 'VPP_2828.JPG', '2');
INSERT INTO `gaucon_albums_image` VALUES ('63', '100', 'VPP_2893.JPG', '3');
INSERT INTO `gaucon_albums_image` VALUES ('64', '100', 'VPP_2916.JPG', '4');
INSERT INTO `gaucon_albums_image` VALUES ('65', '100', 'VPP_3014.JPG', '5');
INSERT INTO `gaucon_albums_image` VALUES ('66', '100', 'VPP_3018.JPG', '6');
INSERT INTO `gaucon_albums_image` VALUES ('67', '100', 'VPP_3047.JPG', '7');
INSERT INTO `gaucon_albums_image` VALUES ('68', '100', 'VPP_3070.JPG', '8');
INSERT INTO `gaucon_albums_image` VALUES ('69', '100', 'VPP_3129.JPG', '9');
INSERT INTO `gaucon_albums_image` VALUES ('70', '100', 'VPP_3153.JPG', '10');
INSERT INTO `gaucon_albums_image` VALUES ('74', '101', 'IMG_5426.JPG', '0');
INSERT INTO `gaucon_albums_image` VALUES ('75', '101', 'IMG_5433.JPG', '1');
INSERT INTO `gaucon_albums_image` VALUES ('76', '101', 'IMG_5471.JPG', '2');
INSERT INTO `gaucon_albums_image` VALUES ('77', '101', 'IMG_5474.JPG', '3');
INSERT INTO `gaucon_albums_image` VALUES ('78', '101', 'IMG_5477.JPG', '4');
INSERT INTO `gaucon_albums_image` VALUES ('79', '101', 'IMG_5488.JPG', '5');
INSERT INTO `gaucon_albums_image` VALUES ('80', '101', 'IMG_5504.JPG', '6');
INSERT INTO `gaucon_albums_image` VALUES ('81', '101', 'IMG_5428.JPG', '7');
INSERT INTO `gaucon_albums_image` VALUES ('82', '102', 'IMG_6165.JPG', '0');
INSERT INTO `gaucon_albums_image` VALUES ('83', '102', 'IMG_57291.JPG', '1');
INSERT INTO `gaucon_albums_image` VALUES ('84', '102', 'IMG_6057.JPG', '2');
INSERT INTO `gaucon_albums_image` VALUES ('85', '102', 'IMG_57891.JPG', '3');
INSERT INTO `gaucon_albums_image` VALUES ('86', '102', 'IMG_4903.JPG', '4');
INSERT INTO `gaucon_albums_image` VALUES ('87', '102', 'IMG_3575.JPG', '5');
INSERT INTO `gaucon_albums_image` VALUES ('88', '102', 'IMG_4427.JPG', '6');

-- ----------------------------
-- Table structure for `gaucon_answer_question`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_answer_question`;
CREATE TABLE `gaucon_answer_question` (
  `id` int(11) NOT NULL auto_increment,
  `answer` text NOT NULL,
  `question` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_answer_question
-- ----------------------------
INSERT INTO `gaucon_answer_question` VALUES ('54', 'Con gái mình 15 tuổi, cháu dậy thì từ năm 10 tuổi, hiện cháu cao 1m55 nặng 52kg, cháu có thể dùng loại sữa nào để tiếp tục cao hơn nữa không ạ?', 'Chào bạn, Như bạn mô tả con bạn đã dậy thì được 5 năm rồi, như vậy chiều cao cũng sẽ tăng chậm, nên bạn cần để ý cho bé đi ngủ sớm, tăng cường vận động (các động tác kéo dài người), và chế độ dinh dưỡng giàu can xi như tôm, cua, cá, sữa và các sản phảm từ sữa nhé. Một trong các loại sữa mà bạn có thể dùng cho bé trong giai đoạn này là Majesty XO vì sữa này có hàm lượng Can xi phù hợp với lứa tuổi con bạn. Chúc con bạn có được chiều cao tối ưu. Để được bác sĩ, tiến sĩ của Viện Dinh dưỡng tư vấn sức khỏe dinh dưỡng trực tiếp cho các bé, bạn liên hệ số điện thoại: 0988602605. Riêng những vấn đề về sản phẩm XO, bạn liên hệ trực tiếp đến số Hotline tại Hà Nội (04. 35 561 599), Đà Nẵng (0511. 371 5555) và Tp. HCM (08. 54 11 55 00). Thân ái, BS Namyang', '1', '13');
INSERT INTO `gaucon_answer_question` VALUES ('51', 'Trên web có ghi: Để được bác sĩ, tiến sĩ của Viện Dinh dưỡng tư vấn sức khỏe dinh dưỡng trực tiếp cho các bé, bạn liên hệ số điện thoại: 0988602605. Riêng những vấn đề về sản phẩm XO, bạn liên hệ trực tiếp đến số Hotline tại Hà Nội (04. 35 561 599), Đà Nẵng (0511. 371 5555) và Tp. HCM (08. 54 11 55 00). Vậy em có thắc mắc về sữa I am mother hay Star gold thì gọi về đâu ạ? Và hiện tại em đang cho con dùng I am mother, khi đọc thông tin trên web site thì em chưa thấy những điểm khác biệt gì nổi trội của sữa này so với sữa XO và Star Gold nhưng giá sữa này thì lại cao hơn. Hay trong chính dòng I am mother thì có I am mother 5 dành cho bé từ 3-6 và I am mother Kid dành cho trẻ từ 1 - 15; vậy sự khác nhau giữa 2 loại là gì, khi trẻ từ 3 - 6 tuổi nên dùng loại nào? Rất mong nhận được sự tư vấn của các bác sĩ!', 'Chào chị! Trước hết rất cảm ơn chị đã tin tưởng cho bé sử dụng sản phẩm của hãng. Về câu hỏi chị đưa ra, chúng tôi xin trả lời như sau: Các sản phẩm Iam Mother, Imperial Dream XO và Star Gold đều là các sản phẩm cao cấp của tập đoàn Namyang phân phối tại Việt Nam, được quản lý thương hiệu độc quyền bởi Công ty Cổ phần đầu tư Nam Dương. Trong quá trình sử dụng sản phẩm của hãng, nếu có bất kỳ thắc mắc nào chị vui lòng liên hệ theo số điện thoại Hotline ghi trên vỏ lon sản phẩm Phòng chăm sách khách hàng - Hà Nội: (04) 35561599 - TP. Đà Nẵng: (0511) 3715555 - TP. HCM: (08) 54115500 Iam Mother là dòng sản phẩm đặc biệt cao cấp của Tập đoàn Namyang tại Việt Nam, được sản xuất từ nguồn nguyen liệu chọn lọc, cao cấp nhất với công nghệ GMO 3 cấp để loại bỏ nguyên liệu biến đổi Gen, trải qua 227 bước kiểm tra nghiêm ngạt từ nguyên liệu đến thanh phẩm, giúp trẻ phát triển toàn diện ngay từ những ngày đầu. IAM MOTHER 5 là sản phẩm dinh dưỡng cao cấp dành cho trẻ từ 3 – 7 tuổi, có công thức gần giống sữa mẹ nhất với hương vị nhạt mát, và được bổ sung dưỡng chất cho sự phát triển toàn diện (là sản phẩm đầu tiên có tới 65 thành phần dưỡng chất cao cấp trong đó 36 thành phần đặc biệt có trong sữa mẹ). Được bổ sung sữa Non với nhiều dưỡng chất quan trọng giúp bé phát triển khỏe mạnh và thông minh. Đặc biệt, có chứa Công thức GP – C (và các thành phần IGF, EGF, TGF-beta, Osteopontin) được chứng minh giúp thúc đẩy phát triển tế bào xương, định hình xương và phát triển xương vững chắc, cho bé một chiều cao hoàn hảo. IAM MOTHER KID là sản phẩm dinh dưỡng cao cấp dành cho trẻ 1- 15 tuổi (vị sữa ngọt hơn IAM 5). Đây là sản phẩm duy nhất được bổ sung các loại thảo dược quý với công thức độc quyền từ thiên nhiên KI-180 giúp trẻ tăng trưởng và phát triển tối ưu. KI-180 là sáng chế độc quyền của Namyang và viện nghiên cứu thực phẩm Hàn Quốc có liên quan đến việc thúc đẩy tăng trưởng, đặc biệt là thúc đẩy tăng trưởng xương giúp hỗ trợ phát triển chiều cao hiệu quả. KI- 180 bao gồm các thành thành phần chính như: canxi rong biển, chiết xuất rong tiểu câu, bột sữa non, chiết xuất dầu thực vật, chiết xuất lúa mạch… giúp kích thích tạo tế bào sụn, phát triển xương hiệu quả giúp trẻ cao hơn mà tuyệt đối an toàn. Cả 2 dòng sản phẩm này đều dựa trên những kết quả nghiên cứu Nhi khoa tiên tiến, đạt các tiêu chuẩn dinh dưỡng quốc tế. Tuy nhiên với bé từ 3-6 tuổi chị nên tham khảo cho bé dùng sản phẩm IAM KID để hỗ trợ bé tăng trưởng, chỉ cần 2-3 ly sữa mỗi ngày theo hướng dẫn đã đảm bảo dinh dưỡng cho bé phát triển tối ưu. Bên cạnh đó ở độ tuổi này vị giác của bé thường thích những đồ ăn uống đa dạng về hương vị, sản phẩm IAM KID có thể pha cùng siro hoa quả, socola… giúp bé thay đổi khẩu vị, không bị ngán sữa. Chúc cháu nhà chị luôn khỏe mạnh và phát triển tốt, Thân ái, BS Namyang ', '1', '10');
INSERT INTO `gaucon_answer_question` VALUES ('49', 'Con tôi năm nay lên 7 tuổi nhưng rất bé nặng 17kg mong bác sỹ tư vấn và nếu đi khám thì nên cho cháu đi khám ở đâu? Xin cảm ơn.', 'Chào bạn, con bạn 7 tuổi mới có 17kg là bị suy dinh dưỡng nhé. Bạn nên để ý đến chế độ ăn của bé, để tăng năng lượng trong từng bữa ăn và dùng các sữa cao năng lượng như sữa Kid XO để nhanh chóng cải thiện tình trạng thiếu hụt dinh dưỡng của trẻ. Ngoài ra, bạn cũng nên cho bé đi khám bác sĩ dinh dưỡng ở các viện dinh dưỡng để nhận được lời khuyên chính xác nhé. Chúc bé mau ăn chóng lớn. Thân ái. Bác sĩ Namyang.', '1', '8');
INSERT INTO `gaucon_answer_question` VALUES ('48', 'Thân chào bác sĩ NamYang! Cuối tuần vừa rồi em có đi khám thai, bác sĩ khám cho em bảo thai nhi của em được khoảng 34 tuần rồi, thai nhi đã quay đầu xuống. Đây là lần đầu em mang bầu nên em không rõ lắm, em muốn hỏi bác sĩ thai nhi quay đầu có phải em sắp sanh rồi không? Và khoảng bao lâu thì em sanh, vì em đang làm ở SG, em về quê sanh nên muốn biết chắc để em sắp xếp ngày về, và em bé của em được 1,902g, vậy có nhỏ quá không thưa bác sĩ. Mong bác sĩ tư vấn giúp em, em chân thành cảm ơn.', 'Chị Vân thân mến!\r\n\r\nChắc hẳn trong hành trình đầu tiên làm mẹ của chị lúc này đang mang nhiều tâm trạng,  hồi hộp, hạnh phúc  xen lẫn âu lo, chờ đợi.  Khi thai nhi bước sang tuần thai thứ 30 trở đi, có rất nhiều thứ cần thiết cần chuẩn bị cho quá trình sinh nở và chào đón bé yêu để có được một kỳ sinh nở \"mẹ tròn con vuông\" cũng như cho bé yêu những điều kiện tốt nhất khi chào đời.\r\n\r\nQua thư của chị cho biết thai nhi đã được 34 tuần và đã quay đầu xuống, như vậy là thai kỳ của chị phát triển bình thường, thai nhi theo ngôi thuận và chị hoàn toàn có thể yên tâm.\r\n\r\nThông thường, với những bà mẹ lần đầu sinh con, thì khoảng 40 tuần trong bụng mẹ, bé yêu sẽ chào đời. Như vậy theo dự đoán khoảng 6 tuần nữa là chị sẽ hạ sinh. Còn về cân nặng, hiện tại em bé trong bụng mẹ được 1,902kg, theo phát triển mỗi tuần sẽ thêm được 2 gr, như vậy 6 tuần nữa sẽ được hơn 1,2 kg. Bé sinh ra sẽ được hơn 3 kg, hoàn toàn bình thường và phát triển tốt như mong đợi dành cho trẻ sơ sinh.\r\n\r\nỞ giai đoạn này, các tâm lý sợ hãi, lo lắng, vội vàng khi sinh con đều có ảnh hưởng không tốt tới các thai phụ và thai nhi. Kì thực sinh con là quá trình sinh lý bình thường, phụ nữ mang thai phần nhiều đều cảm thấy sinh con thuận lợi, cần đợi đến ngày sinh mà tin tưởng vào bản năng làm mẹ của mình. Vì vậy chị hãy giữ gìn tâm trạng nhẹ nhàng, thoải mái và vui vẻ, tăng cường chế độ dinh dưỡng, sinh hoạt và chế độ ngủ hợp lý, để chuẩn bị đón chào bé yêu bé bỏng ra đời.\r\n\r\nHiện tại nếu chị đang sinh sống và làm việc tại Sài Gòn thì tốt nhất chị nên sinh con ở tại đây. Vì ở các bệnh viện lớn sẽ có đầy đủ điều kiện vật chất và ứng phó tốt với mọi tình huống xảy ra hơn. Sau đó chị có thể về quê để được gia đình chăm sóc và bồi dưỡng.\r\n\r\n Chúc Chị luôn vui vẻ chuẩn bị cho ngày “ vượt cạn” và “mẹ tròn con vuông”!\r\n\r\nThân ái, \r\nBS Namyang ', '1', '7');
INSERT INTO `gaucon_answer_question` VALUES ('52', 'Con trai tôi đầy 8 tháng, cháu cao 72 cm, nặng 7.8 kg. Hiện tôi cho bé ăn 3 bữa bột một ngày, có đầy đủ thịt, cá, rau xanh.... Hàng ngày, cháu ăn thêm hoa quả, váng sữa và sữa chua. Từ lúc sinh cháu, tôi đã ít sữa và đến bây giờ là gần như không còn nữa. Tôi có cho cháu ăn thêm sữa ngoài, nhưng cháu không chịu ăn mặc dù tôi đã đổi rất nhiều loại sữa. Cháu cũng hay bị viêm đường hô hấp. Xin Bác sĩ tư vấn giúp tôi về dinh dưỡng cho cháu và xin hỏi bác sĩ con tôi có bị suy dinh dưỡng không?', 'Chào chị, 1. So sánh cân nặng và chiều cao chuẩn của bé trai 8 tháng với bé nhà mình: 7.8/8.6kg, 72/70.6cm thì có thể đánh giá bé đang hơi nhẹ cân hơn so với tiêu chuẩn, còn chiều cao đã vượt chuẩn. Cân nặng của bé nếu dưới 7kg sẽ nằm ở mức suy dinh dưỡng cấp độ 1, bé được 7.8kg nằm trong khoảng trung bình từ 6.9-8.6kg. Vì vậy, chị không nên quá lo lắng. 2. Về chế độ ăn uống, bé ăn 1 ngày 3 bữa, đầy đủ thịt, cá, rau xanh, ngoài ra có chị cũng bổ sung hoa quả, váng sữa, sữa chua… như vậy, chế độ ăn tương đối đa dạng. Tuy nhiên, chị cần chú ý liều lượng cũng phải theo tiêu chuẩn. Xin gửi đến chị thực đơn tham khảo theo độ tuổi của bé để chị tiện theo dõi: 6h: Bú mẹ hoặc sữa công thức 150-200ml 9h: Nước 200ml + Bột gạo tẻ (20g) + Lá rau xanh (10g) + Trứng gà (1 lòng đỏ)(có thể thay đổi thịt,cá) + Dầu - mỡ: 5-7ml 11h: Hoa quả nghiền (40ml) 12h: Bú mẹ hoặc sữa công thức 150-200ml 14h: Nước 200ml+ Bột gạo tẻ (20g) + Lá rau xanh (10g) + Cua đồng: 4-5 con (thay đổi thịt, cá…) + Dầu - mỡ: 5-7ml 16h: Hoa quả nghiền (40ml) 18h: Bú mẹ hoặc sữa công thức 150-200ml 3. Giai đoạn này, dinh dưỡng chính của bé vẫn là sữa nên nếu chị không còn sữa thì phải thay thế bằng sữa ngoài cho bé. Nếu bé không chịu bú bình chị cố gắng đút muỗng cho bé, bé không bú được lượng nhiều 1 lần thì chị chia nhỏ bữa, cho bú nhiều lần để đạt được khoảng 600-700ml/ ngày đêm. 4. Sữa XO có những đặc điểm khác biệt và chất lượng vượt trội, chị có thể tham khảo: XO 3 cho bé từ 6-12 tháng + Có đến 58 dưỡng chất, trong đó có 18 dưỡng chất được tìm thấy trong sữa mẹ + Sử dụng đạm sữa thủy phân, giúp bé dễ hấp thu, tiêu hóa, chống dị ứng, nôn trớ + Không sử dụng đường, hương liệu và chất bảo quản, mùi vị nhạt mát như sữa mẹ + DHA, ARA chiết xuất từ cá biển lưng xanh, là nguồn tự nhiên giúp bé hấp thu và phát triển trí não vượt trội + Sử dụng đến 4 gốc đường cao phân tử:GOS, Raffinose, Lactulose & Galactosyllactose là thức ăn cho vi khuẩn có lợi đường ruột nên giúp ổn định tiêu hóa, chống táo bón, tiêu chảy + CPP (Casein Phospho Peptide) giúp vận chuyển hấp thu Ca, tạo cho bé hệ xương vững chắc và có chiều cao tối đa + Hạn sử dụng ngắn, chỉ 18 tháng, luôn đảm bảo độ tươi ngon, HSD dập nổi dưới đáy lon nên tránh được tình trạng hàng nhái, hàng giả. Để được tư vấn dinh dưỡng và sản phẩm chị vui lòng liên hệ: Hà Nội: (04)3.556.15.99 - Đà Nẵng: (0511)3.71.55.55 - HCM: (08)5.411.55.00 Thân ái, BS Namyang.', '1', '11');
INSERT INTO `gaucon_answer_question` VALUES ('53', 'Con em mới 7tháng tuổi mà được 12kg em biết con đang bị bép phì độ 3, vậy em muốn xin Bác sĩ tư vấn về cách ăn và uống cho cháu được không ạ? ', 'Chào bạn Tư, đúng là con bạn đang bị thừa cân-béo phì. Vì bạn không nói rõ con bạn bú sữa mẹ hay uống sữa công thức, nhưng dù thế nào bạn cũng đừng lo lắng quá, vì độ tuổi này chỉ lựa chọn thực phẩm giàu can xi cho bé để bé phát triển chiều cao tối ưu cho bé chứ không đặt vấn đề giảm cân. Bạn nên duy trì bú mẹ hoặc uống sữa công thức, ngoài ra lứa tuổi này chỉ nên cho ăn 1-2 bữa bột/ngày thôi. Khi bé có thể ăn được cá tôm thì nên tăng cường sử dụng để thêm nguồn can xi. Ngoài ra, bạn nên cho bé tắm nắng và đi ngủ sớm. Điều cuối cùng, bạn nên cho bé đi khám bác sĩ định kỳ để nhận được lời khuyên hợp lý nhé. Chúc bạn thành công! Bs Namyang.', '1', '12');
INSERT INTO `gaucon_answer_question` VALUES ('55', 'Bác sĩ ơi tư vấn giúp em một số vấn đề sau với ạ: 1. Khi sinh ra bé nhà em được 3,6kg, tháng đầu được 4kg, tháng 2 được 5kg, tháng 3 được 5,4kg, và giờ được 4 tháng bé nặng 6kg, dài 62cm, bác sĩ cho em hỏi cân nặng như vậy có được coi là bình thường không ạ? 2. Em cho bé ăn thêm sữa ngoài được hơn 1 tháng, trước bé vẫn ăn uống bình thường, nhưng mấy hôm nay bé biếng ăn, kể cả ti mẹ cũng chỉ được ít thôi. Em đang cho bé uống sữa Dielac của Vinamilk, bác sĩ tư vấn em cần phải làm gì? 3. Khi bé thức bé chơi, nhưng khi buồn ngủ là gắt cáu, khóc mãi mới ngủ, điều này có ảnh hưởng đến vấn đề ăn của bé không? Vì thấy bé biếng ăn, em đã cho bé ăn thêm bột, nhưng chỉ được hai lần là bé chán. Xin bác sĩ tư vấn cho em về vấn đề dinh dưỡng và cách chăm sóc bé. Thực sự em rất lo lắng. Em mong nhận được thông tin tư vấn từ bác sĩ. Cảm ơn bác sĩ nhiều. Điện thoại: 0988209336 ', 'Chào bạn, như bạn mô tả, con bạn 4 tháng mới được 6kg là nhẹ cân so với trung bình, nhưng chưa bị suy dinh dưỡng. Bạn cần điều chỉnh chế độ dinh dưỡng cho bé, nên cho bé tiếp tục bú mẹ, nên cho bé bú được sữa cuối (sữa đặc) sẽ giúp bé nhận được nhiều năng lượng hơn, và nếu sữa mẹ không đủ thì bạn có thể cho bé uống thêm sữa công thức, bạn nên chọn các sữa công thức trên thị trường mà có vị không ngọt lắm để cho bé vừa uống thêm sữa công thức mà không bỏ sữa mẹ. Ví dụ bạn có thể dùng sữa công thức XO2 (dành cho trẻ từ 3 đến 6 tháng tuổi). Đây là sản phẩm nổi trội với 58 thành phần dưỡng chất, trong đó có tới 18 thành phần đặc hữu có trong sữa mẹ với hàm lượng cân đối phù hợp. Sản phẩm sử dụng công nghệ đạm sữa thủy phân kết hợp với 4 gốc đường cao phân tử có trong sữa mẹ (trong đó có GOS) giúp trẻ dễ hấp thu, giảm thiểu tối đa tình trạng táo bón, tiêu chảy và nôn trớ. Bổ sung CPP: Giúp bé hấp thu tối đa lượng canxi cần thiết, tăng trưởng chiều cao. Đặc biệt, sữa không đường, không hương liệu tạo mùi giúp sữa có hương vị tự nhiên gần giống như sữa mẹ (giúp trẻ bú sữa ngoài mà không quên hương vị của sữa mẹ). An toàn với hạn sử dụng dập nổi. Thời gian này chưa thích hợp cho bé ăn dặm, vì các khuyến cáo mới về dinh dưỡng khuyên rằng nên ăn dặm khi bé đã được 6 tháng. Về chuyện bé biếng ăn, bạn nên kiên trì khi cho bé ăn và bạn nên cho bé đi khám BS trực tiếp để BS sẽ tư vấn cụ thể vì sao. Chúc bé nhà bạn hay ăn, chóng lớn. Để được bác sĩ, tiến sĩ của Viện Dinh dưỡng tư vấn sức khỏe dinh dưỡng trực tiếp cho các bé, bạn có thể liên hệ số điện thoại: 0988602605. Còn muốn được tư vấn về sản phẩm XO, bạn liên hệ trực tiếp đến số Hotline tại Hà Nội: (04) 35 561 599 ; Đà Nẵng:(0511) 371 5555 ; Tp. HCM: (08) 54 11 55 00 Thân ái, BS Namyang', '1', '14');
INSERT INTO `gaucon_answer_question` VALUES ('56', 'Bé của mình 4 tuổi, nặng 15kg, cao 1.05m, mình có cho bé uống Kid XO được 4 tháng rồi mà không tăng được cm nào cả, vậy có phải là bé không chịu sữa này không? Tổng cộng nguyên năm vừa rổi bé chỉ cao thêm có 2cm và nặng thêm 1kg thôi. Trước khi uống XO, bé đã uống hươu cao cổ (được 8 tháng không thấy gì), trước đó nữa bé uống sữa Friso. Giúp mình chọn sữa cho bé với? ', 'Chào bạn, Với chiều cao của con bạn hiện tại: cao hơn so với trung bình của lứa tuổi, nhưng cân nặng của bé lại hơi nhẹ hơn so với trung bình. Mà bạn đã dùng một số loại sữa khác chưa thấy hiệu quả nhưng khi chuyển sang Kid XO đã có chuyển biến như vậy rất tốt. Theo tôi bạn nên duy trì dùng sữa Kid XO cho bé vì các lý do sau: bé nhà bạn đang cần lên cân và tiếp tục duy trì phát triển chiều cao, mà sản phẩm KID XO là sản phẩm vừa cao năng lượng vừa cung cấp các dưỡng chất và tỷ lệ can xi hợp lý để con bạn có thể phát triển tối đa chiều cao. Ngoài ra, bạn cũng nên cho bé đi ngủ sớm, và vận động hàng ngày, cũng như trong chế độ ăn nên tăng cường tôm, cua, cá để cung cấp đủ can xi cho bé nhé. Thân mến, BS Namyang', '1', '15');
INSERT INTO `gaucon_answer_question` VALUES ('57', 'Xin cho tôi hỏi: bé nhà tôi nay được ba tháng rưỡi, nặng 7,5kg, cao 62cm, bé hay bị ra mồ hôi, có khi ướt cả đầu và quần áo. Thấy xót con quá, tôi bắt máy lạnh cho thoáng không khí, con đỡ ra mồ hôi, nhưng để nhiệt độ nhỏ bé vẫn bị ra mồ hôi, trong khi mắt thì lạnh ngắt. Như vậy có ảnh hưởng gì đến bé không, bé có được bình thường không? ', 'Bạn Kim Nguyen thân mến, Tình trạng dinh dưỡng của con bạn bình thường, còn chuyện bé ra nhiều mồ hôi bạn nên xem bé có bị bọc quá nhiều quần áo hay không? Nếu do nhiều quần áo hoặc do thời thiết thì bạn nên thay đổi. Ngoài ra, bé có bị rụng tóc không? Ngủ có sâu giấc không? Nếu bé có các dấu hiệu này có thể bé nhà bạn có dấu hiệu bị còi xương, bạn nên cho bé đi khám sức khỏe để được Bs khám trực tiếp, rồi đưa ra các lời khuyên chính xác cho con bạn. Chúc bạn nhanh tìm được đáp án. Thân ái, BS Namyang ', '1', '16');
INSERT INTO `gaucon_answer_question` VALUES ('58', 'Tôi mới sinh cháu được hơn 20 ngày, do đẻ mổ, ít sữa nên phải bổ sung sữa ngoài. Hôm trước chị tôi có gửi về cho cháu 1 hộp sữa XO 1 five secret, và hướng dẫn sử dụng là pha với nước sôi 100độ C. Theo tôi tìm hiểu thì các loại sữa cho bé sơ sinh đều pha với nuớc ở nhiệt độ khoảng 40-50 độ C. Nhưng chị tôi sinh con bên Hàn Quốc bảo là y tá bên đấy đều pha sữa cho trẻ sơ sinh bằng nước sôi 100độ C hết. Hiện tôi rất phân vân không biết như thế nào mới đúng, rất mong nhận được tư vấn. Xin cảm ơn. ', 'Chào bạn, Đúng như bạn suy nghĩ và tìm hiểu, hầu hết các loại sữa chỉ nên pha ở khoảng 40-50 độ C mới đảm bảo duy trì được các thành phần dinh dưỡng của bé. Ngoài ra, một số loại sữa đặc biệt có thể pha ở nhiệt độ cao hơn nhưng sẽ có hướng dẫn chi tiết của BS. Thân ái, BS Namyang ', '1', '17');
INSERT INTO `gaucon_answer_question` VALUES ('59', 'Cho em hỏi bé nhà em được 10kg, hiện giờ em vẫn cho bé uống sữa XO Kid, như vậy có phù hợp không ạ? Hay em cho bé uống XO 4? ', 'Vì bạn không nói con bạn bao nhiêu tháng, con trai hay con gái, mà chỉ nói được 10kg, nên chúng tôi không biết tình trạng dinh dưỡng của con bạn cần tăng thêm cân hay chỉ cần duy trì cân nặng. Theo tôi, con bạn vẫn đang dùng Kid XO thì bạn nên tiếp tục dùng vì sản phẩm này năng lượng cao hơn XO 4, sẽ hỗ trợ con bạn tăng cân hơn, ngoài ra nó cũng cung cấp các dưỡng chất khác giúp con bạn phát triển chiều cao cho bé. Thân ái, BS Namyang', '1', '18');
INSERT INTO `gaucon_answer_question` VALUES ('60', 'Bé con em đang uống sữa XO số 4, cho em hỏi là thành phần trong sữa XO, Iam Mother hay XO Kid có được làm từ nhân sâm ko ah? Em xin cảm ơn.', 'Chào bạn Phạm Thảo, Đối với sản phẩm dinh dưỡng XO4 và IAM 4, 5: đã có 57 dưỡng chất và 6 vitamin, không bổ sung nhân sâm. Đối với sản phẩm dinh dưỡng XO Kid (dành cho bé từ 1-9 tuổi), công thức đặc biệt, ngoài 39 dưỡng chất còn có sự kết hợp của trên 20 loại thảo dược, như: sơn tra, tía tô, lộc nhung, việt quất. Đặc biệt có công thức KI-180 với các thành phần: canxi rong biển, bột sữa non, rong tiểu câu và các thảo dược khác (không có nhân sâm) giúp bé có chiều cao tối đa và ngăn ngừa tình trạng dậy thì sớm. Cảm ơn Quý khách hàng đã gửi thắc mắc về cho Ban Biên tập web Namyangi. Phòng chăm sóc khách hàng.', '1', '19');
INSERT INTO `gaucon_answer_question` VALUES ('61', 'Bé nhà mình đã uống được hai hộp XO2 loại 800g, mình thấy bé toàn đi phân xanh và đóng khuôn. Hiện tại bé mới bước qua tháng thứ 5, cao 65cm, nặng 6,3kg. Như vậy có đạt chuẩn không? Bé uống sữa rất nhiều, một ngày 11 bình loại 110ml, nhưng thấy bé vẫn roi roi không xổ sữa. Liệu XO có phù hợp với bé mình không? Mong bác sĩ tư vấn cho em. ', 'Chào bạn Thanh Trúc, Bé nhà bạn sang tháng thứ 5 được 65 cm và 6,3kg như vậy tình trạng dinh dưỡng bình thường. Còn chuyện màu sắc phân của bé: màu vàng là đẹp nhất, còn phân của bé màu xanh và vẫn có khuôn, không có nước hay máu và hơn nữa bé vẫn lên cân bình thường thì vẫn có thể chấp nhận được một thời gian. Có màu phân như vậy có thể do lượng thức ăn được vận chuyển quá nhanh qua đường tiêu hóa, dẫn đến màu như vậy. Bạn nên cho bé tiếp tục sử dụng sản phẩm dinh dưỡng XO2. Các sản phẩm dinh dưỡng của XO có đầy đủ dưỡng chất giúp cho bé phát triển toàn diện cả về cân nặng, chiều cao, trí tuệ, sức đề kháng…phù hợp với độ tuổi của trẻ. Cho trẻ lên cân quá mức là điều không nên, dễ làm trẻ bị dư cân, béo phì. Ngoài ra, thời điểm này bạn có thể cho tập cho bé ăn thêm bột ăn dăm dành cho lứa tuổi 4-6 tháng nhé. Thân ái, BS Namyang', '1', '20');
INSERT INTO `gaucon_answer_question` VALUES ('62', 'Con trai tôi nay được 8 tháng tuổi Tôi cho trẻ uống sữa XO từ tháng thứ 3 đến nay. Hiện tai bé đang uống sữa XO. Bé uống sữa tốt và hiếu động lắm. Tuy nhiên 1 tháng vừa rồi bé không lên cân. Hiện tại bé được 8.5kg. Tôi cho bé ăn dặm từ tháng thứ 6 đến nay. Bé ăn tốt lắm. Nhưng tôi cũng không biết những thức ăn nào nên và không nên cho bé ăn. Đôi khi cho bé ăn thấy bé nỗi mẩn đỏ, sợ bị dị ứng nên tôi không cho bé ăn nữa (thịt bò, cá thu...) Xin ban tư vấn cung cấp cho tôi các loại thức ăn và cách cho bé ăn cụ thể nhé. Bé nhà tôi hay đổ mồ hôi trộm, có cách nào trị hết không? Cảm ơn nhiều.', 'Chào bạn Tấn Hùng, Cảm ơn sự tin cậy của bạn dành cho sản phẩm của Namyang. Về những câu hỏi của bạn, tôi xin trả lời như sau: Như bạn mô tả, bé trai 8 tháng 8,5 kg như vậy là bình thường, chỉ có 1 tháng nay không lên cân thì chưa có gì đáng lo lắng, vì bây giờ mỗi tháng bé chỉ tăng khoảng 200-300g thôi. Nhưng bạn cũng nên để ý lại chế độ ăn uống của bé nhé, bạn nên cho bé ăn 2-3 bữa bột cháo/ngày thôi, mỗi bữa bạn nên cho bé ăn khoảng 1 bát con (180ml-200ml) trong đó bạn cho khoảng 1/4 lạng thịt (cá ....), dầu mỡ khoảng 5-7ml/bữa nhé. Còn lại, bạn cho bé bú mẹ, uống sản phẩm dinh dưỡng XO3, sữa chua và hoa quả thêm cho bé. Nếu bé có nguy cơ bị dị ứng thức ăn thì khi bạn muốn cho bé ăn hải sản, tôm, cua thì bạn cho ăn số lượng ít hơn bình thường và theo dõi tiêu hóa cũng như phản ứng của cơ thể bé nhé. Chúc bé nhà bạn hay ăn, chóng lớn. Thân ái, BS Namyang', '1', '21');
INSERT INTO `gaucon_answer_question` VALUES ('63', 'Con tôi gần 9 tháng tuổi, tôi nên cho bé uống dòng sản phẩm nào của công ty thì tốt nhất? Tôi chỉ cần sản phẩm giúp bé dễ hấp thu dưỡng chất,tiêu hoá tốt,chiều cao,thị lực và trí não phát triển; không cần bé tăng cân nhiều. Quý công ty hãy tư vấn sản phẩm thích hợp cho bé dùm tôi. Bé hiện 9 tháng, cao 63cm,nặng 6,3kg, chưa mọc răng nào. Xin cám ơn.', 'Chào chị Thiên Kim, Lời đầu tiên xin cảm ơn sự quan tâm của chị đối với sản phẩm của công ty. Hiện tại,công ty đang có dòng sản phẩm dinh dưỡng XO3 dành cho bé từ 6-12 tháng tuổi tương ứng với độ tuổi của bé nhà mình. Sản phẩm dinh dưỡng XO3 được bổ sung 4 gốc đường cao phân tử giúp bé ổn định hệ tiêu hóa, nguồn DHA&ARA - được chiết xuất từ cá biển lưng xanh là 2 thành phần quan trọng nhất giúp trí não bé phát triển. Bên cạnh đó, thành phần Taurine& Choline sẽ giúp bé hoàn thiện hệ thần kinh võng mạc,hình thành trí nhớ và phát triển nhận thức cho trẻ. Chị có thể tham khảo thêm và cho bé dùng thử sản phẩm này giúp bé phát triển toàn diện. Trong thông tin chị gửi về, Phòng CSKH chưa nhận được giới tính của bé nên tư vấn chung đến chị các thông tin chiều cao, cân nặng theo chuẩn 2SD của bé 9 tháng tuổi để chị tham khảo: o Bé gái: Chiều cao: 70,1 cm- Cân nặng: 8.2kg o Bé trai: Chiều cao: 72cm - Cân nặng: 8.9kg Với chiều cao cân nặng của bé mình hiện tại đang hơi thấp hơn so với chuẩn, chị cố gắng bổ sung thêm dinh dưỡng cho bé. Cần tư vấn trực tiếp chị vui lòng liên hệ: Phòng CSKH: Miền Bắc: 04.35565199 – Miền Trung : 0511.3715555 – Miền Nam : 0854.115500 sẽ có nhân viên trực 24/24 hỗ trợ khách hàng. Cảm ơn Quý khách hàng đã gửi câu hỏi về cho Ban Biên tập web Namyangi. ', '1', '22');
INSERT INTO `gaucon_answer_question` VALUES ('64', 'Con tôi năm vừa tròn 2 tuổi. Bé bị bón nặng, bé thay đổi rất nhiều lại sữa từ sữa VN đến sữa ngoại. Tôi thấy Namyang có rất nhiều sữa cho bé: Imperial Dream XO, Imperial KIDS, I am Mother...giá cũng chênh lệch. Tôi có mua 1 lon 800gr số 4 cho bé vì khi có bầu tôi chỉ uống XO vì vị sữa dễ uống và không ngấy. Tôi nên chọn sữa nào cho bé? Thành phần của Imperial Dream XO, Imperial KIDS, I am Mother có gì khác nhau và nổi trội gì không?', 'Chào chị Nguyệt Ánh, Trước tiên, cảm ơn chị đã quan tâm và chọn cho bé dùng sản phẩm dinh dưỡng XO của Tập đoàn Namyang. Bộ phận tư vấn khách hàng của công ty Nam Dương xin trả lời chị như sau: Hiện tại, đúng như chị có tìm hiểu, Namyang có 2 dòng sản phẩm dinh dưỡng cho bé là XO và I am Mother (IAM). Mỗi loại sản phẩm có một công thức khác nhau nhưng sẽ hỗ trợ toàn diện đến 4 tiêu chí phát triển của bé: miễn dịch, tăng trưởng, tiêu hóa-hấp thu và trí não cho bé. Mong rằng khi bé bón, XO sẽ giúp bé cải thiện tiêu hóa, dần ổn định và hấp thu tốt, từ đó bé sẽ tăng cân, phát triển tốt về trí não và chiều cao. Muốn biết thêm chi tiết, chị vui lòng liên hệ số điện thoại hotline để được tư vấn dinh dưỡng và sản phẩm kỹ hơn: HCM: (08)54115500 Trân trọng. ', '1', '23');
INSERT INTO `gaucon_answer_question` VALUES ('65', 'Thưa bác sĩ, em muốn nhờ bác sĩ tư vấn giúp em. Em lập gia đình đến nay được 2 năm, kinh nguyệt thường xuyên không đều. Sau gần 1 năm không dùng biện pháp tránh thai nào mà chưa có thai và cũng là 4-5 tháng không thấy kinh nên em đi khám và được chẩn đoán là \"Buồng trứng phải đa nang\". Bác sĩ cho uống 25 ngày thuốc và có kinh trở lại, siêu âm lại thì hai buồng trứng bình thường. Sau 2-3 lần thấy kinh lại cũng khá đều thì ngày 09/06/08 là ngày kinh đầu tiên của kỳ kinh cuối. ngày 10/07 em thử que thì thấy 02 vạch. Ngày 16/08 em bị ra máu, đi siêu âm thì kết quả là thai lưu 5-6 tuần. ngày 10/11/08 em đi khám tổng quát để tìm nguyên nhân và cũng là để chuẩn bị cho lần mang thai tới thì kết quả xét nghiệm là Dương tính với Rubella IgG (128.70) và CMV IgG (2.10). Bác sĩ nói nguyên nhân thai lưu của em là do 02 con virut này. Bác sĩ nói tránh thai trong 6 tháng, và 6 tháng sau quay lại xét nghiệm 02 loại virut này mà không cho uống thuốc gì cả, cũng trong lần siêu âm này thì có kết quả là \"buồng trứng trái đa nang\". Em rất băn khoăn và lo lắng thưa bác sĩ. vì trong lần mang thai đó em không hề bị cảm hay sốt gì cả. mà theo kết quả siêu âm thì bây giờ em đã nhiễm nghĩa là đã có kháng thể, vậy tại sao bác sĩ lại khuyên em tới 6 tháng sau mới đi xét nghiệm lại? Trong khi từ ngày thai lưu tới nay đã 3 tháng? Bây giờ em không thể có thai ngay được ạ? Hay nguyên nhân thai không phát triển là do trứng của em không tốt? Mà em nghe là CMV và Rubella lây qua quan hệ tình dục và nước bọt. vậy ông xã em có bị lây không? và nếu lần mang thai tới thì em không được \"gần gũi\" hay hôn anh ấy phải không ạ? hay cũng phải đưa anh ấy đi xét nghiệm virut như vậy? Bác sĩ có lời khuyên nào về phía ông xã em không ạ? Em xin chân thành cám ơn!\r\n\r\nĐỗ Thị Thùy  (115 Mạc Đĩnh Chi, Phường 4, TP Vũng Tàu)\r\nEmail: dtthuy@hlvc.edu.vn', 'Theo như chị trình bày thì chị đang gặp những vấn đề sau:\r\n\r\nThứ nhất: Vấn đề về Rubella: Do chị đã bị nhiễm Rubella dó đó, cơ thể chị đã có kháng thể ngừa bệnh này suốt đời, vì vậy chị không phải lo lắng về bệnh này nữa. \r\nThời gian chị bị đã lâu nên chị có thể có thai lại bình thường.  Tuy nhiên, bác sĩ tham vấn cho chị rất cẩn thận nên đã khuyên chị 6 tháng mới có thai lại cũng không hề sai.\r\n \r\nThứ hai: Virus họ CMV là 1 họ virus có nhiều loại gây viêm nhiễm qua đường ăn uống, biểu hiện của bệnh là cảm cúm và phát ban. Loại virus này, hiện nay chưa có thuốc chủng ngừa. Cách phòng ngừa là rửa tay sạch trước khi ăn. CMV khi bị nhiễm trong 20 tuần đầu tiên của thai kỳ thường gây sẩy thai và để lại nhiều dị tật rất nặng nề cho em bé. Do đó, nếu bị nhiễm trong 3 tháng đầu bác sĩ thường khuyên bỏ thai.\r\nDo CMV thường lây qua đường tiêu hóa. Rubella lây qua đường hô hấp, do đó dĩ nhiên là ông xã chị cũng bị nhiễm, tuy nhiên cũng không có gì phải lo lắng nhiều.\r\n\r\nThứ 3: Buồng trứng đa nang là buồng trứng có rất nhiều nang nhỏ và trong 1 chu kỳ không có 1 nang trứng nào vượt trội để rụng trứng và đưa tới vô sinh.\r\nChị đã từng có thai thì tình trạng buồng trứng đa nang sẽ giảm nhưng không có nghĩa là không bị lại. Khi đã bị buồng trứnng đa nang chị cần đi tham vấn bác sĩ và muốn có thai trở lại phải điều trị tình trạng buồng trứng đa ngang này. \r\nVấn đề cần quan tâm của chị hiện nay là: \r\n-    Buồng trứng đa nang\r\n-    Có thể tái nhiễm CMV vì hiện nay không có thuốc chủng ngừa, chỉ phòng rửa tay sạch sẽ trước khi ăn.\r\n-    Rubella thì chị không phải sợ nữa vì cơ thể chị đã có kháng thể rồi .\r\n-    Điều cuối cùng chị đang mong muốn có con hãy đi khám và tham vấn bác sĩ để có lời khuyên chính xác nhất và có hướng điều trị tốt nhất cho chị.', '1', '24');

-- ----------------------------
-- Table structure for `gaucon_baiviet_tinh`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_baiviet_tinh`;
CREATE TABLE `gaucon_baiviet_tinh` (
  `id` int(11) NOT NULL auto_increment,
  `v_detail` text NOT NULL,
  `e_detail` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_baiviet_tinh
-- ----------------------------
INSERT INTO `gaucon_baiviet_tinh` VALUES ('18', '<ol>\r\n	<li>\r\n		<p style=\\\"text-align: justify;\\\">\r\n			<strong>X&aacute;c nhận đặt h&agrave;ng​</strong></p>\r\n		<ul>\r\n			<li>\r\n				<p style=\\\"text-align: justify;\\\">\r\n					Sau khi nhận được th&ocirc;ng tin đặt h&agrave;ng của qu&yacute; kh&aacute;ch, nh&acirc;n vi&ecirc;n ch&uacute;ng t&ocirc;i sẽ li&ecirc;n lạc để th&ocirc;ng b&aacute;o v&agrave; x&aacute;c nhận th&ocirc;ng tin về t&igrave;nh trạng sản phẩm m&agrave; Qu&yacute; kh&aacute;ch đ&atilde; đặt h&agrave;ng trước khi Qu&yacute; kh&aacute;ch thanh to&aacute;n.</p>\r\n			</li>\r\n		</ul>\r\n	</li>\r\n	<li>\r\n		<p style=\\\"text-align: justify;\\\">\r\n			<strong>Thanh to&aacute;n</strong></p>\r\n		<ul>\r\n			<li>\r\n				<p style=\\\"text-align: justify;\\\">\r\n					Qu&yacute; kh&aacute;ch tiến h&agrave;nh thanh to&aacute;n theo qua t&agrave;i khoản của ch&uacute;ng t&ocirc;i:</p>\r\n				<ul>\r\n					<li>\r\n						<p style=\\\"text-align: justify;\\\">\r\n							​Chủ t&agrave;i khoản: xxx</p>\r\n					</li>\r\n					<li>\r\n						<p style=\\\"text-align: justify;\\\">\r\n							Số T&agrave;i khoản: xxx</p>\r\n					</li>\r\n					<li>\r\n						<p style=\\\"text-align: justify;\\\">\r\n							Tại Agribank Chi Nh&aacute;nh 3 - số&nbsp;112 Trần Quốc Thảo, P7, Q3, TP Hồ Ch&iacute; Minh</p>\r\n					</li>\r\n					<li>\r\n						<p style=\\\"text-align: justify;\\\">\r\n							Nội Dung:&nbsp;(T&ecirc;n Qu&yacute; Kh&aacute;ch + số điện thoại + t&ecirc;n m&atilde; sản phẩm)</p>\r\n					</li>\r\n				</ul>\r\n			</li>\r\n			<li>\r\n				<p style=\\\"text-align: justify;\\\">\r\n					​Sau khi chuyển tiền, qu&yacute; kh&aacute;ch vui l&ograve;ng th&ocirc;ng b&aacute;o việc chuyển tiền qua email (info@rausachsg.com) hoặc điện thoại (xxx), để ch&uacute;ng t&ocirc;i tiện trong việc kiểm tra. Ch&uacute;ng t&ocirc;i sẽ gửi h&agrave;ng đến địa chỉ y&ecirc;u cầu của qu&yacute; kh&aacute;ch ngay sau khi nhận được tiền. Thời gian ch&uacute;ng t&ocirc;i nhận được tiền thanh to&aacute;n l&agrave; 24h nếu c&ugrave;ng ng&acirc;n h&agrave;ng v&agrave; từ 1-2 ng&agrave;y nếu kh&aacute;c hệ thống ng&acirc;n h&agrave;ng (trừ ng&agrave;y lể v&agrave; thứ 7 - Chủ Nhật). ​</p>\r\n			</li>\r\n		</ul>\r\n	</li>\r\n	<li>\r\n		<p style=\\\"text-align: justify;\\\">\r\n			<strong>Vận chuyển</strong></p>\r\n		<ul>\r\n			<li>\r\n				<p style=\\\"text-align: justify;\\\">\r\n					​Ngay sau khi nhận được tiền thanh to&aacute;n của qu&yacute; kh&aacute;ch ch&uacute;ng t&ocirc;i sẽ tiến h&agrave;nh giao h&agrave;ng cho Qu&yacute; kh&aacute;ch. Thời gian giao h&agrave;ng như sau:</p>\r\n				<ul>\r\n					<li>\r\n						<p style=\\\"text-align: justify;\\\">\r\n							Từ 24h đến 48h: cho khu vực nội th&agrave;nh TP. HCM.</p>\r\n					</li>\r\n					<li>\r\n						<p style=\\\"text-align: justify;\\\">\r\n							Từ 48h đến 96h: cho khu vực ngoại th&agrave;nh v&agrave; c&aacute;c tỉnh th&agrave;nh kh&aacute;c (trừ thứ 7, chủ nhật v&agrave; c&aacute;c ng&agrave;y lễ, tết).</p>\r\n					</li>\r\n				</ul>\r\n			</li>\r\n		</ul>\r\n	</li>\r\n</ol>\r\n<p style=\\\"text-align: justify;\\\">\r\n	<strong>​Lưu &yacute;:</strong></p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	Qu&yacute; kh&aacute;ch vui l&ograve;ng giữ lại bi&ecirc;n lai, bi&ecirc;n nhận chuyển tiền cho đến khi h&agrave;ng h&oacute;a chuyển đến tay của qu&yacute; kh&aacute;ch để đề ph&ograve;ng một số trục trặc từ ph&iacute;a ng&acirc;n h&agrave;ng hay bưu điện, v&agrave; li&ecirc;n lạc ngay với ch&uacute;ng t&ocirc;i khi qu&aacute; thời gian giao h&agrave;ng.</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	Tr&ecirc;n đ&acirc;y l&agrave; một số th&ocirc;ng tin cơ bản, muốn biết th&ecirc;m chi tiết qu&yacute; kh&aacute;ch vui l&ograve;ng li&ecirc;n lạc trực tiếp theo số điện thoại Hotline tr&ecirc;n.</p>\r\n', '', '', 'huong-dan-mua-hang', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('19', '<p>\r\n	Đang cập nhật</p>\r\n', '', '', 'dat-hang', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('20', '<p style=\\\"text-align: justify;\\\">\r\n	<strong>Trong bữa ăn h&agrave;ng ng&agrave;y của người Việt, rau l&agrave; th&agrave;nh phần kh&ocirc;ng thể thiếu, nhu cầu sử dụng rau ăn h&agrave;ng ng&agrave;y của mỗi hộ gia đ&igrave;nh l&agrave; thường xuy&ecirc;n. Nhưng&nbsp;mối lo thường trực về an to&agrave;n thực phẩm &nbsp;v&agrave;&nbsp;cuộc sống bận rộn nơi thị th&agrave;nh khiến ch&uacute;ng ta kh&oacute; c&oacute; thể h&agrave;ng ng&agrave;y đi chợ, si&ecirc;u thị hay cửa h&agrave;ng rau sạch &nbsp;để mua được&nbsp; những b&oacute; rau tươi ngon m&agrave; bảo đảm an to&agrave;n thực phẩm.</strong></p>\r\n<div style=\\\"text-align: justify;\\\">\r\n	Rau Sạch H&agrave;ng Ng&agrave;y&nbsp;ra đời với mục đ&iacute;ch cung cấp Rau Tươi Sạch&nbsp;&nbsp;đạt ti&ecirc;u chuẩn VIETGAP, VIETGAP NH&Atilde;N XANH&nbsp;Tận Nh&agrave;&nbsp;qu&yacute; kh&aacute;ch&nbsp;Mỗi Ng&agrave;y&nbsp;, gi&uacute;p qu&yacute; kh&aacute;ch lu&ocirc;n c&oacute; rau tươi sạch sử dụng m&agrave; kh&ocirc;ng bị &aacute;m ảnh bởi nỗi lo an to&agrave;n thực phẩm, &nbsp;kh&ocirc;ng phải bận t&acirc;m việc đi chợ, si&ecirc;u thị hay cửa h&agrave;ng rau sạch mỗi ng&agrave;y.</div>\r\n<div style=\\\"text-align: justify;\\\">\r\n	<br />\r\n	Rau Sạch H&agrave;ng Ng&agrave;y&nbsp;l&agrave; sự lựa chọn tối ưu với c&aacute;c ưu điểm:<br />\r\n	<br />\r\n	1. Nguồn rau sạch đạt chuẩn&nbsp;VIETGAP, VIETGAP NH&Atilde;N XANH&nbsp;được cung cấp từ c&aacute;c đơn vị: HTX Anh Đ&agrave;o Đ&agrave; Lạt, HTX Thỏ Việt, HTX Ph&uacute; Lộc, HTX Phước An &nbsp;l&agrave; c&aacute;c đơn vị cung cấp rau, củ, quả cho si&ecirc;u thịMetro,&nbsp;Coopmart,&nbsp;Big C,&nbsp;Lotte...<br />\r\n	&nbsp;<br />\r\n	2. H&agrave;ng h&oacute;a đa dạng, lu&ocirc;n tươi ngon, r&otilde; r&agrave;ng nguồn ngốc với&nbsp;&ldquo;Gi&aacute; Cả Tương Đương Si&ecirc;u Thị&quot;:Coopmart, Big C, Lotte...<br />\r\n	&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<br />\r\n	3. Giao h&agrave;ng&nbsp;&ldquo;Tận Nh&agrave;&rdquo; +&nbsp;&ldquo;Miễn Ph&iacute;&rdquo;&nbsp;c&aacute;c quận:&nbsp;Q.1, Q.3, Q.10, Q.T&acirc;n B&igrave;nh, Q Ph&uacute; Nhuận, G&ograve; Vấp, T&acirc;n Ph&uacute;&nbsp;&nbsp;với&nbsp;ĐƠN H&Agrave;NG C&Oacute; GI&Aacute; TRỊ H&Oacute;A ĐƠN TỪ 70.000 VNĐ.<br />\r\n	<br />\r\n	4. Phương thức đặt h&agrave;ng đa dạng v&agrave; đơn giản: trực tuyến qua trang website&nbsp;Rausachhangngay.com,&nbsp;điện thoại trực tiếp,&nbsp;Facebook, Yahoo, Skype, Email, Viber.<br />\r\n	<br />\r\n	5. Ch&iacute;nh s&aacute;ch giao nhận h&agrave;ng linh hoạt, nhanh ch&oacute;ng<br />\r\n	<br />\r\n	<strong>C&aacute;c dịch vụ của Rau Sạch S&agrave;i G&ograve;n:</strong></div>\r\n<ul>\r\n	<li style=\\\"text-align: justify;\\\">\r\n		Dịch vụ cung cấp rau, củ, quả đạt ti&ecirc;u chuẩn VIETGAP v&agrave; VIETGAP NH&Atilde;N XANH &nbsp;tận nh&agrave; h&agrave;ng ng&agrave;y&nbsp;Q&uacute;y kh&aacute;ch h&agrave;ng đặt h&agrave;ng,&nbsp;Rau Sạch H&agrave;ng Ng&agrave;y&nbsp;giao h&agrave;ng theo đơn h&agrave;ng v&agrave; thanh to&aacute;n trực tiếp tại thời điểm giao h&agrave;ng.</li>\r\n	<li style=\\\"text-align: justify;\\\">\r\n		Dịch vụ cung cấp rau, củ , quả &nbsp;tận nh&agrave;, giao h&agrave;ng ng&agrave;y theo đơn h&agrave;ng v&agrave; thanh to&aacute;n theo tuần</li>\r\n</ul>\r\n<div style=\\\"text-align: justify;\\\">\r\n	Rau Sạch S&agrave;i G&ograve;n&nbsp;sẽ lu&ocirc;n mang đến cho qu&yacute; kh&aacute;ch h&agrave;ng sự an to&agrave;n, an t&acirc;m v&agrave;&nbsp;tiện lợi.<br />\r\n	<br />\r\n	rausachsg.com&nbsp;rất mong nhận được sự ủng hộ của qu&yacute; kh&aacute;ch h&agrave;ng.</div>\r\n', '', '', 'aboutus', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('21', '<p>\r\n	Đang cập nhật</p>\r\n', '', '', 'khuyen-mai', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('22', '<p>\r\n	Đang cập nhật</p>\r\n', '', '', 'giao-hang', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('23', '<p>\r\n	Đang cập nhật</p>\r\n', '', '', 'thanh-toan', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('24', '<p>\r\n	Đang cập nhật</p>\r\n', '', '', 'hotline', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('25', '<p>\r\n	Đang cập nhật</p>\r\n', '', '', 'doi-va-tra-hang', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('26', '<p>\r\n	<strong>C&Ocirc;NG TY TNHH MINH T&Acirc;M Garden</strong></p>\r\n<p>\r\n	<strong>Địa chỉ:</strong> 203 Lương Nhữ Học P11 Q5 Tp.HCM&nbsp;</p>\r\n<p>\r\n	<strong>Hotline:</strong> 09456-85000</p>\r\n<p>\r\n	<strong>Email:&nbsp;</strong>minhtamgarden@gmail.com</p>\r\n<p>\r\n	<strong>Website:</strong>&nbsp;<a href=\\\"http://localhost/rausachsg/\\\" target=\\\"_blank\\\">www.rausachsg.com</a></p>\r\n', '', '', 'lienhe', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('27', '<h2 style=\\\"border: 0px; margin: 0px; padding: 0px; font-size: 16px; color: rgb(0, 0, 0); font-family: \\\'Times New Roman\\\'; text-align: center;\\\">\r\n	<a href=\\\"http://rausachsg.com/khuyen-mai/chi-tiet/giam-gia-toi-10-nhan-ngay-khai-truong.html\\\" style=\\\"text-decoration: none; color: rgb(51, 51, 51); border: 0px; margin: 0px; padding: 0px;\\\" title=\\\"Giảm giá tới 10% nhân ngày Khai Trương\\\">Giảm gi&aacute; tới 10% nh&acirc;n ng&agrave;y Khai Trương</a></h2>\r\n<p style=\\\"text-align: center;\\\">\r\n	Bắt đầu nhận đơn h&agrave;ng từ ng&agrave;y 01/03/ 2015 v&agrave; mừng Khai trương ng&agrave;y25/02/2015. Minh T&acirc;m Garden giảm gi&aacute; 8-12%<br />\r\n	<a href=\\\"http://rausachsg.com/khuyen-mai/chi-tiet/giam-gia-toi-10-nhan-ngay-khai-truong.html\\\" target=\\\"_blank\\\"><strong>&gt;&gt; Xem chi tiết</strong></a></p>\r\n', '', '', 'khuyenmai', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('28', '<div style=\\\"text-align: justify;\\\">\r\n	Khi bạn truy cập v&agrave;o trang web của ch&uacute;ng t&ocirc;i c&oacute; nghĩa l&agrave; bạn đồng &yacute; với c&aacute;c điều khoản n&agrave;y. Trang web c&oacute; quyền thay đổi, chỉnh sửa, th&ecirc;m hoặc lược bỏ bất kỳ phần n&agrave;o trong điều khoản sử dụng, v&agrave;o bất cứ l&uacute;c n&agrave;o. C&aacute;c thay đổi c&oacute; hiệu lực ngay khi được đăng tr&ecirc;n trang web m&agrave; kh&ocirc;ng cần th&ocirc;ng b&aacute;o trước. Khi c&aacute;c thay đổi về điều khoản sử dụng được đăng tải, c&oacute; nghĩa l&agrave; bạn chấp nhận với những thay đổi đ&oacute;. Qu&yacute; kh&aacute;ch vui l&ograve;ng kiểm tra thường xuy&ecirc;n để cập nhật những thay đổi của ch&uacute;ng t&ocirc;i.</div>\r\n<div style=\\\"text-align: justify;\\\">\r\n	&nbsp;</div>\r\n<div style=\\\"text-align: justify;\\\">\r\n	<strong>&Yacute; kiến kh&aacute;ch h&agrave;ng</strong><br />\r\n	Tất cả nội dung trang web v&agrave; &yacute; kiến đ&oacute;ng g&oacute;p của kh&aacute;ch h&agrave;ng đều l&agrave; t&agrave;i sản của ch&uacute;ng t&ocirc;i. Ch&uacute;ng t&ocirc;i sẽ c&oacute; những phản hồi ngay khi nhận được đ&oacute;ng g&oacute;p của kh&aacute;ch h&agrave;ng nhằm đem tới những sản phẩm tốt nhất cho qu&yacute; kh&aacute;ch.<br />\r\n	&nbsp;</div>\r\n<div>\r\n	<div style=\\\"text-align: justify;\\\">\r\n		<strong>Thương hiệu v&agrave; bản quyền</strong><br />\r\n		C&aacute;c điều kiện, điều khoản v&agrave; nội dung của trang web n&agrave;y được điều chỉnh bởi luật ph&aacute;p Việt Nam v&agrave; T&ograve;a &aacute;n c&oacute; thẩm quyền tại Việt Nam sẽ giải quyết bất kỳ tranh chấp n&agrave;o ph&aacute;t sinh từ việc sử dụng tr&aacute;i ph&eacute;p trang web n&agrave;y.</div>\r\n	<div style=\\\"text-align: justify;\\\">\r\n		&nbsp;</div>\r\n	<div>\r\n		<div style=\\\"text-align: justify;\\\">\r\n			<strong>Giải quyết tranh chấp</strong><br />\r\n			Bất kỳ m&acirc;u thuẫn, khiếu nại hoặc tranh chấp ph&aacute;t sinh từ hoặc li&ecirc;n quan đến c&aacute;c điều khoản sử dụng n&agrave;y đều sẽ được giải quyết theo quy định của ph&aacute;p luật hiện h&agrave;nh của nước Cộng H&ograve;a X&atilde; Hội Chủ Nghĩa Việt Nam.</div>\r\n		<div style=\\\"text-align: justify;\\\">\r\n			&nbsp;</div>\r\n		<div style=\\\"text-align: justify;\\\">\r\n			<strong>Luật ph&aacute;p v&agrave; thẩm quyền tại Việt Nam</strong></div>\r\n		<div style=\\\"text-align: justify;\\\">\r\n			Tất cả c&aacute;c điều khoản sử dụng n&agrave;y sẽ bị chi phối v&agrave; được hiểu theo luật ph&aacute;p của Việt Nam. Nếu c&oacute; tranh chấp ph&aacute;t sinh bởi c&aacute;c điều khoản sử dụng n&agrave;y, qu&yacute; kh&aacute;ch gửi khiếu nại l&ecirc;n t&ograve;a &aacute;n Việt Nam để giải quyết.<br />\r\n			&nbsp;</div>\r\n		<div style=\\\"text-align: justify;\\\">\r\n			<strong>Điều h&agrave;nh</strong></div>\r\n		<div style=\\\"text-align: justify;\\\">\r\n			Website http://www.namyangi.com.vn được quản l&yacute; bởi&nbsp;C&ocirc;ng ty Cổ phần Đầu tư Nam Dương</div>\r\n		<div style=\\\"text-align: justify;\\\">\r\n			Trụ sở ch&iacute;nh: &nbsp;R4-93 Hưng Gia 2, P. T&acirc;n Phong, Quận 7, Tp. Hồ Ch&iacute; Minh</div>\r\n	</div>\r\n</div>\r\n<p>\r\n	&nbsp;</p>\r\n', '', '', 'chinh-sach-va-quy-dinh', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('29', '<p style=\\\"text-align: justify;\\\">\r\n	Trang web của ch&uacute;ng t&ocirc;i coi trọng việc bảo mật th&ocirc;ng tin v&agrave; sử dụng c&aacute;c biện ph&aacute;p tốt nhất bảo vệ th&ocirc;ng tin của bạn. Bạn kh&ocirc;ng được sử dụng bất kỳ chương tr&igrave;nh, c&ocirc;ng cụ hay h&igrave;nh thức n&agrave;o kh&aacute;c để can thiệp v&agrave;o hệ thống hay l&agrave;m thay đổi cấu tr&uacute;c dữ liệu. Trang web cũng nghi&ecirc;m cấm việc ph&aacute;t t&aacute;n, truyền b&aacute; hay cổ vũ cho bất kỳ hoạt động n&agrave;o nhằm can thiệp, ph&aacute; hoại hay x&acirc;m nhập v&agrave;o dữ liệu của hệ thống. C&aacute; nh&acirc;n hay tổ chức vi phạm sẽ bị tước bỏ mọi quyền lợi cũng như sẽ bị truy tố trước ph&aacute;p luật nếu cần thiết.</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	Mọi th&ocirc;ng tin của kh&aacute;ch h&agrave;ng sẽ được bảo mật nhưng trong trường hợp cơ quan ph&aacute;p luật y&ecirc;u cầu, ch&uacute;ng t&ocirc;i sẽ buộc phải cung cấp những th&ocirc;ng tin n&agrave;y cho c&aacute;c cơ quan ph&aacute;p luật.&nbsp;</p>\r\n', '', '', 'bao-mat-thong-tin', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('30', '<div class=\\\"block-sitemap\\\">\r\n			<ul>\r\n				<li>\r\n					<a href=\\\"http://demo.namyangi.com.vn/\\\">I. Tập đoàn Namyang</a>\r\n					<ul>\r\n						<li>\r\n							<span class=\\\"line-y\\\"></span>\r\n							<a href=\\\"http://demo.namyangi.com.vn/gioi-thieu/ve-tap-doan.html\\\">1. Về tập đoàn</a>\r\n							<span class=\\\"line-x\\\"></span>\r\n						</li>\r\n						<li>\r\n							<span class=\\\"line-y\\\"></span>\r\n							<a href=\\\"http://demo.namyangi.com.vn/gioi-thieu/lich-su.html\\\">2. Lịch sử</a>\r\n							<span class=\\\"line-x\\\"></span>\r\n						</li>\r\n						<li>\r\n							<span class=\\\"line-y\\\"></span>\r\n							<a href=\\\"http://demo.namyangi.com.vn/gioi-thieu/nha-may.html\\\">3. Nhà máy</a>\r\n							<span class=\\\"line-x\\\"></span>\r\n						</li>\r\n						<li>\r\n							<span class=\\\"line-y\\\"></span>\r\n							<a href=\\\"http://demo.namyangi.com.vn/gioi-thieu/trung-tam-r-d.html\\\">4. Trung tâm R&D</a>\r\n							<span class=\\\"line-x\\\"></span>\r\n						</li>\r\n						<li>\r\n							<span class=\\\"line-y\\\"></span>\r\n							<a href=\\\"http://demo.namyangi.com.vn/gioi-thieu/namyang-tai-viet-nam.html\\\">4. Namyang Việt Nam</a>\r\n							<span class=\\\"line-x\\\"></span>\r\n						</li>\r\n					</ul>\r\n					</li>\r\n					\r\n				<li>\r\n					<span class=\\\"line-y\\\"></span>\r\n					<a href=\\\"http://demo.namyangi.com.vn/san-pham.html\\\">II. Sản phẩm</a>\r\n					<ul>\r\n						<li>\r\n							<span class=\\\"line-y\\\"></span>\r\n							<a href=\\\"http://demo.namyangi.com.vn/loai-san-pham/xo-gt.html\\\">1. XO GT</a>\r\n							<span class=\\\"line-x\\\"></span>\r\n						</li>\r\n						<li>\r\n							<span class=\\\"line-y\\\"></span>\r\n							<a href=\\\"http://demo.namyangi.com.vn/loai-san-pham/xo-care.html\\\">2. XO Care</a>\r\n							<span class=\\\"line-x\\\"></span>\r\n						</li>\r\n						<li>\r\n							<span class=\\\"line-y\\\"></span>\r\n							<a href=\\\"http://demo.namyangi.com.vn/loai-san-pham/i-am-mother.html\\\">3. I am Mother</a>\r\n							<span class=\\\"line-x\\\"></span>\r\n						</li>\r\n						<li>\r\n							<span class=\\\"line-y\\\"></span>\r\n							<a href=\\\"http://demo.namyangi.com.vn/loai-san-pham/star-gold.html\\\">4. Start Gold</a>\r\n							<span class=\\\"line-x\\\"></span>\r\n						</li>\r\n					</ul>\r\n				</li>\r\n				<li>\r\n					<span class=\\\"line-y\\\"></span>\r\n					<a href=\\\"http://demo.namyangi.com.vn/me-va-be.html\\\">III. Mẹ và bé</a>\r\n					<ul>\r\n						<li>\r\n							<span class=\\\"line-y\\\"></span>\r\n							<a href=\\\"http://demo.namyangi.com.vn/nuoi-con.html\\\">1. Nuôi con</a>\r\n							<span class=\\\"line-x\\\"></span>\r\n						</li>\r\n						<li>\r\n							<span class=\\\"line-y\\\"></span>\r\n							<a href=\\\"http://demo.namyangi.com.vn/mang-thai.html\\\">2. Mang thai</a>\r\n							<span class=\\\"line-x\\\"></span>\r\n						</li>\r\n						<li>\r\n							<span class=\\\"line-y\\\"></span>\r\n							<a href=\\\"http://demo.namyangi.com.vn/day-con.html\\\">2. Dạy con</a>\r\n							<span class=\\\"line-x\\\"></span>\r\n						</li>\r\n					</ul>	\r\n				</li>\r\n				<li>\r\n					<span class=\\\"line-y\\\"></span>\r\n					<a href=\\\"http://demo.namyangi.com.vn/bac-si-namyang.html\\\">IV. Bác sĩ Namyang</a>\r\n					<ul>\r\n						<li>\r\n							<span class=\\\"line-y\\\"></span>\r\n							<a href=\\\"http://demo.namyangi.com.vn/tu-van-san-pham.html\\\">1. Tư vấn sản phẩm</a>\r\n							<span class=\\\"line-x\\\"></span>\r\n						</li>\r\n						<li>\r\n							<span class=\\\"line-y\\\"></span>\r\n							<a href=\\\"http://demo.namyangi.com.vn/tu-van-suc-khoe.html\\\">2. Tư vấn sức khỏe</a>\r\n							<span class=\\\"line-x\\\"></span>\r\n						</li>\r\n					</ul>	\r\n				</li>\r\n				<li>\r\n					<span class=\\\"line-y\\\"></span>\r\n					<a href=\\\"http://demo.namyangi.com.vn/khuyen-mai-va-su-kien.html\\\">V. Khuyến mại và sự kiện</a>\r\n				</li>\r\n				<li>\r\n					<span class=\\\"line-y\\\"></span>\r\n					<a href=\\\"http://demo.namyangi.com.vn/thu-vien.html\\\">VI. Thư viện</a>\r\n				</li>\r\n				<li>\r\n					<span class=\\\"line-y\\\"></span>\r\n					<a href=\\\"http://demo.namyangi.com.vn/lien-he.html\\\">VII. Liên hệ</a>\r\n				</li>\r\n			</ul>\r\n		</div>', '', '', 'so-do-trang', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('31', '<p>\r\n	Đang cập nhật</p>\r\n', '', '', 'thu-ngo-ceo', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('32', '<p>\r\n	Đang cập nhật</p>\r\n', '', '', 'lich-su', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('33', '<p style=\\\"text-align: justify;\\\">\r\n	&nbsp;</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	<strong>1. Nh&agrave; m&aacute;y Cheonan mới</strong></p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	&nbsp;</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	<span id=\\\"ctl00_cntContent_ctlNewsContent\\\">Nh&agrave; m&aacute;y M&ocirc; h&igrave;nh mới Cheonan đ&atilde; kết hợp b&iacute; quyết sản xuất mặt h&agrave;ng bơ sữa đ&atilde; được t&iacute;ch lũy hơn 40 năm qua&nbsp; với c&ocirc;ng nghệ sản xuất hiện đại &ndash; đ&oacute; l&agrave; những nỗ lực, phấn đấu kh&ocirc;ng ngừng để trở th&agrave;nh nh&agrave; m&aacute;y chế biến sản phẩm dinh dưỡng đạt ti&ecirc;u chuẩn thế kỉ 21. </span></p>\r\n<p style=\\\"text-align: center;\\\">\r\n	<span><img alt=\\\"\\\" src=\\\"/dataweb/images/KHUYENMAI-SUKIEN/TINTUC/NAMYANG/cheonan-factory-new.gif\\\" style=\\\"width: 620px; height: 192px;\\\" /></span></p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	<span><span id=\\\"ctl00_cntContent_ctlNewsContent\\\">Sự ra đời của nh&agrave; m&aacute;y Cheonam mới l&agrave; kết quả chuẩn bị một thời gian d&agrave;i trước đ&oacute;. Đ&acirc;y l&agrave; nh&agrave; m&aacute;y c&oacute; c&ocirc;ng nghệ sản xuất ho&agrave;n to&agrave;n kh&aacute;c biệt so với những nh&agrave; m&aacute;y trước đ&acirc;y ở chỗ: Nh&agrave; m&aacute;y c&oacute; hệ thống sản xuất tự động từ kh&acirc;u nhập nguy&ecirc;n liệu sản phẩm dinh dưỡng th&ocirc; đến kh&acirc;u ph&acirc;n phối, c&oacute; trục đường giao th&ocirc;ng thuận lợi, v&agrave; được thiết kế theo phong c&aacute;ch hướng tới tiềm năng trong tương lai.</span><br />\r\n	<br />\r\n	<span id=\\\"ctl00_cntContent_ctlNewsContent\\\"><span id=\\\"ctl00_cntContent_ctlNewsContent\\\">Hệ thống điều khiển tự động đầu ti&ecirc;n trong nước</span><br />\r\n	<br />\r\n	A.C.F (Automated Collaborative Filtering - Hệ thống lọc tự động), R.G.V (Rail Guided Vehicle &ndash; Phương tiện chuy&ecirc;n chở đi qua đường ray),&nbsp;v&agrave; BC (Boom Car &ndash; Xe hơi chạy với tốc độ cao).Đ&acirc;y l&agrave; hệ thống&nbsp;điều khiển tự&nbsp;động&nbsp;c&oacute; mặt đầu ti&ecirc;n tại H&agrave;n Quốc. Mỗi loại thiết bị hỗ trợ sản xuất v&agrave; c&aacute;c trang thiết bị đều được điều khiển trong Ph&ograve;ng điều khiển trung t&acirc;m ( C.C.R ) từ kh&acirc;u nhập nguy&ecirc;n liệu th&ocirc; để sản xuất đến kh&acirc;u ph&acirc;n phối.</span><br />\r\n	<br />\r\n	<span id=\\\"ctl00_cntContent_ctlNewsContent\\\"><span id=\\\"ctl00_cntContent_ctlNewsContent\\\">Hệ thống kiểm tra vệ sinh an to&agrave;n thực phẩm ti&ecirc;n tiến</span><br />\r\n	<br />\r\n	Hệ thống được trang bị với những thiết bị v&agrave; c&ocirc;ng cụ t&acirc;n tiến như Bactoscan FC 100H, combiscope FTIR, Milkoscan FT-120, G.C v&agrave; Charm-ll nhằm mục đ&iacute;ch sản xuất ra những sản phẩm si&ecirc;u vệ sinh, sạch nhất, an to&agrave;n nhất cho kh&aacute;ch h&agrave;ng.&nbsp;</span><br />\r\n	<br />\r\n	<span id=\\\"ctl00_cntContent_ctlNewsContent\\\">Nh&agrave; m&aacute;y hướng tới tương lai đầu ti&ecirc;n tại H&agrave;n Quốc</span><br />\r\n	<br />\r\n	Nh&agrave; m&aacute;y được thiết kế thuận lợi để c&oacute; thể lắp đặt, mở rộng c&aacute;c trang thiết bị sản xuất sản phẩm dinh dưỡng v&agrave; thức uống bất cứ l&uacute;c n&agrave;o. Đ&acirc;y cũng l&agrave; nh&agrave; m&aacute;y hướng tới tương lai đầu ti&ecirc;n ở H&agrave;n Quốc c&oacute; lắp đặt hệ thống m&aacute;y điều ho&agrave; trung t&acirc;m cho to&agrave;n nh&agrave; m&aacute;y v&agrave; sản xuất sản phẩm dưới sự điều khiển của hệ thống kiểm so&aacute;t vệ sinh c&oacute; quy m&ocirc; lớn.<br />\r\n	<br />\r\n	<span id=\\\"ctl00_cntContent_ctlNewsContent\\\"><span id=\\\"ctl00_cntContent_ctlNewsContent\\\">Nh&agrave; m&aacute;y hướng đến kh&aacute;ch h&agrave;ng độc nhất trong nước</span><br />\r\n	<br />\r\n	Nh&agrave; m&aacute;y mở cửa cho kh&aacute;ch h&agrave;ng tham quan to&agrave;n bộ quy tr&igrave;nh sản xuất mặt h&agrave;ng bơ sữa, v&igrave; thế kh&aacute;ch h&agrave;ng c&oacute; thể trực tiếp khảo s&aacute;t. Đ&acirc;y l&agrave; kết quả từ những nỗ lực của ch&uacute;ng t&ocirc;i dựa tr&ecirc;n cơ sở xem x&eacute;t đến gi&aacute; cả chất lượng sản phẩm .</span></span></p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	&nbsp;</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	<strong><span><span>2. Nh&agrave; m&aacute;y Honam</span></span></strong></p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	&nbsp;</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	<span><span><span id=\\\"result_box\\\">Nh&agrave; m&aacute;y</span> Honam được ho&agrave;n th&agrave;nh v&agrave;o 09/2008, tọa lạc tại 278 Chongok, Keumcheon, Th&agrave;nh phố Naju . Nh&agrave; m&aacute;y được x&acirc;y dựng tr&ecirc;n khu&ocirc;n vi&ecirc;n rộng 102,300㎡&nbsp; v&agrave; c&oacute; diện t&iacute;ch s&agrave;n l&agrave; 23,100㎡. Nh&agrave; m&aacute;y Honam c&oacute; c&ocirc;ng suất chế biến sữa tươi 300 tấn mỗi ng&agrave;y được vận h&agrave;nh tr&ecirc;n d&acirc;y chuyền sản xuất c&ocirc;ng nghệ cao, ho&agrave;n to&agrave;n tự động.</span></span><br />\r\n	&nbsp;</p>\r\n<p style=\\\"text-align: center;\\\">\r\n	<span><span><img alt=\\\"\\\" src=\\\"/dataweb/images/KHUYENMAI-SUKIEN/TINTUC/NAMYANG/honam-factory.gif\\\" style=\\\"width: 620px; height: 192px;\\\" /></span></span></p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	<span><span>Tập đo&agrave;n Namyang sẽ sản xuất c&aacute;c sản phẩm sữa kh&aacute;c nhau v&agrave; c&oacute; kế hoạch tăng số lượng xuất khẩu c&aacute;c sản phẩm sữa<span><span>.</span></span></span></span></p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	<strong><span><span>3. Nh&agrave; m&aacute;y gyeongju</span></span></strong></p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	<span><span><span id=\\\"ctl00_cntContent_ctlNewsContent\\\">Từ khi th&agrave;nh lập v&agrave;o th&aacute;ng 12 năm 1988, nh&agrave; m&aacute;y Gyeongju sản xuất chủ yếu những mặt h&agrave;ng dinh dưỡng như sản phẩm dinh dưỡng GT, sản phẩm dinh dưỡng đ&atilde; xử l&yacute;, những sản phẩm l&ecirc;n men như E-5, Bulgaris, v&agrave; thức uống như Orchard. </span></span></span></p>\r\n<p style=\\\"text-align: center;\\\">\r\n	<span><span><span><img alt=\\\"\\\" src=\\\"/dataweb/images/KHUYENMAI-SUKIEN/TINTUC/NAMYANG/gyeongju-factory.gif\\\" style=\\\"width: 620px; height: 192px;\\\" /></span></span></span></p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	<span><span><span><span id=\\\"ctl00_cntContent_ctlNewsContent\\\">L&agrave; nh&agrave; m&aacute;y hướng tới sản xuất những sản phẩm c&oacute; t&iacute;nh chất tự nhi&ecirc;n, thực hiện ch&iacute;nh s&aacute;ch an to&agrave;n thực phẩm như &ldquo; th&uacute;c đẩy việc kiểm so&aacute;t nghi&ecirc;m ngặt c&aacute;c cuộc ph&acirc;n t&iacute;ch c&oacute; t&iacute;nh chất mạo hiểm &ldquo; v&agrave; &ldquo;vạch ra chiến lược để hợp thức ho&aacute; sản phẩm&rdquo;. Kết quả l&agrave;, c&ocirc;ng ty đ&atilde; được c&ocirc;ng nhận l&agrave; doanh nghiệp sản xuất &iacute;t ảnh hưởng tới m&ocirc;i trường v&agrave; đ&atilde; nhận được bằng khen ch&iacute;nh thức của thống đốc tỉnh Gyeongsangbuk-do &ldquo;c&ocirc;ng nhận doanh nghiệp sản xuất kh&ocirc;ng ảnh hưởng tới m&ocirc;i trường&rdquo; (Designated Business for Self-control Environment Managerment)</span></span></span></span></p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	<strong><span><span><span><span>4. Nh&agrave; m&aacute;y Gongju</span></span></span></span></strong></p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	<span id=\\\"ctl00_cntContent_ctlNewsContent\\\">Từ khi th&agrave;nh lập th&aacute;ng 02 năm 1980, dựa v&agrave;o th&agrave;nh phần tự nhi&ecirc;n của sữa mẹ, nh&agrave; m&aacute;y Gongju đ&atilde; sản xuất nhiều mặt h&agrave;ng sản phẩm dinh dưỡng trẻ em v&agrave; thức ăn trẻ em như Agisarang, Imperial Dream XO, Namyang Step Granule Saeng...c&oacute; c&ocirc;ng thức tương tự như sữa mẹ. </span></p>\r\n<p style=\\\"text-align: center;\\\">\r\n	<span><img alt=\\\"\\\" src=\\\"/dataweb/images/KHUYENMAI-SUKIEN/TINTUC/NAMYANG/gongju-factory.gif\\\" style=\\\"width: 620px; height: 192px;\\\" /></span></p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	<span><span id=\\\"ctl00_cntContent_ctlNewsContent\\\">C&aacute;c sản phẩm l&ecirc;n men như E-5, Bulgaris... C&aacute;c sản phẩm dinh dưỡng như Einstein, sữa hương vị chuối, sữa hương d&acirc;u, pho m&aacute;t, v&agrave; mặt h&agrave;ng đồ uống như Nearwater O2. Trong suốt chiều d&agrave;i lịch sử 40 năm qua, Namyang vẫn lu&ocirc;n lu&ocirc;n phấn đấu kh&ocirc;ng ngừng nhằm mang lại những sản phẩm dinh dưỡng tốt nhất cho kh&aacute;ch h&agrave;ng.</span></span></p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	<strong><span><span>5. Nh&agrave; m&aacute;y cheonan</span></span></strong></p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	<span><span><span id=\\\"ctl00_cntContent_ctlNewsContent\\\">Từ khi ho&agrave;n th&agrave;nh ng&agrave;y 01 th&aacute;ng 11 năm 1965, v&agrave; bắt đầu đi v&agrave;o hoạt động ng&agrave;y 01 th&aacute;ng 01 năm 1967 đến nay, nh&agrave; m&aacute;y Cheonan nh&agrave; m&aacute;y đầu ti&ecirc;n, được xem l&agrave; nền tảng tạo đ&agrave; cho ch&uacute;ng t&ocirc;i c&oacute; những bước nhảy vọt x&eacute;t cả tr&ecirc;n danh nghĩa v&agrave; thực tế.</span></span></span></p>\r\n<p style=\\\"text-align: center;\\\">\r\n	<span><span><span><img alt=\\\"\\\" src=\\\"/dataweb/images/KHUYENMAI-SUKIEN/TINTUC/NAMYANG/cheonan-factory-old.gif\\\" style=\\\"width: 620px; height: 192px;\\\" /></span></span></span></p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	<span><span><span><span id=\\\"ctl00_cntContent_ctlNewsContent\\\">Nh&agrave; m&aacute;y Cheonan đ&atilde; tạo ra trang sử mới, khởi đầu với sản phẩm đơn thuần chỉ l&agrave; thức ăn d&agrave;nh cho trẻ em c&aacute;ch đ&acirc;y hơn 40 năm. Đ&acirc;y cũng l&agrave; nơi được coi l&agrave; cội nguồn lịch sử đ&aacute;nh dấu những nỗ lực hết sức của những con người trong tập đo&agrave;n Namyang, thực hiện sứ mệnh cao cả của m&igrave;nh, đ&atilde; được ghi nhận trong suốt hơn 40 năm qua. H&agrave;ng năm nh&agrave; m&aacute;y sản xuất trung b&igrave;nh 450 tấn sản phẩm dinh dưỡng v&agrave; 144.000.000 l&iacute;t sữa nước.&nbsp;Ngo&agrave;i ra, nh&agrave; m&aacute;y c&ograve;n&nbsp;sản xuất c&aacute;c mặt h&agrave;ng sản phẩm dinh dưỡng đặc biệt như: Einstein, sản phẩm dinh dưỡng đ&atilde; qua xử l&yacute;, caf&eacute; French, sản phẩm dinh dưỡng kh&ocirc;ng kem, sản phẩm dinh dưỡng nguy&ecirc;n chất sấy kh&ocirc;, v&agrave; Hope Allergy...</span></span></span></span></p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	&nbsp;</p>\r\n', '', '', 'nha-may', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('34', '<p style=\\\"text-align: justify;\\\">\r\n	<img alt=\\\"\\\" src=\\\"/dataweb/images/KHUYENMAI-SUKIEN/TINTUC/NAMYANG/rd.jpg\\\" style=\\\"width: 450px; height: 279px; float: right; margin-left: 10px;\\\" />Central Studying Institute (CSI) -&nbsp;Viện nghi&ecirc;n cứu trung&nbsp;ương,&nbsp;tự h&agrave;o ph&aacute;t triển mặt h&agrave;ng thực phẩm d&agrave;nh cho trẻ em với chất lượng tốt nhất. Tại &quot;Namyang Bunyu&quot; năm 1967, CSI đ&atilde;&nbsp;được&nbsp;nhận Chứng chỉ KOLAS (ph&ograve;ng th&iacute; nghiệm đạt ti&ecirc;u chuẩn cấp quốc gia tại H&agrave;n Quốc), chứng chỉ độc quyền tại H&agrave;n Quốc&nbsp;v&agrave;&nbsp;được c&ocirc;ng nhận l&agrave; Viện nghi&ecirc;n cứu thực phẩm tốt nhất trong nước.<br />\r\n	<br />\r\n	L&agrave; c&ocirc;ng ty c&oacute; thương hiệu đứng đầu, nhờ đầu tư nhiều v&agrave;o những cuộc nghi&ecirc;n cứu với chi ph&iacute; cao v&agrave; c&oacute; sự tham gia của nhiều chuy&ecirc;n gia đầu ng&agrave;nh. CSI đ&atilde; đ&ocirc;t ph&aacute; v&agrave; khởi đầu bằng việc ph&acirc;n t&iacute;ch c&aacute;c th&agrave;nh phần trong nguy&ecirc;n liệu th&ocirc;, ph&aacute;t triển những sản phẩm mới, kiểm so&aacute;t chất lượng tốt hơn.<br />\r\n	<br />\r\n	<strong><img alt=\\\"\\\" height=\\\"12\\\" src=\\\"http://namyangi.com.vn/namyang/upload/image/namyang/red_bullet01.gif\\\" style=\\\"border:0px;vertical-align:bottom;\\\" width=\\\"4\\\" />&nbsp;Nhận chứng chỉ KOLAS độc quyền tại H&agrave;n Quốc</strong><br />\r\n	<br />\r\n	KOLAS(Korea Laboratory Accreditation Scheme) l&agrave; tổ chức c&ocirc;ng nhận về mặt kĩ thuật v&agrave; ti&ecirc;u chuẩn trực thuộc Bộ Thương Mại, C&ocirc;ng nghiệp v&agrave; Năng lượng, chịu tr&aacute;ch nhiệm đối với c&aacute;c cuộc th&iacute; nghiệm được c&ocirc;ng nhận đạt ti&ecirc;u chuẩn quốc gia. Do đ&oacute;, CSI đ&atilde; chiếm được l&ograve;ng tin của người d&acirc;n trong v&agrave; ngo&agrave;i nước theo ti&ecirc;u chuẩn đ&aacute;nh gi&aacute; chất lượng quốc tế ( ISO/IEC 17025).</p>\r\n', '', '', 'trung-tam-r-d', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('35', '<p style=\\\"text-align: justify;\\\">\r\n	<img alt=\\\"\\\" src=\\\"/dataweb/images/KHUYENMAI-SUKIEN/TINTUC/NAMYANG/head-office2.jpg\\\" style=\\\"width: 400px; height: 267px; float: right; margin-left:10px;\\\" />Tập đo&agrave;n Namyang được th&agrave;nh lập từ 13/3/1964, trải qua hơn 50 năm nghi&ecirc;n cứu v&agrave; ph&aacute;t triển. Tập đo&agrave;n nghi&ecirc;n cứu v&agrave; sản xuất c&aacute;c sản phẩm bơ sữa dinh dưỡng Namyang &ndash; H&agrave;n Quốc (Namyang Dairy Products), đ&atilde; g&oacute;p phần quan trọng đưa ngh&agrave;nh c&ocirc;ng nghiệp chế biến dinh dưỡng của H&agrave;n Quốc, vươn tới đỉnh cao của nền khoa học dinh dưỡng ti&ecirc;n tiến thế giới.<br />\r\n	<br />\r\n	Sản phẩm bơ sữa dinh dưỡng d&agrave;nh cho b&agrave; mẹ v&agrave; trẻ em, được c&ocirc;ng ty đặc biệt ch&uacute; trọng v&agrave; kh&ocirc;ng ngừng n&acirc;ng cao chất lượng. Li&ecirc;n tục trong nhiều năm qua c&aacute;c sản phẩm của Namyang đ&atilde; khẳng định được vị tr&iacute; v&agrave; chiếm 70% thị phần của thị trường H&agrave;n Quốc rộng lớn. Kh&ocirc;ng những thế, sản phẩm của Namyang c&ograve;n được xuất khẩu đến nhiều quốc gia tr&ecirc;n thế giới.<br />\r\n	<br />\r\n	Th&aacute;ng 3 năm 2003 đ&aacute;nh dấu sự c&oacute; mặt của Tập đo&agrave;n Namyang tại thị trường Việt Nam với d&ograve;ng sản phẩm dinh dưỡng XO d&agrave;nh cho trẻ nhỏ v&agrave; b&agrave; bầu. Với mong muốn mang tới cho c&aacute;c b&agrave; mẹ v&agrave; trẻ em Việt Nam nguồn dinh dưỡng tốt nhất. Trong thời gian qua, Namyang đ&atilde; v&agrave; đang đẩy mạnh hoạt động tại thị trường n&agrave;y v&agrave; kh&ocirc;ng ngừng ph&aacute;t triển c&aacute;c sản phẩm mới đa dạng v&agrave; phong ph&uacute; hơn.<br />\r\n	<br />\r\n	Được nhập khẩu ch&iacute;nh thức bởi C&ocirc;ng ty cổ phần dược phẩm Traphaco &ndash; Một C&ocirc;ng ty c&oacute; bề d&agrave;y hơn 30 năm trong ngh&agrave;nh dược phẩm. C&ugrave;ng với hệ thống ph&acirc;n phối rộng khắp cả nước. Sản phẩm dinh dưỡng I am Mother, XO, Star Gold của tập đo&agrave;n Namyang &ndash; H&agrave;n Quốc ng&agrave;y c&agrave;ng được c&aacute;c b&agrave; mẹ Việt nam tin d&ugrave;ng bởi chất lượng vượt trội của sản phẩm. C&aacute;c sản phẩm dinh dưỡng của tập đo&agrave;n kh&ocirc;ng những được bổ sung đầy đủ c&aacute;c th&agrave;nh phần dinh dưỡng quan trọng cần thiết trong qu&aacute; tr&igrave;nh ph&aacute;t triển to&agrave;n diện của trẻ m&agrave; đặc biệt hơn nữa, c&aacute;c th&agrave;nh phần c&oacute; trong một số sản phẩm c&ograve;n được chiết xuất từ những thảo dược qu&yacute; hiếm của thi&ecirc;n nhi&ecirc;n.&nbsp;<br />\r\n	<br />\r\n	Từ khi xuất hiện tại thị trường Việt Nam, với sản phẩm chủ lực l&agrave; Sản phẩm dinh dưỡng XO, kh&aacute;c với nhiều doanh nghiệp kh&aacute;c chỉ tập trung v&agrave;o c&ocirc;ng t&aacute;c quảng c&aacute;o, truyền th&ocirc;ng&hellip; Tập đo&agrave;n Namyang đang ch&uacute; trọng x&acirc;y dựng, củng cố niềm tin v&agrave;o chất lượng sản phẩm nơi kh&aacute;ch h&agrave;ng v&agrave; c&aacute;c ng&agrave;nh chức năng bằng ch&iacute;nh những việc l&agrave;m thiết thực của m&igrave;nh. Với quan niệm &quot;<strong>Kinh doanh kh&ocirc;ng đơn thuần l&agrave; lợi nhuận</strong>&quot;, Namyang đ&atilde; tham gia rất nhiều hoạt động cộng đồng như: <strong>&quot;Chung bước y&ecirc;u thương - trao niềm hy vọng&quot;, &quot;Một tr&aacute;i tim - một thế giới&quot;,&nbsp;</strong>&nbsp;<strong>&nbsp;&quot;nối v&ograve;ng tay lớn&quot;, tặng qu&agrave; cho trẻ em ngh&egrave;o c&aacute;c tỉnh v&ugrave;ng s&acirc;u v&ugrave;ng xa như Simacai, C&agrave; Mau, S&oacute;c Trăng, B&igrave;nh Phước, Đắc N&ocirc;ng, Bạc Li&ecirc;u, Lai Ch&acirc;u, M&aacute;i ấm Thi&ecirc;n Phước...</strong><br />\r\n	<br />\r\n	Hiện nay, tr&ecirc;n thị trường Việt Nam đ&atilde; c&oacute; c&aacute;c d&ograve;ng sản phẩm:</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	&nbsp;</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	&bull; I am Mother &nbsp;Mom - Sản phẩm dinh dưỡng đặc biệt cao cấp d&agrave;nh cho&nbsp;phụ nữ mang thai v&agrave; cho con b&uacute;</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	&bull; I am Mother&nbsp;&nbsp;- Sản phẩm dinh dưỡng đặc biệt cao cấp d&agrave;nh cho trẻ nhỏ đến 3 tuổi</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	&bull; &nbsp;I am Mother &nbsp;Kid - Sản phẩm dinh dưỡng đặc biệt cao cấp d&agrave;nh cho trẻ từ 2 đến 15 tuổi</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	&nbsp;</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	&bull; &nbsp;Imperial Mom XO - Sản phẩm dinh dưỡng cao cấp d&agrave;nh cho&nbsp;phụ nữ mang thai v&agrave; cho con b&uacute;</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	&bull; &nbsp;Imperial Dream XO - Sản phẩm dinh dưỡng cao cấp d&agrave;nh cho trẻ nhỏ đến 3 tuổi</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	&bull; Imperial Kid XO -&nbsp;Sản phẩm dinh dưỡng đặc biệt cao cấp d&agrave;nh cho trẻ từ 2 đến 15 tuổi<br />\r\n	<br />\r\n	&bull; Imperial&nbsp;Majesty XO Care - D&agrave;nh cho người trưởng th&agrave;nh, người suy nhược v&agrave; ốm bệnh<br />\r\n	<br />\r\n	&bull; Star Gold Mom - D&agrave;nh cho phụ nữ mang thai v&agrave; cho con b&uacute;&nbsp;</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	&bull; Star Gold - D&agrave;nh cho trẻ theo từng độ tuổi<br />\r\n	<br />\r\n	Với những nỗ lực hết m&igrave;nh nhằm mang đến những hiệu quả tối ưu nhất cho kh&aacute;ch h&agrave;ng. Tại Việt Nam, sản phẩm dinh dưỡng XO của Tập đo&agrave;n Namyang đ&atilde; được c&aacute;c b&aacute;o, tạp ch&iacute; v&agrave; c&aacute;c chuy&ecirc;n gia dinh dưỡng tin cậy v&agrave; đ&aacute;nh gi&aacute; cao. Đặc biệt, năm 2004, sản phẩm dinh dưỡng XO đ&atilde; nhận được Huy Chương V&agrave;ng Thực Phẩm An to&agrave;n của Bộ Y Tế Việt Nam. Ri&ecirc;ng c&aacute;c sản phẩm Imperial Dream XO v&agrave; Imperial Majesty XO đ&atilde; đạt Huy chương v&agrave;ng sản phẩm chất lượng &quot;V&igrave; sức khỏe cộng đồng&quot; tại Hội chợ Triễn l&atilde;m T&ocirc;n vinh thương hiệu v&agrave; sản phẩm V&igrave; sức khỏe cộng đồng th&aacute;ng 4 năm 2008.&nbsp;<br />\r\n	<br />\r\n	Với 3 ph&ograve;ng chăm s&oacute;c kh&aacute;ch h&agrave;ng đặt tại 3 tỉnh th&agrave;nh phố lớn: H&agrave; Nội, Đ&agrave; Nẵng, Hồ Ch&iacute; Minh, Namyang cam kết mang đến cho kh&aacute;ch h&agrave;ng của m&igrave;nh dịch vụ chăm s&oacute;c kh&aacute;ch h&agrave;ng ho&agrave;n hảo nhất, để kh&aacute;ch h&agrave;ng c&oacute; thể ho&agrave;n to&agrave;n tin tưởng v&agrave;o chất lượng sản phẩm của Namyang trong việc mang đến sức khỏe tr&iacute; tuệ cho mọi người.<br />\r\n	<br />\r\n	Với chất lượng v&agrave; hiệu quả vượt trội, sản phẩm sữa của Namyang xứng đ&aacute;ng l&agrave; người bạn đồng h&agrave;nh của c&aacute;c b&agrave; mẹ v&agrave; trẻ em Việt nam.<br />\r\n	<br />\r\n	<strong>Ph&ograve;ng chăm s&aacute;ch kh&aacute;ch h&agrave;ng 1900 7169</strong></p>\r\n', '', '', 'namyang-tai-viet-nam', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('36', '<p style=\\\"text-align: justify;\\\">\r\n	<span style=\\\"font-family: Arial;\\\"><span style=\\\"color: rgb(0, 0, 0);\\\"><span style=\\\"font-size: 12px;\\\"><img alt=\\\"\\\" src=\\\"/dataweb/images/KHUYENMAI-SUKIEN/TINTUC/NAMYANG/head-office1.jpg\\\" style=\\\"width: 450px; height: 300px; float: right;margin-left:10px\\\" />Được th&agrave;nh lập từ 13/03/1964, trải qua hơn 50 năm nghi&ecirc;n cứu v&agrave; ph&aacute;t triển, Tập đo&agrave;n nghi&ecirc;n cứu v&agrave; sản xuất c&aacute;c sản phẩm bơ sữa dinh dưỡng Namyang của H&agrave;n Quốc (Namyang Dairy Products Co., Ltd) đ&atilde; g&oacute;p phần quan</span></span></span><span style=\\\"font-family: Arial;\\\"><span style=\\\"color: rgb(0, 0, 0);\\\"><span style=\\\"font-size: 12px;\\\"> trọng đưa ng&agrave;nh c&ocirc;ng nghiệp chế biến dinh dưỡng của H&agrave;n Quốc vươn tới đỉnh cao của nền khoa học dinh dư&otilde;ng ti&ecirc;n tiến thế giới.<br />\r\n	<br />\r\n	Từ nh&agrave; m&aacute;y Cheonan đầu ti&ecirc;n đi v&agrave;o hoạt động năm 1964, t&iacute;nh đến nay đ&atilde; c&oacute; th&ecirc;m 3 nh&agrave; m&aacute;y nữa ra đời, trong đ&oacute; phải kể đến nh&agrave; m&aacute;y Cheonan mới đi v&agrave;o hoạt động năm 2002. Đ&acirc;y l&agrave; nh&agrave; m&aacute;y quy m&ocirc; lớn nhất v&agrave; được ch&iacute;nh phủ H&agrave;n Quốc chọn l&agrave;m nh&agrave; m&aacute;y kiểu mẫu, nơi đ&oacute;n rất nhiều c&aacute;c đo&agrave;n kh&aacute;ch trong v&agrave; ngo&agrave;i nước tới tham quan, học tập bởi m&ocirc;i trường l&agrave;m việc th&acirc;n thiện với thi&ecirc;n nhi&ecirc;n, thiết bị sản xuất hiện đại. Khi đặt ch&acirc;n tới nh&agrave; m&aacute;y, kh&aacute;ch tham quan sẽ c&oacute; cảm gi&aacute;c đang lạc trong một kh&aacute;ch sạn 5 sao bậc nhất thế giới. Trong c&aacute;c nh&agrave; m&aacute;y của Namyang, nh&acirc;n lực trực tiếp được thay thế bằng những ch&uacute; r&ocirc; bốt điều khiển từ xa. Ti&ecirc;u chuẩn an to&agrave;n vệ sinh thực phẩm được kiểm so&aacute;t rất nghi&ecirc;m ngặt từ kh&acirc;u nguy&ecirc;n liệu đầu v&agrave;o đến kh&acirc;u th&agrave;nh phẩm theo ti&ecirc;u chuẩn khắt khe của thế giới. Để đảm bảo chất lượng với độ an to&agrave;n cao, Namyang c&ograve;n &aacute;p dụng sản xuất theo quy tr&igrave;nh kh&eacute;p k&iacute;n. B&ecirc;n cạnh việc tập trung nu&ocirc;i h&agrave;ng ngh&igrave;n trang trại b&ograve; sữa theo phương thức c&ocirc;ng nghiệp, đảm bảo cung cấp to&agrave;n bộ nguy&ecirc;n liệu sữa cho c&aacute;c nh&agrave; m&aacute;y. Từ năm 1981, Namyang c&ograve;n đầu tư h&agrave;ng triệu USD cho việc x&acirc;y dựng&nbsp;trung t&acirc;m&nbsp;nghi&ecirc;n cứu v&agrave; ph&aacute;t triển với chức năng hoạt động độc lập, phục vụ cho c&ocirc;ng việc&nbsp;nghi&ecirc;n cứu v&agrave; ph&aacute;t triển hệ thống quản l&yacute; c&ocirc;ng nghệ chế biến ng&agrave;y c&agrave;ng tốt hơn.<br />\r\n	<br />\r\n	Th&aacute;ng 2 năm 1980, nh&agrave; m&aacute;y chế biến c&aacute;c sản phẩm bơ sữa quy m&ocirc; lớn nhất của Namyang ra đời tại Kong-ju (thị trấn tại khu trung t&acirc;m H&agrave;n Quốc). T&iacute;nh đến năm 2007, Namyang c&oacute; 4 nh&agrave; m&aacute;y sản xuất trong đ&oacute; c&oacute; nh&agrave; m&aacute;y <strong>Cheonan mới </strong>(năm 2002), với <strong>d&acirc;y chuyền v&agrave; thiết bị hiện đại bậc nhất</strong> được chọn l&agrave; nh&agrave; m&aacute;y kiểu mẫu tại H&agrave;n Quốc. Ti&ecirc;u chuẩn an to&agrave;n vệ sinh thực phẩm được kiểm so&aacute;t rất nghi&ecirc;m ngặt từ kh&acirc;u nguy&ecirc;n liệu đầu v&agrave;o cho đến kh&acirc;u th&agrave;nh phẩm đạt theo ti&ecirc;u chuẩn FDA của Mỹ v&agrave; H&agrave;n Quốc.&nbsp;<br />\r\n	<br />\r\n	<strong>C&aacute;c sản phẩm của Namyang</strong>: Hiện nay, tại H&agrave;n Quốc, Namyang sản xuất tr&ecirc;n 200 sản phẩm bao gồm c&aacute;c loại sản phẩm như: sản phẩm dinh dưỡng, bột dinh dưỡng, c&agrave; ph&ecirc;, đồ uống, nước giải kh&aacute;t c&oacute; lợi cho sức khỏe&hellip; Trong đ&oacute;, sản phẩm bơ sữa dinh dưỡng d&agrave;nh cho b&agrave; mẹ v&agrave; trẻ em được c&ocirc;ng ty đặc biệt ch&uacute; trọng v&agrave; kh&ocirc;ng ngừng n&acirc;ng cao chất lượng. Li&ecirc;n tục trong nhiều năm, c&aacute;c sản phẩm của Namyang đ&atilde; khẳng định được vị tr&iacute; v&agrave; chiếm hơn 70% thị phần của thị trường H&agrave;n Quốc rộng lớn. Kh&ocirc;ng những thế, sản phẩm của Namyang c&ograve;n được xuất khẩu đến nhiều quốc gia &nbsp;tr&ecirc;n thế giới.</span></span></span></p>\r\n', '', '', 've-tap-doan', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('37', '<p style=\\\"text-align: justify;\\\">\r\n	Cửa h&agrave;ng hoa tươi <strong>Gấu Con Flower </strong>được th&agrave;nh lập năm 2011 với mong muốn thay bạn đem lại niềm vui v&agrave; gửi tặng y&ecirc;u thương đến những người th&acirc;n y&ecirc;u, những đối t&aacute;c quan trọng của bạn trong những ng&agrave;y lễ, những khoảnh khắc kh&ocirc;ng thể qu&ecirc;n b&ecirc;n người th&acirc;n y&ecirc;u.</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	Đến với ch&uacute;ng t&ocirc;i, bạn sẽ thật sự an t&acirc;m với th&aacute;i độ phục vụ nhiệt t&igrave;nh của đội ngũ nh&acirc;n vi&ecirc;n năng động, nhiệt t&igrave;nh v&agrave; nhiều năm kinh nghiệm trong ng&agrave;nh.</p>\r\n', '', '', 'intro', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('38', '<p>\n	Tạo ho&aacute; rất y&ecirc;u thương con người ch&uacute;ng ta bởi người đ&atilde; ban cho ta những b&ocirc;ng hoa mu&ocirc;n m&agrave;u, mu&ocirc;n sắc.</p>\n<p>\n	Mỗi 1 nụ hoa mang 1 h&igrave;nh h&agrave;i, sắc th&aacute;i v&agrave; 1 &yacute; nghĩa ri&ecirc;ng! Cũng như con người ch&uacute;ng ta lu&ocirc;n c&oacute; những mối quan hệ ri&ecirc;ng trog cuộc đời.</p>\n<p>\n	Tặng hoa kh&ocirc;ng chỉ đơn thuần l&agrave; gửi trao y&ecirc;u cho người m&igrave;nh y&ecirc;u m&agrave; c&ograve;n c&oacute; những sắc ri&ecirc;ng biệt m&agrave; liệu c&oacute; mấy ai biết!</p>\n<p>\n	Hoa c&oacute; thể thay cho 1 lời tỏ t&igrave;nh ngọt ng&agrave;o l&atilde;ng mạn, cũng c&oacute; thể l&agrave; một lời xin lỗi ch&acirc;n th&agrave;nh! Thay v&agrave;o đ&oacute; c&oacute; thể l&agrave; hoa mừng thọ &ocirc;ng b&agrave;, v&agrave; l&agrave; những sắc m&agrave;u cho ng&agrave;y t&ocirc;n vinh thương hiệu doanh nghiệp! Hoa thể hiện sự thương tiếc l&uacute;c tiễn đưa, thể hiện niềm vui khi trở lại! V&agrave; cũng l&agrave; vật kết nối con người với nhau!</p>\n<p>\n	Hiểu được &yacute; nghĩa v&agrave; tầm quan trọng của những b&ocirc;ng hoa trog cuộc sống mỗi con người, hiểu được mhu cầu của x&atilde; hội hiện đại, Gấu con flower lu&ocirc;n đồng h&agrave;nh c&ugrave;ng bạn, b&ecirc;n cạnh v&agrave; sẽ chia c&ugrave;ng bạn khi bạn cần!</p>\n<p style=\\\"text-align: justify;\\\">\n	Gấu con Flower thay lời muốn n&oacute;i, sẽ chia y&ecirc;u thương, gắng kết con người</p>\n', '', '', 'gioi-thieu', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('39', '<p style=\\\"text-align: justify;\\\">\n	Hiểu được nhu cầu v&agrave; mong muốn của kh&aacute;ch h&agrave;ng &nbsp;Gấu Con cung cấp dịch vụ giao h&agrave;ng tận nơi, giao tận tay người nhận. Tuy nhi&ecirc;n hiện nay hầu hết c&aacute;c website kh&aacute;c họ t&iacute;nh ph&iacute; giao trong c&aacute;c sản phẩm, n&ecirc;n gi&aacute; sẽ tương đối cao, n&ecirc;n để đảm bảo quyền lợi của kh&aacute;ch h&agrave;ng Gấu Con sẽ ấn định mức ph&iacute; giao cho c&aacute;c đơn h&agrave;ng kh&aacute;c nhau với mức phi ph&ugrave; hợp nhất.</p>\n<p style=\\\"text-align: justify;\\\">\n	Gấu Con sẽ<strong> Free Ship</strong> ở c&aacute;c khu vực : <strong>QUẬN 1, QUẬN 3, QUẬN 10</strong> v&agrave; với <strong>gi&aacute; trị đơn h&agrave;ng tr&ecirc;n 500.000 vnđ</strong>. V&agrave; đối với những khu vực l&acirc;n cận sẽ <strong>t&ugrave;y theo đơn h&agrave;ng</strong> v&agrave; <strong>t&ugrave;y theo khu vực</strong> Gấu Con sẽ đưa ra mức ph&iacute; ph&ugrave; hợp nhất, để đảm bảo quyền lợi của kh&aacute;ch h&agrave;ng.</p>\n', '', '', 'phuong-thuc-giao-hang', '0');
INSERT INTO `gaucon_baiviet_tinh` VALUES ('40', '<p style=\\\"text-align: justify;\\\">\n	Sau khi nhận được đơn h&agrave;ng của bạn &nbsp;<strong>Gấu Con Flower </strong>sẽ li&ecirc;n lạc với bạn trong v&ograve;ng 3 giờ &nbsp; để x&aacute;c nhận đơn h&agrave;ng v&agrave; giao h&agrave;ng trong thời gian sớm nhất c&oacute; thể. Trong trường hợp qu&aacute; 5 giờ &nbsp;( hoặc trong trường hợp cần thiết ) nhưng bạn chưa thấy phản hồi từ nh&acirc;n vi&ecirc;n của Gấu Con,. Bạn vui l&ograve;ng &nbsp;trực tiếp li&ecirc;n hệ với Gấu Con qua<strong> Hotline : 0909.300.952 hoặc 0909.306.952 &amp; 08.62908567</strong></p>\n<p style=\\\"text-align: justify;\\\">\n	<strong>Phương Thức Thanh To&aacute;n</strong></p>\n<p style=\\\"text-align: justify;\\\">\n	<b>Đối với kh&aacute;ch h&agrave;ng trong khu vực TP.HCM&nbsp;</b></p>\n<p style=\\\"text-align: justify;\\\">\n	Bạn c&oacute; thể thanh to&aacute;n 100% trước khi nhận h&agrave;ng bằng tiền mặt hoặc th&ocirc;ng qua chuyển khoản.</p>\n<p style=\\\"text-align: justify;\\\">\n	<strong>Đối với kh&aacute;ch h&agrave;ng ngo&agrave;i khu vực TP.HCM&nbsp;</strong></p>\n<p style=\\\"text-align: justify;\\\">\n	Bạn vui l&ograve;ng thanh to&aacute;n 100% gi&aacute; trị đơn h&agrave;ng th&ocirc;ng qua chuyển khoản</p>\n<p style=\\\"text-align: justify;\\\">\n	<strong>TH&Ocirc;NG TIN CHUYỂN KHOẢN :</strong></p>\n<p style=\\\"text-align: justify;\\\">\n	Chủ t&agrave;i khoản : <strong>L&ecirc; Thị Thu Trang&nbsp;</strong></p>\n<p style=\\\"text-align: justify;\\\">\n	1. Ng&acirc;n H&agrave;ng Thương Mại Cổ Phần Ngoại Thương VN - Chi Nh&aacute;nh TP.HCM (VCB) : <strong>007.1003.711.760&nbsp;</strong></p>\n<p style=\\\"text-align: justify;\\\">\n	2. Ng&acirc;n H&agrave;ng Thương Mại Cổ Phần Đ&ocirc;ng &Aacute; : <strong>0103.253.151</strong></p>\n<p style=\\\"text-align: justify;\\\">\n	3. Ng&acirc;n H&agrave;ng Thương Mại Cổ Phần &Aacute; Ch&acirc;u (ACB) : <strong>2053.27129</strong></p>\n', '', '', 'phuong-thuc-thanh-toan', '0');

-- ----------------------------
-- Table structure for `gaucon_city`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_city`;
CREATE TABLE `gaucon_city` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(128) character set utf8 collate utf8_unicode_ci NOT NULL,
  `ship_50` double NOT NULL,
  `ship_50_100` double NOT NULL,
  `ship_100_250` double NOT NULL,
  `ship_250_500` double NOT NULL,
  `ship_500_1000` double NOT NULL,
  `ship_1000_1500` double NOT NULL,
  `ship_1500_2000` double NOT NULL,
  `ship_plus` double NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_city
-- ----------------------------
INSERT INTO `gaucon_city` VALUES ('69', 'TP. Hồ Chí Minh', '10800', '10800', '13500', '16875', '20250', '24300', '28350', '2160');
INSERT INTO `gaucon_city` VALUES ('70', 'TP. Hải Phòng', '1350', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('71', 'TP. Đà Nẵng', '12825', '18225', '27000', '35775', '51975', '66825', '80325', '11475');
INSERT INTO `gaucon_city` VALUES ('72', ' Hà Giang', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('73', ' Cao Bằng', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('74', ' Lai Châu', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('75', ' Lào Cai', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('76', ' Tuyên Quang', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('77', ' Lạng Sơn', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('78', ' Bắc Kạn', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('79', ' Thái Nguyên', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('80', ' Yên Bái', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('81', ' Sơn La', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('82', ' Phú Thọ', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('83', ' Vĩnh Phúc', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('84', ' Quảng Ninh', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('85', ' Bắc Giang', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('86', ' Bắc Ninh', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('87', 'TP. Hà Nội', '12825', '18225', '29025', '37800', '54675', '70875', '85725', '11475');
INSERT INTO `gaucon_city` VALUES ('88', 'TP. Hà Nội( Hà Tây cũ)', '12825', '18225', '29025', '37800', '54675', '70875', '85725', '11475');
INSERT INTO `gaucon_city` VALUES ('89', ' Hải Dương', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('90', ' Hưng Yên', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('91', ' Hòa Bình', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('92', ' Hà Nam', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('93', ' Nam Định', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('94', ' Thái Bình', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('95', ' Ninh Bình', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('96', ' Thanh Hóa', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('97', ' Nghệ An', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('98', ' Hà Tĩnh', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('99', ' Quảng Bình', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('100', ' Quảng Trị', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('101', ' Thừa Thiên - Huế', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('102', ' Quảng Nam', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('103', ' Quảng Ngãi', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('104', ' Kon Tum', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('105', ' Bình Định', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('106', ' Gia Lai', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('107', ' Phú Yên', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('108', ' Đăk Lăk', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('109', ' Khánh Hòa', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('110', ' Lâm Đồng', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('111', ' Bình Phước', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('112', ' Bình Dương', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('113', ' Ninh Thuận', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('114', ' Tây Ninh', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('115', ' Bình Thuận', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('116', ' Đồng Nai', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('117', ' Long An', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('118', ' Đồng Tháp', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('119', ' An Giang', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('120', ' Bà Rịa - Vũng Tàu', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('121', ' Tiền Giang', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('122', ' Kiên Giang', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('123', 'TP. Cần Thơ', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('124', ' Bến Tre', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('125', ' Vĩnh Long', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('126', ' Trà Vinh', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('127', ' Sóc Trăng', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('128', ' Bạc Liêu', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('129', ' Cà Mau', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('130', ' Điện Biên', '13500', '18900', '30375', '39825', '58725', '74925', '91125', '12825');
INSERT INTO `gaucon_city` VALUES ('131', ' Đăk Nông', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');
INSERT INTO `gaucon_city` VALUES ('132', ' Hậu Giang', '11475', '11475', '22275', '31725', '44550', '54000', '65475', '5130');

-- ----------------------------
-- Table structure for `gaucon_comment`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_comment`;
CREATE TABLE `gaucon_comment` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `link` varchar(255) NOT NULL,
  `parent` tinyint(4) NOT NULL,
  `id_news` int(11) NOT NULL,
  `add_date` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_comment
-- ----------------------------
INSERT INTO `gaucon_comment` VALUES ('37', '', '', '', 'sao chán', '', '36', '0', '0000-00-00 00:00:00', '0');
INSERT INTO `gaucon_comment` VALUES ('35', '', '', '', 'vitamin D rất quan trọng bạn lưu ý nhé', '', '34', '0', '0000-00-00 00:00:00', '0');
INSERT INTO `gaucon_comment` VALUES ('39', '', '', '', '', '', '38', '0', '0000-00-00 00:00:00', '0');

-- ----------------------------
-- Table structure for `gaucon_config`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_config`;
CREATE TABLE `gaucon_config` (
  `smtp_user` varchar(255) collate utf8_unicode_ci NOT NULL,
  `smtp_pass` varchar(255) collate utf8_unicode_ci NOT NULL,
  `nick_yahoo` varchar(255) collate utf8_unicode_ci NOT NULL,
  `phone` varchar(255) collate utf8_unicode_ci NOT NULL,
  `hotline` varchar(255) collate utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) collate utf8_unicode_ci NOT NULL,
  `cell_phone` varchar(255) collate utf8_unicode_ci NOT NULL,
  `email` varchar(255) collate utf8_unicode_ci NOT NULL,
  `support_email` varchar(255) collate utf8_unicode_ci NOT NULL,
  `support_phone` varchar(255) collate utf8_unicode_ci NOT NULL,
  `title_web` varchar(255) collate utf8_unicode_ci NOT NULL,
  `meta_keywords` text collate utf8_unicode_ci NOT NULL,
  `meta_description` text collate utf8_unicode_ci NOT NULL,
  `address` text collate utf8_unicode_ci NOT NULL,
  `v_address` text collate utf8_unicode_ci NOT NULL,
  `e_address` text collate utf8_unicode_ci NOT NULL,
  `v_address2` text collate utf8_unicode_ci NOT NULL,
  `e_address2` text collate utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of gaucon_config
-- ----------------------------
INSERT INTO `gaucon_config` VALUES ('mailer@saigondomain.com', '@999saigondomain', '#', 'https://www.facebook.com/gauconflower/', '#', '#', '', 'support@saigondomain.com', 'Email (hỗ trợ)', 'Số điện thoại (hỗ trợ)', 'Gấu Con Flower', 'gấu con flower, cửa hàng hoa tươi gấu con', 'Gấu Con cung cấp đa dạng các loại hoa hiện đang thịnh hành, luôn tươi mới và đảm bảo chất lượng, như các dòng hoa hồng đủ màu sắc, hoa hướng dương, bách hợp, cẩm chướng, hoa lan, hoa huệ, mimosa…', '', '', '', '', '');

-- ----------------------------
-- Table structure for `gaucon_contact`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_contact`;
CREATE TABLE `gaucon_contact` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `contact` text NOT NULL,
  `add_date` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_contact
-- ----------------------------

-- ----------------------------
-- Table structure for `gaucon_customer`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_customer`;
CREATE TABLE `gaucon_customer` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `pas` varchar(255) NOT NULL,
  `add_date` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_customer
-- ----------------------------
INSERT INTO `gaucon_customer` VALUES ('1', 'Huỳnh Văn Được', '0909 257 034', 'support@saigondomain.com', '220 Bui Đinh Tuy', '202cb962ac59075b964b07152d234b70', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `gaucon_district`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_district`;
CREATE TABLE `gaucon_district` (
  `id` int(11) NOT NULL auto_increment,
  `city` int(11) NOT NULL,
  `name` varchar(128) character set utf8 collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=1374 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_district
-- ----------------------------
INSERT INTO `gaucon_district` VALUES ('687', '69', 'Q.1');
INSERT INTO `gaucon_district` VALUES ('688', '69', 'Q.2');
INSERT INTO `gaucon_district` VALUES ('689', '69', 'Q.3');
INSERT INTO `gaucon_district` VALUES ('690', '69', 'Q.4');
INSERT INTO `gaucon_district` VALUES ('691', '69', 'Q.5');
INSERT INTO `gaucon_district` VALUES ('692', '69', 'Q. 6');
INSERT INTO `gaucon_district` VALUES ('693', '69', 'Q.7');
INSERT INTO `gaucon_district` VALUES ('694', '69', 'Q.8');
INSERT INTO `gaucon_district` VALUES ('695', '69', 'Q. 9');
INSERT INTO `gaucon_district` VALUES ('696', '69', 'Q. 10');
INSERT INTO `gaucon_district` VALUES ('697', '69', 'Q. 11');
INSERT INTO `gaucon_district` VALUES ('698', '69', 'Q.12');
INSERT INTO `gaucon_district` VALUES ('699', '69', 'Q. Gò Vấp');
INSERT INTO `gaucon_district` VALUES ('700', '69', 'Q.Tân Bình');
INSERT INTO `gaucon_district` VALUES ('701', '69', 'Q.Tân Phú');
INSERT INTO `gaucon_district` VALUES ('702', '69', 'Q. Bình Thạnh');
INSERT INTO `gaucon_district` VALUES ('703', '69', 'Q.Phú Nhuận');
INSERT INTO `gaucon_district` VALUES ('704', '69', 'Q.Thủ Đức');
INSERT INTO `gaucon_district` VALUES ('705', '69', 'Q. Bình Tân');
INSERT INTO `gaucon_district` VALUES ('706', '69', ' Bình Chánh');
INSERT INTO `gaucon_district` VALUES ('707', '69', ' Củ Chi');
INSERT INTO `gaucon_district` VALUES ('708', '69', ' Hóc Môn');
INSERT INTO `gaucon_district` VALUES ('709', '69', ' Nhà Bè');
INSERT INTO `gaucon_district` VALUES ('710', '69', ' Cần Giờ');
INSERT INTO `gaucon_district` VALUES ('711', '70', 'Quận Hồng Bàng');
INSERT INTO `gaucon_district` VALUES ('712', '70', 'Quận Lê Chân');
INSERT INTO `gaucon_district` VALUES ('713', '70', 'Quận Ngô Quyền');
INSERT INTO `gaucon_district` VALUES ('714', '70', 'Quận Kiến An');
INSERT INTO `gaucon_district` VALUES ('715', '70', 'Quận Hải An');
INSERT INTO `gaucon_district` VALUES ('716', '70', 'Quận Đồ Sơn');
INSERT INTO `gaucon_district` VALUES ('717', '70', ' An Lão');
INSERT INTO `gaucon_district` VALUES ('718', '70', ' Kiến Thuỵ');
INSERT INTO `gaucon_district` VALUES ('719', '70', ' Thủy Nguyên');
INSERT INTO `gaucon_district` VALUES ('720', '70', ' An Dương');
INSERT INTO `gaucon_district` VALUES ('721', '70', ' Tiên Lãng');
INSERT INTO `gaucon_district` VALUES ('722', '70', ' Vĩnh Bảo');
INSERT INTO `gaucon_district` VALUES ('723', '70', ' Cát Hải');
INSERT INTO `gaucon_district` VALUES ('724', '70', ' Bạch Long Vĩ');
INSERT INTO `gaucon_district` VALUES ('725', '70', 'Quận Dương Kinh');
INSERT INTO `gaucon_district` VALUES ('726', '71', 'Quận Hải Châu');
INSERT INTO `gaucon_district` VALUES ('727', '71', 'Quận Thanh Khê');
INSERT INTO `gaucon_district` VALUES ('728', '71', 'Quận Sơn Trà');
INSERT INTO `gaucon_district` VALUES ('729', '71', 'Quận Ngũ Hành Sơn');
INSERT INTO `gaucon_district` VALUES ('730', '71', 'Quận Liên Chiểu');
INSERT INTO `gaucon_district` VALUES ('731', '71', ' Hoà Vang');
INSERT INTO `gaucon_district` VALUES ('732', '71', 'Quận Cẩm Lệ');
INSERT INTO `gaucon_district` VALUES ('733', '72', 'Thành phố Hà Giang');
INSERT INTO `gaucon_district` VALUES ('734', '72', ' Đồng Văn');
INSERT INTO `gaucon_district` VALUES ('735', '72', ' Mèo Vạc');
INSERT INTO `gaucon_district` VALUES ('736', '72', ' Yên Minh');
INSERT INTO `gaucon_district` VALUES ('737', '72', ' Quản Bạ');
INSERT INTO `gaucon_district` VALUES ('738', '72', ' Vị Xuyên');
INSERT INTO `gaucon_district` VALUES ('739', '72', ' Bắc Mê');
INSERT INTO `gaucon_district` VALUES ('740', '72', ' Hoàng Su Phì');
INSERT INTO `gaucon_district` VALUES ('741', '72', ' Xín Mần');
INSERT INTO `gaucon_district` VALUES ('742', '72', ' Bắc Quang');
INSERT INTO `gaucon_district` VALUES ('743', '72', ' Quang Bình');
INSERT INTO `gaucon_district` VALUES ('744', '73', 'Thị xã Cao Bằng');
INSERT INTO `gaucon_district` VALUES ('745', '73', ' Bảo Lạc');
INSERT INTO `gaucon_district` VALUES ('746', '73', ' Thông Nông');
INSERT INTO `gaucon_district` VALUES ('747', '73', ' Hà Quảng');
INSERT INTO `gaucon_district` VALUES ('748', '73', ' Trà Lĩnh');
INSERT INTO `gaucon_district` VALUES ('749', '73', ' Trùng Khánh');
INSERT INTO `gaucon_district` VALUES ('750', '73', ' Nguyên Bình');
INSERT INTO `gaucon_district` VALUES ('751', '73', ' Hoà An');
INSERT INTO `gaucon_district` VALUES ('752', '73', ' Quảng Uyên');
INSERT INTO `gaucon_district` VALUES ('753', '73', ' Thạch An');
INSERT INTO `gaucon_district` VALUES ('754', '73', ' Hạ Lang');
INSERT INTO `gaucon_district` VALUES ('755', '73', ' Bảo Lâm');
INSERT INTO `gaucon_district` VALUES ('756', '73', ' Phục Hoà');
INSERT INTO `gaucon_district` VALUES ('757', '74', 'Thị xã Lai Châu');
INSERT INTO `gaucon_district` VALUES ('758', '74', ' Tam Đường');
INSERT INTO `gaucon_district` VALUES ('759', '74', ' Phong Thổ');
INSERT INTO `gaucon_district` VALUES ('760', '74', ' Sìn Hồ');
INSERT INTO `gaucon_district` VALUES ('761', '74', ' Mường Tè');
INSERT INTO `gaucon_district` VALUES ('762', '74', ' Than Uyên');
INSERT INTO `gaucon_district` VALUES ('763', '75', 'Thành phố Lào Cai');
INSERT INTO `gaucon_district` VALUES ('764', '75', ' Xi Ma Cai');
INSERT INTO `gaucon_district` VALUES ('765', '75', ' Bát Xát');
INSERT INTO `gaucon_district` VALUES ('766', '75', ' Bảo Thắng');
INSERT INTO `gaucon_district` VALUES ('767', '75', ' Sa Pa');
INSERT INTO `gaucon_district` VALUES ('768', '75', ' Văn Bàn');
INSERT INTO `gaucon_district` VALUES ('769', '75', ' Bảo Yên');
INSERT INTO `gaucon_district` VALUES ('770', '75', ' Bắc Hà');
INSERT INTO `gaucon_district` VALUES ('771', '75', ' Mường Khương');
INSERT INTO `gaucon_district` VALUES ('772', '76', 'Thành phố Tuyên Quang');
INSERT INTO `gaucon_district` VALUES ('773', '76', ' Na Hang');
INSERT INTO `gaucon_district` VALUES ('774', '76', ' Chiêm Hoá');
INSERT INTO `gaucon_district` VALUES ('775', '76', ' Hàm Yên');
INSERT INTO `gaucon_district` VALUES ('776', '76', ' Yên Sơn');
INSERT INTO `gaucon_district` VALUES ('777', '76', ' Sơn Dương');
INSERT INTO `gaucon_district` VALUES ('778', '77', 'Thành phố Lạng Sơn');
INSERT INTO `gaucon_district` VALUES ('779', '77', ' Tràng Định');
INSERT INTO `gaucon_district` VALUES ('780', '77', ' Bình Gia');
INSERT INTO `gaucon_district` VALUES ('781', '77', ' Văn Lãng');
INSERT INTO `gaucon_district` VALUES ('782', '77', ' Bắc Sơn');
INSERT INTO `gaucon_district` VALUES ('783', '77', ' Văn Quan');
INSERT INTO `gaucon_district` VALUES ('784', '77', ' Cao Lộc');
INSERT INTO `gaucon_district` VALUES ('785', '77', ' Lộc Bình');
INSERT INTO `gaucon_district` VALUES ('786', '77', ' Chi Lăng');
INSERT INTO `gaucon_district` VALUES ('787', '77', ' Đình Lập');
INSERT INTO `gaucon_district` VALUES ('788', '77', ' Hữu Lũng');
INSERT INTO `gaucon_district` VALUES ('789', '78', 'Thị xã Bắc Kạn');
INSERT INTO `gaucon_district` VALUES ('790', '78', ' Chợ Đồn');
INSERT INTO `gaucon_district` VALUES ('791', '78', ' Bạch Thông');
INSERT INTO `gaucon_district` VALUES ('792', '78', ' Na Rì');
INSERT INTO `gaucon_district` VALUES ('793', '78', ' Ngân Sơn');
INSERT INTO `gaucon_district` VALUES ('794', '78', ' Ba Bể');
INSERT INTO `gaucon_district` VALUES ('795', '78', ' Chợ Mới');
INSERT INTO `gaucon_district` VALUES ('796', '78', ' Pác Nặm');
INSERT INTO `gaucon_district` VALUES ('797', '79', 'TP.Thái Nguyên');
INSERT INTO `gaucon_district` VALUES ('798', '79', 'Thị xã Sông Công');
INSERT INTO `gaucon_district` VALUES ('799', '79', ' Định Hoá');
INSERT INTO `gaucon_district` VALUES ('800', '79', ' Phú Lương');
INSERT INTO `gaucon_district` VALUES ('801', '79', ' Võ Nhai');
INSERT INTO `gaucon_district` VALUES ('802', '79', ' Đại Từ');
INSERT INTO `gaucon_district` VALUES ('803', '79', ' Đồng Hỷ');
INSERT INTO `gaucon_district` VALUES ('804', '79', ' Phú Bình');
INSERT INTO `gaucon_district` VALUES ('805', '79', ' Phổ Yên');
INSERT INTO `gaucon_district` VALUES ('806', '80', 'Thành phố Yên Bái');
INSERT INTO `gaucon_district` VALUES ('807', '80', 'Thị xã Nghĩa Lộ');
INSERT INTO `gaucon_district` VALUES ('808', '80', ' Văn Yên');
INSERT INTO `gaucon_district` VALUES ('809', '80', ' Yên Bình');
INSERT INTO `gaucon_district` VALUES ('810', '80', ' Mù Cang Chải');
INSERT INTO `gaucon_district` VALUES ('811', '80', ' Văn Chấn');
INSERT INTO `gaucon_district` VALUES ('812', '80', ' Trấn Yên');
INSERT INTO `gaucon_district` VALUES ('813', '80', ' Trạm Tấu');
INSERT INTO `gaucon_district` VALUES ('814', '80', ' Lục Yên');
INSERT INTO `gaucon_district` VALUES ('815', '81', 'Thị xã Sơn La');
INSERT INTO `gaucon_district` VALUES ('816', '81', ' Quỳnh Nhai');
INSERT INTO `gaucon_district` VALUES ('817', '81', ' Mường La');
INSERT INTO `gaucon_district` VALUES ('818', '81', ' Thuận Châu');
INSERT INTO `gaucon_district` VALUES ('819', '81', ' Bắc Yên');
INSERT INTO `gaucon_district` VALUES ('820', '81', ' Phù Yên');
INSERT INTO `gaucon_district` VALUES ('821', '81', ' Mai Sơn');
INSERT INTO `gaucon_district` VALUES ('822', '81', ' Yên Châu');
INSERT INTO `gaucon_district` VALUES ('823', '81', ' Sông Mã');
INSERT INTO `gaucon_district` VALUES ('824', '81', ' Mộc Châu');
INSERT INTO `gaucon_district` VALUES ('825', '81', ' Sốp Cộp');
INSERT INTO `gaucon_district` VALUES ('826', '82', 'TP. Việt Trì');
INSERT INTO `gaucon_district` VALUES ('827', '82', 'Thị xã Phú Thọ');
INSERT INTO `gaucon_district` VALUES ('828', '82', ' Đoan Hùng');
INSERT INTO `gaucon_district` VALUES ('829', '82', ' Thanh Ba');
INSERT INTO `gaucon_district` VALUES ('830', '82', ' Hạ Hoà');
INSERT INTO `gaucon_district` VALUES ('831', '82', ' Cẩm Khê');
INSERT INTO `gaucon_district` VALUES ('832', '82', ' Yên Lập');
INSERT INTO `gaucon_district` VALUES ('833', '82', ' Thanh Sơn');
INSERT INTO `gaucon_district` VALUES ('834', '82', ' Phù Ninh');
INSERT INTO `gaucon_district` VALUES ('835', '82', ' Lâm Thao');
INSERT INTO `gaucon_district` VALUES ('836', '82', ' Tam Nông');
INSERT INTO `gaucon_district` VALUES ('837', '82', ' Thanh Thủy');
INSERT INTO `gaucon_district` VALUES ('838', '82', ' Tân Sơn');
INSERT INTO `gaucon_district` VALUES ('839', '83', 'Thành phố Vĩnh Yên');
INSERT INTO `gaucon_district` VALUES ('840', '83', ' Tam Dương');
INSERT INTO `gaucon_district` VALUES ('841', '83', ' Lập Thạch');
INSERT INTO `gaucon_district` VALUES ('842', '83', ' Vĩnh Tường');
INSERT INTO `gaucon_district` VALUES ('843', '83', ' Yên Lạc');
INSERT INTO `gaucon_district` VALUES ('844', '83', ' Bình Xuyên');
INSERT INTO `gaucon_district` VALUES ('845', '83', ' Mê Linh');
INSERT INTO `gaucon_district` VALUES ('846', '83', 'Thị xã Phúc Yên');
INSERT INTO `gaucon_district` VALUES ('847', '83', ' Tam Đảo');
INSERT INTO `gaucon_district` VALUES ('848', '84', 'TP. Hạ Long');
INSERT INTO `gaucon_district` VALUES ('849', '84', 'Thị xã Cẩm Phả');
INSERT INTO `gaucon_district` VALUES ('850', '84', 'Thị xã Uông Bí');
INSERT INTO `gaucon_district` VALUES ('851', '84', 'Thị xã Móng Cái');
INSERT INTO `gaucon_district` VALUES ('852', '84', ' Bình Liêu');
INSERT INTO `gaucon_district` VALUES ('853', '84', ' Đầm Hà');
INSERT INTO `gaucon_district` VALUES ('854', '84', ' Hải Hà');
INSERT INTO `gaucon_district` VALUES ('855', '84', ' Tiên Yên');
INSERT INTO `gaucon_district` VALUES ('856', '84', ' Ba Chẽ');
INSERT INTO `gaucon_district` VALUES ('857', '84', ' Đông Triều');
INSERT INTO `gaucon_district` VALUES ('858', '84', ' Yên Hưng');
INSERT INTO `gaucon_district` VALUES ('859', '84', ' Hoành Bồ');
INSERT INTO `gaucon_district` VALUES ('860', '84', ' Vân Đồn');
INSERT INTO `gaucon_district` VALUES ('861', '84', ' Cô Tô');
INSERT INTO `gaucon_district` VALUES ('862', '85', 'Thành phố Bắc Giang');
INSERT INTO `gaucon_district` VALUES ('863', '85', ' Yên Thế');
INSERT INTO `gaucon_district` VALUES ('864', '85', ' Lục Ngạn');
INSERT INTO `gaucon_district` VALUES ('865', '85', ' Sơn Động');
INSERT INTO `gaucon_district` VALUES ('866', '85', ' Lục Nam');
INSERT INTO `gaucon_district` VALUES ('867', '85', ' Tân Yên');
INSERT INTO `gaucon_district` VALUES ('868', '85', ' Hiệp Hoà');
INSERT INTO `gaucon_district` VALUES ('869', '85', ' Lạng Giang');
INSERT INTO `gaucon_district` VALUES ('870', '85', ' Việt Yên');
INSERT INTO `gaucon_district` VALUES ('871', '85', ' Yên Dũng');
INSERT INTO `gaucon_district` VALUES ('872', '86', 'Thành phố Bắc Ninh');
INSERT INTO `gaucon_district` VALUES ('873', '86', ' Yên Phong');
INSERT INTO `gaucon_district` VALUES ('874', '86', ' Quế Võ.');
INSERT INTO `gaucon_district` VALUES ('875', '86', ' Tiên Du');
INSERT INTO `gaucon_district` VALUES ('876', '86', ' Từ Sơn');
INSERT INTO `gaucon_district` VALUES ('877', '86', ' Thuận Thành');
INSERT INTO `gaucon_district` VALUES ('878', '86', ' Gia Bình');
INSERT INTO `gaucon_district` VALUES ('879', '86', ' Lương Tài');
INSERT INTO `gaucon_district` VALUES ('880', '87', 'Quận Hoàn Kiếm');
INSERT INTO `gaucon_district` VALUES ('881', '87', 'Quận Hai Bà Trưng');
INSERT INTO `gaucon_district` VALUES ('882', '87', 'Quận Đống Đa');
INSERT INTO `gaucon_district` VALUES ('883', '87', 'Quận Tây Hồ');
INSERT INTO `gaucon_district` VALUES ('884', '87', 'Quận Cầu Giấy');
INSERT INTO `gaucon_district` VALUES ('885', '87', 'Quận Thanh Xuân');
INSERT INTO `gaucon_district` VALUES ('886', '87', 'Quận Hoàng Mai');
INSERT INTO `gaucon_district` VALUES ('887', '87', 'Quận Long Biên');
INSERT INTO `gaucon_district` VALUES ('888', '87', ' Từ Liêm');
INSERT INTO `gaucon_district` VALUES ('889', '87', ' Thanh Trì');
INSERT INTO `gaucon_district` VALUES ('890', '87', ' Gia Lâm');
INSERT INTO `gaucon_district` VALUES ('891', '87', ' Đông Anh');
INSERT INTO `gaucon_district` VALUES ('892', '87', ' Sóc Sơn');
INSERT INTO `gaucon_district` VALUES ('893', '88', 'Thị xã Sơn Tây');
INSERT INTO `gaucon_district` VALUES ('894', '88', ' Ba Vì');
INSERT INTO `gaucon_district` VALUES ('895', '88', ' Phúc Thọ');
INSERT INTO `gaucon_district` VALUES ('896', '88', ' Thạch Thất');
INSERT INTO `gaucon_district` VALUES ('897', '88', ' Quốc Oai');
INSERT INTO `gaucon_district` VALUES ('898', '88', ' Chương Mỹ');
INSERT INTO `gaucon_district` VALUES ('899', '88', ' Đan Phượng');
INSERT INTO `gaucon_district` VALUES ('900', '88', ' Hoài Đức');
INSERT INTO `gaucon_district` VALUES ('901', '88', ' Thanh Oai');
INSERT INTO `gaucon_district` VALUES ('902', '88', ' Mỹ Đức');
INSERT INTO `gaucon_district` VALUES ('903', '88', ' Ứng Hoà');
INSERT INTO `gaucon_district` VALUES ('904', '88', ' Thường Tín');
INSERT INTO `gaucon_district` VALUES ('905', '88', ' Phú Xuyên');
INSERT INTO `gaucon_district` VALUES ('906', '88', ' Mê Linh');
INSERT INTO `gaucon_district` VALUES ('907', '89', 'Thành phố Hải Dương');
INSERT INTO `gaucon_district` VALUES ('908', '89', ' Chí Linh');
INSERT INTO `gaucon_district` VALUES ('909', '89', ' Nam Sách');
INSERT INTO `gaucon_district` VALUES ('910', '89', ' Kinh Môn');
INSERT INTO `gaucon_district` VALUES ('911', '89', ' Gia Lộc');
INSERT INTO `gaucon_district` VALUES ('912', '89', ' Tứ Kỳ');
INSERT INTO `gaucon_district` VALUES ('913', '89', ' Thanh Miện');
INSERT INTO `gaucon_district` VALUES ('914', '89', ' Ninh Giang');
INSERT INTO `gaucon_district` VALUES ('915', '89', ' Cẩm Giàng');
INSERT INTO `gaucon_district` VALUES ('916', '89', ' Thanh Hà');
INSERT INTO `gaucon_district` VALUES ('917', '89', ' Kim Thành');
INSERT INTO `gaucon_district` VALUES ('918', '89', ' Bình Giang');
INSERT INTO `gaucon_district` VALUES ('919', '90', 'Thị xã Hưng Yên');
INSERT INTO `gaucon_district` VALUES ('920', '90', ' Kim Động');
INSERT INTO `gaucon_district` VALUES ('921', '90', ' Ân Thi');
INSERT INTO `gaucon_district` VALUES ('922', '90', ' Khoái Châu');
INSERT INTO `gaucon_district` VALUES ('923', '90', ' Yên Mỹ');
INSERT INTO `gaucon_district` VALUES ('924', '90', ' Tiên Lữ');
INSERT INTO `gaucon_district` VALUES ('925', '90', ' Phù Cừ');
INSERT INTO `gaucon_district` VALUES ('926', '90', ' Mỹ Hào');
INSERT INTO `gaucon_district` VALUES ('927', '90', ' Văn Lâm');
INSERT INTO `gaucon_district` VALUES ('928', '90', ' Văn Giang');
INSERT INTO `gaucon_district` VALUES ('929', '91', 'Thành phố Hoà Bình');
INSERT INTO `gaucon_district` VALUES ('930', '91', ' Đà Bắc');
INSERT INTO `gaucon_district` VALUES ('931', '91', ' Mai Châu');
INSERT INTO `gaucon_district` VALUES ('932', '91', ' Tân Lạc');
INSERT INTO `gaucon_district` VALUES ('933', '91', ' Lạc Sơn');
INSERT INTO `gaucon_district` VALUES ('934', '91', ' Kỳ Sơn');
INSERT INTO `gaucon_district` VALUES ('935', '91', ' Lưương Sơn');
INSERT INTO `gaucon_district` VALUES ('936', '91', ' Kim Bôi');
INSERT INTO `gaucon_district` VALUES ('937', '91', ' Lạc Thuỷ');
INSERT INTO `gaucon_district` VALUES ('938', '91', ' Yên Thuỷ');
INSERT INTO `gaucon_district` VALUES ('939', '91', ' Cao Phong');
INSERT INTO `gaucon_district` VALUES ('940', '92', 'Thị xã Phủ Lý');
INSERT INTO `gaucon_district` VALUES ('941', '92', ' Duy Tiên');
INSERT INTO `gaucon_district` VALUES ('942', '92', ' Kim Bảng');
INSERT INTO `gaucon_district` VALUES ('943', '92', ' Lý Nhân');
INSERT INTO `gaucon_district` VALUES ('944', '92', 'Huỵện Thanh Liêm');
INSERT INTO `gaucon_district` VALUES ('945', '92', ' Bình Lục');
INSERT INTO `gaucon_district` VALUES ('946', '93', 'TP. Nam Định');
INSERT INTO `gaucon_district` VALUES ('947', '93', ' Mỹ Lộc');
INSERT INTO `gaucon_district` VALUES ('948', '93', ' Xuân Trường');
INSERT INTO `gaucon_district` VALUES ('949', '93', ' Giao Thủy');
INSERT INTO `gaucon_district` VALUES ('950', '93', ' ý Yên');
INSERT INTO `gaucon_district` VALUES ('951', '93', ' Vụ Bản');
INSERT INTO `gaucon_district` VALUES ('952', '93', ' Nam Trực');
INSERT INTO `gaucon_district` VALUES ('953', '93', ' Trực Ninh');
INSERT INTO `gaucon_district` VALUES ('954', '93', ' Nghĩa Hưng');
INSERT INTO `gaucon_district` VALUES ('955', '93', ' Hải Hậu');
INSERT INTO `gaucon_district` VALUES ('956', '94', 'Thành phố Thái Bình');
INSERT INTO `gaucon_district` VALUES ('957', '94', ' Quỳnh Phụ');
INSERT INTO `gaucon_district` VALUES ('958', '94', ' Hưng Hà');
INSERT INTO `gaucon_district` VALUES ('959', '94', ' Đông Hưng');
INSERT INTO `gaucon_district` VALUES ('960', '94', ' Vũ Thư');
INSERT INTO `gaucon_district` VALUES ('961', '94', ' Kiến Xương');
INSERT INTO `gaucon_district` VALUES ('962', '94', ' Tiền Hải');
INSERT INTO `gaucon_district` VALUES ('963', '94', ' Thái Thuỵ');
INSERT INTO `gaucon_district` VALUES ('964', '95', 'Thành phố Ninh Bình');
INSERT INTO `gaucon_district` VALUES ('965', '95', 'Thị xã Tam Điệp');
INSERT INTO `gaucon_district` VALUES ('966', '95', ' Nho Quan');
INSERT INTO `gaucon_district` VALUES ('967', '95', ' Gia Viễn');
INSERT INTO `gaucon_district` VALUES ('968', '95', ' Hoa Lư');
INSERT INTO `gaucon_district` VALUES ('969', '95', ' Yên Mô');
INSERT INTO `gaucon_district` VALUES ('970', '95', ' Kim Sơn');
INSERT INTO `gaucon_district` VALUES ('971', '95', ' Yên Khánh');
INSERT INTO `gaucon_district` VALUES ('972', '96', 'Thành phố Thanh Hoá');
INSERT INTO `gaucon_district` VALUES ('973', '96', 'Thị xã Bỉm Sơn');
INSERT INTO `gaucon_district` VALUES ('974', '96', 'Thị xã Sầm Sơn');
INSERT INTO `gaucon_district` VALUES ('975', '96', ' Quan Hoá');
INSERT INTO `gaucon_district` VALUES ('976', '96', ' Quan Sơn');
INSERT INTO `gaucon_district` VALUES ('977', '96', ' Mường Lát');
INSERT INTO `gaucon_district` VALUES ('978', '96', ' Bá Thước');
INSERT INTO `gaucon_district` VALUES ('979', '96', ' Thường Xuân');
INSERT INTO `gaucon_district` VALUES ('980', '96', ' Như Xuân');
INSERT INTO `gaucon_district` VALUES ('981', '96', ' Như Thanh');
INSERT INTO `gaucon_district` VALUES ('982', '96', ' Lang Chánh');
INSERT INTO `gaucon_district` VALUES ('983', '96', ' Ngọc Lặc');
INSERT INTO `gaucon_district` VALUES ('984', '96', ' Thạch Thành');
INSERT INTO `gaucon_district` VALUES ('985', '96', ' Cẩm Thủy');
INSERT INTO `gaucon_district` VALUES ('986', '96', ' Thọ Xuân');
INSERT INTO `gaucon_district` VALUES ('987', '96', ' Vĩnh Lộc');
INSERT INTO `gaucon_district` VALUES ('988', '96', ' Thiệu Hoá');
INSERT INTO `gaucon_district` VALUES ('989', '96', ' Triệu Sơn');
INSERT INTO `gaucon_district` VALUES ('990', '96', ' Nông Cống');
INSERT INTO `gaucon_district` VALUES ('991', '96', ' Đông Sơn');
INSERT INTO `gaucon_district` VALUES ('992', '96', ' Hà Trung');
INSERT INTO `gaucon_district` VALUES ('993', '96', ' Hoằng Hoá');
INSERT INTO `gaucon_district` VALUES ('994', '96', ' Nga Sơn');
INSERT INTO `gaucon_district` VALUES ('995', '96', ' Hậu Lộc');
INSERT INTO `gaucon_district` VALUES ('996', '96', ' Quảng Xương');
INSERT INTO `gaucon_district` VALUES ('997', '96', ' Tĩnh Gia');
INSERT INTO `gaucon_district` VALUES ('998', '96', ' Yên Định');
INSERT INTO `gaucon_district` VALUES ('999', '97', 'Thành phố Vinh');
INSERT INTO `gaucon_district` VALUES ('1000', '97', 'Thị xã Cửa Lò');
INSERT INTO `gaucon_district` VALUES ('1001', '97', ' Quỳ Châu');
INSERT INTO `gaucon_district` VALUES ('1002', '97', ' Quỳ Hợp');
INSERT INTO `gaucon_district` VALUES ('1003', '97', ' Nghĩa Đàn');
INSERT INTO `gaucon_district` VALUES ('1004', '97', ' Quỳnh Lưu');
INSERT INTO `gaucon_district` VALUES ('1005', '97', ' Kỳ Sơn');
INSERT INTO `gaucon_district` VALUES ('1006', '97', ' Tương Dương');
INSERT INTO `gaucon_district` VALUES ('1007', '97', ' Con Cuông');
INSERT INTO `gaucon_district` VALUES ('1008', '97', ' Tân Kỳ');
INSERT INTO `gaucon_district` VALUES ('1009', '97', ' Yên Thành');
INSERT INTO `gaucon_district` VALUES ('1010', '97', ' Diễn Châu');
INSERT INTO `gaucon_district` VALUES ('1011', '97', ' Anh Sơn');
INSERT INTO `gaucon_district` VALUES ('1012', '97', ' Đô Lương');
INSERT INTO `gaucon_district` VALUES ('1013', '97', ' Thanh Chương');
INSERT INTO `gaucon_district` VALUES ('1014', '97', ' Nghi Lộc');
INSERT INTO `gaucon_district` VALUES ('1015', '97', ' Nam Đàn');
INSERT INTO `gaucon_district` VALUES ('1016', '97', ' Hưng Nguyên');
INSERT INTO `gaucon_district` VALUES ('1017', '97', ' Quế Phong');
INSERT INTO `gaucon_district` VALUES ('1018', '98', 'Thành phố Hà Tĩnh');
INSERT INTO `gaucon_district` VALUES ('1019', '98', 'Thị xã Hồng Lĩnh');
INSERT INTO `gaucon_district` VALUES ('1020', '98', ' Hương Sơn');
INSERT INTO `gaucon_district` VALUES ('1021', '98', ' Đức Thọ');
INSERT INTO `gaucon_district` VALUES ('1022', '98', ' Nghi Xuân');
INSERT INTO `gaucon_district` VALUES ('1023', '98', ' Can Lộc');
INSERT INTO `gaucon_district` VALUES ('1024', '98', ' Hương Khê');
INSERT INTO `gaucon_district` VALUES ('1025', '98', ' Thạch Hà');
INSERT INTO `gaucon_district` VALUES ('1026', '98', ' Cẩm Xuyên');
INSERT INTO `gaucon_district` VALUES ('1027', '98', ' Kỳ Anh');
INSERT INTO `gaucon_district` VALUES ('1028', '98', ' Vũ Quang');
INSERT INTO `gaucon_district` VALUES ('1029', '98', ' Lộc Hà');
INSERT INTO `gaucon_district` VALUES ('1030', '99', 'Thành phố Đồng Hới');
INSERT INTO `gaucon_district` VALUES ('1031', '99', ' Tuyên Hoá');
INSERT INTO `gaucon_district` VALUES ('1032', '99', ' Minh Hoá');
INSERT INTO `gaucon_district` VALUES ('1033', '99', ' Quảng Trạch');
INSERT INTO `gaucon_district` VALUES ('1034', '99', ' Bố Trạch');
INSERT INTO `gaucon_district` VALUES ('1035', '99', ' Quảng Ninh');
INSERT INTO `gaucon_district` VALUES ('1036', '99', ' Lệ Thuỷ');
INSERT INTO `gaucon_district` VALUES ('1037', '100', 'Thị xã Đông Hà');
INSERT INTO `gaucon_district` VALUES ('1038', '100', 'Thị xã Quảng Trị');
INSERT INTO `gaucon_district` VALUES ('1039', '100', ' Vĩnh Linh');
INSERT INTO `gaucon_district` VALUES ('1040', '100', ' Gio Linh');
INSERT INTO `gaucon_district` VALUES ('1041', '100', ' Cam Lộ');
INSERT INTO `gaucon_district` VALUES ('1042', '100', ' Triệu Phong');
INSERT INTO `gaucon_district` VALUES ('1043', '100', ' Hải Lăng');
INSERT INTO `gaucon_district` VALUES ('1044', '100', ' Hướng Hoá');
INSERT INTO `gaucon_district` VALUES ('1045', '100', ' Đăk Rông');
INSERT INTO `gaucon_district` VALUES ('1046', '100', ' đảo Cồn cỏ');
INSERT INTO `gaucon_district` VALUES ('1047', '101', 'TP. Huế');
INSERT INTO `gaucon_district` VALUES ('1048', '101', ' Phong Điền');
INSERT INTO `gaucon_district` VALUES ('1049', '101', ' Quảng Điền');
INSERT INTO `gaucon_district` VALUES ('1050', '101', ' Hương Trà');
INSERT INTO `gaucon_district` VALUES ('1051', '101', ' Phú Vang');
INSERT INTO `gaucon_district` VALUES ('1052', '101', ' Hương Thuỷ');
INSERT INTO `gaucon_district` VALUES ('1053', '101', ' Phú Lộc');
INSERT INTO `gaucon_district` VALUES ('1054', '101', ' Nam Đông');
INSERT INTO `gaucon_district` VALUES ('1055', '101', ' A Lưới');
INSERT INTO `gaucon_district` VALUES ('1056', '102', 'Thành phố Tam Kỳ');
INSERT INTO `gaucon_district` VALUES ('1057', '102', 'Thị xã Hội An');
INSERT INTO `gaucon_district` VALUES ('1058', '102', ' Duy Xuyên');
INSERT INTO `gaucon_district` VALUES ('1059', '102', ' Điện Bàn');
INSERT INTO `gaucon_district` VALUES ('1060', '102', ' Đại Lộc');
INSERT INTO `gaucon_district` VALUES ('1061', '102', ' Quế Sơn');
INSERT INTO `gaucon_district` VALUES ('1062', '102', ' Hiệp Đức');
INSERT INTO `gaucon_district` VALUES ('1063', '102', ' Thăng Bình');
INSERT INTO `gaucon_district` VALUES ('1064', '102', ' Núi Thành');
INSERT INTO `gaucon_district` VALUES ('1065', '102', ' Tiên Phước');
INSERT INTO `gaucon_district` VALUES ('1066', '102', ' Bắc Trà My');
INSERT INTO `gaucon_district` VALUES ('1067', '102', ' Đông Giang');
INSERT INTO `gaucon_district` VALUES ('1068', '102', ' Nam Giang');
INSERT INTO `gaucon_district` VALUES ('1069', '102', ' Phước Sơn');
INSERT INTO `gaucon_district` VALUES ('1070', '102', ' Nam Trà My');
INSERT INTO `gaucon_district` VALUES ('1071', '102', ' Tây Giang');
INSERT INTO `gaucon_district` VALUES ('1072', '102', ' Phú Ninh');
INSERT INTO `gaucon_district` VALUES ('1073', '103', 'Thành phố Quảng Ngãi');
INSERT INTO `gaucon_district` VALUES ('1074', '103', ' Lý Sơn');
INSERT INTO `gaucon_district` VALUES ('1075', '103', ' Bình Sơn');
INSERT INTO `gaucon_district` VALUES ('1076', '103', ' Trà Bồng');
INSERT INTO `gaucon_district` VALUES ('1077', '103', ' Sơn Tịnh');
INSERT INTO `gaucon_district` VALUES ('1078', '103', ' Sơn Hà');
INSERT INTO `gaucon_district` VALUES ('1079', '103', ' Tư Nghĩa');
INSERT INTO `gaucon_district` VALUES ('1080', '103', ' Nghĩa Hành');
INSERT INTO `gaucon_district` VALUES ('1081', '103', ' Minh Long');
INSERT INTO `gaucon_district` VALUES ('1082', '103', ' Mộ Đức');
INSERT INTO `gaucon_district` VALUES ('1083', '103', ' Đức Phổ');
INSERT INTO `gaucon_district` VALUES ('1084', '103', ' Ba Tơ');
INSERT INTO `gaucon_district` VALUES ('1085', '103', ' Sơn Tây');
INSERT INTO `gaucon_district` VALUES ('1086', '103', ' Tây Trà');
INSERT INTO `gaucon_district` VALUES ('1087', '104', 'Thị xã KonTum');
INSERT INTO `gaucon_district` VALUES ('1088', '104', ' Đăk Glei');
INSERT INTO `gaucon_district` VALUES ('1089', '104', ' Ngọc Hồi');
INSERT INTO `gaucon_district` VALUES ('1090', '104', ' Đăk Tô');
INSERT INTO `gaucon_district` VALUES ('1091', '104', ' Sa Thầy');
INSERT INTO `gaucon_district` VALUES ('1092', '104', ' Kon Plong');
INSERT INTO `gaucon_district` VALUES ('1093', '104', ' Đăk Hà');
INSERT INTO `gaucon_district` VALUES ('1094', '104', ' Kon Rộy');
INSERT INTO `gaucon_district` VALUES ('1095', '104', ' Tu Mơ Rông');
INSERT INTO `gaucon_district` VALUES ('1096', '105', 'Thành phố Quy Nhơn');
INSERT INTO `gaucon_district` VALUES ('1097', '105', ' An Lão');
INSERT INTO `gaucon_district` VALUES ('1098', '105', ' Hoài Ân');
INSERT INTO `gaucon_district` VALUES ('1099', '105', ' Hoài Nhơn');
INSERT INTO `gaucon_district` VALUES ('1100', '105', ' Phù Mỹ');
INSERT INTO `gaucon_district` VALUES ('1101', '105', ' Phù Cát');
INSERT INTO `gaucon_district` VALUES ('1102', '105', ' Vĩnh Thạnh');
INSERT INTO `gaucon_district` VALUES ('1103', '105', ' Tây Sơn');
INSERT INTO `gaucon_district` VALUES ('1104', '105', ' Vân Canh');
INSERT INTO `gaucon_district` VALUES ('1105', '105', ' An Nhơn');
INSERT INTO `gaucon_district` VALUES ('1106', '105', ' Tuy Phước');
INSERT INTO `gaucon_district` VALUES ('1107', '106', 'Thành phố Pleiku');
INSERT INTO `gaucon_district` VALUES ('1108', '106', ' Chư Păh');
INSERT INTO `gaucon_district` VALUES ('1109', '106', ' Mang Yang');
INSERT INTO `gaucon_district` VALUES ('1110', '106', ' Kbang');
INSERT INTO `gaucon_district` VALUES ('1111', '106', 'Thị xã An Khê');
INSERT INTO `gaucon_district` VALUES ('1112', '106', ' Kông Chro');
INSERT INTO `gaucon_district` VALUES ('1113', '106', ' Đức Cơ');
INSERT INTO `gaucon_district` VALUES ('1114', '106', ' Chưprông');
INSERT INTO `gaucon_district` VALUES ('1115', '106', ' Chư Sê');
INSERT INTO `gaucon_district` VALUES ('1116', '106', ' Ayunpa');
INSERT INTO `gaucon_district` VALUES ('1117', '106', ' Krông Pa');
INSERT INTO `gaucon_district` VALUES ('1118', '106', ' Ia Grai');
INSERT INTO `gaucon_district` VALUES ('1119', '106', ' Đăk Đoa');
INSERT INTO `gaucon_district` VALUES ('1120', '106', ' Ia Pa');
INSERT INTO `gaucon_district` VALUES ('1121', '106', ' Đăk Pơ');
INSERT INTO `gaucon_district` VALUES ('1122', '106', ' Phú Thiện');
INSERT INTO `gaucon_district` VALUES ('1123', '107', 'Thị xã Tuy Hoà');
INSERT INTO `gaucon_district` VALUES ('1124', '107', ' Đồng Xuân');
INSERT INTO `gaucon_district` VALUES ('1125', '107', ' Sông Cầu');
INSERT INTO `gaucon_district` VALUES ('1126', '107', ' Tuy An');
INSERT INTO `gaucon_district` VALUES ('1127', '107', ' Sơn Hoà');
INSERT INTO `gaucon_district` VALUES ('1128', '107', ' Sông Hinh');
INSERT INTO `gaucon_district` VALUES ('1129', '107', ' Đông Hoà');
INSERT INTO `gaucon_district` VALUES ('1130', '107', ' Phú Hoà');
INSERT INTO `gaucon_district` VALUES ('1131', '107', ' Tây Hoà');
INSERT INTO `gaucon_district` VALUES ('1132', '108', 'Thành phố Buôn Ma Thuột');
INSERT INTO `gaucon_district` VALUES ('1133', '108', ' Ea H Leo');
INSERT INTO `gaucon_district` VALUES ('1134', '108', ' Krông Buk');
INSERT INTO `gaucon_district` VALUES ('1135', '108', ' Krông Năng');
INSERT INTO `gaucon_district` VALUES ('1136', '108', ' Ea Súp');
INSERT INTO `gaucon_district` VALUES ('1137', '108', ' Cư M gar');
INSERT INTO `gaucon_district` VALUES ('1138', '108', ' Krông Pắc');
INSERT INTO `gaucon_district` VALUES ('1139', '108', ' Ea Kar');
INSERT INTO `gaucon_district` VALUES ('1140', '108', ' M`Đrăk');
INSERT INTO `gaucon_district` VALUES ('1141', '108', ' Krông Ana');
INSERT INTO `gaucon_district` VALUES ('1142', '108', ' Krông Bông');
INSERT INTO `gaucon_district` VALUES ('1143', '108', ' Lăk');
INSERT INTO `gaucon_district` VALUES ('1144', '108', ' Buôn Đôn');
INSERT INTO `gaucon_district` VALUES ('1145', '108', ' Cư Kuin');
INSERT INTO `gaucon_district` VALUES ('1146', '109', 'Thành phố Nha Trang');
INSERT INTO `gaucon_district` VALUES ('1147', '109', ' Vạn Ninh');
INSERT INTO `gaucon_district` VALUES ('1148', '109', ' Ninh Hoà');
INSERT INTO `gaucon_district` VALUES ('1149', '109', ' Diên Khánh');
INSERT INTO `gaucon_district` VALUES ('1150', '109', ' Khánh Vĩnh');
INSERT INTO `gaucon_district` VALUES ('1151', '109', 'Thị xã Cam Ranh');
INSERT INTO `gaucon_district` VALUES ('1152', '109', ' Khánh Sơn');
INSERT INTO `gaucon_district` VALUES ('1153', '109', ' Trường Sa');
INSERT INTO `gaucon_district` VALUES ('1154', '109', ' Cam Lâm');
INSERT INTO `gaucon_district` VALUES ('1155', '110', 'Thành phố Đà Lạt');
INSERT INTO `gaucon_district` VALUES ('1156', '110', 'Thị xã. Bảo Lộc');
INSERT INTO `gaucon_district` VALUES ('1157', '110', ' Đức Trọng');
INSERT INTO `gaucon_district` VALUES ('1158', '110', ' Di Linh');
INSERT INTO `gaucon_district` VALUES ('1159', '110', ' Đơn Dương');
INSERT INTO `gaucon_district` VALUES ('1160', '110', ' Lạc Dương');
INSERT INTO `gaucon_district` VALUES ('1161', '110', ' Đạ Huoai');
INSERT INTO `gaucon_district` VALUES ('1162', '110', ' Đạ Tẻh');
INSERT INTO `gaucon_district` VALUES ('1163', '110', ' Cát Tiên');
INSERT INTO `gaucon_district` VALUES ('1164', '110', ' Lâm Hà');
INSERT INTO `gaucon_district` VALUES ('1165', '110', ' Bảo Lâm');
INSERT INTO `gaucon_district` VALUES ('1166', '110', ' Đam Rông');
INSERT INTO `gaucon_district` VALUES ('1167', '111', 'Thị xã Đồng Xoài');
INSERT INTO `gaucon_district` VALUES ('1168', '111', ' Đồng Phú');
INSERT INTO `gaucon_district` VALUES ('1169', '111', ' Chơn Thành');
INSERT INTO `gaucon_district` VALUES ('1170', '111', ' Bình Long');
INSERT INTO `gaucon_district` VALUES ('1171', '111', ' Lộc Ninh');
INSERT INTO `gaucon_district` VALUES ('1172', '111', ' Bù Đốp');
INSERT INTO `gaucon_district` VALUES ('1173', '111', ' Phước Long');
INSERT INTO `gaucon_district` VALUES ('1174', '111', ' Bù Đăng');
INSERT INTO `gaucon_district` VALUES ('1175', '112', 'Thị xã Thủ Dầu Một');
INSERT INTO `gaucon_district` VALUES ('1176', '112', ' Bến Cát');
INSERT INTO `gaucon_district` VALUES ('1177', '112', ' Tân Uyên');
INSERT INTO `gaucon_district` VALUES ('1178', '112', ' Thuận An');
INSERT INTO `gaucon_district` VALUES ('1179', '112', ' Dĩ An');
INSERT INTO `gaucon_district` VALUES ('1180', '112', ' Phú Giáo');
INSERT INTO `gaucon_district` VALUES ('1181', '112', ' Dầu Tiếng');
INSERT INTO `gaucon_district` VALUES ('1182', '113', 'Thành phố Phan Rang-Tháp Chàm');
INSERT INTO `gaucon_district` VALUES ('1183', '113', ' Ninh Sơn');
INSERT INTO `gaucon_district` VALUES ('1184', '113', ' Ninh Phước');
INSERT INTO `gaucon_district` VALUES ('1185', '113', ' Bác Ái');
INSERT INTO `gaucon_district` VALUES ('1186', '113', ' Thuận Bắc');
INSERT INTO `gaucon_district` VALUES ('1187', '113', ' Ninh Hải');
INSERT INTO `gaucon_district` VALUES ('1188', '114', 'Thị xã Tây Ninh');
INSERT INTO `gaucon_district` VALUES ('1189', '114', ' Tân Biên');
INSERT INTO `gaucon_district` VALUES ('1190', '114', ' Tân Châu');
INSERT INTO `gaucon_district` VALUES ('1191', '114', ' Dương Minh Châu');
INSERT INTO `gaucon_district` VALUES ('1192', '114', ' Châu Thành');
INSERT INTO `gaucon_district` VALUES ('1193', '114', ' Hoà Thành');
INSERT INTO `gaucon_district` VALUES ('1194', '114', ' Bến Cầu');
INSERT INTO `gaucon_district` VALUES ('1195', '114', ' Gò Dầu');
INSERT INTO `gaucon_district` VALUES ('1196', '114', ' Trảng Bàng');
INSERT INTO `gaucon_district` VALUES ('1197', '115', 'Thành phố Phan Thiết');
INSERT INTO `gaucon_district` VALUES ('1198', '115', ' Tuy Phong');
INSERT INTO `gaucon_district` VALUES ('1199', '115', ' Bắc Bình');
INSERT INTO `gaucon_district` VALUES ('1200', '115', ' Hàm Thuận Bắc');
INSERT INTO `gaucon_district` VALUES ('1201', '115', ' Hàm Thuận Nam');
INSERT INTO `gaucon_district` VALUES ('1202', '115', ' Hàm Tân');
INSERT INTO `gaucon_district` VALUES ('1203', '115', ' Đức Linh');
INSERT INTO `gaucon_district` VALUES ('1204', '115', ' Tánh Linh');
INSERT INTO `gaucon_district` VALUES ('1205', '115', ' đảo Phú Quý');
INSERT INTO `gaucon_district` VALUES ('1206', '115', 'Thị xã LaGi');
INSERT INTO `gaucon_district` VALUES ('1207', '116', 'Thành phố Biên Hoà');
INSERT INTO `gaucon_district` VALUES ('1208', '116', ' Vĩnh Cửu');
INSERT INTO `gaucon_district` VALUES ('1209', '116', ' Tân Phú');
INSERT INTO `gaucon_district` VALUES ('1210', '116', ' Định Quán');
INSERT INTO `gaucon_district` VALUES ('1211', '116', ' Thống Nhất');
INSERT INTO `gaucon_district` VALUES ('1212', '116', 'Thị xã Long Khánh');
INSERT INTO `gaucon_district` VALUES ('1213', '116', ' Xuân Lộc');
INSERT INTO `gaucon_district` VALUES ('1214', '116', ' Long Thành');
INSERT INTO `gaucon_district` VALUES ('1215', '116', ' Nhơn Trạch');
INSERT INTO `gaucon_district` VALUES ('1216', '116', ' Trảng Bom');
INSERT INTO `gaucon_district` VALUES ('1217', '116', ' Cẩm Mỹ');
INSERT INTO `gaucon_district` VALUES ('1218', '117', 'Thị xã Tân An');
INSERT INTO `gaucon_district` VALUES ('1219', '117', ' Vĩnh Hưng');
INSERT INTO `gaucon_district` VALUES ('1220', '117', ' Mộc Hoá');
INSERT INTO `gaucon_district` VALUES ('1221', '117', ' Tân Thạnh');
INSERT INTO `gaucon_district` VALUES ('1222', '117', ' Thạnh Hoá');
INSERT INTO `gaucon_district` VALUES ('1223', '117', ' Đức Huệ');
INSERT INTO `gaucon_district` VALUES ('1224', '117', ' Đức Hoà');
INSERT INTO `gaucon_district` VALUES ('1225', '117', ' Bến Lức');
INSERT INTO `gaucon_district` VALUES ('1226', '117', ' Thủ Thừa');
INSERT INTO `gaucon_district` VALUES ('1227', '117', ' Châu Thành');
INSERT INTO `gaucon_district` VALUES ('1228', '117', ' Tân Trụ');
INSERT INTO `gaucon_district` VALUES ('1229', '117', ' Cần Đước');
INSERT INTO `gaucon_district` VALUES ('1230', '117', ' Cần Giuộc');
INSERT INTO `gaucon_district` VALUES ('1231', '117', ' Tân Hưng');
INSERT INTO `gaucon_district` VALUES ('1232', '118', 'Thành phố Cao Lãnh');
INSERT INTO `gaucon_district` VALUES ('1233', '118', 'Thị xã Sa Đéc');
INSERT INTO `gaucon_district` VALUES ('1234', '118', ' Tân Hồng');
INSERT INTO `gaucon_district` VALUES ('1235', '118', ' Hồng Ngự');
INSERT INTO `gaucon_district` VALUES ('1236', '118', ' Tam Nông');
INSERT INTO `gaucon_district` VALUES ('1237', '118', ' Thanh Bình');
INSERT INTO `gaucon_district` VALUES ('1238', '118', ' Cao Lãnh');
INSERT INTO `gaucon_district` VALUES ('1239', '118', ' Lấp Vò');
INSERT INTO `gaucon_district` VALUES ('1240', '118', ' Tháp Mười');
INSERT INTO `gaucon_district` VALUES ('1241', '118', ' Lai Vung');
INSERT INTO `gaucon_district` VALUES ('1242', '118', ' Châu Thành');
INSERT INTO `gaucon_district` VALUES ('1243', '119', 'Thành phố Long Xuyên');
INSERT INTO `gaucon_district` VALUES ('1245', '119', ' An Phú');
INSERT INTO `gaucon_district` VALUES ('1246', '119', ' Tân Châu');
INSERT INTO `gaucon_district` VALUES ('1247', '119', ' Phú Tân');
INSERT INTO `gaucon_district` VALUES ('1248', '119', ' Tịnh Biên');
INSERT INTO `gaucon_district` VALUES ('1249', '119', ' Tri Tôn');
INSERT INTO `gaucon_district` VALUES ('1250', '119', ' Châu Phú');
INSERT INTO `gaucon_district` VALUES ('1251', '119', ' Chợ Mới');
INSERT INTO `gaucon_district` VALUES ('1252', '119', ' Châu Thành');
INSERT INTO `gaucon_district` VALUES ('1253', '119', ' Thoại Sơn');
INSERT INTO `gaucon_district` VALUES ('1254', '120', 'Thành phố Vũng Tàu');
INSERT INTO `gaucon_district` VALUES ('1255', '120', 'Thị xã Bà Rịa');
INSERT INTO `gaucon_district` VALUES ('1256', '120', ' Xuyên Mộc');
INSERT INTO `gaucon_district` VALUES ('1257', '120', ' Long Điền');
INSERT INTO `gaucon_district` VALUES ('1258', '120', ' Côn Đảo');
INSERT INTO `gaucon_district` VALUES ('1259', '120', ' Tân Thành');
INSERT INTO `gaucon_district` VALUES ('1260', '120', ' Châu Đức');
INSERT INTO `gaucon_district` VALUES ('1261', '120', ' Đất Đỏ');
INSERT INTO `gaucon_district` VALUES ('1262', '121', 'Thành phố Mỹ Tho');
INSERT INTO `gaucon_district` VALUES ('1263', '121', 'Thị xã Gò Công');
INSERT INTO `gaucon_district` VALUES ('1264', '121', ' Cái Bè');
INSERT INTO `gaucon_district` VALUES ('1265', '121', ' Cai Lậy');
INSERT INTO `gaucon_district` VALUES ('1266', '121', ' Châu Thành');
INSERT INTO `gaucon_district` VALUES ('1267', '121', ' Chợ Gạo');
INSERT INTO `gaucon_district` VALUES ('1268', '121', ' Gò Công Tây');
INSERT INTO `gaucon_district` VALUES ('1269', '121', ' Gò Công Đông');
INSERT INTO `gaucon_district` VALUES ('1270', '121', ' Tân Phước');
INSERT INTO `gaucon_district` VALUES ('1271', '122', 'Thành phố Rạch Giá');
INSERT INTO `gaucon_district` VALUES ('1272', '122', 'Thị xã Hà Tiên');
INSERT INTO `gaucon_district` VALUES ('1273', '122', ' Kiên Lương');
INSERT INTO `gaucon_district` VALUES ('1274', '122', ' Hòn Đất');
INSERT INTO `gaucon_district` VALUES ('1275', '122', ' Tân Hiệp');
INSERT INTO `gaucon_district` VALUES ('1276', '122', ' Châu Thành');
INSERT INTO `gaucon_district` VALUES ('1277', '122', ' Giồng Riềng');
INSERT INTO `gaucon_district` VALUES ('1278', '122', ' Gò Quao');
INSERT INTO `gaucon_district` VALUES ('1279', '122', ' An Biên');
INSERT INTO `gaucon_district` VALUES ('1280', '122', ' An Minh');
INSERT INTO `gaucon_district` VALUES ('1281', '122', ' Vĩnh Thuận');
INSERT INTO `gaucon_district` VALUES ('1282', '122', ' Phú Quốc');
INSERT INTO `gaucon_district` VALUES ('1283', '122', ' Kiên Hải');
INSERT INTO `gaucon_district` VALUES ('1284', '122', ' U minh Thượng');
INSERT INTO `gaucon_district` VALUES ('1285', '123', 'Quận Ninh Kiều');
INSERT INTO `gaucon_district` VALUES ('1286', '123', 'Quận Bình Thuỷ');
INSERT INTO `gaucon_district` VALUES ('1287', '123', 'Quận Cái Răng');
INSERT INTO `gaucon_district` VALUES ('1288', '123', 'Quận Ô Môn');
INSERT INTO `gaucon_district` VALUES ('1289', '123', ' Phong Điền');
INSERT INTO `gaucon_district` VALUES ('1290', '123', ' Cờ Đỏ');
INSERT INTO `gaucon_district` VALUES ('1291', '123', ' Vĩnh Thạnh');
INSERT INTO `gaucon_district` VALUES ('1292', '123', ' Thốt Nốt');
INSERT INTO `gaucon_district` VALUES ('1293', '124', 'Thị xã Bến Tre');
INSERT INTO `gaucon_district` VALUES ('1294', '124', ' Châu Thành');
INSERT INTO `gaucon_district` VALUES ('1295', '124', ' Chợ Lách');
INSERT INTO `gaucon_district` VALUES ('1296', '124', ' Mỏ Cày');
INSERT INTO `gaucon_district` VALUES ('1297', '124', ' Giồng Trôm');
INSERT INTO `gaucon_district` VALUES ('1298', '124', ' Bình Đại');
INSERT INTO `gaucon_district` VALUES ('1299', '124', ' Ba Tri');
INSERT INTO `gaucon_district` VALUES ('1300', '124', ' Thạnh Phú');
INSERT INTO `gaucon_district` VALUES ('1301', '125', 'Thị xã Vĩnh Long');
INSERT INTO `gaucon_district` VALUES ('1302', '125', ' Long Hồ');
INSERT INTO `gaucon_district` VALUES ('1303', '125', ' Mang Thít');
INSERT INTO `gaucon_district` VALUES ('1304', '125', ' Bình Minh');
INSERT INTO `gaucon_district` VALUES ('1305', '125', ' Tam Bình');
INSERT INTO `gaucon_district` VALUES ('1306', '125', ' Trà Ôn');
INSERT INTO `gaucon_district` VALUES ('1307', '125', ' Vũng Liêm');
INSERT INTO `gaucon_district` VALUES ('1308', '125', ' Bình Tân');
INSERT INTO `gaucon_district` VALUES ('1309', '126', 'Thị xã Trà Vinh');
INSERT INTO `gaucon_district` VALUES ('1310', '126', ' Càng Long');
INSERT INTO `gaucon_district` VALUES ('1311', '126', ' Cầu Kè');
INSERT INTO `gaucon_district` VALUES ('1312', '126', ' Tiểu Cần');
INSERT INTO `gaucon_district` VALUES ('1313', '126', ' Châu Thành');
INSERT INTO `gaucon_district` VALUES ('1314', '126', ' Trà Cú');
INSERT INTO `gaucon_district` VALUES ('1315', '126', ' Cầu Ngang');
INSERT INTO `gaucon_district` VALUES ('1316', '126', ' Duyên Hải');
INSERT INTO `gaucon_district` VALUES ('1317', '127', 'Thành phố Sóc Trăng');
INSERT INTO `gaucon_district` VALUES ('1318', '127', ' Kế Sách');
INSERT INTO `gaucon_district` VALUES ('1319', '127', ' Mỹ Tú');
INSERT INTO `gaucon_district` VALUES ('1320', '127', ' Mỹ Xuyên');
INSERT INTO `gaucon_district` VALUES ('1321', '127', ' Thạnh Trị');
INSERT INTO `gaucon_district` VALUES ('1322', '127', ' Long Phú');
INSERT INTO `gaucon_district` VALUES ('1323', '127', ' Vĩnh Châu');
INSERT INTO `gaucon_district` VALUES ('1324', '127', ' Cù Lao Dung');
INSERT INTO `gaucon_district` VALUES ('1325', '127', ' Ngã Năm');
INSERT INTO `gaucon_district` VALUES ('1326', '127', ' Châu Thành');
INSERT INTO `gaucon_district` VALUES ('1327', '127', ' Trần Đề');
INSERT INTO `gaucon_district` VALUES ('1328', '128', 'Thị xã Bạc Liêu');
INSERT INTO `gaucon_district` VALUES ('1329', '128', ' Vĩnh Lợi');
INSERT INTO `gaucon_district` VALUES ('1330', '128', ' Hồng Dân');
INSERT INTO `gaucon_district` VALUES ('1331', '128', ' Giá Rai');
INSERT INTO `gaucon_district` VALUES ('1332', '128', ' Phước Long');
INSERT INTO `gaucon_district` VALUES ('1333', '128', ' Đông Hải');
INSERT INTO `gaucon_district` VALUES ('1334', '128', ' Hoà Bình');
INSERT INTO `gaucon_district` VALUES ('1335', '129', 'Thành phố Cà Mau');
INSERT INTO `gaucon_district` VALUES ('1336', '129', ' Thới Bình');
INSERT INTO `gaucon_district` VALUES ('1337', '129', ' U Minh');
INSERT INTO `gaucon_district` VALUES ('1338', '129', ' Trần Văn Thời');
INSERT INTO `gaucon_district` VALUES ('1339', '129', ' Cái Nước');
INSERT INTO `gaucon_district` VALUES ('1340', '129', ' Đầm Dơi');
INSERT INTO `gaucon_district` VALUES ('1341', '129', ' Ngọc Hiển');
INSERT INTO `gaucon_district` VALUES ('1342', '129', ' Năm Căn');
INSERT INTO `gaucon_district` VALUES ('1343', '129', ' Phú Tân');
INSERT INTO `gaucon_district` VALUES ('1344', '130', 'TP. Điện Biên Phủ');
INSERT INTO `gaucon_district` VALUES ('1345', '130', 'Thị xã Mường Lay');
INSERT INTO `gaucon_district` VALUES ('1346', '130', ' Điện Biên');
INSERT INTO `gaucon_district` VALUES ('1347', '130', ' Tuần Giáo');
INSERT INTO `gaucon_district` VALUES ('1348', '130', ' Mường Chà');
INSERT INTO `gaucon_district` VALUES ('1349', '130', ' Tủa Chùa');
INSERT INTO `gaucon_district` VALUES ('1350', '130', ' Điện Biên Đông');
INSERT INTO `gaucon_district` VALUES ('1351', '130', ' Mường Nhé');
INSERT INTO `gaucon_district` VALUES ('1352', '130', ' Mường Ảng');
INSERT INTO `gaucon_district` VALUES ('1353', '131', 'Thị xã Gia Nghĩa');
INSERT INTO `gaucon_district` VALUES ('1354', '131', ' Dăk RLấp');
INSERT INTO `gaucon_district` VALUES ('1355', '131', ' Dăk Mil');
INSERT INTO `gaucon_district` VALUES ('1356', '131', ' Cư Jút');
INSERT INTO `gaucon_district` VALUES ('1357', '131', ' Dăk Song');
INSERT INTO `gaucon_district` VALUES ('1358', '131', ' Krông Nô');
INSERT INTO `gaucon_district` VALUES ('1359', '131', ' Dăk GLong');
INSERT INTO `gaucon_district` VALUES ('1360', '131', ' Tuy Đức');
INSERT INTO `gaucon_district` VALUES ('1361', '132', 'Thành phố Vị Thanh');
INSERT INTO `gaucon_district` VALUES ('1362', '132', ' Vị Thuỷ');
INSERT INTO `gaucon_district` VALUES ('1363', '132', ' Long Mỹ');
INSERT INTO `gaucon_district` VALUES ('1364', '132', ' Phụng Hiệp');
INSERT INTO `gaucon_district` VALUES ('1365', '132', ' Châu Thành');
INSERT INTO `gaucon_district` VALUES ('1366', '132', ' Châu Thành A');
INSERT INTO `gaucon_district` VALUES ('1367', '132', 'Thị xã Ngã Bảy');
INSERT INTO `gaucon_district` VALUES ('1373', '87', 'Quận Ba Đình');

-- ----------------------------
-- Table structure for `gaucon_history`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_history`;
CREATE TABLE `gaucon_history` (
  `id` int(11) NOT NULL auto_increment,
  `year` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `ordering` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_history
-- ----------------------------
INSERT INTO `gaucon_history` VALUES ('1', '1964', '<p>\r\n	Th&agrave;nh lập Tập đo&agrave;n bơ sữa Namyang.</p>\r\n', '1', '1');
INSERT INTO `gaucon_history` VALUES ('2', '1965', '<p>\r\n	Ho&agrave;n th&agrave;nh nh&agrave; m&aacute;y Cheonan</p>\r\n', '2', '1');
INSERT INTO `gaucon_history` VALUES ('3', '1967', '<p>\r\n	Sản xuất sữa bột d&agrave;nh cho trẻ em</p>\r\n', '3', '1');
INSERT INTO `gaucon_history` VALUES ('4', '1977', '<p>\r\n	Sản xuất nước uống sữa chua</p>\r\n', '4', '1');
INSERT INTO `gaucon_history` VALUES ('5', '1978', '<p>\r\n	IPO (Initial Public Offering), listed stocks.</p>\r\n', '5', '1');
INSERT INTO `gaucon_history` VALUES ('6', '1991', '<p>\r\n	<font face=\"Arial\" size=\"2\">Nh&agrave; m&aacute;y Gyeongju bắt đầu sản xuất mặt h&agrave;ng &#39;Bulgaris&#39; - một loại sữa chua.</font></p>\r\n', '6', '1');
INSERT INTO `gaucon_history` VALUES ('7', '1994', '<p>\r\n	<font face=\"Arial\" size=\"2\">Bắt đầu sản xuất &quot;Einstein&quot; l&agrave; loại sữa c&oacute; chứa DHA đầu ti&ecirc;n tr&ecirc;n thế giới.</font></p>\r\n', '7', '1');
INSERT INTO `gaucon_history` VALUES ('8', '1995', '<p>\r\n	<font face=\"Arial\" size=\"2\">Tham gia kinh doanh trong lĩnh vực nh&agrave; h&agrave;ng: mở chi nh&aacute;nh Myongdong Pizza Piatti.</font></p>\r\n', '8', '1');
INSERT INTO `gaucon_history` VALUES ('9', '1996', '<p>\r\n	<font face=\"Arial\" size=\"2\">Kho h&agrave;ng nh&agrave; m&aacute;y Gyeongju được tự động ho&aacute; đi v&agrave;o hoạt động.</font></p>\r\n', '9', '1');
INSERT INTO `gaucon_history` VALUES ('10', '1997', '<p>\r\n	<font face=\"Arial\" size=\"2\">- Bắt đầu sản xuất thực phẩm dinh dưỡng đặc biệt: &#39;HopeAllergy v&agrave; &#39;HopeDoctor&#39;</font></p>\r\n<br />\r\n<p>\r\n	<font face=\"Arial\" size=\"2\"><font face=\"Arial\" size=\"2\">- Trang thiết bị sản xuất tự động ho&aacute; bắt đầu hoạt động ở nh&agrave; m&aacute;y Gyeongju.</font></font></p>\r\n<br />\r\n<p>\r\n	<font face=\"Arial\" size=\"2\"><font face=\"Arial\" size=\"2\"><font face=\"Arial\" size=\"2\">- Thực hiện cuộc c&aacute;ch mạng sử dụng loại sữa chất lượng h&agrave;ng đầu để sản xuất sản phẩm sữa ở H&agrave;n Quốc.</font></font></font></p>\r\n', '10', '1');
INSERT INTO `gaucon_history` VALUES ('11', '1998', '<p>\r\n	<font face=\"Arial\" size=\"2\">- Nhận giải thưởng ISO 9001 (Hệ thống quản l&iacute; chất lượng)</font></p>\r\n<br />\r\n<p>\r\n	<font face=\"Arial\" size=\"2\">- <font face=\"Arial\" size=\"2\">Bắt đầu sản xuất nước ngọt &#39;French Cafe&#39; d&agrave;nh cho thế hệ mới</font></font></p>\r\n<br />\r\n<p>\r\n	<font face=\"Arial\" size=\"2\"><font face=\"Arial\" size=\"2\">- <font face=\"Arial\" size=\"2\">Được Ban quản trị Dược thực phẩm H&agrave;n Quốc c&ocirc;ng nhận l&agrave; C&ocirc;ng ty mẫu mực.</font></font></font></p>\r\n<br />\r\n<p>\r\n	<font face=\"Arial\" size=\"2\"><font face=\"Arial\" size=\"2\"><font face=\"Arial\" size=\"2\">- <font face=\"Arial\" size=\"2\">Nhận Giải nhất d&agrave;nh cho C&ocirc;ng ty quản l&yacute; chất lượng xuất sắc (mặt h&agrave;ng thực phẩm v&agrave; nước uống) do Bộ Thương mại, C&ocirc;ng nghiệp v&agrave; Năng lượng cấp.</font></font></font></font></p>\r\n', '11', '1');
INSERT INTO `gaucon_history` VALUES ('12', '1999', '<p>\r\n	- <font face=\"Arial\" size=\"2\">Bắt đầu sản xuất sữa bột &#39;Agisarang Su&#39;</font></p>\r\n<br />\r\n<p>\r\n	<font face=\"Arial\" size=\"2\">- <font face=\"Arial\" size=\"2\">Nhận giải thưởng C&ocirc;ng ty xuất sắc trong lĩnh vực sản xuất (ph&aacute;t triển nh&acirc;n t&agrave;i) do Bộ Thương mại, C&ocirc;ng nghiệp v&agrave; Năng lượng trao tặng.</font></font></p>\r\n<br />\r\n<p>\r\n	<font face=\"Arial\" size=\"2\"><font face=\"Arial\" size=\"2\">- <font face=\"Arial\" size=\"2\">Nhận giải thưởng cao qu&yacute; năm 1999 d&agrave;nh cho C&ocirc;ng ty quản l&yacute; chất lượng từ Hiệp hội quản l&yacute; H&agrave;n Quốc.</font></font></font></p>\r\n', '12', '1');

-- ----------------------------
-- Table structure for `gaucon_news`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_news`;
CREATE TABLE `gaucon_news` (
  `id` int(11) NOT NULL auto_increment,
  `id_cat` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `sumary` text NOT NULL,
  `info` text NOT NULL,
  `detail` text NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `is_hot` tinyint(4) NOT NULL,
  `is_home` tinyint(4) NOT NULL,
  `ordering` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_news
-- ----------------------------

-- ----------------------------
-- Table structure for `gaucon_news_category`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_news_category`;
CREATE TABLE `gaucon_news_category` (
  `id` int(11) NOT NULL auto_increment,
  `avatar` varchar(255) NOT NULL,
  `banner` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `parent` int(11) NOT NULL,
  `is_menu` tinyint(4) NOT NULL,
  `is_title` tinyint(4) NOT NULL,
  `is_desc` tinyint(4) NOT NULL,
  `ordering` tinyint(4) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_news_category
-- ----------------------------

-- ----------------------------
-- Table structure for `gaucon_order`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_order`;
CREATE TABLE `gaucon_order` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(255) NOT NULL,
  `name_nguoigui` varchar(255) NOT NULL,
  `diachi_nguoigui` varchar(255) NOT NULL,
  `dienthoai_nguoigui` varchar(255) NOT NULL,
  `email_nguoigui` varchar(255) NOT NULL,
  `time_order` datetime NOT NULL,
  `time_delivery` datetime NOT NULL,
  `name_nguoinhan` varchar(255) NOT NULL,
  `dienthoai_nguoinhan` varchar(255) NOT NULL,
  `diachi_nguoinhan` varchar(255) NOT NULL,
  `city` int(11) NOT NULL,
  `district` int(11) NOT NULL,
  `is_hide` tinyint(4) NOT NULL,
  `tangcho` int(11) NOT NULL,
  `nhandip` int(11) NOT NULL,
  `thongdiep` text NOT NULL,
  `msg_gaucon` text NOT NULL,
  `method_payment` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_order
-- ----------------------------

-- ----------------------------
-- Table structure for `gaucon_order_products`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_order_products`;
CREATE TABLE `gaucon_order_products` (
  `id` int(11) NOT NULL auto_increment,
  `id_order` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `id_product` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `sale` double NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_order_products
-- ----------------------------
INSERT INTO `gaucon_order_products` VALUES ('1', '1', '3702864', '336', '5', '150000', '300000');
INSERT INTO `gaucon_order_products` VALUES ('2', '2', '3702864', '336', '5', '150000', '300000');
INSERT INTO `gaucon_order_products` VALUES ('3', '3', '3702864', '336', '3', '150000', '300000');
INSERT INTO `gaucon_order_products` VALUES ('4', '3', '3702864', '335', '10', '0', '700000');
INSERT INTO `gaucon_order_products` VALUES ('5', '3', '3702864', '358', '1', '0', '500000');
INSERT INTO `gaucon_order_products` VALUES ('6', '6', '0067281', '336', '4', '150000', '300000');
INSERT INTO `gaucon_order_products` VALUES ('7', '6', '3702864', '336', '1', '150000', '300000');
INSERT INTO `gaucon_order_products` VALUES ('8', '6', '3262163', '335', '1', '0', '700000');
INSERT INTO `gaucon_order_products` VALUES ('9', '7', '3472656', '336', '1', '150000', '300000');
INSERT INTO `gaucon_order_products` VALUES ('10', '8', '4167167', '340', '5', '0', '350000');
INSERT INTO `gaucon_order_products` VALUES ('11', '8', '4167167', '336', '4', '150000', '300000');
INSERT INTO `gaucon_order_products` VALUES ('12', '9', '5382005', '336', '6', '150000', '300000');
INSERT INTO `gaucon_order_products` VALUES ('13', '9', '5382005', '339', '1', '0', '850000');
INSERT INTO `gaucon_order_products` VALUES ('14', '9', '5382005', '240', '3', '0', '500000');

-- ----------------------------
-- Table structure for `gaucon_orders`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_orders`;
CREATE TABLE `gaucon_orders` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) character set utf8 NOT NULL,
  `address` varchar(255) character set utf8 NOT NULL,
  `phone` varchar(255) character set utf8 NOT NULL,
  `email` varchar(255) character set utf8 NOT NULL,
  `comment` text character set utf8 NOT NULL,
  `quality` int(11) NOT NULL,
  `name_product` varchar(255) character set utf8 NOT NULL,
  `link_product` varchar(255) character set utf8 NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gaucon_orders
-- ----------------------------

-- ----------------------------
-- Table structure for `gaucon_partner`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_partner`;
CREATE TABLE `gaucon_partner` (
  `id` int(11) NOT NULL auto_increment,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_partner
-- ----------------------------
INSERT INTO `gaucon_partner` VALUES ('26', '#', 'upload/quangcao/20160122100228-a3e9.jpg', '2', '1');
INSERT INTO `gaucon_partner` VALUES ('25', '#', 'upload/quangcao/20151106162649-c2311.jpg', '1', '1');

-- ----------------------------
-- Table structure for `gaucon_permission_group`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_permission_group`;
CREATE TABLE `gaucon_permission_group` (
  `id` int(11) NOT NULL auto_increment,
  `controller` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_permission_group
-- ----------------------------
INSERT INTO `gaucon_permission_group` VALUES ('1', 'slide', 'Slider');
INSERT INTO `gaucon_permission_group` VALUES ('2', 'products', 'Sản phẩm');
INSERT INTO `gaucon_permission_group` VALUES ('3', 'category', 'Danh mục sản phẩm');
INSERT INTO `gaucon_permission_group` VALUES ('4', 'age', 'Đối tượng');
INSERT INTO `gaucon_permission_group` VALUES ('5', 'news', 'Tin tức');
INSERT INTO `gaucon_permission_group` VALUES ('6', 'news_category', 'Danh mục tin tức');
INSERT INTO `gaucon_permission_group` VALUES ('7', 'albums', 'Albums');
INSERT INTO `gaucon_permission_group` VALUES ('8', 'video', 'Video');
INSERT INTO `gaucon_permission_group` VALUES ('9', 'answer_question', 'Hỏi đáp');
INSERT INTO `gaucon_permission_group` VALUES ('10', 'comment', 'Bình luận');
INSERT INTO `gaucon_permission_group` VALUES ('15', 'staticbaiviet', 'Pages');

-- ----------------------------
-- Table structure for `gaucon_products`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_products`;
CREATE TABLE `gaucon_products` (
  `id` int(11) NOT NULL auto_increment,
  `link_album` varchar(255) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `overview` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `sale` double NOT NULL,
  `tag` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `notes` text NOT NULL,
  `uses` text NOT NULL,
  `thanhphan` text NOT NULL,
  `type` int(11) NOT NULL,
  `views` int(11) NOT NULL,
  `add_date` datetime NOT NULL,
  `is_popup` tinyint(4) NOT NULL,
  `is_phukien` tinyint(4) NOT NULL,
  `ordering` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=370 DEFAULT CHARSET=utf8 COMMENT='gaucon_products';

-- ----------------------------
-- Records of gaucon_products
-- ----------------------------
INSERT INTO `gaucon_products` VALUES ('99', 'http://saigondomain.com/clients/gaucon/thu-vien-chi-tiet/hoa-tuoi-gau-con-92.html', '187', '<p>\n	Kh&ocirc;ng chỉ l&agrave; t&igrave;nh y&ecirc;u, hoa hồng phớt c&ograve;n đại diện cho hạnh ph&uacute;c tr&ograve;n đầy với lời nhắn nhủ: &quot;H&atilde;y tin v&agrave;o anh nh&eacute;!&quot;. Kết hợp c&ugrave;ng với lan Mokara, b&oacute; hoa &quot;Hạnh ph&uacute;c&quot; mang th&ocirc;ng điệp của niềm tin v&agrave; ki&ecirc;u h&atilde;nh.</p>\n<p>\n	Bạn c&oacute; muốn nhắn gửi điều n&agrave;y đến người thương, hoặc đơn giản chỉ l&agrave; lời nhắn nhủ động vi&ecirc;n v&agrave; quan t&acirc;m đến bạn b&egrave;?</p>\n<p>\n	<strong>Gi&aacute; thị trường</strong>:&nbsp;<strike>220.000 đ</strike></p>\n<p itemprop=\"offers\" itemscope=\"\" itemtype=\"https://schema.org/Offer\">\n	<strong>Gi&aacute; sản phẩm:</strong>&nbsp;200.000 đ</p>\n', 'HB-03', '450000', '0', 'hb03', '<p>\n	&quot;Hạnh ph&uacute;c 2&quot; được thiết kế từ:&nbsp;</p>\n<p>\n	- 5 hoa hồng phấn</p>\n<p>\n	- Lan mokara</p>\n<p>\n	- L&aacute; phụ v&agrave; c&aacute;c phụ liệu kh&aacute;c.</p>\n<p>\n	Lưu&nbsp;&yacute;:</p>\n<p>\n	gauconflower.com&nbsp;c&oacute; thể thay thế m&agrave;u sắc của hoa, giấy g&oacute;i v&agrave; nơ t&ugrave;y theo sở th&iacute;ch của kh&aacute;ch h&agrave;ng</p>\n', 'upload/images//hanh-phuc-2.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '3', '1');
INSERT INTO `gaucon_products` VALUES ('98', '', '187', '<p>\n	Mỗi khi ai đ&oacute; tặng bạn một b&oacute; hoa C&aacute;t tường th&igrave; người đ&oacute; mong bạn lu&ocirc;n lu&ocirc;n c&oacute; những ph&uacute;t gi&acirc;y thoải m&aacute;i, thanh b&igrave;nh v&agrave; may mắn trong cuộc sống. C&aacute;t tường</p>\n', 'HB-04', '400000', '0', 'hb04', '<p>\n	B&oacute; hoa &quot;T&igrave;nh m&atilde;i xanh&quot; gồm</p>\n<p>\n	- C&aacute;t tường xanh</p>\n<p>\n	- Giấy lưới cao cấp</p>\n', 'upload/images//tinh-mai-xanh.jpg', '', '', '<p>\n	Bạn c&oacute; thể chọn một số c&aacute;ch thanh to&aacute;n sau:</p>\n<p>\n	<strong>1.Thanh to&aacute;n trực tiếp tại văn ph&ograve;ng c&ocirc;ng ty:</strong></p>\n<p>\n	Gấu Con Flower<br />\n	Địa chỉ:&nbsp;39 B&agrave;n Cờ, Phường 3, Quận 3<br />\n	ĐT:&nbsp;08.62908567 - 0909.306.952 - 0909.300.952&nbsp;<br />\n	Giờ l&agrave;m việc từ 8h-17h30 (Thứ 2 - Thứ 6), &nbsp;8h-12h00(Thứ 7)</p>\n<p>\n	(Ngo&agrave;i giờ l&agrave;m việc bạn vui l&ograve;ng li&ecirc;n lạc qua điện thoại để biết th&ecirc;m)</p>\n<p>\n	<strong>2. Thanh to&aacute;n chuyển khoản qua ng&acirc;n h&agrave;ng:&nbsp;</strong></p>\n<p>\n	Bạn đến bất kỳ ng&acirc;n h&agrave;ng n&agrave;o ở Việt Nam để chuyển tiền theo th&ocirc;ng tin b&ecirc;n dưới (bạn kh&ocirc;ng nhất thiết phải c&oacute; t&agrave;i khoản ng&acirc;n h&agrave;ng)</p>\n<p>\n	<strong>3. Thanh to&aacute;n qua bưu điện:</strong></p>\n<p>\n	Nếu gửi tiền qua đường bưu điện, Qu&yacute; kh&aacute;ch li&ecirc;n hệ với bưu điện gần nhất để tiến h&agrave;nh thủ tục chuyển tiền theo địa chỉ:</p>\n<p>\n	Khi nhận được chi ph&iacute; thanh to&aacute;n, ch&uacute;ng t&ocirc;i sẽ chuyền hoa v&agrave; qu&agrave; đến địa chỉ của Qu&yacute; kh&aacute;ch y&ecirc;u cầu.</p>\n<p>\n	Thời gian hợp lệ để&nbsp;Hoayeuthuong.com&nbsp;nhận được giấy b&aacute;o tiền trước thời gian giao h&agrave;ng theo y&ecirc;u cầu của qu&yacute; kh&aacute;ch l&agrave; 8h v&agrave; đối với c&aacute;c h&agrave;ng đặc biệt l&agrave; 24h.</p>\n', '0', '0', '0000-00-00 00:00:00', '0', '0', '4', '1');
INSERT INTO `gaucon_products` VALUES ('123', '', '186', '', 'HG-08', '350000', '0', 'hg08', '', 'upload/images/IMG_6049.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '25', '1');
INSERT INTO `gaucon_products` VALUES ('124', '', '185', '', 'BH-01', '400000', '0', 'bh01', '', 'upload/images/IMG_7592.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '26', '1');
INSERT INTO `gaucon_products` VALUES ('125', '', '185', '', 'BH-02', '500000', '0', 'bh02', '', 'upload/images/IMG_3812.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '27', '1');
INSERT INTO `gaucon_products` VALUES ('126', '', '185', '', 'BH-03', '400000', '0', 'bh03', '', 'upload/images/IMG_3693.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '28', '1');
INSERT INTO `gaucon_products` VALUES ('127', '', '185', '', 'BH-04', '600000', '0', 'bh04', '', 'upload/images/IMG_3734.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '29', '1');
INSERT INTO `gaucon_products` VALUES ('128', '', '185', '', 'BH-05', '1000000', '0', 'bh05', '', 'upload/images/IMG_4574.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '30', '1');
INSERT INTO `gaucon_products` VALUES ('129', '', '187', '', 'HB-09', '400000', '0', 'hb09', '', 'upload/images/IMG_1751.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '31', '1');
INSERT INTO `gaucon_products` VALUES ('130', '', '187', '', 'HB-10', '400000', '0', 'hb10', '', 'upload/images/IMG_4396.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '32', '1');
INSERT INTO `gaucon_products` VALUES ('131', '', '186', '', 'HG-09', '700000', '0', 'hg09', '', 'upload/images/IMG_6233.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '33', '1');
INSERT INTO `gaucon_products` VALUES ('132', '', '186', '', 'HG-10', '500000', '0', 'hg10', '', 'upload/images/IMG_1452.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '34', '1');
INSERT INTO `gaucon_products` VALUES ('133', '', '185', '', 'BH-06', '1000000', '0', 'bh06', '', 'upload/images/IMG_1708.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '35', '1');
INSERT INTO `gaucon_products` VALUES ('134', '', '185', '', 'BH-07', '0', '0', 'bh07', '', 'upload/images/IMG_3524.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '36', '1');
INSERT INTO `gaucon_products` VALUES ('135', '', '185', '', 'BH-08', '400000', '0', 'bh08', '', 'upload/images/IMG_3854.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '37', '1');
INSERT INTO `gaucon_products` VALUES ('136', '', '185', '', 'BH-09', '300000', '0', 'bh09', '', 'upload/images/IMG_3927.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '38', '1');
INSERT INTO `gaucon_products` VALUES ('137', '', '185', '', 'BH-10', '800000', '0', 'bh10', '', 'upload/images/IMG_4829.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '39', '1');
INSERT INTO `gaucon_products` VALUES ('139', '', '188', '', 'HCM-06', '900000', '0', 'hcm06', '', 'upload/images/IMG_1025.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '40', '1');
INSERT INTO `gaucon_products` VALUES ('140', '', '188', '', 'HCM-07', '1000000', '0', 'hcm07', '', 'upload/images/IMG_4357.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '41', '1');
INSERT INTO `gaucon_products` VALUES ('141', '', '188', '', 'HCM-08', '1800000', '0', 'hcm08', '', 'upload/images/IMG_2614.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '42', '1');
INSERT INTO `gaucon_products` VALUES ('142', '', '186', '', 'HG-11', '800000', '0', 'hg11', '', 'upload/images/IMG_6777.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '43', '1');
INSERT INTO `gaucon_products` VALUES ('143', '', '186', '', 'HG-12', '500000', '0', 'hg12', '', 'upload/images/IMG_0221.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '44', '1');
INSERT INTO `gaucon_products` VALUES ('144', '', '186', '', 'HG-13', '500000', '0', 'hg13', '', 'upload/images/IMG_7699.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '45', '1');
INSERT INTO `gaucon_products` VALUES ('145', '', '186', '', 'HG-14', '350000', '0', 'hg14', '', 'upload/images/IMG_9833.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '46', '1');
INSERT INTO `gaucon_products` VALUES ('146', '', '186', '', 'HG-15', '700000', '0', 'hg15', '', 'upload/images/IMG_9861.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '47', '1');
INSERT INTO `gaucon_products` VALUES ('147', '', '185', '', 'BH-11', '1000000', '0', 'bh11', '', 'upload/images/IMG_4265.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '48', '1');
INSERT INTO `gaucon_products` VALUES ('148', '', '185', '', 'BH-12', '400000', '0', 'bh12', '', 'upload/images/IMG_38541.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '49', '1');
INSERT INTO `gaucon_products` VALUES ('149', '', '185', '', 'BH-13', '500000', '0', 'bh13', '', 'upload/images/IMG_7584.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '50', '1');
INSERT INTO `gaucon_products` VALUES ('150', '', '185', '', 'BH-14', '0', '0', 'bh14', '', 'upload/images/IMG_7368.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '51', '1');
INSERT INTO `gaucon_products` VALUES ('151', '', '185', '', 'BH-15', '0', '0', 'bh15', '', 'upload/images/IMG_6722.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '52', '1');
INSERT INTO `gaucon_products` VALUES ('101', 'http://saigondomain.com/clients/gaucon/thu-vien-chi-tiet/su-kien-cuoi-97.html', '198', '<p>\n	Cổng hoa vải từ hoa hồng trắng đổ lộng lẫy cho đ&aacute;m cưới.&nbsp;Cổng hoa vải t&ocirc;ng t&iacute;m hồng, cắm từ hoa hồng trắng, t&uacute; cầu t&iacute;m hồng lộng lẫy cho đ&aacute;m cưới</p>\n', 'Công hoa cưới Vu Quy', '0', '0', 'cong-hoa-cuoi-vu-quy', '<p>\n	Cổng hoa vải từ hoa hồng trắng đổ lộng lẫy cho đ&aacute;m cưới.&nbsp;Cổng hoa vải t&ocirc;ng t&iacute;m hồng, cắm từ hoa hồng trắng, t&uacute; cầu t&iacute;m hồng lộng lẫy cho đ&aacute;m cưới</p>\n', 'upload/images//cong-hoa-cuoi1.jpg', '<p>\n	Bạn c&oacute; thể chọn một số c&aacute;ch thanh to&aacute;n sau:</p>\n<p>\n	1.Thanh to&aacute;n trực tiếp tại văn ph&ograve;ng c&ocirc;ng ty:</p>\n<p>\n	Gấu Con Flower<br />\n	Địa chỉ:&nbsp;39 B&agrave;n Cờ, Phường 3, Quận 3<br />\n	ĐT:&nbsp;08.62908567 - 0909.306.952 - 0909.300.952&nbsp;<br />\n	Giờ l&agrave;m việc từ 8h-17h30 (Thứ 2 - Thứ 6), &nbsp;8h-12h00(Thứ 7)</p>\n<p>\n	(Ngo&agrave;i giờ l&agrave;m việc bạn vui l&ograve;ng li&ecirc;n lạc qua điện thoại để biết th&ecirc;m)</p>\n<p>\n	2. Thanh to&aacute;n chuyển khoản qua ng&acirc;n h&agrave;ng:&nbsp;</p>\n<p>\n	Bạn đến bất kỳ ng&acirc;n h&agrave;ng n&agrave;o ở Việt Nam để chuyển tiền theo th&ocirc;ng tin b&ecirc;n dưới (bạn kh&ocirc;ng nhất thiết phải c&oacute; t&agrave;i khoản ng&acirc;n h&agrave;ng)</p>\n<p>\n	3. Thanh to&aacute;n qua bưu điện:</p>\n<p>\n	Nếu gửi tiền qua đường bưu điện, Qu&yacute; kh&aacute;ch li&ecirc;n hệ với bưu điện gần nhất để tiến h&agrave;nh thủ tục chuyển tiền theo địa chỉ:</p>\n<p>\n	Khi nhận được chi ph&iacute; thanh to&aacute;n, ch&uacute;ng t&ocirc;i sẽ chuyền hoa v&agrave; qu&agrave; đến địa chỉ của Qu&yacute; kh&aacute;ch y&ecirc;u cầu.</p>\n<p>\n	Thời gian hợp lệ để&nbsp;Hoayeuthuong.com&nbsp;nhận được giấy b&aacute;o tiền trước thời gian giao h&agrave;ng theo y&ecirc;u cầu của qu&yacute; kh&aacute;ch l&agrave; 8h v&agrave; đối với c&aacute;c h&agrave;ng đặc biệt l&agrave; 24h.</p>\n', '', '<p>\n	C&aacute;c Điều Khoản v&agrave; quy định Mua H&agrave;ng</p>\n<p>\n	&Aacute;p dụng từ 20/10/2010</p>\n<p>\n	C&aacute;c điều khoản hợp đồng sau đ&acirc;y l&agrave; thỏa thuận giữa c&aacute;c b&ecirc;n, bao gồm&nbsp;:<br />\n	<br />\n	- C&aacute; nh&acirc;n hoặc tổ chức thực hiện việc đặt mua h&agrave;ng trực tuyến qua trang web thuộc sở hữu của c&ocirc;ng ty l&agrave;:&nbsp;http://hoayeuthuong.com, trang web thương mại điện tử, hoặc đặt h&agrave;ng qua điện thoại sau đ&acirc;y gọi l&agrave; &ldquo;&nbsp;Kh&aacute;ch h&agrave;ng&rdquo;.<br />\n	<br />\n	- Kh&aacute;ch h&agrave;ng sau khi mua h&agrave;ng sẽ được gọi l&agrave; &ldquo;Kh&aacute;ch mua h&agrave;ng&rdquo;. Kh&aacute;ch mua h&agrave;ng sẽ được hiểu chấp nhận quyền v&agrave; nghĩa vụ của kh&aacute;ch h&agrave;ng.<br />\n	<br />\n	Mọi y&ecirc;u cầu đặt mua h&agrave;ng tr&ecirc;n trang web&nbsp;hoặc qua c&aacute;c c&ocirc;ng cụ hỗ trợ (điện thoại, yahoo, skype,&hellip;) đều được hiểu l&agrave; kh&aacute;ch h&agrave;ng đ&atilde; hiểu v&agrave; chấp nhận c&aacute;c điều khoản qui định trong &ldquo;&nbsp;C&aacute;c Điều Khoản Mua H&agrave;ng&nbsp;&ldquo;. Sự hiểu v&agrave; chấp nhận c&aacute;c điều khoản n&agrave;y kh&ocirc;ng cần thiết phải c&oacute; chữ k&yacute; tay của kh&aacute;ch h&agrave;ng.<br />\n	<br />\n	Kh&aacute;ch h&agrave;ng c&oacute; thể truy cập nội dung &ldquo;&nbsp;C&aacute;c Điều Khoản Mua H&agrave;ng&nbsp;&rdquo; bất cứ l&uacute;c n&agrave;o tr&ecirc;n trang web. C&aacute;c điều khoản c&oacute; trong &ldquo;C&aacute;c Điều Khoản Mua H&agrave;ng&nbsp;&ldquo; n&agrave;y sẽ được &aacute;p dụng trong trường hợp c&oacute; bất kỳ nội dung hoặc văn bản n&agrave;o m&acirc;u thuẫn.<br />\n	<br />\n	Th&ocirc;ng tin của kh&aacute;ch h&agrave;ng khi đăng k&yacute; sẽ được nh&acirc;n vi&ecirc;n của ch&uacute;ng t&ocirc;i li&ecirc;n hệ lại để đảm bảo mọi th&ocirc;ng tin của kh&aacute;ch h&agrave;ng l&agrave; ch&iacute;nh x&aacute;c. C&aacute;c th&ocirc;ng tin n&agrave;y sẽ được sử dụng như c&aacute;c yếu tố cần thiết để thực hiện việc mua h&agrave;ng.</p>\n<p>\n	Ch&uacute;ng t&ocirc;i giữ quyền thay đổi nội dung &ldquo;&nbsp;C&aacute;c Điều Khoản Mua H&agrave;ng&nbsp;&ldquo; bất kỳ l&uacute;c n&agrave;o. Điều khoản được &aacute;p dụng sẽ l&agrave; những điều khoản được qui định tại thời điểm thực hiện việc mua hang. Kh&aacute;ch mua h&agrave;ng n&ecirc;n in v&agrave; lưu giữ những qui định n&agrave;y.<br />\n	&nbsp;<br />\n	ĐIỀU KHOẢN 1 &ndash; MỤC Đ&Iacute;CH &Aacute;P DỤNG<br />\n	&nbsp;&nbsp;<br />\n	C&aacute;c nội dung trong &ldquo;&nbsp;C&aacute;c Điều Khoản Mua H&agrave;ng&nbsp;&ldquo; được &aacute;p dụng d&agrave;nh ri&ecirc;ng cho việc b&aacute;n h&agrave;ng của ch&uacute;ng t&ocirc;i tr&ecirc;n trang web&nbsp;&nbsp;hoặc đặt qua điện thoại. Kh&aacute;ch h&agrave;ng đặt mua h&agrave;ng đồng nghĩa với việc kh&aacute;ch h&agrave;ng chấp nhận một c&aacute;ch kh&ocirc;ng điều kiện c&aacute;c điều khoản c&oacute; trong &ldquo;&nbsp;C&aacute;c Điều Khoản Mua H&agrave;ng&nbsp;&ldquo;. C&aacute;c th&ocirc;ng tin kh&aacute;ch h&agrave;ng d&ugrave;ng để đăng k&yacute; tr&ecirc;n trang web của ch&uacute;ng t&ocirc;i sẽ l&agrave; những yếu tố được sử dụng trong cam kết của kh&aacute;ch h&agrave;ng với ch&uacute;ng t&ocirc;i.</p>\n<p>\n	Ngược lại, tất cả c&aacute;c y&ecirc;u cầu của kh&aacute;ch h&agrave;ng kh&ocirc;ng được chấp nhận bởi c&ocirc;ng ty ch&uacute;ng t&ocirc;i đều kh&ocirc;ng c&oacute; gi&aacute; trị. C&ocirc;ng ty ch&uacute;ng t&ocirc;i c&oacute; quyền thay đổi nội dung c&aacute;c điều khoản bất kỳ l&uacute;c n&agrave;o. Ch&uacute;ng t&ocirc;i khuyến c&aacute;o kh&aacute;ch h&agrave;ng n&ecirc;n tham khảo thường xuy&ecirc;n tr&ecirc;n trang web để cập nhập nội dung c&aacute;c điều khoản mua h&agrave;ng.<br />\n	&nbsp;<br />\n	ĐIỀU KHOẢN 2 &ndash; TRUY CẬP TRANG WEB<br />\n	&nbsp;<br />\n	2.1. C&aacute;c mặt h&agrave;ng c&oacute; gi&aacute; kh&aacute;c 0đ (kh&ocirc;ng đồng) đươc đặt tr&ecirc;n website đều l&agrave; c&aacute;c mặt h&agrave;ng kh&aacute;ch h&agrave;ng c&oacute; thể mua.<br />\n	<br />\n	2.2. Để c&oacute; thể đặt mua h&agrave;ng, kh&aacute;ch h&agrave;ng cần phải ho&agrave;n tất thủ tục cung cấp c&aacute;c th&ocirc;ng tin li&ecirc;n lạc c&aacute; nh&acirc;n. Trong đ&oacute;, kh&aacute;ch h&agrave;ng cần điền một số th&ocirc;ng tin bắt buộc để c&oacute; thể thực hiện việc đặt mua. Kh&aacute;ch h&agrave;ng cần thiết phải nhập th&ocirc;ng tin ch&iacute;nh x&aacute;c, cụ thể v&agrave; đầy đủ. Trong trường hợp kh&aacute;ch h&agrave;ng cung cấp th&ocirc;ng tin sai, kh&ocirc;ng ch&iacute;nh x&aacute;c v&agrave; kh&ocirc;ng đầy đủ, c&ocirc;ng ty ch&uacute;ng t&ocirc;i c&oacute; quyền từ chối v&agrave; hủy bỏ đơn h&agrave;ng. Kh&aacute;ch h&agrave;ng chịu ho&agrave;n to&agrave;n tr&aacute;ch nhiệm đối với tất cả hậu quả t&agrave;i ch&iacute;nh c&oacute; thể xảy ra trong qu&aacute; tr&igrave;nh sử dụng trang web bằng t&agrave;i khoản đăng k&yacute; của m&igrave;nh.<br />\n	<br />\n	2.3. Ch&uacute;ng t&ocirc;i khuyến c&aacute;o kh&aacute;ch h&agrave;ng một số ch&uacute; &yacute; sau đ&acirc;y&nbsp;:<br />\n	- Kh&aacute;ch h&agrave;ng chỉ được ph&eacute;p sử dụng v&agrave;/hoặc tải xuống c&aacute;c th&ocirc;ng tin c&oacute; tr&ecirc;n trang web của ch&uacute;ng t&ocirc;i v&igrave; mục đ&iacute;ch c&aacute; nh&acirc;n, kh&ocirc;ng v&igrave; mục đ&iacute;ch thương mại v&agrave; c&oacute; giới hạn về thời gian.<br />\n	- Kh&aacute;ch h&agrave;ng kh&ocirc;ng được ph&eacute;p copy hoặc sử dụng nội dung c&oacute; tr&ecirc;n trang web của ch&uacute;ng t&ocirc;i tr&ecirc;n c&aacute;c trang web kh&aacute;c.<br />\n	- Kh&aacute;ch h&agrave;ng kh&ocirc;ng được ph&eacute;p sử dụng, sử dụng lại, thay đổi, di chuyển, tr&iacute;ch dẫn, thay thế, ph&aacute;t t&aacute;n &hellip; trực tiếp hoặc gi&aacute;n tiếp, dưới bất kỳ h&igrave;nh thức n&agrave;o to&agrave;n bộ hoặc từng phần (h&igrave;nh ảnh, nội dung &hellip;) c&oacute; tr&ecirc;n trang web của ch&uacute;ng t&ocirc;i cũng như t&ecirc;n, biểu trưng, biểu tượng của trang web v&agrave; của c&ocirc;ng ty.<br />\n	- Kh&aacute;ch h&agrave;ng vui l&ograve;ng th&ocirc;ng b&aacute;o cho ch&uacute;ng t&ocirc;i về bất kỳ những vi phạm kể tr&ecirc;n, để ch&uacute;ng t&ocirc;i c&oacute; hướng xử l&yacute; nhằm phục vụ kh&aacute;ch h&agrave;ng ng&agrave;y một tốt hơn.<br />\n	&nbsp;<br />\n	ĐIỀU KHOẢN 3 &ndash; ĐẶT MUA H&Agrave;NG<br />\n	&nbsp;<br />\n	Hiện nay, ch&uacute;ng t&ocirc;i chỉ tiếp nhận những đơn đặt h&agrave;ng giao tại Việt Nam v&agrave; một số quốc gia tr&ecirc;n thế giới.<br />\n	Kh&aacute;ch h&agrave;ng cần phải ho&agrave;n tất c&aacute;c y&ecirc;u cầu khi thanh to&aacute;n mua h&agrave;ng để c&oacute; thể đặt mua h&agrave;ng. Sau khi kh&aacute;ch h&agrave;ng mua h&agrave;ng, ch&uacute;ng t&ocirc;i sẽ gọi điện thoại x&aacute;c nhận về thời điểm cũng nhưng phương c&aacute;ch mua h&agrave;ng, tất cả th&ocirc;ng tin n&agrave;y đều được th&acirc;u &acirc;m v&agrave; chỉ được sử dụng khi c&oacute; khiếu nại.<br />\n	&nbsp;<br />\n	C&aacute;c th&ocirc;ng tin kh&aacute;ch h&agrave;ng sử dụng khi đăng k&yacute; với ch&uacute;ng t&ocirc;i sẽ được sử dụng sau đ&oacute; trong qu&aacute; tr&igrave;nh giao h&agrave;ng. Ch&uacute;ng t&ocirc;i c&oacute; quyền tạm ngưng, hủy bỏ hoặc từ chối tất cả c&aacute;c đơn h&agrave;ng của một kh&aacute;ch h&agrave;ng trong trường hợp c&oacute; vấn đề li&ecirc;n quan đến th&ocirc;ng tin cung cấp v&agrave; thanh to&aacute;n của c&aacute;c đơn h&agrave;ng trước đ&oacute;.<br />\n	&nbsp;<br />\n	ĐIỀU KHOẢN 4 &ndash; T&Igrave;NH TRẠNG H&Agrave;NG H&Oacute;A<br />\n	&nbsp;<br />\n	C&aacute;c mẫu h&agrave;ng v&agrave; gi&aacute; đều sẵn s&agrave;ng khi ch&uacute;ng xuất hiện tr&ecirc;n trang web của ch&uacute;ng t&ocirc;i, trong giới hạn số lượng tồn kho sẵn c&oacute;. Trong trường hợp kh&ocirc;ng c&oacute; đủ nguy&ecirc;n vật liệu cấu th&agrave;nh sản phẩm của kh&aacute;ch h&agrave;ng, ch&uacute;ng t&ocirc;i sẽ tiến h&agrave;nh trao đổi với kh&aacute;ch h&agrave;ng phương &aacute;n thay thế, sau khi được sự đồng &yacute; của kh&aacute;ch h&agrave;ng th&igrave; đơn h&agrave;ng mới c&oacute; hiệu lực.<br />\n	&nbsp;<br />\n	ĐIỀU KHOẢN 5 &ndash; GI&Aacute;<br />\n	&nbsp;<br />\n	Tất cả gi&aacute; sản phẩm đều bằng tiền VND, đ&atilde; bao gồm ph&iacute; chuyển h&agrave;ng qua đối t&aacute;c của ch&uacute;ng t&ocirc;i v&agrave; chưa bao gồm thuế VAT (10%). Kh&aacute;ch h&agrave;ng thanh to&aacute;n bằng tiền Mỹ Kim (USD) sẽ &aacute;p dụng tỉ gi&aacute; ngoại tệ của Ng&acirc;n h&agrave;ng Vietcombank tại thời điểm thanh to&aacute;n. Kh&aacute;ch h&agrave;ng cũng c&oacute; thể chuyển khoản cho ch&uacute;ng t&ocirc;i bằng c&aacute;c ngoại tệ kh&aacute;c. Chi tiết vui l&ograve;ng li&ecirc;n hệ địa chỉ mail.</p>\n<p>\n	Ch&uacute;ng t&ocirc;i nhận chuyển h&agrave;ng tới Tp. Hồ Ch&iacute; Minh, H&agrave; Nội v&agrave; c&aacute;c tỉnh th&agrave;nh tr&ecirc;n to&agrave;n quốc, ngo&agrave;i ra ch&uacute;ng t&ocirc;i cũng c&oacute; chuyển một số đơn h&agrave;ng đến c&aacute;c quốc gia kh&aacute;c, c&aacute;c đơn h&agrave;ng chuyển ngo&agrave;i Tp. Hồ Ch&iacute; Minh v&agrave; H&agrave; Nội vui l&ograve;ng li&ecirc;n lạc với số điện thoại của ch&uacute;ng t&ocirc;i ở phần li&ecirc;n hệ.&nbsp;<br />\n	Ch&uacute;ng t&ocirc;i giữ quyền thay đổi gi&aacute; h&agrave;ng h&oacute;a bất kỳ l&uacute;c n&agrave;o, nhưng lu&ocirc;n ở mức gi&aacute; hợp l&yacute; nhất với chất lượng tốt nhất để n&acirc;ng cao t&iacute;nh cạnh tranh v&agrave; đảm bảo lợi &iacute;ch của kh&aacute;ch h&agrave;ng.<br />\n	&nbsp;<br />\n	ĐIỀU KHOẢN 6 &ndash; THỜI HẠN GIAO H&Agrave;NG V&Agrave; VẬN CHUYỂN<br />\n	&nbsp;<br />\n	6.1 Điều Kiện Chung</p>\n<p>\n	H&agrave;ng sẽ được giao tại địa chỉ được n&ecirc;u bởi kh&aacute;ch h&agrave;ng khi đặt mua. Kh&aacute;ch h&agrave;ng c&oacute; tr&aacute;ch nhiệm cung cấp đầy đủ c&aacute;c th&ocirc;ng tin cần thiết để qu&aacute; tr&igrave;nh giao h&agrave;ng được diễn ra thuận lợi như&nbsp;: địa chỉ ch&iacute;nh x&aacute;c, số nh&agrave;, tỉnh, t&ecirc;n phố, t&ecirc;n quận, huyện, phường, x&atilde;, số điện thoại cần li&ecirc;n hệ &hellip; Ch&uacute;ng t&ocirc;i kh&ocirc;ng chịu tr&aacute;ch nhiệm đối với c&aacute;c trường hợp chuyển h&agrave;ng sai địa chỉ do lỗi của kh&aacute;ch h&agrave;ng.<br />\n	Trong trường hợp kh&ocirc;ng c&oacute; người nhận, vấn đề sẽ xử l&yacute; sẽ như sau:<br />\n	- C&aacute;c mặt h&agrave;ng kh&ocirc;ng thể để l&acirc;u được: Hoa, b&aacute;nh kem: Kh&aacute;ch h&agrave;ng phải trả 100% gi&aacute; trị.<br />\n	- C&aacute;c mặt h&agrave;ng c&acirc;y cảnh, c&acirc;y trồng: Kh&aacute;ch h&agrave;ng trả 50% gi&aacute; trị.<br />\n	- C&aacute;c mặt h&agrave;ng kh&aacute;c (B&aacute;nh kẹo, g&acirc;u b&ocirc;ng): Kh&aacute;ch h&agrave;ng kh&ocirc;ng phải trả.<br />\n	&nbsp;<br />\n	6.2 Thời Gian Giao H&agrave;ng</p>\n<p>\n	Sau khi nhận được thanh to&aacute;n trước của kh&aacute;ch h&agrave;ng, ch&uacute;ng t&ocirc;i sẽ giao đ&uacute;ng thời gian kh&aacute;ch h&agrave;ng y&ecirc;u cầu, trong trường hợp giờ giao kh&ocirc;ng ph&ugrave; hợp ch&uacute;ng t&ocirc;i sẽ trao đổi trước. Đơn h&agrave;ng chỉ được thực hiện khi c&oacute; sự đồng &yacute; của cả 2 b&ecirc;n.&nbsp;<br />\n	&nbsp;<br />\n	6.3 C&aacute;c vấn đề khi giao h&agrave;ng</p>\n<p>\n	Việc giao h&agrave;ng sẽ được thực hiện bởi đối t&aacute;c do ch&uacute;ng t&ocirc;i lựa chọn. H&agrave;ng h&oacute;a trong qu&aacute; tr&igrave;nh vận chuyển c&oacute; thể chịu rủi ro ngo&agrave;i &yacute; muốn như g&atilde;y, hỏng &hellip; Kh&aacute;ch h&agrave;ng cần kiểm tra kỹ t&igrave;nh trạng g&oacute;i h&agrave;ng cũng như số lượng đơn h&agrave;ng. Mọi khiếu nại li&ecirc;n quan đến việc vận chuyển h&agrave;ng h&oacute;a cần phải được kh&aacute;ch h&agrave;ng th&ocirc;ng b&aacute;o lại cho ch&uacute;ng t&ocirc;i trong v&ograve;ng 1 giờ đồng hồ sau khi nhận được h&agrave;ng. Kh&aacute;ch h&agrave;ng c&oacute; thể th&ocirc;ng b&aacute;o&nbsp;<br />\n	- Li&ecirc;n hệ trực tiếp qua điện thoại trong phần li&ecirc;n hệ<br />\n	- Gửi th&ocirc;ng tin &yacute; kiến phản hồi về địa chỉ email<br />\n	&nbsp;<br />\n	6.4 Theo d&otilde;i đơn h&agrave;ng</p>\n<p>\n	Kh&aacute;ch h&agrave;ng c&oacute; thể theo d&otilde;i t&igrave;nh trạng giao h&agrave;ng tại phần Thao d&otilde;i đơn h&agrave;ng tr&ecirc;n website hoặc gọi điện về tổng đ&agrave;i của ch&uacute;ng t&ocirc;i. Nh&acirc;n vi&ecirc;n của ch&uacute;ng t&ocirc;i sẽ c&oacute; tr&aacute;ch nhiệm th&ocirc;ng b&aacute;o tới kh&aacute;ch h&agrave;ng về t&igrave;nh trạng đơn h&agrave;ng. Ngay khi nhận được đơn đặt h&agrave;ng của kh&aacute;ch h&agrave;ng, nh&acirc;n vi&ecirc;n của ch&uacute;ng t&ocirc;i sẽ li&ecirc;n lạc với kh&aacute;ch h&agrave;ng để th&ocirc;ng b&aacute;o về t&igrave;nh trạng của đơn h&agrave;ng đ&oacute; v&agrave; c&aacute;c bước cần l&agrave;m tiếp theo. Nh&acirc;n vi&ecirc;n đ&oacute; sẽ chịu tr&aacute;ch nhiệm giữ li&ecirc;n lạc đến khi kh&aacute;ch h&agrave;ng nhận được h&agrave;ng của m&igrave;nh.</p>\n', '0', '0', '0000-00-00 00:00:00', '0', '0', '4', '1');
INSERT INTO `gaucon_products` VALUES ('100', 'http://saigondomain.com/clients/gaucon/thu-vien-chi-tiet/hoa-tuoi-gau-con-92.html', '187', '<p>\n	Trust and Hope l&agrave; b&oacute; hoa thể hiện niềm tin v&agrave; hi vọng trong t&igrave;nh y&ecirc;u, lu&ocirc;n hướng về điều tươi s&aacute;ng nhất trong cuộc sống d&ugrave; c&oacute; phải trải qua kh&oacute; khăn.</p>\n<p>\n	Th&iacute;ch hợp d&agrave;nh tặng người y&ecirc;u, những người c&oacute; sự khởi đầu mới...</p>\n<p>\n	<strong>Gi&aacute; sản phẩm:</strong>&nbsp;350.000 đ</p>\n', 'HB-02', '450000', '0', 'hb02', '<p>\n	B&oacute; hoa &quot;Trust and Hope&quot; được thiết kế từ:</p>\n<p>\n	- 9 hoa hồng đỏ</p>\n<p>\n	- Hoa cẩm chướng nhật&nbsp;</p>\n<p>\n	- Hoa c&uacute;c Calimero</p>\n<p>\n	- Hoa c&aacute;t tường xanh</p>\n<p>\n	- L&aacute; phụ</p>\n<p>\n	- Phụ liệu</p>\n', 'upload/images//trust-and-hope.jpg', '<p>\n	Bạn c&oacute; thể chọn một số c&aacute;ch thanh to&aacute;n sau:</p>\n<p>\n	1.Thanh to&aacute;n trực tiếp tại văn ph&ograve;ng c&ocirc;ng ty:</p>\n<p>\n	Gấu Con Flower<br />\n	Địa chỉ:&nbsp;39 B&agrave;n Cờ, Phường 3, Quận 3<br />\n	ĐT:&nbsp;08.62908567 - 0909.306.952 - 0909.300.952&nbsp;<br />\n	Giờ l&agrave;m việc từ 8h-17h30 (Thứ 2 - Thứ 6), &nbsp;8h-12h00(Thứ 7)</p>\n<p>\n	(Ngo&agrave;i giờ l&agrave;m việc bạn vui l&ograve;ng li&ecirc;n lạc qua điện thoại để biết th&ecirc;m)</p>\n<p>\n	2. Thanh to&aacute;n chuyển khoản qua ng&acirc;n h&agrave;ng:&nbsp;</p>\n<p>\n	Bạn đến bất kỳ ng&acirc;n h&agrave;ng n&agrave;o ở Việt Nam để chuyển tiền theo th&ocirc;ng tin b&ecirc;n dưới (bạn kh&ocirc;ng nhất thiết phải c&oacute; t&agrave;i khoản ng&acirc;n h&agrave;ng)</p>\n<p>\n	3. Thanh to&aacute;n qua bưu điện:</p>\n<p>\n	Nếu gửi tiền qua đường bưu điện, Qu&yacute; kh&aacute;ch li&ecirc;n hệ với bưu điện gần nhất để tiến h&agrave;nh thủ tục chuyển tiền theo địa chỉ:</p>\n<p>\n	Khi nhận được chi ph&iacute; thanh to&aacute;n, ch&uacute;ng t&ocirc;i sẽ chuyền hoa v&agrave; qu&agrave; đến địa chỉ của Qu&yacute; kh&aacute;ch y&ecirc;u cầu.</p>\n<p>\n	Thời gian hợp lệ để&nbsp;Hoayeuthuong.com&nbsp;nhận được giấy b&aacute;o tiền trước thời gian giao h&agrave;ng theo y&ecirc;u cầu của qu&yacute; kh&aacute;ch l&agrave; 8h v&agrave; đối với c&aacute;c h&agrave;ng đặc biệt l&agrave; 24h.</p>\n', '', '<p>\n	C&aacute;c Điều Khoản v&agrave; quy định Mua H&agrave;ng</p>\n<p>\n	&Aacute;p dụng từ 20/10/2010</p>\n<p>\n	C&aacute;c điều khoản hợp đồng sau đ&acirc;y l&agrave; thỏa thuận giữa c&aacute;c b&ecirc;n, bao gồm&nbsp;:<br />\n	<br />\n	- C&aacute; nh&acirc;n hoặc tổ chức thực hiện việc đặt mua h&agrave;ng trực tuyến qua trang web thuộc sở hữu của c&ocirc;ng ty l&agrave;:&nbsp;http://hoayeuthuong.com, trang web thương mại điện tử, hoặc đặt h&agrave;ng qua điện thoại sau đ&acirc;y gọi l&agrave; &ldquo;&nbsp;Kh&aacute;ch h&agrave;ng&rdquo;.<br />\n	<br />\n	- Kh&aacute;ch h&agrave;ng sau khi mua h&agrave;ng sẽ được gọi l&agrave; &ldquo;Kh&aacute;ch mua h&agrave;ng&rdquo;. Kh&aacute;ch mua h&agrave;ng sẽ được hiểu chấp nhận quyền v&agrave; nghĩa vụ của kh&aacute;ch h&agrave;ng.<br />\n	<br />\n	Mọi y&ecirc;u cầu đặt mua h&agrave;ng tr&ecirc;n trang web&nbsp;hoặc qua c&aacute;c c&ocirc;ng cụ hỗ trợ (điện thoại, yahoo, skype,&hellip;) đều được hiểu l&agrave; kh&aacute;ch h&agrave;ng đ&atilde; hiểu v&agrave; chấp nhận c&aacute;c điều khoản qui định trong &ldquo;&nbsp;C&aacute;c Điều Khoản Mua H&agrave;ng&nbsp;&ldquo;. Sự hiểu v&agrave; chấp nhận c&aacute;c điều khoản n&agrave;y kh&ocirc;ng cần thiết phải c&oacute; chữ k&yacute; tay của kh&aacute;ch h&agrave;ng.<br />\n	<br />\n	Kh&aacute;ch h&agrave;ng c&oacute; thể truy cập nội dung &ldquo;&nbsp;C&aacute;c Điều Khoản Mua H&agrave;ng&nbsp;&rdquo; bất cứ l&uacute;c n&agrave;o tr&ecirc;n trang web. C&aacute;c điều khoản c&oacute; trong &ldquo;C&aacute;c Điều Khoản Mua H&agrave;ng&nbsp;&ldquo; n&agrave;y sẽ được &aacute;p dụng trong trường hợp c&oacute; bất kỳ nội dung hoặc văn bản n&agrave;o m&acirc;u thuẫn.<br />\n	<br />\n	Th&ocirc;ng tin của kh&aacute;ch h&agrave;ng khi đăng k&yacute; sẽ được nh&acirc;n vi&ecirc;n của ch&uacute;ng t&ocirc;i li&ecirc;n hệ lại để đảm bảo mọi th&ocirc;ng tin của kh&aacute;ch h&agrave;ng l&agrave; ch&iacute;nh x&aacute;c. C&aacute;c th&ocirc;ng tin n&agrave;y sẽ được sử dụng như c&aacute;c yếu tố cần thiết để thực hiện việc mua h&agrave;ng.</p>\n<p>\n	Ch&uacute;ng t&ocirc;i giữ quyền thay đổi nội dung &ldquo;&nbsp;C&aacute;c Điều Khoản Mua H&agrave;ng&nbsp;&ldquo; bất kỳ l&uacute;c n&agrave;o. Điều khoản được &aacute;p dụng sẽ l&agrave; những điều khoản được qui định tại thời điểm thực hiện việc mua hang. Kh&aacute;ch mua h&agrave;ng n&ecirc;n in v&agrave; lưu giữ những qui định n&agrave;y.<br />\n	&nbsp;<br />\n	ĐIỀU KHOẢN 1 &ndash; MỤC Đ&Iacute;CH &Aacute;P DỤNG<br />\n	&nbsp;&nbsp;<br />\n	C&aacute;c nội dung trong &ldquo;&nbsp;C&aacute;c Điều Khoản Mua H&agrave;ng&nbsp;&ldquo; được &aacute;p dụng d&agrave;nh ri&ecirc;ng cho việc b&aacute;n h&agrave;ng của ch&uacute;ng t&ocirc;i tr&ecirc;n trang web&nbsp;&nbsp;hoặc đặt qua điện thoại. Kh&aacute;ch h&agrave;ng đặt mua h&agrave;ng đồng nghĩa với việc kh&aacute;ch h&agrave;ng chấp nhận một c&aacute;ch kh&ocirc;ng điều kiện c&aacute;c điều khoản c&oacute; trong &ldquo;&nbsp;C&aacute;c Điều Khoản Mua H&agrave;ng&nbsp;&ldquo;. C&aacute;c th&ocirc;ng tin kh&aacute;ch h&agrave;ng d&ugrave;ng để đăng k&yacute; tr&ecirc;n trang web của ch&uacute;ng t&ocirc;i sẽ l&agrave; những yếu tố được sử dụng trong cam kết của kh&aacute;ch h&agrave;ng với ch&uacute;ng t&ocirc;i.</p>\n<p>\n	Ngược lại, tất cả c&aacute;c y&ecirc;u cầu của kh&aacute;ch h&agrave;ng kh&ocirc;ng được chấp nhận bởi c&ocirc;ng ty ch&uacute;ng t&ocirc;i đều kh&ocirc;ng c&oacute; gi&aacute; trị. C&ocirc;ng ty ch&uacute;ng t&ocirc;i c&oacute; quyền thay đổi nội dung c&aacute;c điều khoản bất kỳ l&uacute;c n&agrave;o. Ch&uacute;ng t&ocirc;i khuyến c&aacute;o kh&aacute;ch h&agrave;ng n&ecirc;n tham khảo thường xuy&ecirc;n tr&ecirc;n trang web để cập nhập nội dung c&aacute;c điều khoản mua h&agrave;ng.<br />\n	&nbsp;<br />\n	ĐIỀU KHOẢN 2 &ndash; TRUY CẬP TRANG WEB<br />\n	&nbsp;<br />\n	2.1. C&aacute;c mặt h&agrave;ng c&oacute; gi&aacute; kh&aacute;c 0đ (kh&ocirc;ng đồng) đươc đặt tr&ecirc;n website đều l&agrave; c&aacute;c mặt h&agrave;ng kh&aacute;ch h&agrave;ng c&oacute; thể mua.<br />\n	<br />\n	2.2. Để c&oacute; thể đặt mua h&agrave;ng, kh&aacute;ch h&agrave;ng cần phải ho&agrave;n tất thủ tục cung cấp c&aacute;c th&ocirc;ng tin li&ecirc;n lạc c&aacute; nh&acirc;n. Trong đ&oacute;, kh&aacute;ch h&agrave;ng cần điền một số th&ocirc;ng tin bắt buộc để c&oacute; thể thực hiện việc đặt mua. Kh&aacute;ch h&agrave;ng cần thiết phải nhập th&ocirc;ng tin ch&iacute;nh x&aacute;c, cụ thể v&agrave; đầy đủ. Trong trường hợp kh&aacute;ch h&agrave;ng cung cấp th&ocirc;ng tin sai, kh&ocirc;ng ch&iacute;nh x&aacute;c v&agrave; kh&ocirc;ng đầy đủ, c&ocirc;ng ty ch&uacute;ng t&ocirc;i c&oacute; quyền từ chối v&agrave; hủy bỏ đơn h&agrave;ng. Kh&aacute;ch h&agrave;ng chịu ho&agrave;n to&agrave;n tr&aacute;ch nhiệm đối với tất cả hậu quả t&agrave;i ch&iacute;nh c&oacute; thể xảy ra trong qu&aacute; tr&igrave;nh sử dụng trang web bằng t&agrave;i khoản đăng k&yacute; của m&igrave;nh.<br />\n	<br />\n	2.3. Ch&uacute;ng t&ocirc;i khuyến c&aacute;o kh&aacute;ch h&agrave;ng một số ch&uacute; &yacute; sau đ&acirc;y&nbsp;:<br />\n	- Kh&aacute;ch h&agrave;ng chỉ được ph&eacute;p sử dụng v&agrave;/hoặc tải xuống c&aacute;c th&ocirc;ng tin c&oacute; tr&ecirc;n trang web của ch&uacute;ng t&ocirc;i v&igrave; mục đ&iacute;ch c&aacute; nh&acirc;n, kh&ocirc;ng v&igrave; mục đ&iacute;ch thương mại v&agrave; c&oacute; giới hạn về thời gian.<br />\n	- Kh&aacute;ch h&agrave;ng kh&ocirc;ng được ph&eacute;p copy hoặc sử dụng nội dung c&oacute; tr&ecirc;n trang web của ch&uacute;ng t&ocirc;i tr&ecirc;n c&aacute;c trang web kh&aacute;c.<br />\n	- Kh&aacute;ch h&agrave;ng kh&ocirc;ng được ph&eacute;p sử dụng, sử dụng lại, thay đổi, di chuyển, tr&iacute;ch dẫn, thay thế, ph&aacute;t t&aacute;n &hellip; trực tiếp hoặc gi&aacute;n tiếp, dưới bất kỳ h&igrave;nh thức n&agrave;o to&agrave;n bộ hoặc từng phần (h&igrave;nh ảnh, nội dung &hellip;) c&oacute; tr&ecirc;n trang web của ch&uacute;ng t&ocirc;i cũng như t&ecirc;n, biểu trưng, biểu tượng của trang web v&agrave; của c&ocirc;ng ty.<br />\n	- Kh&aacute;ch h&agrave;ng vui l&ograve;ng th&ocirc;ng b&aacute;o cho ch&uacute;ng t&ocirc;i về bất kỳ những vi phạm kể tr&ecirc;n, để ch&uacute;ng t&ocirc;i c&oacute; hướng xử l&yacute; nhằm phục vụ kh&aacute;ch h&agrave;ng ng&agrave;y một tốt hơn.<br />\n	&nbsp;<br />\n	ĐIỀU KHOẢN 3 &ndash; ĐẶT MUA H&Agrave;NG<br />\n	&nbsp;<br />\n	Hiện nay, ch&uacute;ng t&ocirc;i chỉ tiếp nhận những đơn đặt h&agrave;ng giao tại Việt Nam v&agrave; một số quốc gia tr&ecirc;n thế giới.<br />\n	Kh&aacute;ch h&agrave;ng cần phải ho&agrave;n tất c&aacute;c y&ecirc;u cầu khi thanh to&aacute;n mua h&agrave;ng để c&oacute; thể đặt mua h&agrave;ng. Sau khi kh&aacute;ch h&agrave;ng mua h&agrave;ng, ch&uacute;ng t&ocirc;i sẽ gọi điện thoại x&aacute;c nhận về thời điểm cũng nhưng phương c&aacute;ch mua h&agrave;ng, tất cả th&ocirc;ng tin n&agrave;y đều được th&acirc;u &acirc;m v&agrave; chỉ được sử dụng khi c&oacute; khiếu nại.<br />\n	&nbsp;<br />\n	C&aacute;c th&ocirc;ng tin kh&aacute;ch h&agrave;ng sử dụng khi đăng k&yacute; với ch&uacute;ng t&ocirc;i sẽ được sử dụng sau đ&oacute; trong qu&aacute; tr&igrave;nh giao h&agrave;ng. Ch&uacute;ng t&ocirc;i c&oacute; quyền tạm ngưng, hủy bỏ hoặc từ chối tất cả c&aacute;c đơn h&agrave;ng của một kh&aacute;ch h&agrave;ng trong trường hợp c&oacute; vấn đề li&ecirc;n quan đến th&ocirc;ng tin cung cấp v&agrave; thanh to&aacute;n của c&aacute;c đơn h&agrave;ng trước đ&oacute;.<br />\n	&nbsp;<br />\n	ĐIỀU KHOẢN 4 &ndash; T&Igrave;NH TRẠNG H&Agrave;NG H&Oacute;A<br />\n	&nbsp;<br />\n	C&aacute;c mẫu h&agrave;ng v&agrave; gi&aacute; đều sẵn s&agrave;ng khi ch&uacute;ng xuất hiện tr&ecirc;n trang web của ch&uacute;ng t&ocirc;i, trong giới hạn số lượng tồn kho sẵn c&oacute;. Trong trường hợp kh&ocirc;ng c&oacute; đủ nguy&ecirc;n vật liệu cấu th&agrave;nh sản phẩm của kh&aacute;ch h&agrave;ng, ch&uacute;ng t&ocirc;i sẽ tiến h&agrave;nh trao đổi với kh&aacute;ch h&agrave;ng phương &aacute;n thay thế, sau khi được sự đồng &yacute; của kh&aacute;ch h&agrave;ng th&igrave; đơn h&agrave;ng mới c&oacute; hiệu lực.<br />\n	&nbsp;<br />\n	ĐIỀU KHOẢN 5 &ndash; GI&Aacute;<br />\n	&nbsp;<br />\n	Tất cả gi&aacute; sản phẩm đều bằng tiền VND, đ&atilde; bao gồm ph&iacute; chuyển h&agrave;ng qua đối t&aacute;c của ch&uacute;ng t&ocirc;i v&agrave; chưa bao gồm thuế VAT (10%). Kh&aacute;ch h&agrave;ng thanh to&aacute;n bằng tiền Mỹ Kim (USD) sẽ &aacute;p dụng tỉ gi&aacute; ngoại tệ của Ng&acirc;n h&agrave;ng Vietcombank tại thời điểm thanh to&aacute;n. Kh&aacute;ch h&agrave;ng cũng c&oacute; thể chuyển khoản cho ch&uacute;ng t&ocirc;i bằng c&aacute;c ngoại tệ kh&aacute;c. Chi tiết vui l&ograve;ng li&ecirc;n hệ địa chỉ mail.</p>\n<p>\n	Ch&uacute;ng t&ocirc;i nhận chuyển h&agrave;ng tới Tp. Hồ Ch&iacute; Minh, H&agrave; Nội v&agrave; c&aacute;c tỉnh th&agrave;nh tr&ecirc;n to&agrave;n quốc, ngo&agrave;i ra ch&uacute;ng t&ocirc;i cũng c&oacute; chuyển một số đơn h&agrave;ng đến c&aacute;c quốc gia kh&aacute;c, c&aacute;c đơn h&agrave;ng chuyển ngo&agrave;i Tp. Hồ Ch&iacute; Minh v&agrave; H&agrave; Nội vui l&ograve;ng li&ecirc;n lạc với số điện thoại của ch&uacute;ng t&ocirc;i ở phần li&ecirc;n hệ.&nbsp;<br />\n	Ch&uacute;ng t&ocirc;i giữ quyền thay đổi gi&aacute; h&agrave;ng h&oacute;a bất kỳ l&uacute;c n&agrave;o, nhưng lu&ocirc;n ở mức gi&aacute; hợp l&yacute; nhất với chất lượng tốt nhất để n&acirc;ng cao t&iacute;nh cạnh tranh v&agrave; đảm bảo lợi &iacute;ch của kh&aacute;ch h&agrave;ng.<br />\n	&nbsp;<br />\n	ĐIỀU KHOẢN 6 &ndash; THỜI HẠN GIAO H&Agrave;NG V&Agrave; VẬN CHUYỂN<br />\n	&nbsp;<br />\n	6.1 Điều Kiện Chung</p>\n<p>\n	H&agrave;ng sẽ được giao tại địa chỉ được n&ecirc;u bởi kh&aacute;ch h&agrave;ng khi đặt mua. Kh&aacute;ch h&agrave;ng c&oacute; tr&aacute;ch nhiệm cung cấp đầy đủ c&aacute;c th&ocirc;ng tin cần thiết để qu&aacute; tr&igrave;nh giao h&agrave;ng được diễn ra thuận lợi như&nbsp;: địa chỉ ch&iacute;nh x&aacute;c, số nh&agrave;, tỉnh, t&ecirc;n phố, t&ecirc;n quận, huyện, phường, x&atilde;, số điện thoại cần li&ecirc;n hệ &hellip; Ch&uacute;ng t&ocirc;i kh&ocirc;ng chịu tr&aacute;ch nhiệm đối với c&aacute;c trường hợp chuyển h&agrave;ng sai địa chỉ do lỗi của kh&aacute;ch h&agrave;ng.<br />\n	Trong trường hợp kh&ocirc;ng c&oacute; người nhận, vấn đề sẽ xử l&yacute; sẽ như sau:<br />\n	- C&aacute;c mặt h&agrave;ng kh&ocirc;ng thể để l&acirc;u được: Hoa, b&aacute;nh kem: Kh&aacute;ch h&agrave;ng phải trả 100% gi&aacute; trị.<br />\n	- C&aacute;c mặt h&agrave;ng c&acirc;y cảnh, c&acirc;y trồng: Kh&aacute;ch h&agrave;ng trả 50% gi&aacute; trị.<br />\n	- C&aacute;c mặt h&agrave;ng kh&aacute;c (B&aacute;nh kẹo, g&acirc;u b&ocirc;ng): Kh&aacute;ch h&agrave;ng kh&ocirc;ng phải trả.<br />\n	&nbsp;<br />\n	6.2 Thời Gian Giao H&agrave;ng</p>\n<p>\n	Sau khi nhận được thanh to&aacute;n trước của kh&aacute;ch h&agrave;ng, ch&uacute;ng t&ocirc;i sẽ giao đ&uacute;ng thời gian kh&aacute;ch h&agrave;ng y&ecirc;u cầu, trong trường hợp giờ giao kh&ocirc;ng ph&ugrave; hợp ch&uacute;ng t&ocirc;i sẽ trao đổi trước. Đơn h&agrave;ng chỉ được thực hiện khi c&oacute; sự đồng &yacute; của cả 2 b&ecirc;n.&nbsp;<br />\n	&nbsp;<br />\n	6.3 C&aacute;c vấn đề khi giao h&agrave;ng</p>\n<p>\n	Việc giao h&agrave;ng sẽ được thực hiện bởi đối t&aacute;c do ch&uacute;ng t&ocirc;i lựa chọn. H&agrave;ng h&oacute;a trong qu&aacute; tr&igrave;nh vận chuyển c&oacute; thể chịu rủi ro ngo&agrave;i &yacute; muốn như g&atilde;y, hỏng &hellip; Kh&aacute;ch h&agrave;ng cần kiểm tra kỹ t&igrave;nh trạng g&oacute;i h&agrave;ng cũng như số lượng đơn h&agrave;ng. Mọi khiếu nại li&ecirc;n quan đến việc vận chuyển h&agrave;ng h&oacute;a cần phải được kh&aacute;ch h&agrave;ng th&ocirc;ng b&aacute;o lại cho ch&uacute;ng t&ocirc;i trong v&ograve;ng 1 giờ đồng hồ sau khi nhận được h&agrave;ng. Kh&aacute;ch h&agrave;ng c&oacute; thể th&ocirc;ng b&aacute;o&nbsp;<br />\n	- Li&ecirc;n hệ trực tiếp qua điện thoại trong phần li&ecirc;n hệ<br />\n	- Gửi th&ocirc;ng tin &yacute; kiến phản hồi về địa chỉ email<br />\n	&nbsp;<br />\n	6.4 Theo d&otilde;i đơn h&agrave;ng</p>\n<p>\n	Kh&aacute;ch h&agrave;ng c&oacute; thể theo d&otilde;i t&igrave;nh trạng giao h&agrave;ng tại phần Thao d&otilde;i đơn h&agrave;ng tr&ecirc;n website hoặc gọi điện về tổng đ&agrave;i của ch&uacute;ng t&ocirc;i. Nh&acirc;n vi&ecirc;n của ch&uacute;ng t&ocirc;i sẽ c&oacute; tr&aacute;ch nhiệm th&ocirc;ng b&aacute;o tới kh&aacute;ch h&agrave;ng về t&igrave;nh trạng đơn h&agrave;ng. Ngay khi nhận được đơn đặt h&agrave;ng của kh&aacute;ch h&agrave;ng, nh&acirc;n vi&ecirc;n của ch&uacute;ng t&ocirc;i sẽ li&ecirc;n lạc với kh&aacute;ch h&agrave;ng để th&ocirc;ng b&aacute;o về t&igrave;nh trạng của đơn h&agrave;ng đ&oacute; v&agrave; c&aacute;c bước cần l&agrave;m tiếp theo. Nh&acirc;n vi&ecirc;n đ&oacute; sẽ chịu tr&aacute;ch nhiệm giữ li&ecirc;n lạc đến khi kh&aacute;ch h&agrave;ng nhận được h&agrave;ng của m&igrave;nh.</p>\n', '0', '0', '0000-00-00 00:00:00', '0', '0', '2', '1');
INSERT INTO `gaucon_products` VALUES ('201', '', '201', '', 'Tiệc Cưới Bình Dương Tháng 4/2015', '0', '0', 'tiec-cuoi-binh-duong-thang-42015', '', 'upload/images/IMG_2759.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '100', '1');
INSERT INTO `gaucon_products` VALUES ('103', '', '191', '<p>\n	C&ocirc;ng dụng:&nbsp;Chậu lan Hồ Điệp 2 c&agrave;nh&nbsp;với v&ograve;i hoa d&agrave;i, c&aacute;nh hoa to, sắc hoa tươi thắm c&ugrave;ng chậu sứ trắng sang trọng sẽ gi&uacute;p cho kh&ocirc;ng gian trang tr&iacute; đẹp hơn, độc đ&aacute;o hơn. B&agrave;n l&agrave;m việc của c&aacute;c sếp, đặc biệt l&agrave; sếp nữ cũng nữ t&iacute;nh hơn, gi&uacute;p tinh thần l&agrave;m việc tốt hơn.</p>\n', 'Chậu Lan Hồ Điệp 2 Cành', '0', '0', 'chau-lan-ho-diep-2-canh', '<p style=\"text-align: justify;\">\n	T&ecirc;n thường gọi:&nbsp;Chậu&nbsp;lan Hồ Điệp 2&nbsp;c&agrave;nh</p>\n<p style=\"text-align: justify;\">\n	T&ecirc;n tiếng anh:&nbsp;Moth orchird</p>\n<p style=\"text-align: justify;\">\n	T&ecirc;n khoa học:&nbsp;Phalaenopsis amabilis</p>\n<p style=\"text-align: justify;\">\n	Họ thực vật:&nbsp;Orchidaceae&nbsp;(Phong lan)</p>\n<p style=\"text-align: justify;\">\n	Trong mu&ocirc;n ng&agrave;n lo&agrave;i hoa đẹp th&igrave;&nbsp;hoa&nbsp;lan Hồ Điệp&nbsp;l&agrave; loại hoa vừa sang trọng, thanh nh&atilde; vừa mang gi&aacute; trị thẩm mỹ v&agrave; tinh thần cao cho con người. Đ&acirc;y l&agrave; lo&agrave;i hoa kh&ocirc;ng chỉ đa dạng về h&igrave;nh d&aacute;ng, k&iacute;ch thước, m&agrave; m&agrave;u sắc c&ograve;n rất phong ph&uacute;. Ngo&agrave;i c&aacute;c m&agrave;u đơn giản nhẹ nh&agrave;n như hoa&nbsp;lan Hồ Điệp trắng, hoa&nbsp;lan Hồ Điệp v&agrave;ng nhạt th&igrave; lo&agrave;i lan n&agrave;y c&ograve;n c&oacute; nhiều m&agrave;u nổi bật v&agrave; được rất nhiều người y&ecirc;u th&iacute;ch như hoa lan&nbsp;Hồ Điệp đỏ, Hồ Điệp t&iacute;m, Hồ Điệp cam&hellip;</p>\n<p style=\"text-align: center;\">\n	<a href=\"http://saigonhoa.com/wp-content/uploads/2014/08/hoa-lan-ho-diep-cam-trang-tri.jpg\"><img alt=\"hoa lan ho diep cam trang tri\" src=\"http://saigonhoa.com/wp-content/uploads/2014/08/hoa-lan-ho-diep-cam-trang-tri-300x225.jpg\" /></a>&nbsp;<a href=\"http://saigonhoa.com/wp-content/uploads/2014/08/ho-diep-hoa-vang.jpg\"><img alt=\"ho diep hoa vang\" src=\"http://saigonhoa.com/wp-content/uploads/2014/08/ho-diep-hoa-vang-300x225.jpg\" /></a></p>\n<p style=\"text-align: center;\">\n	<a href=\"http://saigonhoa.com/wp-content/uploads/2014/08/lan-ho-diep-mau-tim.jpg\"><img alt=\"lan ho diep mau tim\" src=\"http://saigonhoa.com/wp-content/uploads/2014/08/lan-ho-diep-mau-tim-300x225.jpg\" /></a>&nbsp;<a href=\"http://saigonhoa.com/wp-content/uploads/2014/08/ho-diep-hoa-trang.jpg\"><img alt=\"ho diep hoa trang\" src=\"http://saigonhoa.com/wp-content/uploads/2014/08/ho-diep-hoa-trang-300x225.jpg\" /></a></p>\n', 'upload/images/ctr1.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '6', '1');
INSERT INTO `gaucon_products` VALUES ('122', '', '187', '', 'HB-08', '600000', '0', 'hb08', '', 'upload/images/IMG_16514.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '24', '1');
INSERT INTO `gaucon_products` VALUES ('105', '', '187', '<p>\n	<em>Kh&ocirc;ng chỉ l&agrave; nụ hoa đơn thuần, m&agrave; c&ograve;n l&agrave; thay lời muốn n&oacute;i.. hoa hồng đỏ một t&igrave;nh y&ecirc;u nồng n&agrave;n v&agrave; sự l&atilde;ng mạng tinh kh&ocirc;i</em></p>\n<p>\n	<em>Gi&agrave;nh tặng ai đ&oacute;, ch&uacute;t nắng, ch&uacute;t gi&oacute;, ch&uacute;t y&ecirc;u thương v&agrave; ch&uacute;t l&atilde;ng mạng&nbsp;</em></p>\n', 'HB-05', '600000', '0', 'hb05', '', 'upload/images/IMG_35671.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '5', '1');
INSERT INTO `gaucon_products` VALUES ('106', '', '187', '', 'HB-06', '450000', '0', 'hb06', '', 'upload/images/IMG_3705.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '6', '1');
INSERT INTO `gaucon_products` VALUES ('107', '', '187', '', 'HB-07', '450000', '0', 'hb07', '', 'upload/images/IMG_35121.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '7', '1');
INSERT INTO `gaucon_products` VALUES ('108', '', '187', '<p>\n	Tặng nhau nụ cười, gi&agrave;nh cho nhau niềm vui v&agrave; c&ugrave;ng nhau vun đắp hạnh ph&uacute;c.</p>\n<p>\n	Mỗi một lo&agrave;i hoa mang một &yacute; nghĩa ri&ecirc;ng v&agrave; mỗi một m&oacute;n qu&agrave; điều mang lại 1 niềm vui ri&ecirc;ng. Kh&ocirc;ng cần cầu kỳ chỉ cần th&agrave;nh &yacute;, kh&ocirc;ng cần ph&ocirc; trương chỉ cần ch&acirc;n thật.</p>\n<p>\n	Hoa kia khoe sắc với đời</p>\n<p>\n	Tặng ai sắc thắm, trọn đời c&oacute; nhau.</p>\n', 'HB-01', '450000', '0', 'hb01', '', 'upload/images/IMG_4196.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '1', '1');
INSERT INTO `gaucon_products` VALUES ('109', '', '186', '', 'HG-01', '400000', '0', 'hg01', '', 'upload/images/11043199_1055282744500088_1730631311813168548_o.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '12', '1');
INSERT INTO `gaucon_products` VALUES ('110', '', '186', '', 'HG-02', '550000', '0', 'hg02', '', 'upload/images/11053673_1062324900462539_3617720501311843662_o_(2).jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '13', '1');
INSERT INTO `gaucon_products` VALUES ('111', '', '186', '', 'HG-03', '600000', '0', 'hg03', '', 'upload/images/12042668_583103465161311_8324609260386128203_n.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '14', '1');
INSERT INTO `gaucon_products` VALUES ('112', '', '186', '', 'HG-04', '300000', '0', 'hg04', '', 'upload/images/IMG_0055.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '15', '1');
INSERT INTO `gaucon_products` VALUES ('113', '', '186', '', 'HG-05', '500000', '0', 'hg05', '', 'upload/images/IMG_0241.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '16', '1');
INSERT INTO `gaucon_products` VALUES ('114', '', '186', '', 'HG-06', '500000', '0', 'hg06', '', 'upload/images/IMG_1432.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '17', '1');
INSERT INTO `gaucon_products` VALUES ('115', '', '186', '', 'HG-07', '550000', '0', 'hg07', '', 'upload/images/IMG_1444.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '18', '1');
INSERT INTO `gaucon_products` VALUES ('116', '', '188', '', 'HCM-01', '800000', '0', 'hcm01', '', 'upload/images/IMG_9821.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '19', '1');
INSERT INTO `gaucon_products` VALUES ('117', '', '188', '', 'HCM-02', '1000000', '0', 'hcm02', '', 'upload/images/_DSC0030.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '20', '1');
INSERT INTO `gaucon_products` VALUES ('118', '', '188', '', 'HCM-03', '1000000', '0', 'hcm03', '', 'upload/images/_DSC0040.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '21', '1');
INSERT INTO `gaucon_products` VALUES ('119', '', '188', '', 'HCM-04', '1000000', '0', 'hcm04', '', 'upload/images/DSC_0231.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '22', '1');
INSERT INTO `gaucon_products` VALUES ('120', '', '188', '', 'HCM-05', '1000000', '0', 'hcm05', '', 'upload/images/DSC_0304.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '23', '1');
INSERT INTO `gaucon_products` VALUES ('153', '', '187', '', 'HB-11', '400000', '0', 'hb11', '', 'upload/images/IMG_1104.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '53', '1');
INSERT INTO `gaucon_products` VALUES ('154', '', '187', '', 'HB-12', '400000', '0', 'hb12', '', 'upload/images/IMG_1117.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '54', '1');
INSERT INTO `gaucon_products` VALUES ('155', '', '187', '', 'HB-14', '500000', '0', 'hb14', '', 'upload/images/IMG_1158.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '55', '1');
INSERT INTO `gaucon_products` VALUES ('156', '', '187', '', 'HB-15', '500000', '0', 'hb15', '', 'upload/images/IMG_1949.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '56', '1');
INSERT INTO `gaucon_products` VALUES ('157', '', '187', '', 'HB-16', '500000', '0', 'hb16', '', 'upload/images/IMG_2278.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '57', '1');
INSERT INTO `gaucon_products` VALUES ('158', '', '191', '', 'LHĐ-01', '900000', '0', 'lhd01', '', 'upload/images/IMG_3530.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '58', '1');
INSERT INTO `gaucon_products` VALUES ('159', '', '191', '', 'LHĐ-02', '0', '0', 'lhd02', '', 'access/image/filemanager/upload_photo.png', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '59', '1');
INSERT INTO `gaucon_products` VALUES ('160', '', '191', '', 'LHĐ-03', '0', '0', 'lhd03', '', 'access/image/filemanager/upload_photo.png', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '60', '1');
INSERT INTO `gaucon_products` VALUES ('161', '', '186', '', 'HG-16', '600000', '0', 'hg16', '', 'upload/images/IMG_0395.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '58', '1');
INSERT INTO `gaucon_products` VALUES ('162', '', '186', '', 'HG-17', '500000', '0', 'hg17', '', 'upload/images/IMG_0479.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '61', '1');
INSERT INTO `gaucon_products` VALUES ('163', '', '186', '', 'HG-18', '500000', '0', 'hg18', '', 'upload/images/IMG_0898.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '62', '1');
INSERT INTO `gaucon_products` VALUES ('164', '', '186', '', 'HG-19', '600000', '0', 'hg19', '', 'upload/images/IMG_0918.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '63', '1');
INSERT INTO `gaucon_products` VALUES ('165', '', '186', '', 'HG-20', '450000', '0', 'hg20', '', 'upload/images/IMG_1007.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '64', '1');
INSERT INTO `gaucon_products` VALUES ('166', '', '186', '', 'HG-21', '450000', '0', 'hg21', '', 'upload/images/IMG_1011.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '65', '1');
INSERT INTO `gaucon_products` VALUES ('167', '', '186', '', 'HG-22', '600000', '0', 'hg22', '', 'upload/images/IMG_1126.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '66', '1');
INSERT INTO `gaucon_products` VALUES ('168', '', '186', '', 'HG-23', '400000', '0', 'hg23', '', 'upload/images/IMG_1144.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '67', '1');
INSERT INTO `gaucon_products` VALUES ('169', '', '186', '', 'HG-24', '550000', '0', 'hg24', '', 'upload/images/IMG_1210.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '68', '1');
INSERT INTO `gaucon_products` VALUES ('170', '', '186', '', 'HG-25', '500000', '0', 'hg25', '', 'upload/images/IMG_1215.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '69', '1');
INSERT INTO `gaucon_products` VALUES ('171', '', '186', '<p>\n	<em>Hoa kh&ocirc;ng chỉ đơn thuần l&agrave; để trang tr&iacute; m&agrave; Hoa c&ograve;n lằm cho ta th&ecirc;m y&ecirc;u đời, cho cuộc sống của ta th&ecirc;m m&agrave;u sắc v&agrave; đặt biệt l&agrave; gi&uacute;p ta gần nhau hơn!</em></p>\n', 'HG-26', '500000', '0', 'hg26', '', 'upload/images/IMG_1239.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '70', '1');
INSERT INTO `gaucon_products` VALUES ('172', '', '186', '', 'HG-27', '1000000', '0', 'hg27', '', 'upload/images/IMG_1530.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '71', '1');
INSERT INTO `gaucon_products` VALUES ('173', '', '186', '', 'HG-28', '500000', '0', 'hg28', '', 'upload/images/IMG_1805.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '72', '1');
INSERT INTO `gaucon_products` VALUES ('174', '', '186', '', 'HG-29', '500000', '0', 'hg29', '', 'upload/images/IMG_1835.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '73', '1');
INSERT INTO `gaucon_products` VALUES ('175', '', '186', '', 'HG-30', '1200000', '0', 'hg30', '', 'upload/images/IMG_1874.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '74', '1');
INSERT INTO `gaucon_products` VALUES ('176', '', '186', '', 'HG-31', '400000', '0', 'hg31', '', 'upload/images/IMG_1887.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '75', '1');
INSERT INTO `gaucon_products` VALUES ('177', '', '186', '', 'HG-32', '500000', '0', 'hg32', '', 'upload/images/IMG_1918.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '76', '1');
INSERT INTO `gaucon_products` VALUES ('178', '', '186', '', 'HG-33', '450000', '0', 'hg33', '', 'upload/images/IMG_1959.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '77', '1');
INSERT INTO `gaucon_products` VALUES ('179', '', '186', '', 'HG-34', '550000', '0', 'hg34', '', 'upload/images/IMG_2226.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '78', '1');
INSERT INTO `gaucon_products` VALUES ('180', '', '186', '', 'HG-35', '600000', '0', 'hg35', '', 'upload/images/IMG_2246.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '79', '1');
INSERT INTO `gaucon_products` VALUES ('181', '', '186', '<div>\n	&nbsp;</div>\n<div>\n	<em><span style=\"color: rgb(64, 64, 64); font-family: Roboto, arial, sans-serif; font-size: 13px; line-height: 18.2px;\">Những người đi ngang đời ta, d&ugrave; v&ocirc; t&igrave;nh hay hữu &yacute;, cũng đều thật đặc biệt...</span></em></div>\n', 'HG-36', '400000', '0', 'hg36', '', 'upload/images/IMG_2234.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '80', '1');
INSERT INTO `gaucon_products` VALUES ('182', '', '186', '<div class=\"tG QF\" style=\"position: absolute; width: 0px; color: rgb(64, 64, 64); font-family: Roboto, arial, sans-serif; font-size: 13px; line-height: 18.2px;\">\n	&nbsp;</div>\n<div class=\"Ct\" style=\"color: rgb(64, 64, 64); font-family: Roboto, arial, sans-serif; font-size: 13px; line-height: 18.2px;\">\n	<em>Cuộc sống lu&ocirc;n c&oacute; những thăng trầm, kh&oacute; khăn, c&oacute; đ&ocirc;i l&uacute;c bạn sẽ cảm thấy v&ocirc; c&ugrave;ng mệt mỏi, đ&ocirc;i l&uacute;c bạn sẽ mất niềm tin, mất đi sự mạnh mẽ vốn c&oacute; của m&igrave;nh... H&atilde;y tưởng tượng đến những tia nắng, nguồn sống của vạn vật v&agrave; t&igrave;m cho m&igrave;nh nguồn sức mạnh như nắng, đ&oacute; c&oacute; thể l&agrave; l&agrave; gia đ&igrave;nh, bạn b&egrave;, người th&acirc;n hay ch&iacute;nh bản th&acirc;n bạn. H&atilde;y mạnh mẽ hơn, tự tin hơn v&agrave; biết hi vọng hơn...</em></div>\n', 'HG-37', '800000', '0', 'hg37', '', 'upload/images/IMG_2369.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '81', '1');
INSERT INTO `gaucon_products` VALUES ('183', '', '189', '', 'HCB-01', '1000000', '0', 'hcb01', '', 'upload/images/10855164_1074499845911711_8940554998780887886_o.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '82', '1');
INSERT INTO `gaucon_products` VALUES ('184', '', '189', '', 'HCB-02', '1000000', '0', 'hcb02', '', 'upload/images/IMG_2489.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '83', '1');
INSERT INTO `gaucon_products` VALUES ('185', '', '188', '', 'HCM-09', '1200000', '0', 'hcm09', '', 'upload/images/IMG_0623.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '84', '1');
INSERT INTO `gaucon_products` VALUES ('186', '', '188', '', 'HCM-10', '1000000', '0', 'hcm10', '', 'upload/images/IMG_1848.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '85', '1');
INSERT INTO `gaucon_products` VALUES ('187', '', '188', '', 'HCM-11', '900000', '0', 'hcm11', '', 'upload/images/IMG_2299.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '86', '1');
INSERT INTO `gaucon_products` VALUES ('188', '', '188', '', 'HCM-12', '900000', '0', 'hcm12', '', 'upload/images/IMG_2306.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '87', '1');
INSERT INTO `gaucon_products` VALUES ('189', '', '192', '', 'HSK-01', '0', '0', 'hsk01', '', 'upload/images/293286_136254049846257_1441858476_n_(3).jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '88', '1');
INSERT INTO `gaucon_products` VALUES ('190', '', '194', '', 'CHERRY', '0', '0', 'cherry', '', 'upload/images/chery.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '89', '1');
INSERT INTO `gaucon_products` VALUES ('191', '', '193', '', 'CAM XOÀN ĐỒNG THÁP', '0', '0', 'cam-xoan-dong-thap', '', 'upload/images/cam-xoan-dong-thapc_w220_h220.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '90', '1');
INSERT INTO `gaucon_products` VALUES ('192', '', '193', '', 'BƯỞI DA XANH', '0', '0', 'buoi-da-xanh', '', 'upload/images/buoi.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '91', '1');
INSERT INTO `gaucon_products` VALUES ('193', '', '193', '', 'XOÀI CÁT HÒA LỘC', '0', '0', 'xoai-cat-hoa-loc', '', 'upload/images/IMG_0420.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '92', '1');
INSERT INTO `gaucon_products` VALUES ('194', '', '194', '', 'CAM NAM PHI', '0', '0', 'cam-nam-phi', '', 'upload/images/IMG_0450.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '93', '1');
INSERT INTO `gaucon_products` VALUES ('195', '', '194', '', 'KIWI RUỘT VÀNG', '0', '0', 'kiwi-ruot-vang', '', 'upload/images/IMG_2399.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '94', '1');
INSERT INTO `gaucon_products` VALUES ('196', '', '195', '', 'GTC-01', '0', '0', 'gtc01', '', 'upload/images/IMG_2354.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '95', '1');
INSERT INTO `gaucon_products` VALUES ('197', '', '193', '', 'CAM SÀNH', '0', '0', 'cam-sanh', '', 'upload/images/IMG_0440.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '96', '1');
INSERT INTO `gaucon_products` VALUES ('198', '', '194', '', 'NHO ĐEN KHÔNG HẠT ', '0', '0', 'nho-den-khong-hat', '', 'upload/images/nho-den-khong-hat-My.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '97', '1');
INSERT INTO `gaucon_products` VALUES ('199', '', '194', '', 'TÁO GALA MỸ', '0', '0', 'tao-gala-my', '', 'upload/images/images.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '98', '1');
INSERT INTO `gaucon_products` VALUES ('200', '', '195', '', 'GTC-02', '0', '0', 'gtc02', '', 'upload/images/IMG_0375.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '99', '1');
INSERT INTO `gaucon_products` VALUES ('202', '', '185', '', 'BH-16', '1200000', '0', 'bh16', '', 'upload/images/IMG_0911.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '101', '1');
INSERT INTO `gaucon_products` VALUES ('203', '', '185', '', 'BH-17', '1000000', '0', 'bh17', '', 'upload/images/IMG_1935.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '102', '1');
INSERT INTO `gaucon_products` VALUES ('204', '', '185', '', 'BH-18', '1500000', '0', 'bh18', '', 'upload/images/IMG_2557.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '103', '1');
INSERT INTO `gaucon_products` VALUES ('205', '', '185', '', 'BH-19', '1000000', '0', 'bh19', '', 'upload/images/IMG_2573.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '104', '1');
INSERT INTO `gaucon_products` VALUES ('206', '', '185', '', 'BH-20', '900000', '0', 'bh20', '', 'upload/images/IMG_2579.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '105', '1');
INSERT INTO `gaucon_products` VALUES ('207', '', '189', '', 'HCB-03', '1000000', '0', 'hcb03', '', 'upload/images/IMG_0097.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '106', '1');
INSERT INTO `gaucon_products` VALUES ('211', '', '185', '', 'BH-21', '800000', '0', 'bh21', '', 'upload/images/IMG_0350.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '109', '1');
INSERT INTO `gaucon_products` VALUES ('209', '', '189', '', 'HCB-05', '800000', '0', 'hcb05', '', 'upload/images/IMG_0130.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '108', '1');
INSERT INTO `gaucon_products` VALUES ('212', '', '185', '', 'BH-22', '1200000', '0', 'bh22', '', 'upload/images/IMG_0349.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '110', '1');
INSERT INTO `gaucon_products` VALUES ('213', '', '191', '', 'LHD-04', '1400000', '0', 'lhd04', '', 'upload/images/IMG_0841.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '111', '1');
INSERT INTO `gaucon_products` VALUES ('214', '', '191', '', 'LHD-05', '1100000', '0', 'lhd05', '', 'upload/images/IMG_9441.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '112', '1');
INSERT INTO `gaucon_products` VALUES ('215', '', '191', '', 'LHD-06', '1400000', '0', 'lhd06', '', 'upload/images/IMG_0840.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '113', '1');
INSERT INTO `gaucon_products` VALUES ('216', '', '186', '<p style=\"text-align: center;\">\n	<em>Mỗi một m&oacute;n qu&agrave; mang một &yacute; nghĩa...mỗi một nụ hoa mong một th&ocirc;ng điệp ri&ecirc;ng!!</em></p>\n<p style=\"text-align: center;\">\n	<em>Gửi trao y&ecirc;u thương.. gi&agrave;nh cho người n&agrave;o đ&oacute; đặt biệt &lt;3&nbsp;</em></p>\n', 'HG-38', '400000', '0', 'hg38', '', 'upload/images/IMG_0060.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '114', '1');
INSERT INTO `gaucon_products` VALUES ('217', '', '192', '', 'HSK-02', '900000', '0', 'hsk02', '', 'upload/images/IMG_0202.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '115', '1');
INSERT INTO `gaucon_products` VALUES ('218', '', '192', '', 'HSK-03', '800000', '0', 'hsk03', '', 'upload/images/IMG_0422.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '116', '1');
INSERT INTO `gaucon_products` VALUES ('219', '', '188', '', 'HCM-14', '1800000', '0', 'hcm14', '', 'upload/images/IMG_0894.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '117', '1');
INSERT INTO `gaucon_products` VALUES ('220', '', '188', '', 'HCM-15', '700000', '0', 'hcm15', '', 'upload/images/IMG_4357.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '118', '1');
INSERT INTO `gaucon_products` VALUES ('221', '', '188', '', 'HCM-16', '900000', '0', 'hcm16', '', 'upload/images/IMG_2462.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '119', '1');
INSERT INTO `gaucon_products` VALUES ('222', '', '188', '', 'HCM-17', '1000000', '0', 'hcm17', '', 'upload/images/IMG_1277.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '120', '1');
INSERT INTO `gaucon_products` VALUES ('223', '', '188', '', 'HCM-18', '1500000', '0', 'hcm18', '', 'upload/images/IMG_1553.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '121', '1');
INSERT INTO `gaucon_products` VALUES ('224', '', '188', '', 'HCM-19', '700000', '0', 'hcm19', '', 'upload/images/IMG_1933.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '122', '1');
INSERT INTO `gaucon_products` VALUES ('225', '', '188', '', 'HCM-20', '1500000', '0', 'hcm20', '', 'upload/images/IMG_1674.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '123', '1');
INSERT INTO `gaucon_products` VALUES ('226', '', '188', '', 'HCM-20', '1500000', '0', 'hcm20', '', 'upload/images/IMG_1674.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '123', '1');
INSERT INTO `gaucon_products` VALUES ('227', '', '188', '', 'HCM-21', '1100000', '0', 'hcm21', '', 'upload/images/IMG_2097.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '124', '1');
INSERT INTO `gaucon_products` VALUES ('228', '', '188', '', 'HCM-22', '1200000', '0', 'hcm22', '', 'upload/images/IMG_1940.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '125', '1');
INSERT INTO `gaucon_products` VALUES ('229', '', '188', '', 'HCM-23', '1000000', '0', 'hcm23', '', 'upload/images/IMG_1930.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '126', '1');
INSERT INTO `gaucon_products` VALUES ('230', '', '188', '', 'HCM-24', '1000000', '0', 'hcm24', '', 'upload/images/10486303_511302249008100_4208518711966299989_n2.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '127', '1');
INSERT INTO `gaucon_products` VALUES ('231', '', '189', '', 'HCB-07', '2000000', '0', 'hcb07', '', 'upload/images/IMG_4361.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '128', '1');
INSERT INTO `gaucon_products` VALUES ('236', '', '187', '<p>\n	<em>Gấu Con Flower nhắn gửi y&ecirc;u thương, thay lời muốn n&oacute;i, &lt;3</em></p>\n', 'HB-17', '500000', '0', 'hb17', '', 'upload/images/IMG_5578.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '133', '1');
INSERT INTO `gaucon_products` VALUES ('233', '', '188', '', 'HCM-25', '0', '0', 'hcm25', '', 'upload/images/hoa-chia-buon.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '130', '1');
INSERT INTO `gaucon_products` VALUES ('234', '', '186', '', 'HG-39', '0', '0', 'hg39', '', 'upload/images/IMG_46972.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '131', '1');
INSERT INTO `gaucon_products` VALUES ('235', '', '188', '', 'HCM-26', '1000000', '0', 'hcm26', '', 'upload/images/hcm-261.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '132', '1');
INSERT INTO `gaucon_products` VALUES ('237', '', '187', '<p>\n	<em>Gấu Con Flower nhắn gửi y&ecirc;u thương, thay lời muốn n&oacute;i&nbsp;</em></p>\n', 'HB-17.', '500000', '0', 'hb17.', '', 'upload/images/IMG_5582.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '134', '1');
INSERT INTO `gaucon_products` VALUES ('238', '', '187', '<p>\n	Gấu Con flower c&ugrave;ng bạn sẽ chia :)</p>\n', 'HB-18', '300000', '0', 'hb18', '', 'upload/images/IMG_5606.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '135', '1');
INSERT INTO `gaucon_products` VALUES ('239', '', '186', '<p>\n	Gắn kết y&ecirc;u thương, Gấu Con lu&ocirc;n đồng h&agrave;nh c&ugrave;ng bạn&nbsp;</p>\n', 'HG-40', '450000', '0', 'hg40', '', 'upload/images/IMG_5622.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '136', '1');
INSERT INTO `gaucon_products` VALUES ('240', '', '186', '', 'HG-41', '500000', '0', 'hg41', '', 'upload/images/IMG_5625.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '1', '137', '1');
INSERT INTO `gaucon_products` VALUES ('241', '', '185', '', 'BH-23', '600000', '0', 'bh23', '', 'upload/images/IMG_5619.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '138', '1');
INSERT INTO `gaucon_products` VALUES ('242', '', '186', '', 'HG-42', '400000', '0', 'hg42', '', 'upload/images/IMG_5391.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '139', '1');
INSERT INTO `gaucon_products` VALUES ('243', '', '187', '', 'HB-19', '500000', '0', 'hb19', '', 'upload/images/12166589_513848812117630_1426533276_n.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '140', '1');
INSERT INTO `gaucon_products` VALUES ('244', '', '188', '', 'HCM-27', '900000', '0', 'hcm27', '', 'upload/images/IMG_1025.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '141', '1');
INSERT INTO `gaucon_products` VALUES ('245', '', '188', '', 'HCM-28', '1000000', '0', 'hcm28', '', 'upload/images/IMG_4973.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '142', '1');
INSERT INTO `gaucon_products` VALUES ('246', '', '189', '', 'HCB-08', '600000', '0', 'hcb08', '', 'upload/images/IMG_2623.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '143', '1');
INSERT INTO `gaucon_products` VALUES ('247', '', '189', '', 'HCB-09', '1000000', '0', 'hcb09', '', 'upload/images/IMG_6201.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '144', '1');
INSERT INTO `gaucon_products` VALUES ('248', '', '189', '', 'HCB-10', '800000', '0', 'hcb10', '', 'upload/images/IMG_3482.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '145', '1');
INSERT INTO `gaucon_products` VALUES ('249', '', '187', '', 'HB-20', '500000', '0', 'hb20', '', 'upload/images/IMG_5638.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '146', '1');
INSERT INTO `gaucon_products` VALUES ('250', '', '185', '', 'BH-24', '500000', '0', 'bh24', '', 'upload/images/IMG_6232.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '147', '1');
INSERT INTO `gaucon_products` VALUES ('251', '', '185', '', 'BH-25', '800000', '0', 'bh25', '', 'upload/images/IMG_0057.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '148', '1');
INSERT INTO `gaucon_products` VALUES ('253', '', '186', '', 'HG-43', '600000', '0', 'hg43', '', 'upload/images/11053673_1062324900462539_3617720501311843662_o_(1).jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '149', '1');
INSERT INTO `gaucon_products` VALUES ('254', '', '186', '', 'HG-44', '500000', '0', 'hg44', '', 'upload/images/IMG_1452.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '150', '1');
INSERT INTO `gaucon_products` VALUES ('255', '', '190', '', 'HT-01', '900000', '0', 'ht01', '', 'upload/images/11150199_1079339902094372_5047981541421153051_n.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '151', '1');
INSERT INTO `gaucon_products` VALUES ('256', '', '190', '', 'HT-02', '0', '0', 'ht02', '', 'upload/images/IMG_1090.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '152', '1');
INSERT INTO `gaucon_products` VALUES ('257', '', '190', '', 'HT-03', '650000', '0', 'ht03', '', 'upload/images/IMG_4971.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '153', '1');
INSERT INTO `gaucon_products` VALUES ('258', '', '190', '', 'HT-04', '800000', '0', 'ht04', '', 'upload/images/IMG_4807.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '154', '1');
INSERT INTO `gaucon_products` VALUES ('259', '', '190', '', 'HT-05', '900000', '0', 'ht05', '', 'upload/images/IMG_4859.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '155', '1');
INSERT INTO `gaucon_products` VALUES ('260', '', '190', '', 'HT-06', '900000', '0', 'ht06', '', 'upload/images/IMG_4866.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '156', '1');
INSERT INTO `gaucon_products` VALUES ('261', '', '190', '', 'HT-07', '850000', '0', 'ht07', '', 'upload/images/IMG_5041.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '157', '1');
INSERT INTO `gaucon_products` VALUES ('262', '', '195', '', 'GTC-03', '500000', '0', 'gtc03', '', 'upload/images/IMG_4758.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '158', '1');
INSERT INTO `gaucon_products` VALUES ('263', '', '188', '', 'HCM-29', '900000', '0', 'hcm29', '', 'upload/images/11070202_511301812341477_111366494309575441_n.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '159', '1');
INSERT INTO `gaucon_products` VALUES ('264', '', '188', '', 'HCM-30', '800000', '0', 'hcm30', '', 'upload/images/11082574_511302552341403_5435377676378214215_n.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '160', '1');
INSERT INTO `gaucon_products` VALUES ('265', '', '188', '', 'HCM-31', '1000000', '0', 'hcm31', '', 'upload/images/IMG_4344.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '161', '1');
INSERT INTO `gaucon_products` VALUES ('266', '', '188', '', 'HCM-32', '600000', '0', 'hcm32', '', 'upload/images/IMG_5720.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '162', '1');
INSERT INTO `gaucon_products` VALUES ('267', '', '188', '', 'HCM-33', '1500000', '0', 'hcm33', '', 'upload/images/IMG_7379.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '163', '1');
INSERT INTO `gaucon_products` VALUES ('268', '', '188', '', 'HCM-34', '1000000', '0', 'hcm34', '', 'upload/images/IMG_6329.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '164', '1');
INSERT INTO `gaucon_products` VALUES ('269', '', '188', '', 'HCM-35', '800000', '0', 'hcm35', '', 'upload/images/IMG_4355.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '165', '1');
INSERT INTO `gaucon_products` VALUES ('270', '', '188', '', 'HCM-36', '1000000', '0', 'hcm36', '', 'upload/images/11026119_511301762341482_1015518282560457552_n.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '166', '1');
INSERT INTO `gaucon_products` VALUES ('271', '', '188', '', 'HCM-37', '1000000', '0', 'hcm37', '', 'upload/images/IMG_3539.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '167', '1');
INSERT INTO `gaucon_products` VALUES ('272', '', '188', '', 'HCM-38', '1000000', '0', 'hcm38', '', 'upload/images/IMG_5724.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '168', '1');
INSERT INTO `gaucon_products` VALUES ('273', '', '188', '', 'HCM-39', '1200000', '0', 'hcm39', '', 'upload/images/IMG_3805.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '169', '1');
INSERT INTO `gaucon_products` VALUES ('274', '', '189', '', 'HCB-11', '800000', '0', 'hcb11', '', 'upload/images/DSC_0222.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '170', '1');
INSERT INTO `gaucon_products` VALUES ('275', '', '189', '', 'HCB-12', '1000000', '0', 'hcb12', '', 'upload/images/IMG_0100.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '171', '1');
INSERT INTO `gaucon_products` VALUES ('276', '', '189', '', 'HCB-14', '900000', '0', 'hcb14', '', 'upload/images/3.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '172', '1');
INSERT INTO `gaucon_products` VALUES ('277', '', '188', '', 'HG-40', '1500000', '0', 'hg40', '', 'upload/images/IMG_5388.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '173', '1');
INSERT INTO `gaucon_products` VALUES ('278', '', '185', '', 'BH-25', '900000', '0', 'bh25', '', 'upload/images/IMG_5321.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '174', '1');
INSERT INTO `gaucon_products` VALUES ('279', '', '188', '', 'HCM-41', '1000000', '0', 'hcm41', '', 'upload/images/IMG_5030.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '175', '1');
INSERT INTO `gaucon_products` VALUES ('280', '', '186', '', 'HG-45', '400000', '0', 'hg45', '', 'upload/images/IMG_6168.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '176', '1');
INSERT INTO `gaucon_products` VALUES ('281', '', '188', '', 'HCM-42', '900000', '0', 'hcm42', '', 'upload/images/IMG_3963.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '177', '1');
INSERT INTO `gaucon_products` VALUES ('282', '', '195', '', 'GTC-04', '0', '0', 'gtc04', '', 'upload/images/IMG_5172.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '178', '1');
INSERT INTO `gaucon_products` VALUES ('283', '', '192', '', 'HSK-04', '0', '0', 'hsk04', '', 'upload/images/mau-backdrop-dam-cuoi-dep-cbgt65.png', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '179', '1');
INSERT INTO `gaucon_products` VALUES ('284', '', '186', '', 'HG-46', '800000', '0', 'hg46', '', 'upload/images/IMG_5808.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '180', '1');
INSERT INTO `gaucon_products` VALUES ('285', '', '186', '', 'HG-47', '450000', '0', 'hg47', '', 'upload/images/IMG_5815.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '181', '1');
INSERT INTO `gaucon_products` VALUES ('286', '', '186', '', 'HG-48', '800000', '0', 'hg48', '', 'upload/images/IMG_5641.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '182', '1');
INSERT INTO `gaucon_products` VALUES ('287', '', '187', '<p>\n	Gửi hương, gửi hoa, gửi cả t&acirc;m t&igrave;nh..</p>\n<p>\n	Y&ecirc;u thương gửi gi&oacute; heo m&acirc;y, chi xin ai h&atilde;y giữ t&igrave;nh bền l&acirc;u&nbsp;</p>\n', 'HB-21', '500000', '0', 'hb21', '', 'upload/images/IMG_5644.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '183', '1');
INSERT INTO `gaucon_products` VALUES ('288', '', '188', '', 'HCM-42', '1300000', '0', 'hcm42', '', 'upload/images/IMG_5794.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '184', '1');
INSERT INTO `gaucon_products` VALUES ('289', '', '188', '', 'HCM-43', '2000000', '0', 'hcm43', '', 'upload/images/IMG_5806.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '185', '1');
INSERT INTO `gaucon_products` VALUES ('290', '', '188', '<p>\n	Một lời ch&uacute;c mừng, một m&oacute;n qu&agrave; giản đơn cũng kh&ocirc;ng k&eacute;m phần sang trọng v&agrave; mang lại kh&ocirc;ng kh&iacute; vui tươi đầy m&agrave;u sắc :)</p>\n', 'HCM-44', '800000', '0', 'hcm44', '', 'upload/images/IMG_5820.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '186', '1');
INSERT INTO `gaucon_products` VALUES ('291', '', '186', '<p>\n	Đ&ocirc;i khi cuộc sống cần c&oacute; th&ecirc;m một t&iacute; hương v&agrave; th&ecirc;m 1 &iacute;t m&agrave;u sắc của hoa để c&oacute; th&ecirc;m m&agrave;u th&ecirc;m hương cho mỗi một ng&agrave;y tr&ocirc;i qua kh&ocirc;ng qu&aacute; nhạt nhẽo, kh&ocirc;ng qu&aacute; tẻ nhạt :)</p>\n', 'HG-49', '700000', '0', 'hg49', '', 'upload/images/IMG_5654.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '187', '1');
INSERT INTO `gaucon_products` VALUES ('292', '', '186', '', 'HG-50', '800000', '0', 'hg50', '', 'upload/images/IMG_5778.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '188', '1');
INSERT INTO `gaucon_products` VALUES ('293', '', '186', '<p>\n	Chắc ai đ&oacute; ai sẽ về.</p>\n<p>\n	Hướng dương m&atilde;i đợi m&atilde;i chờ, mong rằng ai đ&oacute; sẽ về với ta :)</p>\n', 'HG-51', '400000', '0', 'hg51', '', 'upload/images/IMG_4694.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '189', '1');
INSERT INTO `gaucon_products` VALUES ('295', '', '187', '<p>\n	Thương ai ch&uacute;t đợi ch&uacute;t chờ, nhớ ai gửi tặng lại người b&oacute; hoa :)</p>\n<div>\n	&nbsp;</div>\n', 'HB-22', '400000', '0', 'hb22', '', 'upload/images/IMG_5751.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '191', '1');
INSERT INTO `gaucon_products` VALUES ('296', '', '185', '<p>\n	Hoa kia khoe sắc với đời.. Xinh tươi thơm ng&aacute;t chơi đời th&ecirc;m vui :v :v :v&nbsp;</p>\n', 'BH-26', '500000', '0', 'bh26', '', 'upload/images/IMG_50651.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '192', '1');
INSERT INTO `gaucon_products` VALUES ('297', '', '189', '<p>\n	Gửi ch&uacute;t t&igrave;nh với l&ograve;ng thương tiếc.</p>\n<p>\n	Tiễn người về nới g&oacute;c trời xa x&ocirc;i&nbsp;</p>\n', 'HCB-15', '1000000', '0', 'hcb15', '', 'upload/images/IMG_5870.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '193', '1');
INSERT INTO `gaucon_products` VALUES ('298', '', '189', '<p>\n	Gửi ch&uacute;t t&igrave;nh với l&ograve;ng thương tiếc.</p>\n<p>\n	Tiễn người về nới g&oacute;c trời xa x&ocirc;i&nbsp;</p>\n', 'HCB-16', '1000000', '0', 'hcb16', '', 'upload/images/IMG_5867.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '194', '1');
INSERT INTO `gaucon_products` VALUES ('299', '', '187', '<p>\n	Thương ai ch&uacute;t đợi ch&uacute;t chờ, nhớ ai gửi tặng lại người b&oacute; hoa :)</p>\n<p>\n	&nbsp;</p>\n', 'HB-23', '400000', '0', 'hb23', '', 'upload/images/IMG_5662.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '195', '1');
INSERT INTO `gaucon_products` VALUES ('300', '', '189', '<p>\n	Gửi ch&uacute;t t&igrave;nh với l&ograve;ng thương tiếc.</p>\n<p>\n	Tiễn người về nới g&oacute;c trời xa x&ocirc;i&nbsp;</p>\n', 'HCB-17', '900000', '0', 'hcb17', '', 'upload/images/IMG_5878.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '196', '1');
INSERT INTO `gaucon_products` VALUES ('301', '', '190', '<p>\n	Gửi hoa, gửi quả gửi ch&uacute;t t&igrave;nh.&nbsp;</p>\n<p>\n	Tặng ai sức khỏe, tặng ai ch&uacute;t t&igrave;nh :)</p>\n<p>\n	Thay v&igrave; những m&oacute;n qua sa xỉ, ta c&oacute; thể gửi tặng đến bạn b&egrave;, người th&acirc;n m&oacute;n qu&agrave; thiết thực kh&ocirc;ng qu&aacute; cầu k&igrave;, nhưng lại mang nhiều &yacute; nghĩa .</p>\n', 'HT-08', '750000', '0', 'ht08', '', 'upload/images/IMG_5906.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '197', '1');
INSERT INTO `gaucon_products` VALUES ('302', '', '188', '<p>\n	Một lời ch&uacute;c mừng ch&acirc;n th&agrave;nh thể hiện đơn giản qua những đ&oacute;a hoa đang xinh tươi sắc thắm, nhưn c&ugrave;ng h&ograve;a chung v&agrave;o niềm vui mừng của Doanh Nghiệp, t&ocirc; điểm th&ecirc;m cho một ng&agrave;y vui&nbsp;</p>\n', 'HCM-45', '1900000', '0', 'hcm45', '', 'upload/images/IMG_5923.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '198', '1');
INSERT INTO `gaucon_products` VALUES ('303', '', '188', '', 'HCM-46', '1900000', '0', 'hcm46', '', 'upload/images/IMG_5914.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '199', '1');
INSERT INTO `gaucon_products` VALUES ('304', '', '189', '', 'HCB-18', '1800000', '0', 'hcb18', '', 'upload/images/IMG_5934.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '200', '1');
INSERT INTO `gaucon_products` VALUES ('305', '', '188', '<p>\n	Một lời ch&uacute;c mừng ch&acirc;n th&agrave;nh thể hiện đơn giản qua những đ&oacute;a hoa đang xinh tươi sắc thắm, nhưn c&ugrave;ng h&ograve;a chung v&agrave;o niềm vui mừng của Doanh Nghiệp, t&ocirc; điểm th&ecirc;m cho một ng&agrave;y vui&nbsp;</p>\n', 'HCM-47', '1900000', '0', 'hcm47', '', 'upload/images/IMG_5940.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '201', '1');
INSERT INTO `gaucon_products` VALUES ('306', '', '188', '<p>\n	Với m&agrave;u sắc tươi vui, v&agrave; hương thơm thoang thoảng c&ugrave;ng một ch&uacute;t kh&eacute;o l&eacute;o của những người thợ cắm hoa Gi&uacute;p ta gửi tặng lời ch&uacute;c mừng, lời chia sẽ với những Doanh Nghiệp mới th&agrave;nh lập, hay lời ch&uacute;c mừng với những ng&agrave;y kỷ niệm thập lập của C&ocirc;ng ty. :)&nbsp;</p>\n', 'HCM-48', '1900000', '0', 'hcm48', '', 'upload/images/IMG_5948.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '202', '1');
INSERT INTO `gaucon_products` VALUES ('307', '', '185', '<p>\n	Với vai tr&ograve; thiết yếu trong cuộc sống v&agrave; kh&ocirc;ng qu&aacute; cầu k&igrave; hay k&eacute;n chọn. Những đ&oacute;a hoa lu&ocirc;n thay người tặng b&agrave;y tỏ những lời chưa n&oacute;i, hoặc t&ocirc; điểm th&ecirc;m cho cuộc sống của mỗi người.&nbsp;</p>\n<p>\n	&nbsp;</p>\n', 'BH-27', '1000000', '0', 'bh27', '', 'upload/images/IMG_65711.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '203', '1');
INSERT INTO `gaucon_products` VALUES ('308', '', '189', '', 'HCB-19', '800000', '0', 'hcb19', '', 'upload/images/IMG_6143.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '204', '1');
INSERT INTO `gaucon_products` VALUES ('309', '', '189', '', 'HCB-20', '900000', '0', 'hcb20', '', 'upload/images/IMG_6141.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '205', '1');
INSERT INTO `gaucon_products` VALUES ('310', '', '187', '', 'HB-24', '300000', '0', 'hb24', '', 'upload/images/IMG_6103.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '206', '1');
INSERT INTO `gaucon_products` VALUES ('311', '', '186', '', 'HG-52', '600000', '0', 'hg52', '', 'upload/images/IMG_6098.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '207', '1');
INSERT INTO `gaucon_products` VALUES ('312', '', '186', '', 'HG-53', '800000', '0', 'hg53', '', 'upload/images/IMG_6153.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '208', '1');
INSERT INTO `gaucon_products` VALUES ('313', '', '188', '', 'HCM-49', '1000000', '0', 'hcm49', '', 'upload/images/IMG_6093.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '209', '1');
INSERT INTO `gaucon_products` VALUES ('314', '', '202', '', 'HTY-01', '600000', '0', 'hty01', '', 'upload/images/IMG_4600.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '210', '1');
INSERT INTO `gaucon_products` VALUES ('315', '', '187', '', 'HB-25', '500000', '0', 'hb25', '', 'upload/images/IMG_5758.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '211', '1');
INSERT INTO `gaucon_products` VALUES ('316', '', '187', '', 'HB-26', '600000', '0', 'hb26', '', 'upload/images/IMG_46001.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '212', '1');
INSERT INTO `gaucon_products` VALUES ('317', '', '187', '', 'HB-27', '550000', '0', 'hb27', '', 'upload/images/IMG_4606.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '213', '1');
INSERT INTO `gaucon_products` VALUES ('318', '', '202', '', 'HTY-02', '1000000', '0', 'hty02', '', 'upload/images/IMG_6199.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '214', '1');
INSERT INTO `gaucon_products` VALUES ('319', '', '199', '', 'PH-01', '0', '0', 'ph01', '', 'upload/images/11796257_561886230616368_41342088694573898_n.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '215', '1');
INSERT INTO `gaucon_products` VALUES ('320', '', '199', '', 'PH-02', '0', '0', 'ph02', '', 'upload/images/11091_511243289013996_2187686363299412105_n.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '216', '1');
INSERT INTO `gaucon_products` VALUES ('321', '', '199', '', 'PH-03', '0', '0', 'ph03', '', 'upload/images/mau-backdrop-dam-cuoi-dep-cbgt62.png', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '217', '1');
INSERT INTO `gaucon_products` VALUES ('322', '', '199', '', 'PH-04', '0', '0', 'ph04', '', 'upload/images/mau-backdrop-dam-cuoi-dep-cbgt651.png', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '218', '1');
INSERT INTO `gaucon_products` VALUES ('324', '', '186', '', 'HG-54', '700000', '0', 'hg54', '', 'upload/images/IMG_62481.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '219', '1');
INSERT INTO `gaucon_products` VALUES ('325', '', '187', '', 'HB-28', '500000', '0', 'hb28', '', 'upload/images/IMG_3938.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '220', '1');
INSERT INTO `gaucon_products` VALUES ('326', '', '185', '', 'BH-28', '1000000', '0', 'bh28', '', 'upload/images/trang.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '221', '1');
INSERT INTO `gaucon_products` VALUES ('327', '', '185', '', 'BH-29', '1100000', '0', 'bh29', '', 'upload/images/IMG_6342-copy.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '222', '1');
INSERT INTO `gaucon_products` VALUES ('328', '', '185', '', 'BH-30', '1000000', '0', 'bh30', '', 'upload/images/IMG_6700.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '223', '1');
INSERT INTO `gaucon_products` VALUES ('329', '', '185', '', 'BH-31', '900000', '800000', 'bh31', '', 'upload/images/IMG_6688.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '224', '1');
INSERT INTO `gaucon_products` VALUES ('330', '', '185', '', 'BH-32', '1000000', '0', 'bh32', '', 'upload/images/IMG_6684.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '225', '1');
INSERT INTO `gaucon_products` VALUES ('331', '', '185', '', 'BH-33', '1000000', '0', 'bh33', '', 'upload/images/IMG_6670.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '226', '1');
INSERT INTO `gaucon_products` VALUES ('332', '', '185', '', 'BH-34', '900000', '0', 'bh34', '', 'upload/images/IMG_6647.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '227', '1');
INSERT INTO `gaucon_products` VALUES ('333', '', '185', '', 'BH-35', '1000000', '0', 'bh35', '', 'upload/images/IMG_6461-copy.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '228', '1');
INSERT INTO `gaucon_products` VALUES ('334', '', '187', '', 'HB-29', '600000', '0', 'hb29', '', 'upload/images/IMG_6301-copy.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '229', '1');
INSERT INTO `gaucon_products` VALUES ('335', '', '187', '', 'HB-30', '700000', '0', 'hb30', '', 'upload/images/IMG_6679.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '230', '1');
INSERT INTO `gaucon_products` VALUES ('336', '', '187', '', 'HB-31', '300000', '150000', 'hb31', '', 'upload/images/IMG_6547.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '231', '1');
INSERT INTO `gaucon_products` VALUES ('337', '', '185', '', 'BH-36', '1000000', '0', 'bh36', '', 'upload/images/IMG_6817.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '232', '1');
INSERT INTO `gaucon_products` VALUES ('338', '', '185', '', 'BH-37', '650000', '0', 'bh37', '', 'upload/images/IMG_6825.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '1', '233', '1');
INSERT INTO `gaucon_products` VALUES ('339', '', '186', '', 'HG - 55', '850000', '0', 'hg-55', '', 'upload/images/IMG_6829.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '1', '234', '1');
INSERT INTO `gaucon_products` VALUES ('340', '', '185', '', 'BH-38', '350000', '0', 'bh38', '', 'upload/images/IMG_6900.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '1', '235', '1');
INSERT INTO `gaucon_products` VALUES ('341', '', '185', '', 'BH-39', '1000000', '0', 'bh39', '', 'upload/images/IMG_6886.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '236', '1');
INSERT INTO `gaucon_products` VALUES ('342', '', '185', '', 'BH-40', '250000', '0', 'bh40', '', 'upload/images/IMG_6923.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '237', '1');
INSERT INTO `gaucon_products` VALUES ('343', '', '185', '', 'BH-41', '1000000', '0', 'bh41', '', 'upload/images/IMG_6936.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '238', '1');
INSERT INTO `gaucon_products` VALUES ('344', '', '188', '', 'HCM-50', '0', '0', 'hcm50', '', 'upload/images/IMG_5601.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '239', '1');
INSERT INTO `gaucon_products` VALUES ('345', '', '202', '', 'HTY-03', '0', '0', 'hty03', '', 'upload/images/IMG_66881.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '240', '1');
INSERT INTO `gaucon_products` VALUES ('346', '', '202', '', 'HTY-04', '0', '0', 'hty04', '', 'upload/images/IMG_4789.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '241', '1');
INSERT INTO `gaucon_products` VALUES ('347', '', '202', '', 'HTY-05', '0', '0', 'hty05', '', 'upload/images/_DSC0034.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '242', '1');
INSERT INTO `gaucon_products` VALUES ('348', '', '202', '', 'HTY-06', '0', '0', 'hty06', '', 'upload/images/IMG_3756.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '243', '1');
INSERT INTO `gaucon_products` VALUES ('349', '', '202', '', 'HTY-07', '0', '0', 'hty07', '', 'upload/images/IMG_4594.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '244', '1');
INSERT INTO `gaucon_products` VALUES ('351', '', '202', '', 'HTY-08', '0', '0', 'hty08', '', 'upload/images/IMG_6248.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '245', '1');
INSERT INTO `gaucon_products` VALUES ('352', '', '202', '', 'HTY-09', '0', '0', 'hty09', '', 'upload/images/IMG_68251.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '246', '1');
INSERT INTO `gaucon_products` VALUES ('353', '', '202', '', 'HTY-10', '0', '0', 'hty10', '', 'upload/images/IMG_6233.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '247', '1');
INSERT INTO `gaucon_products` VALUES ('354', '', '202', '', 'HTY-11', '0', '0', 'hty11', '', 'upload/images/IMG_7699.JPG', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '248', '1');
INSERT INTO `gaucon_products` VALUES ('355', '', '185', '', 'BH-42', '1500000', '0', 'bh42', '', 'upload/images/IMG_7004.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '249', '1');
INSERT INTO `gaucon_products` VALUES ('356', '', '202', '', 'HTY-12', '650000', '0', 'hty12', '', 'upload/images/IMG_7065.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '250', '1');
INSERT INTO `gaucon_products` VALUES ('357', '', '186', '', 'HG-56', '650000', '0', 'hg56', '', 'upload/images/IMG_70651.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '251', '1');
INSERT INTO `gaucon_products` VALUES ('358', '', '185', '', 'BH-43', '500000', '0', 'bh43', '', 'upload/images/IMG_7072.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '252', '1');
INSERT INTO `gaucon_products` VALUES ('359', '', '185', '', 'BH-44', '700000', '0', 'bh44', '', 'upload/images/IMG_7077.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '253', '1');
INSERT INTO `gaucon_products` VALUES ('360', '', '189', '', 'HCB-21', '1200000', '0', 'hcb21', '', 'upload/images/IMG_6310.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '254', '1');
INSERT INTO `gaucon_products` VALUES ('361', '', '189', '', 'HCB-22', '1000000', '0', 'hcb22', '', 'upload/images/IMG_6956.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '255', '1');
INSERT INTO `gaucon_products` VALUES ('362', '', '189', '', 'HCB-23', '1000000', '0', 'hcb23', '', 'upload/images/IMG_6804-copy.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '256', '1');
INSERT INTO `gaucon_products` VALUES ('363', '', '189', '', 'HCB-24', '1000000', '90000', 'hcb24', '', 'upload/images/IMG_7337.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '257', '1');
INSERT INTO `gaucon_products` VALUES ('364', '', '204', '<p>\r\n	C&oacute; nhiều size kh&aacute;c nhau cho c&aacute;c bạn lựa chọn.</p>\r\n<ul>\r\n	<li>\r\n		Cao 18cm: 140.000 đ</li>\r\n	<li>\r\n		Cao&nbsp;25cm: 190.000 đ</li>\r\n	<li>\r\n		Cao&nbsp;35cm : 310.000 đ</li>\r\n</ul>\r\n', 'Mèo bông Doodle đáng yêu', '140000', '0', 'meo-bong-doodle-dang-yeu', '<div id=\"tab-description\">\r\n	<p style=\"text-align: center;\">\r\n		<img alt=\"meo-doodly-bong\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/05/meo-doodly-bong.jpg\" /></p>\r\n	<p style=\"text-align: center;\">\r\n		<img alt=\"meo-nhoi-bong\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/05/meo-nhoi-bong.jpg\" /></p>\r\n	<p style=\"text-align: center;\">\r\n		<img alt=\"meo-nhoi-bong-dang-yeu\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/05/meo-nhoi-bong-dang-yeu.jpg\" /></p>\r\n	<p style=\"text-align: center;\">\r\n		&nbsp; <img alt=\"meo-nhoi-bong-de-thuong\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/05/meo-nhoi-bong-de-thuong.jpg\" /></p>\r\n	<p style=\"text-align: center;\">\r\n		<img alt=\"thu-bong-meo-dooly\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/05/thu-bong-meo-dooly.jpg\" /></p>\r\n	<p style=\"text-align: center;\">\r\n		<img alt=\"thu-nhoi-bong-hinh-meo-doodly\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/05/thu-nhoi-bong-hinh-meo-doodly.jpg\" /></p>\r\n	<p style=\"text-align: center;\">\r\n		<img alt=\"thu-nhoi-bong-meo-bong\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/05/thu-nhoi-bong-meo-bong.jpg\" /></p>\r\n	<div data-id=\"3339\">\r\n		<div>\r\n			<div style=\"text-align: center;\">\r\n				&nbsp;</div>\r\n			<p style=\"text-align: center;\">\r\n				<!-- kksr-fuel --></p>\r\n		</div>\r\n		<p style=\"text-align: center;\">\r\n			<!-- kksr-stars --></p>\r\n		<div>\r\n			<div style=\"text-align: center;\" typeof=\"v:Review-aggregate\" xmlns:v=\"http://rdf.data-vocabulary.org/#\">\r\n				M&egrave;o b&ocirc;ng Doodle đ&aacute;ng y&ecirc;u 5.00/5 (100.00%) 3 votes</div>\r\n		</div>\r\n		<!-- kksr-legend --></div>\r\n	<!-- kk-star-ratings --></div>\r\n<p>\r\n	&nbsp;</p>\r\n', 'upload/images/meo-doodly-bong1.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '258', '1');
INSERT INTO `gaucon_products` VALUES ('365', '', '204', '<p>\r\n	Sản phẩm đẹp mắt, c&oacute; rất nhiều size kh&aacute;c nhau để c&aacute;c bạn tha hồ lựa chọn lu&ocirc;n nh&eacute;</p>\r\n<ul>\r\n	<li>\r\n		Size SX (35cm): 190.000</li>\r\n	<li>\r\n		Size S (45cm): 250.000</li>\r\n	<li>\r\n		Size M (60cm): 320.000</li>\r\n	<li>\r\n		Size L (80cm): 450.000</li>\r\n</ul>\r\n', 'Khỉ bông đuôi dài', '140000', '0', 'khi-bong-duoi-dai', '<div id=\"tab-description\">\r\n	<h2 style=\"text-align: center;\">\r\n		Ảnh chụp sản phẩm</h2>\r\n	<div id=\"attachment_2960\">\r\n		<p style=\"text-align: center;\">\r\n			<img alt=\"thu-nhoi-bong-hinh-khi-duoi-dai-3\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/04/thu-nhoi-bong-hinh-khi-duoi-dai-3.jpg\" /></p>\r\n		<p style=\"text-align: center;\">\r\n			Th&uacute; nhồi b&ocirc;ng h&igrave;nh khỉ b&ocirc;ng đu&ocirc;i d&agrave;i</p>\r\n	</div>\r\n	<p style=\"text-align: center;\">\r\n		<img alt=\"thu-nhoi-bong-hinh-khi-duoi-dai-2\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/04/thu-nhoi-bong-hinh-khi-duoi-dai-2.jpg\" /></p>\r\n	<p style=\"text-align: center;\">\r\n		<img alt=\"thu-nhoi-bong-hinh-khi-duoi-dai-3\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/04/thu-nhoi-bong-hinh-khi-duoi-dai-3.jpg\" /></p>\r\n	<p style=\"text-align: center;\">\r\n		<img alt=\"thu-nhoi-bong-hinh-khi-duoi-dai-4\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/04/thu-nhoi-bong-hinh-khi-duoi-dai-4.jpg\" /></p>\r\n	<p style=\"text-align: center;\">\r\n		<img alt=\"thu-nhoi-bong-hinh-khi-duoi-dai-5\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/04/thu-nhoi-bong-hinh-khi-duoi-dai-5.jpg\" /></p>\r\n	<p style=\"text-align: center;\">\r\n		<img alt=\"thu-nhoi-bong-hinh-khi-duoi-dai-6\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/04/thu-nhoi-bong-hinh-khi-duoi-dai-6.jpg\" /><br />\r\n		<noscript>&lt;/p&gt;\r\n&lt;div style=\"display:inline;\"&gt;\r\n&lt;img height=\"1\" width=\"1\" style=\"border-style:none;\" alt=\"\" src=\"//googleads.g.doubleclick.net/pagead/viewthroughconversion/948150013/?value=0&amp;amp;guid=ON&amp;amp;script=0\"/&gt;&lt;/div&gt;\r\n&lt;p&gt;</noscript></p>\r\n	<div data-id=\"2958\">\r\n		<div>\r\n			<div style=\"text-align: center;\">\r\n				&nbsp;</div>\r\n			<p style=\"text-align: center;\">\r\n				<!-- kksr-fuel --></p>\r\n		</div>\r\n		<p style=\"text-align: center;\">\r\n			<!-- kksr-stars --></p>\r\n		<div style=\"text-align: center;\">\r\n			Rate this post</div>\r\n		<!-- kksr-legend --></div>\r\n	<!-- kk-star-ratings --></div>\r\n<p>\r\n	&nbsp;</p>\r\n', 'upload/images/thu-nhoi-bong-hinh-khi-duoi-dai-2.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '259', '1');
INSERT INTO `gaucon_products` VALUES ('366', '', '204', '<p>\r\n	C&oacute; 2 size kh&aacute;c nhau cho c&aacute;c bạn lựa chọn</p>\r\n<ul>\r\n	<li>\r\n		Size S: 150.000 (35 cm)</li>\r\n	<li>\r\n		Size M: 520.000 (60 cm)</li>\r\n</ul>\r\n', 'Chó bông Stitch', '150000', '0', 'cho-bong-stitch', '<div id=\"tab-description\">\r\n	<div id=\"attachment_3316\">\r\n		<p style=\"text-align: center;\">\r\n			<img alt=\"cho-bong-stitch-noi-tieng\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/05/cho-bong-stitch-noi-tieng.jpg\" /></p>\r\n		<p style=\"text-align: center;\">\r\n			Ch&oacute; b&ocirc;ng Stitch</p>\r\n	</div>\r\n	<div id=\"attachment_3317\">\r\n		<p style=\"text-align: center;\">\r\n			<img alt=\"cho-nhoi-bong-stitch\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/05/cho-nhoi-bong-stitch.jpg\" /></p>\r\n		<p style=\"text-align: center;\">\r\n			Sản phẩm được l&agrave;m bằng chất liệu b&ocirc;ng PP cao cấp</p>\r\n	</div>\r\n	<div id=\"attachment_3314\">\r\n		<p style=\"text-align: center;\">\r\n			<img alt=\"anh-chup-phia-sau-cho-bong-stitch\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/05/anh-chup-phia-sau-cho-bong-stitch.jpg\" /></p>\r\n		<p style=\"text-align: center;\">\r\n			L&ocirc;ng đẹp, mềm v&agrave; mịn, an to&agrave;n cho sức khỏe</p>\r\n	</div>\r\n	<div id=\"attachment_3315\">\r\n		<p style=\"text-align: center;\">\r\n			<img alt=\"cho-bong-stitch\" src=\"http://www.quayeuthuong.vn/wp-content/uploads/2015/05/cho-bong-stitch.jpg\" /></p>\r\n		<p style=\"text-align: center;\">\r\n			Qu&agrave; tặng đẹp cho bạn b&egrave; v&agrave; người th&acirc;n</p>\r\n	</div>\r\n	<p style=\"text-align: center;\">\r\n		<!-- Google Code dành cho Thẻ tiếp thị lại --><br />\r\n		<!--------------------------------------------------\r\nKhông thể liên kết thẻ tiếp thị lại với thông tin nhận dạng cá nhân hay đặt thẻ tiếp thị lại trên các trang có liên quan đến danh mục nhạy cảm. Xem thêm thông tin và hướng dẫn về cách thiết lập thẻ trên: http://google.com/ads/remarketingsetup\r\n---------------------------------------------------><br />\r\n		<script type=\"text/javascript\">\r\nvar google_tag_params = {\r\ndynx_itemid: \'gau2\',\r\ndynx_itemid2: \'ninh\',\r\ndynx_pagetype: \'gaubong\',\r\ndynx_totalvalue: \'1\',\r\n};\r\n</script><br />\r\n		<script type=\"text/javascript\">\r\n/* <![CDATA[ */\r\nvar google_conversion_id = 948150013;\r\nvar google_custom_params = window.google_tag_params;\r\nvar google_remarketing_only = true;\r\n/* ]]&gt; */\r\n</script><br />\r\n		<script type=\"text/javascript\" src=\"//www.googleadservices.com/pagead/conversion.js\">\r\n</script><iframe allowtransparency=\"true\" frameborder=\"0\" height=\"13\" hspace=\"0\" marginheight=\"0\" marginwidth=\"0\" name=\"google_conversion_frame\" scrolling=\"no\" src=\"https://googleads.g.doubleclick.net/pagead/viewthroughconversion/948150013/?random=1449815655886&amp;cv=8&amp;fst=1449815655886&amp;num=1&amp;fmt=1&amp;guid=ON&amp;u_h=768&amp;u_w=1366&amp;u_ah=728&amp;u_aw=1366&amp;u_cd=24&amp;u_his=2&amp;u_tz=420&amp;u_java=false&amp;u_nplug=5&amp;u_nmime=7&amp;data=dynx_itemid%3Dgau2%3Bdynx_itemid2%3Dninh%3Bdynx_pagetype%3Dgaubong%3Bdynx_totalvalue%3D1&amp;frm=0&amp;url=http%3A//www.quayeuthuong.vn/san-pham/cho-bong-stitch/&amp;ref=http%3A//www.quayeuthuong.vn/danh-muc/gau-bong-de-thuong/\" title=\"Google conversion frame\" vspace=\"0\" width=\"300\">&amp;lt;img height=&quot;1&quot; width=&quot;1&quot; border=&quot;0&quot; alt=&quot;&quot; src=&quot;https://googleads.g.doubleclick.net/pagead/viewthroughconversion/948150013/?frame=0&amp;amp;random=1449815655886&amp;amp;cv=8&amp;amp;fst=1449815655886&amp;amp;num=1&amp;amp;fmt=1&amp;amp;guid=ON&amp;amp;u_h=768&amp;amp;u_w=1366&amp;amp;u_ah=728&amp;amp;u_aw=1366&amp;amp;u_cd=24&amp;amp;u_his=2&amp;amp;u_tz=420&amp;amp;u_java=false&amp;amp;u_nplug=5&amp;amp;u_nmime=7&amp;amp;data=dynx_itemid%3Dgau2%3Bdynx_itemid2%3Dninh%3Bdynx_pagetype%3Dgaubong%3Bdynx_totalvalue%3D1&amp;amp;frm=0&amp;amp;url=http%3A//www.quayeuthuong.vn/san-pham/cho-bong-stitch/&amp;amp;ref=http%3A//www.quayeuthuong.vn/danh-muc/gau-bong-de-thuong/&quot; /&amp;gt;</iframe><br />\r\n		<noscript>&lt;/p&gt;\r\n&lt;div style=\"display:inline;\"&gt;\r\n&lt;img height=\"1\" width=\"1\" style=\"border-style:none;\" alt=\"\" src=\"//googleads.g.doubleclick.net/pagead/viewthroughconversion/948150013/?value=0&amp;amp;guid=ON&amp;amp;script=0\"/&gt;\r\n&lt;/div&gt;\r\n&lt;p&gt;</noscript></p>\r\n	<div data-id=\"3313\">\r\n		<div>\r\n			<div style=\"text-align: center;\">\r\n				&nbsp;</div>\r\n			<p style=\"text-align: center;\">\r\n				<!-- kksr-fuel --></p>\r\n		</div>\r\n		<p style=\"text-align: center;\">\r\n			<!-- kksr-stars --></p>\r\n		<div>\r\n			<div style=\"text-align: center;\" typeof=\"v:Review-aggregate\" xmlns:v=\"http://rdf.data-vocabulary.org/#\">\r\n				Ch&oacute; b&ocirc;ng Stitch 5.00/5 (100.00%) 2 votes</div>\r\n		</div>\r\n		<p style=\"text-align: center;\">\r\n			<!-- kksr-legend --></p>\r\n	</div>\r\n	<p style=\"text-align: center;\">\r\n		<!-- kk-star-ratings --></p>\r\n</div>\r\n<p style=\"text-align: center;\">\r\n	&nbsp;</p>\r\n', 'upload/images/cho-bong-stitch.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '260', '1');
INSERT INTO `gaucon_products` VALUES ('367', '', '205', '<div>\r\n	<p>\r\n		Hộp t&aacute;o Envy nhỏ xinh với 15 quả t&aacute;o Envy size 135 khoảng 7 quả/kg, vừa đẹp, sang trọng rất ph&ugrave; hợp l&agrave;m qu&agrave; biếu, qu&agrave; tặng. T&aacute;o Envy size rất được thị trường Ch&acirc;u &Acirc;u ưa chuộng v&igrave; vừa ăn, độ gi&ograve;n, ngọt v&agrave; m&ugrave;i thơm lại cao hơn hẳn những quả size to.</p>\r\n	<p>\r\n		Lần đầu ti&ecirc;n c&oacute; mặt tại Việt Nam v&agrave; được độc quyền bởi Lu&ocirc;n Tươi Sạch, những hộp t&aacute;o đ&atilde; tạo l&ecirc;n cơn sốt mạnh mẽ trong người ti&ecirc;u d&ugrave;ng H&agrave; Th&agrave;nh v&agrave; c&aacute;c tỉnh l&acirc;n cận.</p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>\r\n', 'Hộp táo Envy', '180000', '0', 'hop-tao-envy', '<p>\r\n	- Với mỗi 100.000đ trong đơn h&agrave;ng, qu&yacute; kh&aacute;ch được tặng 1 điểm v&agrave;o thẻ kh&aacute;ch h&agrave;ng</p>\r\n<p>\r\n	- Qu&yacute; kh&aacute;ch t&iacute;ch lũy đủ &nbsp;50 điểm sẽ nhận được 01 Voucher trị gi&aacute; 50.000đ để trừ v&agrave;o gi&aacute; trị đơn h&agrave;ng khi mua h&agrave;ng, Voucher c&oacute; thể được sử dụng tr&ecirc;n to&agrave;n bộ c&aacute;c cửa h&agrave;ng v&agrave; kh&ocirc;ng quan trọng ai l&agrave; người sử dụng.</p>\r\n<p>\r\n	- Khi qu&yacute; kh&aacute;ch mua h&agrave;ng t&iacute;ch lũy gi&aacute; trị mua h&agrave;ng trong th&aacute;ng từ 7.000.000đ v&agrave; đến 10.000.000đ, qu&yacute; kh&aacute;ch sẽ được giảm 5% cho lần mua h&agrave;ng tiếp theo</p>\r\n<p>\r\n	- Khi qu&yacute; kh&aacute;ch t&iacute;ch lũy gi&aacute; trị mua h&agrave;ng trong th&aacute;ng từ 10.000.000đ trở l&ecirc;n, qu&yacute; kh&aacute;ch sẽ được giảm 10% cho lần mua h&agrave;ng tiếp theo</p>\r\n', 'upload/images/1444900394hop-envy2.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '261', '1');
INSERT INTO `gaucon_products` VALUES ('368', '', '205', '<p>\r\n	- &nbsp; Cam Midknight Nam Phi</p>\r\n<p>\r\n	- &nbsp; Giỏ v&agrave; phụ kiện</p>\r\n', 'Hộp Cam Midknight', '200000', '0', 'hop-cam-midknight', '', 'upload/images/1447474626hop-cam-20-11.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '262', '1');
INSERT INTO `gaucon_products` VALUES ('369', '', '205', '<p>\r\n	Chi tiết c&aacute;c loại quả trong&nbsp;giỏ qu&agrave; tặng&nbsp;LTS-Đ02:</p>\r\n<p>\r\n	- &nbsp; Kiwi Xanh New Zealand</p>\r\n<p>\r\n	- &nbsp; Cam v&agrave;ng Mỹ</p>\r\n<p>\r\n	- &nbsp; T&aacute;o Envy New Zealand</p>\r\n<p>\r\n	- &nbsp; T&aacute;o Xanh New Zealand</p>\r\n<p>\r\n	- &nbsp; Nho Xanh Mỹ</p>\r\n', 'Giỏ Trái Cây LTS -Đ02', '250000', '0', 'gio-trai-cay-lts-d02', '', 'upload/images/14286495751413918622gio-trai-cay-td02.jpg', '', '', '', '0', '0', '0000-00-00 00:00:00', '0', '0', '263', '1');

-- ----------------------------
-- Table structure for `gaucon_products_age`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_products_age`;
CREATE TABLE `gaucon_products_age` (
  `id` int(11) NOT NULL auto_increment,
  `id_product` int(11) NOT NULL,
  `id_age` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=293 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_products_age
-- ----------------------------
INSERT INTO `gaucon_products_age` VALUES ('15', '79', '10', '0');
INSERT INTO `gaucon_products_age` VALUES ('17', '77', '9', '1');
INSERT INTO `gaucon_products_age` VALUES ('16', '77', '1', '0');
INSERT INTO `gaucon_products_age` VALUES ('13', '78', '10', '0');
INSERT INTO `gaucon_products_age` VALUES ('285', '80', '1', '0');
INSERT INTO `gaucon_products_age` VALUES ('284', '81', '3', '0');
INSERT INTO `gaucon_products_age` VALUES ('283', '82', '4', '0');
INSERT INTO `gaucon_products_age` VALUES ('282', '83', '6', '0');
INSERT INTO `gaucon_products_age` VALUES ('291', '84', '7', '0');
INSERT INTO `gaucon_products_age` VALUES ('292', '85', '10', '0');
INSERT INTO `gaucon_products_age` VALUES ('288', '86', '7', '0');
INSERT INTO `gaucon_products_age` VALUES ('278', '87', '6', '0');
INSERT INTO `gaucon_products_age` VALUES ('279', '88', '4', '0');
INSERT INTO `gaucon_products_age` VALUES ('280', '89', '3', '0');
INSERT INTO `gaucon_products_age` VALUES ('281', '90', '1', '0');
INSERT INTO `gaucon_products_age` VALUES ('290', '91', '10', '0');
INSERT INTO `gaucon_products_age` VALUES ('289', '92', '11', '0');
INSERT INTO `gaucon_products_age` VALUES ('287', '93', '10', '0');
INSERT INTO `gaucon_products_age` VALUES ('277', '94', '2', '0');
INSERT INTO `gaucon_products_age` VALUES ('276', '95', '4', '0');
INSERT INTO `gaucon_products_age` VALUES ('275', '96', '5', '0');
INSERT INTO `gaucon_products_age` VALUES ('274', '97', '7', '0');

-- ----------------------------
-- Table structure for `gaucon_products_image`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_products_image`;
CREATE TABLE `gaucon_products_image` (
  `id` int(11) NOT NULL auto_increment,
  `id_product` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=581 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_products_image
-- ----------------------------
INSERT INTO `gaucon_products_image` VALUES ('477', '201', 'IMG_2789.JPG', '5');
INSERT INTO `gaucon_products_image` VALUES ('462', '176', 'IMG_1892.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('461', '175', 'IMG_1882.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('460', '166', 'IMG_1009.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('465', '98', 'hoa-cuoi-ket-tu-hoa-hong-mau-xanh-la1.jpg', '2');
INSERT INTO `gaucon_products_image` VALUES ('476', '201', 'IMG_2760.JPG', '4');
INSERT INTO `gaucon_products_image` VALUES ('475', '201', 'IMG_2829.JPG', '3');
INSERT INTO `gaucon_products_image` VALUES ('474', '201', 'IMG_2828.JPG', '2');
INSERT INTO `gaucon_products_image` VALUES ('473', '201', 'IMG_2810.JPG', '1');
INSERT INTO `gaucon_products_image` VALUES ('416', '101', 'tu21.jpg', '2');
INSERT INTO `gaucon_products_image` VALUES ('415', '101', 'vvfi13703405221.jpg', '1');
INSERT INTO `gaucon_products_image` VALUES ('414', '101', '1-2-8050-14097196291.jpg', '0');
INSERT INTO `gaucon_products_image` VALUES ('428', '102', '1-2-8050-14097196292.jpg', '2');
INSERT INTO `gaucon_products_image` VALUES ('426', '102', 'cong-hoa-cuoi-dep-101.jpg', '0');
INSERT INTO `gaucon_products_image` VALUES ('427', '102', 'vtk13613676292.jpg', '1');
INSERT INTO `gaucon_products_image` VALUES ('434', '103', '497665427439.jpg', '1');
INSERT INTO `gaucon_products_image` VALUES ('433', '103', 'ctr2.jpg', '0');
INSERT INTO `gaucon_products_image` VALUES ('435', '103', 'IMG20150201113835.jpg', '2');
INSERT INTO `gaucon_products_image` VALUES ('464', '98', 'hinh-anh-hoa-hong-5.jpg', '1');
INSERT INTO `gaucon_products_image` VALUES ('463', '98', 'hoa-cuoi-ket-tu-hoa-hong-mau-xanh-la.jpg', '0');
INSERT INTO `gaucon_products_image` VALUES ('472', '201', 'IMG_2778.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('479', '219', 'IMG_0895.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('481', '232', 'IMG_5061.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('508', '236', 'IMG_5579.JPG', '1');
INSERT INTO `gaucon_products_image` VALUES ('507', '236', 'IMG_5575.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('506', '239', 'IMG_5624.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('572', '240', 'IMG_5627.JPG', '1');
INSERT INTO `gaucon_products_image` VALUES ('571', '240', 'IMG_5626.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('502', '241', 'IMG_5617.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('501', '249', 'IMG_5637.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('503', '241', 'IMG_5615.JPG', '1');
INSERT INTO `gaucon_products_image` VALUES ('500', '266', 'IMG_5718.JPG', '1');
INSERT INTO `gaucon_products_image` VALUES ('499', '266', 'IMG_5720.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('498', '272', 'IMG_5726.JPG', '1');
INSERT INTO `gaucon_products_image` VALUES ('497', '272', 'IMG_5727.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('509', '277', 'IMG_5387.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('557', '278', 'IMG_5320.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('516', '282', 'IMG_5176.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('528', '285', 'IMG_5817.JPG', '1');
INSERT INTO `gaucon_products_image` VALUES ('527', '285', 'IMG_5816.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('517', '294', 'IMG_5630.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('518', '295', 'IMG_5753.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('547', '302', 'IMG_59231.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('534', '303', 'IMG_5916.JPG', '1');
INSERT INTO `gaucon_products_image` VALUES ('533', '303', 'IMG_5915.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('539', '304', 'IMG_5935.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('546', '305', 'IMG_5943.JPG', '1');
INSERT INTO `gaucon_products_image` VALUES ('545', '305', 'IMG_5938.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('548', '302', 'IMG_5922.JPG', '1');
INSERT INTO `gaucon_products_image` VALUES ('540', '304', 'IMG_5936.JPG', '1');
INSERT INTO `gaucon_products_image` VALUES ('544', '306', 'IMG_5949.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('563', '327', 'IMG_6343.jpg', '0');
INSERT INTO `gaucon_products_image` VALUES ('549', '308', 'IMG_6144.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('553', '309', 'IMG_6140.JPG', '1');
INSERT INTO `gaucon_products_image` VALUES ('552', '309', 'IMG_61411.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('554', '310', 'IMG_6102.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('555', '310', 'IMG_6105.JPG', '1');
INSERT INTO `gaucon_products_image` VALUES ('556', '313', 'IMG_6092.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('560', '324', 'IMG_62401.JPG', '0');
INSERT INTO `gaucon_products_image` VALUES ('561', '324', 'IMG_6249.JPG', '1');
INSERT INTO `gaucon_products_image` VALUES ('570', '339', 'IMG_68291.jpg', '1');
INSERT INTO `gaucon_products_image` VALUES ('569', '339', 'IMG_6831.jpg', '0');
INSERT INTO `gaucon_products_image` VALUES ('567', '355', 'IMG_7006.jpg', '0');
INSERT INTO `gaucon_products_image` VALUES ('568', '359', 'IMG_7080.jpg', '0');
INSERT INTO `gaucon_products_image` VALUES ('576', '369', '14286495751413918622gio-trai-cay-td021.jpg', '0');
INSERT INTO `gaucon_products_image` VALUES ('577', '369', 'banh-kem1.jpg', '1');
INSERT INTO `gaucon_products_image` VALUES ('578', '369', 'banner-ads.jpg', '2');
INSERT INTO `gaucon_products_image` VALUES ('579', '369', 'banh-kem2.jpg', '3');
INSERT INTO `gaucon_products_image` VALUES ('580', '369', 'trai-cay1.jpg', '4');

-- ----------------------------
-- Table structure for `gaucon_project_category`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_project_category`;
CREATE TABLE `gaucon_project_category` (
  `id` int(11) NOT NULL auto_increment,
  `avatar` varchar(255) NOT NULL,
  `banner` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `keyword` text NOT NULL,
  `parent` int(11) NOT NULL,
  `ordering` tinyint(4) NOT NULL,
  `is_title` tinyint(4) NOT NULL,
  `is_desc` tinyint(4) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=207 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_project_category
-- ----------------------------
INSERT INTO `gaucon_project_category` VALUES ('185', 'upload/images/IMG_6400.jpg', '', 'Bình hoa', 'binh-hoa', '', '', '182', '3', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('186', 'upload/images/IMG_7062.jpg', '', 'Giỏ hoa', 'gio-hoa', '', '', '182', '2', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('184', 'access/image/filemanager/upload_photo.png', '', 'Trang trí tiệc cưới', 'trang-tri-tiec-cuoi', '', '', '0', '3', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('183', 'access/image/filemanager/upload_photo.png', '', 'Trái cây', 'trai-cay', '', '', '0', '2', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('182', 'access/image/filemanager/upload_photo.png', '', 'Hoa tươi', 'hoa-tuoi', '', '', '0', '1', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('187', 'upload/images/IMG_6219.JPG', '', 'Hoa bó', 'hoa-bo', '', '', '182', '1', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('188', 'upload/images/IMG_26141.JPG', '', 'Kệ hoa chúc mừng', 'ke-hoa-chuc-mung', '', '', '182', '4', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('189', 'upload/images/IMG_0100.JPG', '', 'Kệ hoa chia buồn', 'ke-hoa-chia-buon', '', '', '182', '5', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('190', 'upload/images/IMG_6129.JPG', '', 'Giỏ hoa tươi và trái cây', 'gio-hoa-tuoi-va-trai-cay', '', '', '182', '6', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('191', 'upload/images/IMG_3432.JPG', '', 'Lan hồ điệp chậu', 'lan-ho-diep-chau', '', '', '182', '7', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('192', 'upload/images/11800215_886574224745400_7185919671764066386_n.jpg', '', 'Hoa trang trí sự kiện', 'hoa-trang-tri-su-kien', '', '', '182', '8', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('193', 'upload/images/bo.jpg', '', 'Trái cây Việt Nam', 'trai-cay-viet-nam', '', '', '183', '1', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('194', 'upload/images/chery.jpg', '', 'Trái cây nhập khẩu', 'trai-cay-nhap-khau', '', '', '183', '2', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('195', 'upload/images/gio-qua-trai-cay.jpg', '', 'Giỏ quà trái cây', 'gio-qua-trai-cay', '', '', '183', '3', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('196', 'upload/images/11200880_707175536076272_7560412946224851554_n.jpg', '', 'Hoa cầm tay cô dâu', 'hoa-cam-tay-co-dau', '', '', '184', '1', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('197', 'upload/images/xe-hoa.png', '', 'Xe hoa', 'xe-hoa', '', '', '184', '2', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('198', 'upload/images/cong-hoa-cuoi.jpg', '', 'Cổng hoa cưới', 'cong-hoa-cuoi', '', '', '184', '3', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('199', 'upload/images/phong-hinh-luu-niem.png', '', 'Phông hình lưu niệm', 'phong-hinh-luu-niem', '', '', '184', '4', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('200', 'upload/images/hoa-ban-tiec.png', '', 'Hoa bàn tiệc', 'hoa-ban-tiec', '', '', '184', '5', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('201', 'upload/images/album-mau-tiec-cuoi2.jpg', '', 'Album mẫu', 'album-mau', '', '', '184', '9', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('202', 'upload/images/IMG_4261.JPG', '', 'HOA TÌNH YÊU', 'hoa-tinh-yeu', '', '', '182', '10', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('203', 'access/image/filemanager/upload_photo.png', '', 'Quà tặng kèm', 'qua-tang-kem', '', '', '0', '11', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('204', 'upload/images/gau-bong.jpg', '', 'Gấu bông', 'gau-bong', '', '', '203', '12', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('205', 'upload/images/trai-cay.jpg', '', 'Trái cây', 'qua-tang-trai-cay', '', '', '203', '13', '0', '0', '', '1');
INSERT INTO `gaucon_project_category` VALUES ('206', 'upload/images/banh-kem.jpg', '', 'Bánh kem', 'banh-kem', '', '', '203', '14', '0', '0', '', '1');

-- ----------------------------
-- Table structure for `gaucon_shortcode`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_shortcode`;
CREATE TABLE `gaucon_shortcode` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `type` varchar(255) NOT NULL,
  `is_title` tinyint(4) NOT NULL,
  `is_desc` tinyint(4) NOT NULL,
  `is_link` tinyint(4) NOT NULL,
  `ordering` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=159 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_shortcode
-- ----------------------------
INSERT INTO `gaucon_shortcode` VALUES ('157', 'Trái cây', 'trai-cay.html', 'upload/images/11203098_525653837572941_4944551662623084391_n.jpg', 'Cung cấp các loại trái cây tươi ngon, sạch, sẵn sàng đáp ứng nhu cầu của khách hàng. Trái cây tại cửa hàng nhập khẩu từ nước ngoài và lấy trực tiếp từ các vùng miền của Việt Nam nên nguồn gốc xuất xứ rõ ràng, an toàn.', '', '0', '0', '0', '2', '1');
INSERT INTO `gaucon_shortcode` VALUES ('156', 'Hoa tươi', 'hoa-tuoi.html', 'upload/images/gau-con-461x223-big.jpg', 'Gấu Con cung cấp đa dạng các loại hoa hiện đang thịnh hành, luôn tươi mới và đảm bảo chất lượng, như các dòng hoa hồng đủ màu sắc, hoa hướng dương, bách hợp, cẩm chướng, hoa lan, hoa huệ, mimosa… ', '', '0', '0', '0', '3', '1');
INSERT INTO `gaucon_shortcode` VALUES ('158', 'Chậu Lan hồ điệp', 'hoa-tuoi/lan-ho-diep-chau.html', 'upload/images/ctr.jpg', 'Cửa hàng hoa tươi Gấu Con flower sẽ tư vấn cho quý khách hàng những bó hoa cưới, hoa cầm tay cô dâu, hoa trang trí', '', '0', '0', '0', '1', '1');

-- ----------------------------
-- Table structure for `gaucon_slide`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_slide`;
CREATE TABLE `gaucon_slide` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `type` varchar(255) NOT NULL,
  `is_title` tinyint(4) NOT NULL,
  `is_desc` tinyint(4) NOT NULL,
  `is_link` tinyint(4) NOT NULL,
  `ordering` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=167 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_slide
-- ----------------------------
INSERT INTO `gaucon_slide` VALUES ('165', 'Linh Tây', '#', 'upload/slider/kha-van-can-linh-tay-2.jpg', '', '', '0', '0', '0', '3', '1');
INSERT INTO `gaucon_slide` VALUES ('166', 'Trường Lưu', '#', 'upload/slider/truong-luu.jpg', '', '', '0', '0', '0', '4', '1');
INSERT INTO `gaucon_slide` VALUES ('164', 'Đường 6 Hiệp Bình Phước', '#', 'upload/slider/duong-so-6-hiep-binh-phuoc.jpg', '', '', '0', '0', '0', '2', '1');
INSERT INTO `gaucon_slide` VALUES ('163', 'Đặng Văn Bi', '#', 'upload/slider/163-Dang-Van-Bi.jpg', '', '', '0', '0', '0', '1', '1');

-- ----------------------------
-- Table structure for `gaucon_slider_about`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_slider_about`;
CREATE TABLE `gaucon_slider_about` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `type` varchar(255) NOT NULL,
  `is_title` tinyint(4) NOT NULL,
  `is_desc` tinyint(4) NOT NULL,
  `is_link` tinyint(4) NOT NULL,
  `ordering` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=164 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_slider_about
-- ----------------------------
INSERT INTO `gaucon_slider_about` VALUES ('162', 'Hình 4', '', 'upload/images/IMG_54312.JPG', '', '', '0', '0', '0', '3', '1');
INSERT INTO `gaucon_slider_about` VALUES ('163', 'gauconflower.com', '', 'upload/images/gau-con-378x237-big.jpg', '', '', '0', '0', '0', '5', '1');
INSERT INTO `gaucon_slider_about` VALUES ('159', 'Hình 1', '', 'upload/images/gau-con.jpg', '', '', '0', '0', '0', '4', '1');
INSERT INTO `gaucon_slider_about` VALUES ('160', 'Hinh 2', '', 'upload/images/gau-con-461x223-big1.jpg', '', '', '0', '0', '0', '2', '1');
INSERT INTO `gaucon_slider_about` VALUES ('161', 'Hình 3', '', 'upload/images/IMG_5889.JPG', '', '', '0', '0', '0', '1', '1');

-- ----------------------------
-- Table structure for `gaucon_slider_position`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_slider_position`;
CREATE TABLE `gaucon_slider_position` (
  `id` int(11) NOT NULL auto_increment,
  `controller` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_slider_position
-- ----------------------------
INSERT INTO `gaucon_slider_position` VALUES ('20', 'trung-tam-r-d', 'Giới thiệu >> Trung tâm R & D');
INSERT INTO `gaucon_slider_position` VALUES ('19', 'nha-may', 'Giới thiệu >> Nhà máy');
INSERT INTO `gaucon_slider_position` VALUES ('18', 'lich-su', 'Giới thiệu >> Lịch sử');
INSERT INTO `gaucon_slider_position` VALUES ('17', 've-tap-doan', 'Giới thiệu >> Về tập đoàn');
INSERT INTO `gaucon_slider_position` VALUES ('16', 'homepage', 'Trang chủ');
INSERT INTO `gaucon_slider_position` VALUES ('21', 'namyang-tai-viet-nam', 'Giới thiệu >> NamYang tại Việt Nam');
INSERT INTO `gaucon_slider_position` VALUES ('22', 'san-pham', 'Sản phẩm');
INSERT INTO `gaucon_slider_position` VALUES ('23', 'xo-gt', 'Sản phẩm >> XO GT');
INSERT INTO `gaucon_slider_position` VALUES ('24', 'xo-care', 'Sản phẩm >> Xo Care');
INSERT INTO `gaucon_slider_position` VALUES ('25', 'i-am-mother', 'Sản phẩm >> i-am-mother');
INSERT INTO `gaucon_slider_position` VALUES ('26', 'star-gold', 'Sản phẩm >> star-gold');
INSERT INTO `gaucon_slider_position` VALUES ('27', 'me-va-be', 'Mẹ & Bé');
INSERT INTO `gaucon_slider_position` VALUES ('28', 'nuoi-con', 'Mẹ & Bé >> Nuôi con');
INSERT INTO `gaucon_slider_position` VALUES ('29', 'mang-thai', 'Mẹ & Bé >> Mang thai');
INSERT INTO `gaucon_slider_position` VALUES ('30', 'day-con', 'Mẹ & Bé >> Dạy con');
INSERT INTO `gaucon_slider_position` VALUES ('31', 'bac-si-namyang', 'Bác sĩ Nam Yang');
INSERT INTO `gaucon_slider_position` VALUES ('32', 'tu-van-san-pham', 'Bác sĩ Nam Yang >> Tư vấn sản phẩm');
INSERT INTO `gaucon_slider_position` VALUES ('33', 'tu-van-suc-khoe', 'Bác sĩ Nam Yang >> Tư vấn sức khỏe');
INSERT INTO `gaucon_slider_position` VALUES ('34', 'khuyen-mai-va-su-kien', 'Khuyến mãi & Sự kiện');
INSERT INTO `gaucon_slider_position` VALUES ('35', 'chinh-sach-va-quy-dinh', 'Chính sách và quy định');
INSERT INTO `gaucon_slider_position` VALUES ('36', 'bao-mat-thong-tin', 'Bảo mật thông tin');
INSERT INTO `gaucon_slider_position` VALUES ('37', 'so-do-trang', 'Sơ đồ trang');

-- ----------------------------
-- Table structure for `gaucon_user`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_user`;
CREATE TABLE `gaucon_user` (
  `id` int(11) NOT NULL auto_increment,
  `email` varchar(255) collate utf8_unicode_ci NOT NULL,
  `username` varchar(255) collate utf8_unicode_ci NOT NULL,
  `password` varchar(255) collate utf8_unicode_ci NOT NULL,
  `name` varchar(255) collate utf8_unicode_ci NOT NULL,
  `sex` tinyint(4) NOT NULL,
  `phone` varchar(255) collate utf8_unicode_ci NOT NULL,
  `yahoo` varchar(255) collate utf8_unicode_ci NOT NULL,
  `birthday` int(11) NOT NULL,
  `add_date` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  `is_admin` tinyint(1) NOT NULL,
  `level` tinyint(4) NOT NULL,
  `ordering` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of gaucon_user
-- ----------------------------
INSERT INTO `gaucon_user` VALUES ('1', 'duoc.huynh@dpassion.com', 'admin', '202cb962ac59075b964b07152d234b70', 'Adminstrator', '1', '0903627890', 'huynhvanduoc034', '1989', '2012-11-23 11:21:35', '1', '1', '3', '1');

-- ----------------------------
-- Table structure for `gaucon_user_permission_group`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_user_permission_group`;
CREATE TABLE `gaucon_user_permission_group` (
  `id_user` int(11) NOT NULL,
  `controller` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_user_permission_group
-- ----------------------------
INSERT INTO `gaucon_user_permission_group` VALUES ('2', 'slide');
INSERT INTO `gaucon_user_permission_group` VALUES ('3', 'news');
INSERT INTO `gaucon_user_permission_group` VALUES ('3', 'news_category');
INSERT INTO `gaucon_user_permission_group` VALUES ('3', 'comment');
INSERT INTO `gaucon_user_permission_group` VALUES ('3', 'staticbaiviet');
INSERT INTO `gaucon_user_permission_group` VALUES ('4', 'news');
INSERT INTO `gaucon_user_permission_group` VALUES ('4', 'news_category');
INSERT INTO `gaucon_user_permission_group` VALUES ('4', 'comment');
INSERT INTO `gaucon_user_permission_group` VALUES ('4', 'staticbaiviet');
INSERT INTO `gaucon_user_permission_group` VALUES ('5', 'news');
INSERT INTO `gaucon_user_permission_group` VALUES ('5', 'news_category');
INSERT INTO `gaucon_user_permission_group` VALUES ('5', 'staticbaiviet');

-- ----------------------------
-- Table structure for `gaucon_video`
-- ----------------------------
DROP TABLE IF EXISTS `gaucon_video`;
CREATE TABLE `gaucon_video` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gaucon_video
-- ----------------------------
INSERT INTO `gaucon_video` VALUES ('4', 'XO - Vị ngon của bột sữa nguyên chất', '', 'https://www.youtube.com/watch?v=7xrGO2ITzJw', '2', '1');
INSERT INTO `gaucon_video` VALUES ('5', 'Majesty XO Care', '', 'https://www.youtube.com/watch?v=SaClJyYqlRE', '3', '1');
INSERT INTO `gaucon_video` VALUES ('3', 'Star Gold - Ngôi Sao Của Mẹ', '', 'https://www.youtube.com/watch?v=uhdwyXB1vDY', '1', '1');
INSERT INTO `gaucon_video` VALUES ('6', 'Star Gold - dinh dưỡng cho bé', '', 'https://www.youtube.com/watch?v=FRSIEDWzgrY', '4', '1');
INSERT INTO `gaucon_video` VALUES ('7', 'Imperial Mon XO', '', 'https://www.youtube.com/watch?v=GG3Diki79As', '5', '1');
INSERT INTO `gaucon_video` VALUES ('8', 'Bài tập thể dục dành cho phụ nữ mang thai', '', 'https://www.youtube.com/watch?v=LF9liNKhkV4', '6', '1');
INSERT INTO `gaucon_video` VALUES ('9', 'Sự thật về sữa tốt!', '', 'https://www.youtube.com/watch?v=RhH35qHPjJY', '7', '1');
INSERT INTO `gaucon_video` VALUES ('10', 'Luyện tập Suối Nguồn Tươi Trẻ Cùng HH Ngô Phương Lan', '', 'https://www.youtube.com/watch?v=JS9TgrjsW38', '8', '1');
INSERT INTO `gaucon_video` VALUES ('11', 'Star Gold - Sữa ngoại giá nội', '', 'https://www.youtube.com/watch?v=W2XGbrtNX7M', '9', '1');
