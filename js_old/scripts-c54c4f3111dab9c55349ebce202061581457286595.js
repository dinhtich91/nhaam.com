/*! jQuery v1.9.1 | (c) 2005, 2012 jQuery Foundation, Inc. | jquery.org/license
*/(function(e,t){var n,r,i=typeof t,o=e.document,a=e.location,s=e.jQuery,u=e.$,l={},c=[],p="1.9.1",f=c.concat,d=c.push,h=c.slice,g=c.indexOf,m=l.toString,y=l.hasOwnProperty,v=p.trim,b=function(e,t){return new b.fn.init(e,t,r)},x=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,w=/\S+/g,T=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,N=/^(?:(<[\w\W]+>)[^>]*|#([\w-]*))$/,C=/^<(\w+)\s*\/?>(?:<\/\1>|)$/,k=/^[\],:{}\s]*$/,E=/(?:^|:|,)(?:\s*\[)+/g,S=/\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,A=/"[^"\\\r\n]*"|true|false|null|-?(?:\d+\.|)\d+(?:[eE][+-]?\d+|)/g,j=/^-ms-/,D=/-([\da-z])/gi,L=function(e,t){return t.toUpperCase()},H=function(e){(o.addEventListener||"load"===e.type||"complete"===o.readyState)&&(q(),b.ready())},q=function(){o.addEventListener?(o.removeEventListener("DOMContentLoaded",H,!1),e.removeEventListener("load",H,!1)):(o.detachEvent("onreadystatechange",H),e.detachEvent("onload",H))};b.fn=b.prototype={jquery:p,constructor:b,init:function(e,n,r){var i,a;if(!e)return this;if("string"==typeof e){if(i="<"===e.charAt(0)&&">"===e.charAt(e.length-1)&&e.length>=3?[null,e,null]:N.exec(e),!i||!i[1]&&n)return!n||n.jquery?(n||r).find(e):this.constructor(n).find(e);if(i[1]){if(n=n instanceof b?n[0]:n,b.merge(this,b.parseHTML(i[1],n&&n.nodeType?n.ownerDocument||n:o,!0)),C.test(i[1])&&b.isPlainObject(n))for(i in n)b.isFunction(this[i])?this[i](n[i]):this.attr(i,n[i]);return this}if(a=o.getElementById(i[2]),a&&a.parentNode){if(a.id!==i[2])return r.find(e);this.length=1,this[0]=a}return this.context=o,this.selector=e,this}return e.nodeType?(this.context=this[0]=e,this.length=1,this):b.isFunction(e)?r.ready(e):(e.selector!==t&&(this.selector=e.selector,this.context=e.context),b.makeArray(e,this))},selector:"",length:0,size:function(){return this.length},toArray:function(){return h.call(this)},get:function(e){return null==e?this.toArray():0>e?this[this.length+e]:this[e]},pushStack:function(e){var t=b.merge(this.constructor(),e);return t.prevObject=this,t.context=this.context,t},each:function(e,t){return b.each(this,e,t)},ready:function(e){return b.ready.promise().done(e),this},slice:function(){return this.pushStack(h.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(e){var t=this.length,n=+e+(0>e?t:0);return this.pushStack(n>=0&&t>n?[this[n]]:[])},map:function(e){return this.pushStack(b.map(this,function(t,n){return e.call(t,n,t)}))},end:function(){return this.prevObject||this.constructor(null)},push:d,sort:[].sort,splice:[].splice},b.fn.init.prototype=b.fn,b.extend=b.fn.extend=function(){var e,n,r,i,o,a,s=arguments[0]||{},u=1,l=arguments.length,c=!1;for("boolean"==typeof s&&(c=s,s=arguments[1]||{},u=2),"object"==typeof s||b.isFunction(s)||(s={}),l===u&&(s=this,--u);l>u;u++)if(null!=(o=arguments[u]))for(i in o)e=s[i],r=o[i],s!==r&&(c&&r&&(b.isPlainObject(r)||(n=b.isArray(r)))?(n?(n=!1,a=e&&b.isArray(e)?e:[]):a=e&&b.isPlainObject(e)?e:{},s[i]=b.extend(c,a,r)):r!==t&&(s[i]=r));return s},b.extend({noConflict:function(t){return e.$===b&&(e.$=u),t&&e.jQuery===b&&(e.jQuery=s),b},isReady:!1,readyWait:1,holdReady:function(e){e?b.readyWait++:b.ready(!0)},ready:function(e){if(e===!0?!--b.readyWait:!b.isReady){if(!o.body)return setTimeout(b.ready);b.isReady=!0,e!==!0&&--b.readyWait>0||(n.resolveWith(o,[b]),b.fn.trigger&&b(o).trigger("ready").off("ready"))}},isFunction:function(e){return"function"===b.type(e)},isArray:Array.isArray||function(e){return"array"===b.type(e)},isWindow:function(e){return null!=e&&e==e.window},isNumeric:function(e){return!isNaN(parseFloat(e))&&isFinite(e)},type:function(e){return null==e?e+"":"object"==typeof e||"function"==typeof e?l[m.call(e)]||"object":typeof e},isPlainObject:function(e){if(!e||"object"!==b.type(e)||e.nodeType||b.isWindow(e))return!1;try{if(e.constructor&&!y.call(e,"constructor")&&!y.call(e.constructor.prototype,"isPrototypeOf"))return!1}catch(n){return!1}var r;for(r in e);return r===t||y.call(e,r)},isEmptyObject:function(e){var t;for(t in e)return!1;return!0},error:function(e){throw Error(e)},parseHTML:function(e,t,n){if(!e||"string"!=typeof e)return null;"boolean"==typeof t&&(n=t,t=!1),t=t||o;var r=C.exec(e),i=!n&&[];return r?[t.createElement(r[1])]:(r=b.buildFragment([e],t,i),i&&b(i).remove(),b.merge([],r.childNodes))},parseJSON:function(n){return e.JSON&&e.JSON.parse?e.JSON.parse(n):null===n?n:"string"==typeof n&&(n=b.trim(n),n&&k.test(n.replace(S,"@").replace(A,"]").replace(E,"")))?Function("return "+n)():(b.error("Invalid JSON: "+n),t)},parseXML:function(n){var r,i;if(!n||"string"!=typeof n)return null;try{e.DOMParser?(i=new DOMParser,r=i.parseFromString(n,"text/xml")):(r=new ActiveXObject("Microsoft.XMLDOM"),r.async="false",r.loadXML(n))}catch(o){r=t}return r&&r.documentElement&&!r.getElementsByTagName("parsererror").length||b.error("Invalid XML: "+n),r},noop:function(){},globalEval:function(t){t&&b.trim(t)&&(e.execScript||function(t){e.eval.call(e,t)})(t)},camelCase:function(e){return e.replace(j,"ms-").replace(D,L)},nodeName:function(e,t){return e.nodeName&&e.nodeName.toLowerCase()===t.toLowerCase()},each:function(e,t,n){var r,i=0,o=e.length,a=M(e);if(n){if(a){for(;o>i;i++)if(r=t.apply(e[i],n),r===!1)break}else for(i in e)if(r=t.apply(e[i],n),r===!1)break}else if(a){for(;o>i;i++)if(r=t.call(e[i],i,e[i]),r===!1)break}else for(i in e)if(r=t.call(e[i],i,e[i]),r===!1)break;return e},trim:v&&!v.call("\ufeff\u00a0")?function(e){return null==e?"":v.call(e)}:function(e){return null==e?"":(e+"").replace(T,"")},makeArray:function(e,t){var n=t||[];return null!=e&&(M(Object(e))?b.merge(n,"string"==typeof e?[e]:e):d.call(n,e)),n},inArray:function(e,t,n){var r;if(t){if(g)return g.call(t,e,n);for(r=t.length,n=n?0>n?Math.max(0,r+n):n:0;r>n;n++)if(n in t&&t[n]===e)return n}return-1},merge:function(e,n){var r=n.length,i=e.length,o=0;if("number"==typeof r)for(;r>o;o++)e[i++]=n[o];else while(n[o]!==t)e[i++]=n[o++];return e.length=i,e},grep:function(e,t,n){var r,i=[],o=0,a=e.length;for(n=!!n;a>o;o++)r=!!t(e[o],o),n!==r&&i.push(e[o]);return i},map:function(e,t,n){var r,i=0,o=e.length,a=M(e),s=[];if(a)for(;o>i;i++)r=t(e[i],i,n),null!=r&&(s[s.length]=r);else for(i in e)r=t(e[i],i,n),null!=r&&(s[s.length]=r);return f.apply([],s)},guid:1,proxy:function(e,n){var r,i,o;return"string"==typeof n&&(o=e[n],n=e,e=o),b.isFunction(e)?(r=h.call(arguments,2),i=function(){return e.apply(n||this,r.concat(h.call(arguments)))},i.guid=e.guid=e.guid||b.guid++,i):t},access:function(e,n,r,i,o,a,s){var u=0,l=e.length,c=null==r;if("object"===b.type(r)){o=!0;for(u in r)b.access(e,n,u,r[u],!0,a,s)}else if(i!==t&&(o=!0,b.isFunction(i)||(s=!0),c&&(s?(n.call(e,i),n=null):(c=n,n=function(e,t,n){return c.call(b(e),n)})),n))for(;l>u;u++)n(e[u],r,s?i:i.call(e[u],u,n(e[u],r)));return o?e:c?n.call(e):l?n(e[0],r):a},now:function(){return(new Date).getTime()}}),b.ready.promise=function(t){if(!n)if(n=b.Deferred(),"complete"===o.readyState)setTimeout(b.ready);else if(o.addEventListener)o.addEventListener("DOMContentLoaded",H,!1),e.addEventListener("load",H,!1);else{o.attachEvent("onreadystatechange",H),e.attachEvent("onload",H);var r=!1;try{r=null==e.frameElement&&o.documentElement}catch(i){}r&&r.doScroll&&function a(){if(!b.isReady){try{r.doScroll("left")}catch(e){return setTimeout(a,50)}q(),b.ready()}}()}return n.promise(t)},b.each("Boolean Number String Function Array Date RegExp Object Error".split(" "),function(e,t){l["[object "+t+"]"]=t.toLowerCase()});function M(e){var t=e.length,n=b.type(e);return b.isWindow(e)?!1:1===e.nodeType&&t?!0:"array"===n||"function"!==n&&(0===t||"number"==typeof t&&t>0&&t-1 in e)}r=b(o);var _={};function F(e){var t=_[e]={};return b.each(e.match(w)||[],function(e,n){t[n]=!0}),t}b.Callbacks=function(e){e="string"==typeof e?_[e]||F(e):b.extend({},e);var n,r,i,o,a,s,u=[],l=!e.once&&[],c=function(t){for(r=e.memory&&t,i=!0,a=s||0,s=0,o=u.length,n=!0;u&&o>a;a++)if(u[a].apply(t[0],t[1])===!1&&e.stopOnFalse){r=!1;break}n=!1,u&&(l?l.length&&c(l.shift()):r?u=[]:p.disable())},p={add:function(){if(u){var t=u.length;(function i(t){b.each(t,function(t,n){var r=b.type(n);"function"===r?e.unique&&p.has(n)||u.push(n):n&&n.length&&"string"!==r&&i(n)})})(arguments),n?o=u.length:r&&(s=t,c(r))}return this},remove:function(){return u&&b.each(arguments,function(e,t){var r;while((r=b.inArray(t,u,r))>-1)u.splice(r,1),n&&(o>=r&&o--,a>=r&&a--)}),this},has:function(e){return e?b.inArray(e,u)>-1:!(!u||!u.length)},empty:function(){return u=[],this},disable:function(){return u=l=r=t,this},disabled:function(){return!u},lock:function(){return l=t,r||p.disable(),this},locked:function(){return!l},fireWith:function(e,t){return t=t||[],t=[e,t.slice?t.slice():t],!u||i&&!l||(n?l.push(t):c(t)),this},fire:function(){return p.fireWith(this,arguments),this},fired:function(){return!!i}};return p},b.extend({Deferred:function(e){var t=[["resolve","done",b.Callbacks("once memory"),"resolved"],["reject","fail",b.Callbacks("once memory"),"rejected"],["notify","progress",b.Callbacks("memory")]],n="pending",r={state:function(){return n},always:function(){return i.done(arguments).fail(arguments),this},then:function(){var e=arguments;return b.Deferred(function(n){b.each(t,function(t,o){var a=o[0],s=b.isFunction(e[t])&&e[t];i[o[1]](function(){var e=s&&s.apply(this,arguments);e&&b.isFunction(e.promise)?e.promise().done(n.resolve).fail(n.reject).progress(n.notify):n[a+"With"](this===r?n.promise():this,s?[e]:arguments)})}),e=null}).promise()},promise:function(e){return null!=e?b.extend(e,r):r}},i={};return r.pipe=r.then,b.each(t,function(e,o){var a=o[2],s=o[3];r[o[1]]=a.add,s&&a.add(function(){n=s},t[1^e][2].disable,t[2][2].lock),i[o[0]]=function(){return i[o[0]+"With"](this===i?r:this,arguments),this},i[o[0]+"With"]=a.fireWith}),r.promise(i),e&&e.call(i,i),i},when:function(e){var t=0,n=h.call(arguments),r=n.length,i=1!==r||e&&b.isFunction(e.promise)?r:0,o=1===i?e:b.Deferred(),a=function(e,t,n){return function(r){t[e]=this,n[e]=arguments.length>1?h.call(arguments):r,n===s?o.notifyWith(t,n):--i||o.resolveWith(t,n)}},s,u,l;if(r>1)for(s=Array(r),u=Array(r),l=Array(r);r>t;t++)n[t]&&b.isFunction(n[t].promise)?n[t].promise().done(a(t,l,n)).fail(o.reject).progress(a(t,u,s)):--i;return i||o.resolveWith(l,n),o.promise()}}),b.support=function(){var t,n,r,a,s,u,l,c,p,f,d=o.createElement("div");if(d.setAttribute("className","t"),d.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",n=d.getElementsByTagName("*"),r=d.getElementsByTagName("a")[0],!n||!r||!n.length)return{};s=o.createElement("select"),l=s.appendChild(o.createElement("option")),a=d.getElementsByTagName("input")[0],r.style.cssText="top:1px;float:left;opacity:.5",t={getSetAttribute:"t"!==d.className,leadingWhitespace:3===d.firstChild.nodeType,tbody:!d.getElementsByTagName("tbody").length,htmlSerialize:!!d.getElementsByTagName("link").length,style:/top/.test(r.getAttribute("style")),hrefNormalized:"/a"===r.getAttribute("href"),opacity:/^0.5/.test(r.style.opacity),cssFloat:!!r.style.cssFloat,checkOn:!!a.value,optSelected:l.selected,enctype:!!o.createElement("form").enctype,html5Clone:"<:nav></:nav>"!==o.createElement("nav").cloneNode(!0).outerHTML,boxModel:"CSS1Compat"===o.compatMode,deleteExpando:!0,noCloneEvent:!0,inlineBlockNeedsLayout:!1,shrinkWrapBlocks:!1,reliableMarginRight:!0,boxSizingReliable:!0,pixelPosition:!1},a.checked=!0,t.noCloneChecked=a.cloneNode(!0).checked,s.disabled=!0,t.optDisabled=!l.disabled;try{delete d.test}catch(h){t.deleteExpando=!1}a=o.createElement("input"),a.setAttribute("value",""),t.input=""===a.getAttribute("value"),a.value="t",a.setAttribute("type","radio"),t.radioValue="t"===a.value,a.setAttribute("checked","t"),a.setAttribute("name","t"),u=o.createDocumentFragment(),u.appendChild(a),t.appendChecked=a.checked,t.checkClone=u.cloneNode(!0).cloneNode(!0).lastChild.checked,d.attachEvent&&(d.attachEvent("onclick",function(){t.noCloneEvent=!1}),d.cloneNode(!0).click());for(f in{submit:!0,change:!0,focusin:!0})d.setAttribute(c="on"+f,"t"),t[f+"Bubbles"]=c in e||d.attributes[c].expando===!1;return d.style.backgroundClip="content-box",d.cloneNode(!0).style.backgroundClip="",t.clearCloneStyle="content-box"===d.style.backgroundClip,b(function(){var n,r,a,s="padding:0;margin:0;border:0;display:block;box-sizing:content-box;-moz-box-sizing:content-box;-webkit-box-sizing:content-box;",u=o.getElementsByTagName("body")[0];u&&(n=o.createElement("div"),n.style.cssText="border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px",u.appendChild(n).appendChild(d),d.innerHTML="<table><tr><td></td><td>t</td></tr></table>",a=d.getElementsByTagName("td"),a[0].style.cssText="padding:0;margin:0;border:0;display:none",p=0===a[0].offsetHeight,a[0].style.display="",a[1].style.display="none",t.reliableHiddenOffsets=p&&0===a[0].offsetHeight,d.innerHTML="",d.style.cssText="box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;",t.boxSizing=4===d.offsetWidth,t.doesNotIncludeMarginInBodyOffset=1!==u.offsetTop,e.getComputedStyle&&(t.pixelPosition="1%"!==(e.getComputedStyle(d,null)||{}).top,t.boxSizingReliable="4px"===(e.getComputedStyle(d,null)||{width:"4px"}).width,r=d.appendChild(o.createElement("div")),r.style.cssText=d.style.cssText=s,r.style.marginRight=r.style.width="0",d.style.width="1px",t.reliableMarginRight=!parseFloat((e.getComputedStyle(r,null)||{}).marginRight)),typeof d.style.zoom!==i&&(d.innerHTML="",d.style.cssText=s+"width:1px;padding:1px;display:inline;zoom:1",t.inlineBlockNeedsLayout=3===d.offsetWidth,d.style.display="block",d.innerHTML="<div></div>",d.firstChild.style.width="5px",t.shrinkWrapBlocks=3!==d.offsetWidth,t.inlineBlockNeedsLayout&&(u.style.zoom=1)),u.removeChild(n),n=d=a=r=null)}),n=s=u=l=r=a=null,t}();var O=/(?:\{[\s\S]*\}|\[[\s\S]*\])$/,B=/([A-Z])/g;function P(e,n,r,i){if(b.acceptData(e)){var o,a,s=b.expando,u="string"==typeof n,l=e.nodeType,p=l?b.cache:e,f=l?e[s]:e[s]&&s;if(f&&p[f]&&(i||p[f].data)||!u||r!==t)return f||(l?e[s]=f=c.pop()||b.guid++:f=s),p[f]||(p[f]={},l||(p[f].toJSON=b.noop)),("object"==typeof n||"function"==typeof n)&&(i?p[f]=b.extend(p[f],n):p[f].data=b.extend(p[f].data,n)),o=p[f],i||(o.data||(o.data={}),o=o.data),r!==t&&(o[b.camelCase(n)]=r),u?(a=o[n],null==a&&(a=o[b.camelCase(n)])):a=o,a}}function R(e,t,n){if(b.acceptData(e)){var r,i,o,a=e.nodeType,s=a?b.cache:e,u=a?e[b.expando]:b.expando;if(s[u]){if(t&&(o=n?s[u]:s[u].data)){b.isArray(t)?t=t.concat(b.map(t,b.camelCase)):t in o?t=[t]:(t=b.camelCase(t),t=t in o?[t]:t.split(" "));for(r=0,i=t.length;i>r;r++)delete o[t[r]];if(!(n?$:b.isEmptyObject)(o))return}(n||(delete s[u].data,$(s[u])))&&(a?b.cleanData([e],!0):b.support.deleteExpando||s!=s.window?delete s[u]:s[u]=null)}}}b.extend({cache:{},expando:"jQuery"+(p+Math.random()).replace(/\D/g,""),noData:{embed:!0,object:"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",applet:!0},hasData:function(e){return e=e.nodeType?b.cache[e[b.expando]]:e[b.expando],!!e&&!$(e)},data:function(e,t,n){return P(e,t,n)},removeData:function(e,t){return R(e,t)},_data:function(e,t,n){return P(e,t,n,!0)},_removeData:function(e,t){return R(e,t,!0)},acceptData:function(e){if(e.nodeType&&1!==e.nodeType&&9!==e.nodeType)return!1;var t=e.nodeName&&b.noData[e.nodeName.toLowerCase()];return!t||t!==!0&&e.getAttribute("classid")===t}}),b.fn.extend({data:function(e,n){var r,i,o=this[0],a=0,s=null;if(e===t){if(this.length&&(s=b.data(o),1===o.nodeType&&!b._data(o,"parsedAttrs"))){for(r=o.attributes;r.length>a;a++)i=r[a].name,i.indexOf("data-")||(i=b.camelCase(i.slice(5)),W(o,i,s[i]));b._data(o,"parsedAttrs",!0)}return s}return"object"==typeof e?this.each(function(){b.data(this,e)}):b.access(this,function(n){return n===t?o?W(o,e,b.data(o,e)):null:(this.each(function(){b.data(this,e,n)}),t)},null,n,arguments.length>1,null,!0)},removeData:function(e){return this.each(function(){b.removeData(this,e)})}});function W(e,n,r){if(r===t&&1===e.nodeType){var i="data-"+n.replace(B,"-$1").toLowerCase();if(r=e.getAttribute(i),"string"==typeof r){try{r="true"===r?!0:"false"===r?!1:"null"===r?null:+r+""===r?+r:O.test(r)?b.parseJSON(r):r}catch(o){}b.data(e,n,r)}else r=t}return r}function $(e){var t;for(t in e)if(("data"!==t||!b.isEmptyObject(e[t]))&&"toJSON"!==t)return!1;return!0}b.extend({queue:function(e,n,r){var i;return e?(n=(n||"fx")+"queue",i=b._data(e,n),r&&(!i||b.isArray(r)?i=b._data(e,n,b.makeArray(r)):i.push(r)),i||[]):t},dequeue:function(e,t){t=t||"fx";var n=b.queue(e,t),r=n.length,i=n.shift(),o=b._queueHooks(e,t),a=function(){b.dequeue(e,t)};"inprogress"===i&&(i=n.shift(),r--),o.cur=i,i&&("fx"===t&&n.unshift("inprogress"),delete o.stop,i.call(e,a,o)),!r&&o&&o.empty.fire()},_queueHooks:function(e,t){var n=t+"queueHooks";return b._data(e,n)||b._data(e,n,{empty:b.Callbacks("once memory").add(function(){b._removeData(e,t+"queue"),b._removeData(e,n)})})}}),b.fn.extend({queue:function(e,n){var r=2;return"string"!=typeof e&&(n=e,e="fx",r--),r>arguments.length?b.queue(this[0],e):n===t?this:this.each(function(){var t=b.queue(this,e,n);b._queueHooks(this,e),"fx"===e&&"inprogress"!==t[0]&&b.dequeue(this,e)})},dequeue:function(e){return this.each(function(){b.dequeue(this,e)})},delay:function(e,t){return e=b.fx?b.fx.speeds[e]||e:e,t=t||"fx",this.queue(t,function(t,n){var r=setTimeout(t,e);n.stop=function(){clearTimeout(r)}})},clearQueue:function(e){return this.queue(e||"fx",[])},promise:function(e,n){var r,i=1,o=b.Deferred(),a=this,s=this.length,u=function(){--i||o.resolveWith(a,[a])};"string"!=typeof e&&(n=e,e=t),e=e||"fx";while(s--)r=b._data(a[s],e+"queueHooks"),r&&r.empty&&(i++,r.empty.add(u));return u(),o.promise(n)}});var I,z,X=/[\t\r\n]/g,U=/\r/g,V=/^(?:input|select|textarea|button|object)$/i,Y=/^(?:a|area)$/i,J=/^(?:checked|selected|autofocus|autoplay|async|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped)$/i,G=/^(?:checked|selected)$/i,Q=b.support.getSetAttribute,K=b.support.input;b.fn.extend({attr:function(e,t){return b.access(this,b.attr,e,t,arguments.length>1)},removeAttr:function(e){return this.each(function(){b.removeAttr(this,e)})},prop:function(e,t){return b.access(this,b.prop,e,t,arguments.length>1)},removeProp:function(e){return e=b.propFix[e]||e,this.each(function(){try{this[e]=t,delete this[e]}catch(n){}})},addClass:function(e){var t,n,r,i,o,a=0,s=this.length,u="string"==typeof e&&e;if(b.isFunction(e))return this.each(function(t){b(this).addClass(e.call(this,t,this.className))});if(u)for(t=(e||"").match(w)||[];s>a;a++)if(n=this[a],r=1===n.nodeType&&(n.className?(" "+n.className+" ").replace(X," "):" ")){o=0;while(i=t[o++])0>r.indexOf(" "+i+" ")&&(r+=i+" ");n.className=b.trim(r)}return this},removeClass:function(e){var t,n,r,i,o,a=0,s=this.length,u=0===arguments.length||"string"==typeof e&&e;if(b.isFunction(e))return this.each(function(t){b(this).removeClass(e.call(this,t,this.className))});if(u)for(t=(e||"").match(w)||[];s>a;a++)if(n=this[a],r=1===n.nodeType&&(n.className?(" "+n.className+" ").replace(X," "):"")){o=0;while(i=t[o++])while(r.indexOf(" "+i+" ")>=0)r=r.replace(" "+i+" "," ");n.className=e?b.trim(r):""}return this},toggleClass:function(e,t){var n=typeof e,r="boolean"==typeof t;return b.isFunction(e)?this.each(function(n){b(this).toggleClass(e.call(this,n,this.className,t),t)}):this.each(function(){if("string"===n){var o,a=0,s=b(this),u=t,l=e.match(w)||[];while(o=l[a++])u=r?u:!s.hasClass(o),s[u?"addClass":"removeClass"](o)}else(n===i||"boolean"===n)&&(this.className&&b._data(this,"__className__",this.className),this.className=this.className||e===!1?"":b._data(this,"__className__")||"")})},hasClass:function(e){var t=" "+e+" ",n=0,r=this.length;for(;r>n;n++)if(1===this[n].nodeType&&(" "+this[n].className+" ").replace(X," ").indexOf(t)>=0)return!0;return!1},val:function(e){var n,r,i,o=this[0];{if(arguments.length)return i=b.isFunction(e),this.each(function(n){var o,a=b(this);1===this.nodeType&&(o=i?e.call(this,n,a.val()):e,null==o?o="":"number"==typeof o?o+="":b.isArray(o)&&(o=b.map(o,function(e){return null==e?"":e+""})),r=b.valHooks[this.type]||b.valHooks[this.nodeName.toLowerCase()],r&&"set"in r&&r.set(this,o,"value")!==t||(this.value=o))});if(o)return r=b.valHooks[o.type]||b.valHooks[o.nodeName.toLowerCase()],r&&"get"in r&&(n=r.get(o,"value"))!==t?n:(n=o.value,"string"==typeof n?n.replace(U,""):null==n?"":n)}}}),b.extend({valHooks:{option:{get:function(e){var t=e.attributes.value;return!t||t.specified?e.value:e.text}},select:{get:function(e){var t,n,r=e.options,i=e.selectedIndex,o="select-one"===e.type||0>i,a=o?null:[],s=o?i+1:r.length,u=0>i?s:o?i:0;for(;s>u;u++)if(n=r[u],!(!n.selected&&u!==i||(b.support.optDisabled?n.disabled:null!==n.getAttribute("disabled"))||n.parentNode.disabled&&b.nodeName(n.parentNode,"optgroup"))){if(t=b(n).val(),o)return t;a.push(t)}return a},set:function(e,t){var n=b.makeArray(t);return b(e).find("option").each(function(){this.selected=b.inArray(b(this).val(),n)>=0}),n.length||(e.selectedIndex=-1),n}}},attr:function(e,n,r){var o,a,s,u=e.nodeType;if(e&&3!==u&&8!==u&&2!==u)return typeof e.getAttribute===i?b.prop(e,n,r):(a=1!==u||!b.isXMLDoc(e),a&&(n=n.toLowerCase(),o=b.attrHooks[n]||(J.test(n)?z:I)),r===t?o&&a&&"get"in o&&null!==(s=o.get(e,n))?s:(typeof e.getAttribute!==i&&(s=e.getAttribute(n)),null==s?t:s):null!==r?o&&a&&"set"in o&&(s=o.set(e,r,n))!==t?s:(e.setAttribute(n,r+""),r):(b.removeAttr(e,n),t))},removeAttr:function(e,t){var n,r,i=0,o=t&&t.match(w);if(o&&1===e.nodeType)while(n=o[i++])r=b.propFix[n]||n,J.test(n)?!Q&&G.test(n)?e[b.camelCase("default-"+n)]=e[r]=!1:e[r]=!1:b.attr(e,n,""),e.removeAttribute(Q?n:r)},attrHooks:{type:{set:function(e,t){if(!b.support.radioValue&&"radio"===t&&b.nodeName(e,"input")){var n=e.value;return e.setAttribute("type",t),n&&(e.value=n),t}}}},propFix:{tabindex:"tabIndex",readonly:"readOnly","for":"htmlFor","class":"className",maxlength:"maxLength",cellspacing:"cellSpacing",cellpadding:"cellPadding",rowspan:"rowSpan",colspan:"colSpan",usemap:"useMap",frameborder:"frameBorder",contenteditable:"contentEditable"},prop:function(e,n,r){var i,o,a,s=e.nodeType;if(e&&3!==s&&8!==s&&2!==s)return a=1!==s||!b.isXMLDoc(e),a&&(n=b.propFix[n]||n,o=b.propHooks[n]),r!==t?o&&"set"in o&&(i=o.set(e,r,n))!==t?i:e[n]=r:o&&"get"in o&&null!==(i=o.get(e,n))?i:e[n]},propHooks:{tabIndex:{get:function(e){var n=e.getAttributeNode("tabindex");return n&&n.specified?parseInt(n.value,10):V.test(e.nodeName)||Y.test(e.nodeName)&&e.href?0:t}}}}),z={get:function(e,n){var r=b.prop(e,n),i="boolean"==typeof r&&e.getAttribute(n),o="boolean"==typeof r?K&&Q?null!=i:G.test(n)?e[b.camelCase("default-"+n)]:!!i:e.getAttributeNode(n);return o&&o.value!==!1?n.toLowerCase():t},set:function(e,t,n){return t===!1?b.removeAttr(e,n):K&&Q||!G.test(n)?e.setAttribute(!Q&&b.propFix[n]||n,n):e[b.camelCase("default-"+n)]=e[n]=!0,n}},K&&Q||(b.attrHooks.value={get:function(e,n){var r=e.getAttributeNode(n);return b.nodeName(e,"input")?e.defaultValue:r&&r.specified?r.value:t},set:function(e,n,r){return b.nodeName(e,"input")?(e.defaultValue=n,t):I&&I.set(e,n,r)}}),Q||(I=b.valHooks.button={get:function(e,n){var r=e.getAttributeNode(n);return r&&("id"===n||"name"===n||"coords"===n?""!==r.value:r.specified)?r.value:t},set:function(e,n,r){var i=e.getAttributeNode(r);return i||e.setAttributeNode(i=e.ownerDocument.createAttribute(r)),i.value=n+="","value"===r||n===e.getAttribute(r)?n:t}},b.attrHooks.contenteditable={get:I.get,set:function(e,t,n){I.set(e,""===t?!1:t,n)}},b.each(["width","height"],function(e,n){b.attrHooks[n]=b.extend(b.attrHooks[n],{set:function(e,r){return""===r?(e.setAttribute(n,"auto"),r):t}})})),b.support.hrefNormalized||(b.each(["href","src","width","height"],function(e,n){b.attrHooks[n]=b.extend(b.attrHooks[n],{get:function(e){var r=e.getAttribute(n,2);return null==r?t:r}})}),b.each(["href","src"],function(e,t){b.propHooks[t]={get:function(e){return e.getAttribute(t,4)}}})),b.support.style||(b.attrHooks.style={get:function(e){return e.style.cssText||t},set:function(e,t){return e.style.cssText=t+""}}),b.support.optSelected||(b.propHooks.selected=b.extend(b.propHooks.selected,{get:function(e){var t=e.parentNode;return t&&(t.selectedIndex,t.parentNode&&t.parentNode.selectedIndex),null}})),b.support.enctype||(b.propFix.enctype="encoding"),b.support.checkOn||b.each(["radio","checkbox"],function(){b.valHooks[this]={get:function(e){return null===e.getAttribute("value")?"on":e.value}}}),b.each(["radio","checkbox"],function(){b.valHooks[this]=b.extend(b.valHooks[this],{set:function(e,n){return b.isArray(n)?e.checked=b.inArray(b(e).val(),n)>=0:t}})});var Z=/^(?:input|select|textarea)$/i,et=/^key/,tt=/^(?:mouse|contextmenu)|click/,nt=/^(?:focusinfocus|focusoutblur)$/,rt=/^([^.]*)(?:\.(.+)|)$/;function it(){return!0}function ot(){return!1}b.event={global:{},add:function(e,n,r,o,a){var s,u,l,c,p,f,d,h,g,m,y,v=b._data(e);if(v){r.handler&&(c=r,r=c.handler,a=c.selector),r.guid||(r.guid=b.guid++),(u=v.events)||(u=v.events={}),(f=v.handle)||(f=v.handle=function(e){return typeof b===i||e&&b.event.triggered===e.type?t:b.event.dispatch.apply(f.elem,arguments)},f.elem=e),n=(n||"").match(w)||[""],l=n.length;while(l--)s=rt.exec(n[l])||[],g=y=s[1],m=(s[2]||"").split(".").sort(),p=b.event.special[g]||{},g=(a?p.delegateType:p.bindType)||g,p=b.event.special[g]||{},d=b.extend({type:g,origType:y,data:o,handler:r,guid:r.guid,selector:a,needsContext:a&&b.expr.match.needsContext.test(a),namespace:m.join(".")},c),(h=u[g])||(h=u[g]=[],h.delegateCount=0,p.setup&&p.setup.call(e,o,m,f)!==!1||(e.addEventListener?e.addEventListener(g,f,!1):e.attachEvent&&e.attachEvent("on"+g,f))),p.add&&(p.add.call(e,d),d.handler.guid||(d.handler.guid=r.guid)),a?h.splice(h.delegateCount++,0,d):h.push(d),b.event.global[g]=!0;e=null}},remove:function(e,t,n,r,i){var o,a,s,u,l,c,p,f,d,h,g,m=b.hasData(e)&&b._data(e);if(m&&(c=m.events)){t=(t||"").match(w)||[""],l=t.length;while(l--)if(s=rt.exec(t[l])||[],d=g=s[1],h=(s[2]||"").split(".").sort(),d){p=b.event.special[d]||{},d=(r?p.delegateType:p.bindType)||d,f=c[d]||[],s=s[2]&&RegExp("(^|\\.)"+h.join("\\.(?:.*\\.|)")+"(\\.|$)"),u=o=f.length;while(o--)a=f[o],!i&&g!==a.origType||n&&n.guid!==a.guid||s&&!s.test(a.namespace)||r&&r!==a.selector&&("**"!==r||!a.selector)||(f.splice(o,1),a.selector&&f.delegateCount--,p.remove&&p.remove.call(e,a));u&&!f.length&&(p.teardown&&p.teardown.call(e,h,m.handle)!==!1||b.removeEvent(e,d,m.handle),delete c[d])}else for(d in c)b.event.remove(e,d+t[l],n,r,!0);b.isEmptyObject(c)&&(delete m.handle,b._removeData(e,"events"))}},trigger:function(n,r,i,a){var s,u,l,c,p,f,d,h=[i||o],g=y.call(n,"type")?n.type:n,m=y.call(n,"namespace")?n.namespace.split("."):[];if(l=f=i=i||o,3!==i.nodeType&&8!==i.nodeType&&!nt.test(g+b.event.triggered)&&(g.indexOf(".")>=0&&(m=g.split("."),g=m.shift(),m.sort()),u=0>g.indexOf(":")&&"on"+g,n=n[b.expando]?n:new b.Event(g,"object"==typeof n&&n),n.isTrigger=!0,n.namespace=m.join("."),n.namespace_re=n.namespace?RegExp("(^|\\.)"+m.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,n.result=t,n.target||(n.target=i),r=null==r?[n]:b.makeArray(r,[n]),p=b.event.special[g]||{},a||!p.trigger||p.trigger.apply(i,r)!==!1)){if(!a&&!p.noBubble&&!b.isWindow(i)){for(c=p.delegateType||g,nt.test(c+g)||(l=l.parentNode);l;l=l.parentNode)h.push(l),f=l;f===(i.ownerDocument||o)&&h.push(f.defaultView||f.parentWindow||e)}d=0;while((l=h[d++])&&!n.isPropagationStopped())n.type=d>1?c:p.bindType||g,s=(b._data(l,"events")||{})[n.type]&&b._data(l,"handle"),s&&s.apply(l,r),s=u&&l[u],s&&b.acceptData(l)&&s.apply&&s.apply(l,r)===!1&&n.preventDefault();if(n.type=g,!(a||n.isDefaultPrevented()||p._default&&p._default.apply(i.ownerDocument,r)!==!1||"click"===g&&b.nodeName(i,"a")||!b.acceptData(i)||!u||!i[g]||b.isWindow(i))){f=i[u],f&&(i[u]=null),b.event.triggered=g;try{i[g]()}catch(v){}b.event.triggered=t,f&&(i[u]=f)}return n.result}},dispatch:function(e){e=b.event.fix(e);var n,r,i,o,a,s=[],u=h.call(arguments),l=(b._data(this,"events")||{})[e.type]||[],c=b.event.special[e.type]||{};if(u[0]=e,e.delegateTarget=this,!c.preDispatch||c.preDispatch.call(this,e)!==!1){s=b.event.handlers.call(this,e,l),n=0;while((o=s[n++])&&!e.isPropagationStopped()){e.currentTarget=o.elem,a=0;while((i=o.handlers[a++])&&!e.isImmediatePropagationStopped())(!e.namespace_re||e.namespace_re.test(i.namespace))&&(e.handleObj=i,e.data=i.data,r=((b.event.special[i.origType]||{}).handle||i.handler).apply(o.elem,u),r!==t&&(e.result=r)===!1&&(e.preventDefault(),e.stopPropagation()))}return c.postDispatch&&c.postDispatch.call(this,e),e.result}},handlers:function(e,n){var r,i,o,a,s=[],u=n.delegateCount,l=e.target;if(u&&l.nodeType&&(!e.button||"click"!==e.type))for(;l!=this;l=l.parentNode||this)if(1===l.nodeType&&(l.disabled!==!0||"click"!==e.type)){for(o=[],a=0;u>a;a++)i=n[a],r=i.selector+" ",o[r]===t&&(o[r]=i.needsContext?b(r,this).index(l)>=0:b.find(r,this,null,[l]).length),o[r]&&o.push(i);o.length&&s.push({elem:l,handlers:o})}return n.length>u&&s.push({elem:this,handlers:n.slice(u)}),s},fix:function(e){if(e[b.expando])return e;var t,n,r,i=e.type,a=e,s=this.fixHooks[i];s||(this.fixHooks[i]=s=tt.test(i)?this.mouseHooks:et.test(i)?this.keyHooks:{}),r=s.props?this.props.concat(s.props):this.props,e=new b.Event(a),t=r.length;while(t--)n=r[t],e[n]=a[n];return e.target||(e.target=a.srcElement||o),3===e.target.nodeType&&(e.target=e.target.parentNode),e.metaKey=!!e.metaKey,s.filter?s.filter(e,a):e},props:"altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(e,t){return null==e.which&&(e.which=null!=t.charCode?t.charCode:t.keyCode),e}},mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(e,n){var r,i,a,s=n.button,u=n.fromElement;return null==e.pageX&&null!=n.clientX&&(i=e.target.ownerDocument||o,a=i.documentElement,r=i.body,e.pageX=n.clientX+(a&&a.scrollLeft||r&&r.scrollLeft||0)-(a&&a.clientLeft||r&&r.clientLeft||0),e.pageY=n.clientY+(a&&a.scrollTop||r&&r.scrollTop||0)-(a&&a.clientTop||r&&r.clientTop||0)),!e.relatedTarget&&u&&(e.relatedTarget=u===e.target?n.toElement:u),e.which||s===t||(e.which=1&s?1:2&s?3:4&s?2:0),e}},special:{load:{noBubble:!0},click:{trigger:function(){return b.nodeName(this,"input")&&"checkbox"===this.type&&this.click?(this.click(),!1):t}},focus:{trigger:function(){if(this!==o.activeElement&&this.focus)try{return this.focus(),!1}catch(e){}},delegateType:"focusin"},blur:{trigger:function(){return this===o.activeElement&&this.blur?(this.blur(),!1):t},delegateType:"focusout"},beforeunload:{postDispatch:function(e){e.result!==t&&(e.originalEvent.returnValue=e.result)}}},simulate:function(e,t,n,r){var i=b.extend(new b.Event,n,{type:e,isSimulated:!0,originalEvent:{}});r?b.event.trigger(i,null,t):b.event.dispatch.call(t,i),i.isDefaultPrevented()&&n.preventDefault()}},b.removeEvent=o.removeEventListener?function(e,t,n){e.removeEventListener&&e.removeEventListener(t,n,!1)}:function(e,t,n){var r="on"+t;e.detachEvent&&(typeof e[r]===i&&(e[r]=null),e.detachEvent(r,n))},b.Event=function(e,n){return this instanceof b.Event?(e&&e.type?(this.originalEvent=e,this.type=e.type,this.isDefaultPrevented=e.defaultPrevented||e.returnValue===!1||e.getPreventDefault&&e.getPreventDefault()?it:ot):this.type=e,n&&b.extend(this,n),this.timeStamp=e&&e.timeStamp||b.now(),this[b.expando]=!0,t):new b.Event(e,n)},b.Event.prototype={isDefaultPrevented:ot,isPropagationStopped:ot,isImmediatePropagationStopped:ot,preventDefault:function(){var e=this.originalEvent;this.isDefaultPrevented=it,e&&(e.preventDefault?e.preventDefault():e.returnValue=!1)},stopPropagation:function(){var e=this.originalEvent;this.isPropagationStopped=it,e&&(e.stopPropagation&&e.stopPropagation(),e.cancelBubble=!0)},stopImmediatePropagation:function(){this.isImmediatePropagationStopped=it,this.stopPropagation()}},b.each({mouseenter:"mouseover",mouseleave:"mouseout"},function(e,t){b.event.special[e]={delegateType:t,bindType:t,handle:function(e){var n,r=this,i=e.relatedTarget,o=e.handleObj;
return(!i||i!==r&&!b.contains(r,i))&&(e.type=o.origType,n=o.handler.apply(this,arguments),e.type=t),n}}}),b.support.submitBubbles||(b.event.special.submit={setup:function(){return b.nodeName(this,"form")?!1:(b.event.add(this,"click._submit keypress._submit",function(e){var n=e.target,r=b.nodeName(n,"input")||b.nodeName(n,"button")?n.form:t;r&&!b._data(r,"submitBubbles")&&(b.event.add(r,"submit._submit",function(e){e._submit_bubble=!0}),b._data(r,"submitBubbles",!0))}),t)},postDispatch:function(e){e._submit_bubble&&(delete e._submit_bubble,this.parentNode&&!e.isTrigger&&b.event.simulate("submit",this.parentNode,e,!0))},teardown:function(){return b.nodeName(this,"form")?!1:(b.event.remove(this,"._submit"),t)}}),b.support.changeBubbles||(b.event.special.change={setup:function(){return Z.test(this.nodeName)?(("checkbox"===this.type||"radio"===this.type)&&(b.event.add(this,"propertychange._change",function(e){"checked"===e.originalEvent.propertyName&&(this._just_changed=!0)}),b.event.add(this,"click._change",function(e){this._just_changed&&!e.isTrigger&&(this._just_changed=!1),b.event.simulate("change",this,e,!0)})),!1):(b.event.add(this,"beforeactivate._change",function(e){var t=e.target;Z.test(t.nodeName)&&!b._data(t,"changeBubbles")&&(b.event.add(t,"change._change",function(e){!this.parentNode||e.isSimulated||e.isTrigger||b.event.simulate("change",this.parentNode,e,!0)}),b._data(t,"changeBubbles",!0))}),t)},handle:function(e){var n=e.target;return this!==n||e.isSimulated||e.isTrigger||"radio"!==n.type&&"checkbox"!==n.type?e.handleObj.handler.apply(this,arguments):t},teardown:function(){return b.event.remove(this,"._change"),!Z.test(this.nodeName)}}),b.support.focusinBubbles||b.each({focus:"focusin",blur:"focusout"},function(e,t){var n=0,r=function(e){b.event.simulate(t,e.target,b.event.fix(e),!0)};b.event.special[t]={setup:function(){0===n++&&o.addEventListener(e,r,!0)},teardown:function(){0===--n&&o.removeEventListener(e,r,!0)}}}),b.fn.extend({on:function(e,n,r,i,o){var a,s;if("object"==typeof e){"string"!=typeof n&&(r=r||n,n=t);for(a in e)this.on(a,n,r,e[a],o);return this}if(null==r&&null==i?(i=n,r=n=t):null==i&&("string"==typeof n?(i=r,r=t):(i=r,r=n,n=t)),i===!1)i=ot;else if(!i)return this;return 1===o&&(s=i,i=function(e){return b().off(e),s.apply(this,arguments)},i.guid=s.guid||(s.guid=b.guid++)),this.each(function(){b.event.add(this,e,i,r,n)})},one:function(e,t,n,r){return this.on(e,t,n,r,1)},off:function(e,n,r){var i,o;if(e&&e.preventDefault&&e.handleObj)return i=e.handleObj,b(e.delegateTarget).off(i.namespace?i.origType+"."+i.namespace:i.origType,i.selector,i.handler),this;if("object"==typeof e){for(o in e)this.off(o,n,e[o]);return this}return(n===!1||"function"==typeof n)&&(r=n,n=t),r===!1&&(r=ot),this.each(function(){b.event.remove(this,e,r,n)})},bind:function(e,t,n){return this.on(e,null,t,n)},unbind:function(e,t){return this.off(e,null,t)},delegate:function(e,t,n,r){return this.on(t,e,n,r)},undelegate:function(e,t,n){return 1===arguments.length?this.off(e,"**"):this.off(t,e||"**",n)},trigger:function(e,t){return this.each(function(){b.event.trigger(e,t,this)})},triggerHandler:function(e,n){var r=this[0];return r?b.event.trigger(e,n,r,!0):t}}),function(e,t){var n,r,i,o,a,s,u,l,c,p,f,d,h,g,m,y,v,x="sizzle"+-new Date,w=e.document,T={},N=0,C=0,k=it(),E=it(),S=it(),A=typeof t,j=1<<31,D=[],L=D.pop,H=D.push,q=D.slice,M=D.indexOf||function(e){var t=0,n=this.length;for(;n>t;t++)if(this[t]===e)return t;return-1},_="[\\x20\\t\\r\\n\\f]",F="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",O=F.replace("w","w#"),B="([*^$|!~]?=)",P="\\["+_+"*("+F+")"+_+"*(?:"+B+_+"*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|("+O+")|)|)"+_+"*\\]",R=":("+F+")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|"+P.replace(3,8)+")*)|.*)\\)|)",W=RegExp("^"+_+"+|((?:^|[^\\\\])(?:\\\\.)*)"+_+"+$","g"),$=RegExp("^"+_+"*,"+_+"*"),I=RegExp("^"+_+"*([\\x20\\t\\r\\n\\f>+~])"+_+"*"),z=RegExp(R),X=RegExp("^"+O+"$"),U={ID:RegExp("^#("+F+")"),CLASS:RegExp("^\\.("+F+")"),NAME:RegExp("^\\[name=['\"]?("+F+")['\"]?\\]"),TAG:RegExp("^("+F.replace("w","w*")+")"),ATTR:RegExp("^"+P),PSEUDO:RegExp("^"+R),CHILD:RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+_+"*(even|odd|(([+-]|)(\\d*)n|)"+_+"*(?:([+-]|)"+_+"*(\\d+)|))"+_+"*\\)|)","i"),needsContext:RegExp("^"+_+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+_+"*((?:-\\d)?\\d*)"+_+"*\\)|)(?=[^-]|$)","i")},V=/[\x20\t\r\n\f]*[+~]/,Y=/^[^{]+\{\s*\[native code/,J=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,G=/^(?:input|select|textarea|button)$/i,Q=/^h\d$/i,K=/'|\\/g,Z=/\=[\x20\t\r\n\f]*([^'"\]]*)[\x20\t\r\n\f]*\]/g,et=/\\([\da-fA-F]{1,6}[\x20\t\r\n\f]?|.)/g,tt=function(e,t){var n="0x"+t-65536;return n!==n?t:0>n?String.fromCharCode(n+65536):String.fromCharCode(55296|n>>10,56320|1023&n)};try{q.call(w.documentElement.childNodes,0)[0].nodeType}catch(nt){q=function(e){var t,n=[];while(t=this[e++])n.push(t);return n}}function rt(e){return Y.test(e+"")}function it(){var e,t=[];return e=function(n,r){return t.push(n+=" ")>i.cacheLength&&delete e[t.shift()],e[n]=r}}function ot(e){return e[x]=!0,e}function at(e){var t=p.createElement("div");try{return e(t)}catch(n){return!1}finally{t=null}}function st(e,t,n,r){var i,o,a,s,u,l,f,g,m,v;if((t?t.ownerDocument||t:w)!==p&&c(t),t=t||p,n=n||[],!e||"string"!=typeof e)return n;if(1!==(s=t.nodeType)&&9!==s)return[];if(!d&&!r){if(i=J.exec(e))if(a=i[1]){if(9===s){if(o=t.getElementById(a),!o||!o.parentNode)return n;if(o.id===a)return n.push(o),n}else if(t.ownerDocument&&(o=t.ownerDocument.getElementById(a))&&y(t,o)&&o.id===a)return n.push(o),n}else{if(i[2])return H.apply(n,q.call(t.getElementsByTagName(e),0)),n;if((a=i[3])&&T.getByClassName&&t.getElementsByClassName)return H.apply(n,q.call(t.getElementsByClassName(a),0)),n}if(T.qsa&&!h.test(e)){if(f=!0,g=x,m=t,v=9===s&&e,1===s&&"object"!==t.nodeName.toLowerCase()){l=ft(e),(f=t.getAttribute("id"))?g=f.replace(K,"\\$&"):t.setAttribute("id",g),g="[id='"+g+"'] ",u=l.length;while(u--)l[u]=g+dt(l[u]);m=V.test(e)&&t.parentNode||t,v=l.join(",")}if(v)try{return H.apply(n,q.call(m.querySelectorAll(v),0)),n}catch(b){}finally{f||t.removeAttribute("id")}}}return wt(e.replace(W,"$1"),t,n,r)}a=st.isXML=function(e){var t=e&&(e.ownerDocument||e).documentElement;return t?"HTML"!==t.nodeName:!1},c=st.setDocument=function(e){var n=e?e.ownerDocument||e:w;return n!==p&&9===n.nodeType&&n.documentElement?(p=n,f=n.documentElement,d=a(n),T.tagNameNoComments=at(function(e){return e.appendChild(n.createComment("")),!e.getElementsByTagName("*").length}),T.attributes=at(function(e){e.innerHTML="<select></select>";var t=typeof e.lastChild.getAttribute("multiple");return"boolean"!==t&&"string"!==t}),T.getByClassName=at(function(e){return e.innerHTML="<div class='hidden e'></div><div class='hidden'></div>",e.getElementsByClassName&&e.getElementsByClassName("e").length?(e.lastChild.className="e",2===e.getElementsByClassName("e").length):!1}),T.getByName=at(function(e){e.id=x+0,e.innerHTML="<a name='"+x+"'></a><div name='"+x+"'></div>",f.insertBefore(e,f.firstChild);var t=n.getElementsByName&&n.getElementsByName(x).length===2+n.getElementsByName(x+0).length;return T.getIdNotName=!n.getElementById(x),f.removeChild(e),t}),i.attrHandle=at(function(e){return e.innerHTML="<a href='#'></a>",e.firstChild&&typeof e.firstChild.getAttribute!==A&&"#"===e.firstChild.getAttribute("href")})?{}:{href:function(e){return e.getAttribute("href",2)},type:function(e){return e.getAttribute("type")}},T.getIdNotName?(i.find.ID=function(e,t){if(typeof t.getElementById!==A&&!d){var n=t.getElementById(e);return n&&n.parentNode?[n]:[]}},i.filter.ID=function(e){var t=e.replace(et,tt);return function(e){return e.getAttribute("id")===t}}):(i.find.ID=function(e,n){if(typeof n.getElementById!==A&&!d){var r=n.getElementById(e);return r?r.id===e||typeof r.getAttributeNode!==A&&r.getAttributeNode("id").value===e?[r]:t:[]}},i.filter.ID=function(e){var t=e.replace(et,tt);return function(e){var n=typeof e.getAttributeNode!==A&&e.getAttributeNode("id");return n&&n.value===t}}),i.find.TAG=T.tagNameNoComments?function(e,n){return typeof n.getElementsByTagName!==A?n.getElementsByTagName(e):t}:function(e,t){var n,r=[],i=0,o=t.getElementsByTagName(e);if("*"===e){while(n=o[i++])1===n.nodeType&&r.push(n);return r}return o},i.find.NAME=T.getByName&&function(e,n){return typeof n.getElementsByName!==A?n.getElementsByName(name):t},i.find.CLASS=T.getByClassName&&function(e,n){return typeof n.getElementsByClassName===A||d?t:n.getElementsByClassName(e)},g=[],h=[":focus"],(T.qsa=rt(n.querySelectorAll))&&(at(function(e){e.innerHTML="<select><option selected=''></option></select>",e.querySelectorAll("[selected]").length||h.push("\\["+_+"*(?:checked|disabled|ismap|multiple|readonly|selected|value)"),e.querySelectorAll(":checked").length||h.push(":checked")}),at(function(e){e.innerHTML="<input type='hidden' i=''/>",e.querySelectorAll("[i^='']").length&&h.push("[*^$]="+_+"*(?:\"\"|'')"),e.querySelectorAll(":enabled").length||h.push(":enabled",":disabled"),e.querySelectorAll("*,:x"),h.push(",.*:")})),(T.matchesSelector=rt(m=f.matchesSelector||f.mozMatchesSelector||f.webkitMatchesSelector||f.oMatchesSelector||f.msMatchesSelector))&&at(function(e){T.disconnectedMatch=m.call(e,"div"),m.call(e,"[s!='']:x"),g.push("!=",R)}),h=RegExp(h.join("|")),g=RegExp(g.join("|")),y=rt(f.contains)||f.compareDocumentPosition?function(e,t){var n=9===e.nodeType?e.documentElement:e,r=t&&t.parentNode;return e===r||!(!r||1!==r.nodeType||!(n.contains?n.contains(r):e.compareDocumentPosition&&16&e.compareDocumentPosition(r)))}:function(e,t){if(t)while(t=t.parentNode)if(t===e)return!0;return!1},v=f.compareDocumentPosition?function(e,t){var r;return e===t?(u=!0,0):(r=t.compareDocumentPosition&&e.compareDocumentPosition&&e.compareDocumentPosition(t))?1&r||e.parentNode&&11===e.parentNode.nodeType?e===n||y(w,e)?-1:t===n||y(w,t)?1:0:4&r?-1:1:e.compareDocumentPosition?-1:1}:function(e,t){var r,i=0,o=e.parentNode,a=t.parentNode,s=[e],l=[t];if(e===t)return u=!0,0;if(!o||!a)return e===n?-1:t===n?1:o?-1:a?1:0;if(o===a)return ut(e,t);r=e;while(r=r.parentNode)s.unshift(r);r=t;while(r=r.parentNode)l.unshift(r);while(s[i]===l[i])i++;return i?ut(s[i],l[i]):s[i]===w?-1:l[i]===w?1:0},u=!1,[0,0].sort(v),T.detectDuplicates=u,p):p},st.matches=function(e,t){return st(e,null,null,t)},st.matchesSelector=function(e,t){if((e.ownerDocument||e)!==p&&c(e),t=t.replace(Z,"='$1']"),!(!T.matchesSelector||d||g&&g.test(t)||h.test(t)))try{var n=m.call(e,t);if(n||T.disconnectedMatch||e.document&&11!==e.document.nodeType)return n}catch(r){}return st(t,p,null,[e]).length>0},st.contains=function(e,t){return(e.ownerDocument||e)!==p&&c(e),y(e,t)},st.attr=function(e,t){var n;return(e.ownerDocument||e)!==p&&c(e),d||(t=t.toLowerCase()),(n=i.attrHandle[t])?n(e):d||T.attributes?e.getAttribute(t):((n=e.getAttributeNode(t))||e.getAttribute(t))&&e[t]===!0?t:n&&n.specified?n.value:null},st.error=function(e){throw Error("Syntax error, unrecognized expression: "+e)},st.uniqueSort=function(e){var t,n=[],r=1,i=0;if(u=!T.detectDuplicates,e.sort(v),u){for(;t=e[r];r++)t===e[r-1]&&(i=n.push(r));while(i--)e.splice(n[i],1)}return e};function ut(e,t){var n=t&&e,r=n&&(~t.sourceIndex||j)-(~e.sourceIndex||j);if(r)return r;if(n)while(n=n.nextSibling)if(n===t)return-1;return e?1:-1}function lt(e){return function(t){var n=t.nodeName.toLowerCase();return"input"===n&&t.type===e}}function ct(e){return function(t){var n=t.nodeName.toLowerCase();return("input"===n||"button"===n)&&t.type===e}}function pt(e){return ot(function(t){return t=+t,ot(function(n,r){var i,o=e([],n.length,t),a=o.length;while(a--)n[i=o[a]]&&(n[i]=!(r[i]=n[i]))})})}o=st.getText=function(e){var t,n="",r=0,i=e.nodeType;if(i){if(1===i||9===i||11===i){if("string"==typeof e.textContent)return e.textContent;for(e=e.firstChild;e;e=e.nextSibling)n+=o(e)}else if(3===i||4===i)return e.nodeValue}else for(;t=e[r];r++)n+=o(t);return n},i=st.selectors={cacheLength:50,createPseudo:ot,match:U,find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(e){return e[1]=e[1].replace(et,tt),e[3]=(e[4]||e[5]||"").replace(et,tt),"~="===e[2]&&(e[3]=" "+e[3]+" "),e.slice(0,4)},CHILD:function(e){return e[1]=e[1].toLowerCase(),"nth"===e[1].slice(0,3)?(e[3]||st.error(e[0]),e[4]=+(e[4]?e[5]+(e[6]||1):2*("even"===e[3]||"odd"===e[3])),e[5]=+(e[7]+e[8]||"odd"===e[3])):e[3]&&st.error(e[0]),e},PSEUDO:function(e){var t,n=!e[5]&&e[2];return U.CHILD.test(e[0])?null:(e[4]?e[2]=e[4]:n&&z.test(n)&&(t=ft(n,!0))&&(t=n.indexOf(")",n.length-t)-n.length)&&(e[0]=e[0].slice(0,t),e[2]=n.slice(0,t)),e.slice(0,3))}},filter:{TAG:function(e){return"*"===e?function(){return!0}:(e=e.replace(et,tt).toLowerCase(),function(t){return t.nodeName&&t.nodeName.toLowerCase()===e})},CLASS:function(e){var t=k[e+" "];return t||(t=RegExp("(^|"+_+")"+e+"("+_+"|$)"))&&k(e,function(e){return t.test(e.className||typeof e.getAttribute!==A&&e.getAttribute("class")||"")})},ATTR:function(e,t,n){return function(r){var i=st.attr(r,e);return null==i?"!="===t:t?(i+="","="===t?i===n:"!="===t?i!==n:"^="===t?n&&0===i.indexOf(n):"*="===t?n&&i.indexOf(n)>-1:"$="===t?n&&i.slice(-n.length)===n:"~="===t?(" "+i+" ").indexOf(n)>-1:"|="===t?i===n||i.slice(0,n.length+1)===n+"-":!1):!0}},CHILD:function(e,t,n,r,i){var o="nth"!==e.slice(0,3),a="last"!==e.slice(-4),s="of-type"===t;return 1===r&&0===i?function(e){return!!e.parentNode}:function(t,n,u){var l,c,p,f,d,h,g=o!==a?"nextSibling":"previousSibling",m=t.parentNode,y=s&&t.nodeName.toLowerCase(),v=!u&&!s;if(m){if(o){while(g){p=t;while(p=p[g])if(s?p.nodeName.toLowerCase()===y:1===p.nodeType)return!1;h=g="only"===e&&!h&&"nextSibling"}return!0}if(h=[a?m.firstChild:m.lastChild],a&&v){c=m[x]||(m[x]={}),l=c[e]||[],d=l[0]===N&&l[1],f=l[0]===N&&l[2],p=d&&m.childNodes[d];while(p=++d&&p&&p[g]||(f=d=0)||h.pop())if(1===p.nodeType&&++f&&p===t){c[e]=[N,d,f];break}}else if(v&&(l=(t[x]||(t[x]={}))[e])&&l[0]===N)f=l[1];else while(p=++d&&p&&p[g]||(f=d=0)||h.pop())if((s?p.nodeName.toLowerCase()===y:1===p.nodeType)&&++f&&(v&&((p[x]||(p[x]={}))[e]=[N,f]),p===t))break;return f-=i,f===r||0===f%r&&f/r>=0}}},PSEUDO:function(e,t){var n,r=i.pseudos[e]||i.setFilters[e.toLowerCase()]||st.error("unsupported pseudo: "+e);return r[x]?r(t):r.length>1?(n=[e,e,"",t],i.setFilters.hasOwnProperty(e.toLowerCase())?ot(function(e,n){var i,o=r(e,t),a=o.length;while(a--)i=M.call(e,o[a]),e[i]=!(n[i]=o[a])}):function(e){return r(e,0,n)}):r}},pseudos:{not:ot(function(e){var t=[],n=[],r=s(e.replace(W,"$1"));return r[x]?ot(function(e,t,n,i){var o,a=r(e,null,i,[]),s=e.length;while(s--)(o=a[s])&&(e[s]=!(t[s]=o))}):function(e,i,o){return t[0]=e,r(t,null,o,n),!n.pop()}}),has:ot(function(e){return function(t){return st(e,t).length>0}}),contains:ot(function(e){return function(t){return(t.textContent||t.innerText||o(t)).indexOf(e)>-1}}),lang:ot(function(e){return X.test(e||"")||st.error("unsupported lang: "+e),e=e.replace(et,tt).toLowerCase(),function(t){var n;do if(n=d?t.getAttribute("xml:lang")||t.getAttribute("lang"):t.lang)return n=n.toLowerCase(),n===e||0===n.indexOf(e+"-");while((t=t.parentNode)&&1===t.nodeType);return!1}}),target:function(t){var n=e.location&&e.location.hash;return n&&n.slice(1)===t.id},root:function(e){return e===f},focus:function(e){return e===p.activeElement&&(!p.hasFocus||p.hasFocus())&&!!(e.type||e.href||~e.tabIndex)},enabled:function(e){return e.disabled===!1},disabled:function(e){return e.disabled===!0},checked:function(e){var t=e.nodeName.toLowerCase();return"input"===t&&!!e.checked||"option"===t&&!!e.selected},selected:function(e){return e.parentNode&&e.parentNode.selectedIndex,e.selected===!0},empty:function(e){for(e=e.firstChild;e;e=e.nextSibling)if(e.nodeName>"@"||3===e.nodeType||4===e.nodeType)return!1;return!0},parent:function(e){return!i.pseudos.empty(e)},header:function(e){return Q.test(e.nodeName)},input:function(e){return G.test(e.nodeName)},button:function(e){var t=e.nodeName.toLowerCase();return"input"===t&&"button"===e.type||"button"===t},text:function(e){var t;return"input"===e.nodeName.toLowerCase()&&"text"===e.type&&(null==(t=e.getAttribute("type"))||t.toLowerCase()===e.type)},first:pt(function(){return[0]}),last:pt(function(e,t){return[t-1]}),eq:pt(function(e,t,n){return[0>n?n+t:n]}),even:pt(function(e,t){var n=0;for(;t>n;n+=2)e.push(n);return e}),odd:pt(function(e,t){var n=1;for(;t>n;n+=2)e.push(n);return e}),lt:pt(function(e,t,n){var r=0>n?n+t:n;for(;--r>=0;)e.push(r);return e}),gt:pt(function(e,t,n){var r=0>n?n+t:n;for(;t>++r;)e.push(r);return e})}};for(n in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})i.pseudos[n]=lt(n);for(n in{submit:!0,reset:!0})i.pseudos[n]=ct(n);function ft(e,t){var n,r,o,a,s,u,l,c=E[e+" "];if(c)return t?0:c.slice(0);s=e,u=[],l=i.preFilter;while(s){(!n||(r=$.exec(s)))&&(r&&(s=s.slice(r[0].length)||s),u.push(o=[])),n=!1,(r=I.exec(s))&&(n=r.shift(),o.push({value:n,type:r[0].replace(W," ")}),s=s.slice(n.length));for(a in i.filter)!(r=U[a].exec(s))||l[a]&&!(r=l[a](r))||(n=r.shift(),o.push({value:n,type:a,matches:r}),s=s.slice(n.length));if(!n)break}return t?s.length:s?st.error(e):E(e,u).slice(0)}function dt(e){var t=0,n=e.length,r="";for(;n>t;t++)r+=e[t].value;return r}function ht(e,t,n){var i=t.dir,o=n&&"parentNode"===i,a=C++;return t.first?function(t,n,r){while(t=t[i])if(1===t.nodeType||o)return e(t,n,r)}:function(t,n,s){var u,l,c,p=N+" "+a;if(s){while(t=t[i])if((1===t.nodeType||o)&&e(t,n,s))return!0}else while(t=t[i])if(1===t.nodeType||o)if(c=t[x]||(t[x]={}),(l=c[i])&&l[0]===p){if((u=l[1])===!0||u===r)return u===!0}else if(l=c[i]=[p],l[1]=e(t,n,s)||r,l[1]===!0)return!0}}function gt(e){return e.length>1?function(t,n,r){var i=e.length;while(i--)if(!e[i](t,n,r))return!1;return!0}:e[0]}function mt(e,t,n,r,i){var o,a=[],s=0,u=e.length,l=null!=t;for(;u>s;s++)(o=e[s])&&(!n||n(o,r,i))&&(a.push(o),l&&t.push(s));return a}function yt(e,t,n,r,i,o){return r&&!r[x]&&(r=yt(r)),i&&!i[x]&&(i=yt(i,o)),ot(function(o,a,s,u){var l,c,p,f=[],d=[],h=a.length,g=o||xt(t||"*",s.nodeType?[s]:s,[]),m=!e||!o&&t?g:mt(g,f,e,s,u),y=n?i||(o?e:h||r)?[]:a:m;if(n&&n(m,y,s,u),r){l=mt(y,d),r(l,[],s,u),c=l.length;while(c--)(p=l[c])&&(y[d[c]]=!(m[d[c]]=p))}if(o){if(i||e){if(i){l=[],c=y.length;while(c--)(p=y[c])&&l.push(m[c]=p);i(null,y=[],l,u)}c=y.length;while(c--)(p=y[c])&&(l=i?M.call(o,p):f[c])>-1&&(o[l]=!(a[l]=p))}}else y=mt(y===a?y.splice(h,y.length):y),i?i(null,a,y,u):H.apply(a,y)})}function vt(e){var t,n,r,o=e.length,a=i.relative[e[0].type],s=a||i.relative[" "],u=a?1:0,c=ht(function(e){return e===t},s,!0),p=ht(function(e){return M.call(t,e)>-1},s,!0),f=[function(e,n,r){return!a&&(r||n!==l)||((t=n).nodeType?c(e,n,r):p(e,n,r))}];for(;o>u;u++)if(n=i.relative[e[u].type])f=[ht(gt(f),n)];else{if(n=i.filter[e[u].type].apply(null,e[u].matches),n[x]){for(r=++u;o>r;r++)if(i.relative[e[r].type])break;return yt(u>1&&gt(f),u>1&&dt(e.slice(0,u-1)).replace(W,"$1"),n,r>u&&vt(e.slice(u,r)),o>r&&vt(e=e.slice(r)),o>r&&dt(e))}f.push(n)}return gt(f)}function bt(e,t){var n=0,o=t.length>0,a=e.length>0,s=function(s,u,c,f,d){var h,g,m,y=[],v=0,b="0",x=s&&[],w=null!=d,T=l,C=s||a&&i.find.TAG("*",d&&u.parentNode||u),k=N+=null==T?1:Math.random()||.1;for(w&&(l=u!==p&&u,r=n);null!=(h=C[b]);b++){if(a&&h){g=0;while(m=e[g++])if(m(h,u,c)){f.push(h);break}w&&(N=k,r=++n)}o&&((h=!m&&h)&&v--,s&&x.push(h))}if(v+=b,o&&b!==v){g=0;while(m=t[g++])m(x,y,u,c);if(s){if(v>0)while(b--)x[b]||y[b]||(y[b]=L.call(f));y=mt(y)}H.apply(f,y),w&&!s&&y.length>0&&v+t.length>1&&st.uniqueSort(f)}return w&&(N=k,l=T),x};return o?ot(s):s}s=st.compile=function(e,t){var n,r=[],i=[],o=S[e+" "];if(!o){t||(t=ft(e)),n=t.length;while(n--)o=vt(t[n]),o[x]?r.push(o):i.push(o);o=S(e,bt(i,r))}return o};function xt(e,t,n){var r=0,i=t.length;for(;i>r;r++)st(e,t[r],n);return n}function wt(e,t,n,r){var o,a,u,l,c,p=ft(e);if(!r&&1===p.length){if(a=p[0]=p[0].slice(0),a.length>2&&"ID"===(u=a[0]).type&&9===t.nodeType&&!d&&i.relative[a[1].type]){if(t=i.find.ID(u.matches[0].replace(et,tt),t)[0],!t)return n;e=e.slice(a.shift().value.length)}o=U.needsContext.test(e)?0:a.length;while(o--){if(u=a[o],i.relative[l=u.type])break;if((c=i.find[l])&&(r=c(u.matches[0].replace(et,tt),V.test(a[0].type)&&t.parentNode||t))){if(a.splice(o,1),e=r.length&&dt(a),!e)return H.apply(n,q.call(r,0)),n;break}}}return s(e,p)(r,t,d,n,V.test(e)),n}i.pseudos.nth=i.pseudos.eq;function Tt(){}i.filters=Tt.prototype=i.pseudos,i.setFilters=new Tt,c(),st.attr=b.attr,b.find=st,b.expr=st.selectors,b.expr[":"]=b.expr.pseudos,b.unique=st.uniqueSort,b.text=st.getText,b.isXMLDoc=st.isXML,b.contains=st.contains}(e);var at=/Until$/,st=/^(?:parents|prev(?:Until|All))/,ut=/^.[^:#\[\.,]*$/,lt=b.expr.match.needsContext,ct={children:!0,contents:!0,next:!0,prev:!0};b.fn.extend({find:function(e){var t,n,r,i=this.length;if("string"!=typeof e)return r=this,this.pushStack(b(e).filter(function(){for(t=0;i>t;t++)if(b.contains(r[t],this))return!0}));for(n=[],t=0;i>t;t++)b.find(e,this[t],n);return n=this.pushStack(i>1?b.unique(n):n),n.selector=(this.selector?this.selector+" ":"")+e,n},has:function(e){var t,n=b(e,this),r=n.length;return this.filter(function(){for(t=0;r>t;t++)if(b.contains(this,n[t]))return!0})},not:function(e){return this.pushStack(ft(this,e,!1))},filter:function(e){return this.pushStack(ft(this,e,!0))},is:function(e){return!!e&&("string"==typeof e?lt.test(e)?b(e,this.context).index(this[0])>=0:b.filter(e,this).length>0:this.filter(e).length>0)},closest:function(e,t){var n,r=0,i=this.length,o=[],a=lt.test(e)||"string"!=typeof e?b(e,t||this.context):0;for(;i>r;r++){n=this[r];while(n&&n.ownerDocument&&n!==t&&11!==n.nodeType){if(a?a.index(n)>-1:b.find.matchesSelector(n,e)){o.push(n);break}n=n.parentNode}}return this.pushStack(o.length>1?b.unique(o):o)},index:function(e){return e?"string"==typeof e?b.inArray(this[0],b(e)):b.inArray(e.jquery?e[0]:e,this):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(e,t){var n="string"==typeof e?b(e,t):b.makeArray(e&&e.nodeType?[e]:e),r=b.merge(this.get(),n);return this.pushStack(b.unique(r))},addBack:function(e){return this.add(null==e?this.prevObject:this.prevObject.filter(e))}}),b.fn.andSelf=b.fn.addBack;function pt(e,t){do e=e[t];while(e&&1!==e.nodeType);return e}b.each({parent:function(e){var t=e.parentNode;return t&&11!==t.nodeType?t:null},parents:function(e){return b.dir(e,"parentNode")},parentsUntil:function(e,t,n){return b.dir(e,"parentNode",n)},next:function(e){return pt(e,"nextSibling")},prev:function(e){return pt(e,"previousSibling")},nextAll:function(e){return b.dir(e,"nextSibling")},prevAll:function(e){return b.dir(e,"previousSibling")},nextUntil:function(e,t,n){return b.dir(e,"nextSibling",n)},prevUntil:function(e,t,n){return b.dir(e,"previousSibling",n)},siblings:function(e){return b.sibling((e.parentNode||{}).firstChild,e)},children:function(e){return b.sibling(e.firstChild)},contents:function(e){return b.nodeName(e,"iframe")?e.contentDocument||e.contentWindow.document:b.merge([],e.childNodes)}},function(e,t){b.fn[e]=function(n,r){var i=b.map(this,t,n);return at.test(e)||(r=n),r&&"string"==typeof r&&(i=b.filter(r,i)),i=this.length>1&&!ct[e]?b.unique(i):i,this.length>1&&st.test(e)&&(i=i.reverse()),this.pushStack(i)}}),b.extend({filter:function(e,t,n){return n&&(e=":not("+e+")"),1===t.length?b.find.matchesSelector(t[0],e)?[t[0]]:[]:b.find.matches(e,t)},dir:function(e,n,r){var i=[],o=e[n];while(o&&9!==o.nodeType&&(r===t||1!==o.nodeType||!b(o).is(r)))1===o.nodeType&&i.push(o),o=o[n];return i},sibling:function(e,t){var n=[];for(;e;e=e.nextSibling)1===e.nodeType&&e!==t&&n.push(e);return n}});function ft(e,t,n){if(t=t||0,b.isFunction(t))return b.grep(e,function(e,r){var i=!!t.call(e,r,e);return i===n});if(t.nodeType)return b.grep(e,function(e){return e===t===n});if("string"==typeof t){var r=b.grep(e,function(e){return 1===e.nodeType});if(ut.test(t))return b.filter(t,r,!n);t=b.filter(t,r)}return b.grep(e,function(e){return b.inArray(e,t)>=0===n})}function dt(e){var t=ht.split("|"),n=e.createDocumentFragment();if(n.createElement)while(t.length)n.createElement(t.pop());return n}var ht="abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",gt=/ jQuery\d+="(?:null|\d+)"/g,mt=RegExp("<(?:"+ht+")[\\s/>]","i"),yt=/^\s+/,vt=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,bt=/<([\w:]+)/,xt=/<tbody/i,wt=/<|&#?\w+;/,Tt=/<(?:script|style|link)/i,Nt=/^(?:checkbox|radio)$/i,Ct=/checked\s*(?:[^=]|=\s*.checked.)/i,kt=/^$|\/(?:java|ecma)script/i,Et=/^true\/(.*)/,St=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,At={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],area:[1,"<map>","</map>"],param:[1,"<object>","</object>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:b.support.htmlSerialize?[0,"",""]:[1,"X<div>","</div>"]},jt=dt(o),Dt=jt.appendChild(o.createElement("div"));At.optgroup=At.option,At.tbody=At.tfoot=At.colgroup=At.caption=At.thead,At.th=At.td,b.fn.extend({text:function(e){return b.access(this,function(e){return e===t?b.text(this):this.empty().append((this[0]&&this[0].ownerDocument||o).createTextNode(e))},null,e,arguments.length)},wrapAll:function(e){if(b.isFunction(e))return this.each(function(t){b(this).wrapAll(e.call(this,t))});if(this[0]){var t=b(e,this[0].ownerDocument).eq(0).clone(!0);this[0].parentNode&&t.insertBefore(this[0]),t.map(function(){var e=this;while(e.firstChild&&1===e.firstChild.nodeType)e=e.firstChild;return e}).append(this)}return this},wrapInner:function(e){return b.isFunction(e)?this.each(function(t){b(this).wrapInner(e.call(this,t))}):this.each(function(){var t=b(this),n=t.contents();n.length?n.wrapAll(e):t.append(e)})},wrap:function(e){var t=b.isFunction(e);return this.each(function(n){b(this).wrapAll(t?e.call(this,n):e)})},unwrap:function(){return this.parent().each(function(){b.nodeName(this,"body")||b(this).replaceWith(this.childNodes)}).end()},append:function(){return this.domManip(arguments,!0,function(e){(1===this.nodeType||11===this.nodeType||9===this.nodeType)&&this.appendChild(e)})},prepend:function(){return this.domManip(arguments,!0,function(e){(1===this.nodeType||11===this.nodeType||9===this.nodeType)&&this.insertBefore(e,this.firstChild)})},before:function(){return this.domManip(arguments,!1,function(e){this.parentNode&&this.parentNode.insertBefore(e,this)})},after:function(){return this.domManip(arguments,!1,function(e){this.parentNode&&this.parentNode.insertBefore(e,this.nextSibling)})},remove:function(e,t){var n,r=0;for(;null!=(n=this[r]);r++)(!e||b.filter(e,[n]).length>0)&&(t||1!==n.nodeType||b.cleanData(Ot(n)),n.parentNode&&(t&&b.contains(n.ownerDocument,n)&&Mt(Ot(n,"script")),n.parentNode.removeChild(n)));return this},empty:function(){var e,t=0;for(;null!=(e=this[t]);t++){1===e.nodeType&&b.cleanData(Ot(e,!1));while(e.firstChild)e.removeChild(e.firstChild);e.options&&b.nodeName(e,"select")&&(e.options.length=0)}return this},clone:function(e,t){return e=null==e?!1:e,t=null==t?e:t,this.map(function(){return b.clone(this,e,t)})},html:function(e){return b.access(this,function(e){var n=this[0]||{},r=0,i=this.length;if(e===t)return 1===n.nodeType?n.innerHTML.replace(gt,""):t;if(!("string"!=typeof e||Tt.test(e)||!b.support.htmlSerialize&&mt.test(e)||!b.support.leadingWhitespace&&yt.test(e)||At[(bt.exec(e)||["",""])[1].toLowerCase()])){e=e.replace(vt,"<$1></$2>");try{for(;i>r;r++)n=this[r]||{},1===n.nodeType&&(b.cleanData(Ot(n,!1)),n.innerHTML=e);n=0}catch(o){}}n&&this.empty().append(e)},null,e,arguments.length)},replaceWith:function(e){var t=b.isFunction(e);return t||"string"==typeof e||(e=b(e).not(this).detach()),this.domManip([e],!0,function(e){var t=this.nextSibling,n=this.parentNode;n&&(b(this).remove(),n.insertBefore(e,t))})},detach:function(e){return this.remove(e,!0)},domManip:function(e,n,r){e=f.apply([],e);var i,o,a,s,u,l,c=0,p=this.length,d=this,h=p-1,g=e[0],m=b.isFunction(g);if(m||!(1>=p||"string"!=typeof g||b.support.checkClone)&&Ct.test(g))return this.each(function(i){var o=d.eq(i);m&&(e[0]=g.call(this,i,n?o.html():t)),o.domManip(e,n,r)});if(p&&(l=b.buildFragment(e,this[0].ownerDocument,!1,this),i=l.firstChild,1===l.childNodes.length&&(l=i),i)){for(n=n&&b.nodeName(i,"tr"),s=b.map(Ot(l,"script"),Ht),a=s.length;p>c;c++)o=l,c!==h&&(o=b.clone(o,!0,!0),a&&b.merge(s,Ot(o,"script"))),r.call(n&&b.nodeName(this[c],"table")?Lt(this[c],"tbody"):this[c],o,c);if(a)for(u=s[s.length-1].ownerDocument,b.map(s,qt),c=0;a>c;c++)o=s[c],kt.test(o.type||"")&&!b._data(o,"globalEval")&&b.contains(u,o)&&(o.src?b.ajax({url:o.src,type:"GET",dataType:"script",async:!1,global:!1,"throws":!0}):b.globalEval((o.text||o.textContent||o.innerHTML||"").replace(St,"")));l=i=null}return this}});function Lt(e,t){return e.getElementsByTagName(t)[0]||e.appendChild(e.ownerDocument.createElement(t))}function Ht(e){var t=e.getAttributeNode("type");return e.type=(t&&t.specified)+"/"+e.type,e}function qt(e){var t=Et.exec(e.type);return t?e.type=t[1]:e.removeAttribute("type"),e}function Mt(e,t){var n,r=0;for(;null!=(n=e[r]);r++)b._data(n,"globalEval",!t||b._data(t[r],"globalEval"))}function _t(e,t){if(1===t.nodeType&&b.hasData(e)){var n,r,i,o=b._data(e),a=b._data(t,o),s=o.events;if(s){delete a.handle,a.events={};for(n in s)for(r=0,i=s[n].length;i>r;r++)b.event.add(t,n,s[n][r])}a.data&&(a.data=b.extend({},a.data))}}function Ft(e,t){var n,r,i;if(1===t.nodeType){if(n=t.nodeName.toLowerCase(),!b.support.noCloneEvent&&t[b.expando]){i=b._data(t);for(r in i.events)b.removeEvent(t,r,i.handle);t.removeAttribute(b.expando)}"script"===n&&t.text!==e.text?(Ht(t).text=e.text,qt(t)):"object"===n?(t.parentNode&&(t.outerHTML=e.outerHTML),b.support.html5Clone&&e.innerHTML&&!b.trim(t.innerHTML)&&(t.innerHTML=e.innerHTML)):"input"===n&&Nt.test(e.type)?(t.defaultChecked=t.checked=e.checked,t.value!==e.value&&(t.value=e.value)):"option"===n?t.defaultSelected=t.selected=e.defaultSelected:("input"===n||"textarea"===n)&&(t.defaultValue=e.defaultValue)}}b.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(e,t){b.fn[e]=function(e){var n,r=0,i=[],o=b(e),a=o.length-1;for(;a>=r;r++)n=r===a?this:this.clone(!0),b(o[r])[t](n),d.apply(i,n.get());return this.pushStack(i)}});function Ot(e,n){var r,o,a=0,s=typeof e.getElementsByTagName!==i?e.getElementsByTagName(n||"*"):typeof e.querySelectorAll!==i?e.querySelectorAll(n||"*"):t;if(!s)for(s=[],r=e.childNodes||e;null!=(o=r[a]);a++)!n||b.nodeName(o,n)?s.push(o):b.merge(s,Ot(o,n));return n===t||n&&b.nodeName(e,n)?b.merge([e],s):s}function Bt(e){Nt.test(e.type)&&(e.defaultChecked=e.checked)}b.extend({clone:function(e,t,n){var r,i,o,a,s,u=b.contains(e.ownerDocument,e);if(b.support.html5Clone||b.isXMLDoc(e)||!mt.test("<"+e.nodeName+">")?o=e.cloneNode(!0):(Dt.innerHTML=e.outerHTML,Dt.removeChild(o=Dt.firstChild)),!(b.support.noCloneEvent&&b.support.noCloneChecked||1!==e.nodeType&&11!==e.nodeType||b.isXMLDoc(e)))for(r=Ot(o),s=Ot(e),a=0;null!=(i=s[a]);++a)r[a]&&Ft(i,r[a]);if(t)if(n)for(s=s||Ot(e),r=r||Ot(o),a=0;null!=(i=s[a]);a++)_t(i,r[a]);else _t(e,o);return r=Ot(o,"script"),r.length>0&&Mt(r,!u&&Ot(e,"script")),r=s=i=null,o},buildFragment:function(e,t,n,r){var i,o,a,s,u,l,c,p=e.length,f=dt(t),d=[],h=0;for(;p>h;h++)if(o=e[h],o||0===o)if("object"===b.type(o))b.merge(d,o.nodeType?[o]:o);else if(wt.test(o)){s=s||f.appendChild(t.createElement("div")),u=(bt.exec(o)||["",""])[1].toLowerCase(),c=At[u]||At._default,s.innerHTML=c[1]+o.replace(vt,"<$1></$2>")+c[2],i=c[0];while(i--)s=s.lastChild;if(!b.support.leadingWhitespace&&yt.test(o)&&d.push(t.createTextNode(yt.exec(o)[0])),!b.support.tbody){o="table"!==u||xt.test(o)?"<table>"!==c[1]||xt.test(o)?0:s:s.firstChild,i=o&&o.childNodes.length;while(i--)b.nodeName(l=o.childNodes[i],"tbody")&&!l.childNodes.length&&o.removeChild(l)
}b.merge(d,s.childNodes),s.textContent="";while(s.firstChild)s.removeChild(s.firstChild);s=f.lastChild}else d.push(t.createTextNode(o));s&&f.removeChild(s),b.support.appendChecked||b.grep(Ot(d,"input"),Bt),h=0;while(o=d[h++])if((!r||-1===b.inArray(o,r))&&(a=b.contains(o.ownerDocument,o),s=Ot(f.appendChild(o),"script"),a&&Mt(s),n)){i=0;while(o=s[i++])kt.test(o.type||"")&&n.push(o)}return s=null,f},cleanData:function(e,t){var n,r,o,a,s=0,u=b.expando,l=b.cache,p=b.support.deleteExpando,f=b.event.special;for(;null!=(n=e[s]);s++)if((t||b.acceptData(n))&&(o=n[u],a=o&&l[o])){if(a.events)for(r in a.events)f[r]?b.event.remove(n,r):b.removeEvent(n,r,a.handle);l[o]&&(delete l[o],p?delete n[u]:typeof n.removeAttribute!==i?n.removeAttribute(u):n[u]=null,c.push(o))}}});var Pt,Rt,Wt,$t=/alpha\([^)]*\)/i,It=/opacity\s*=\s*([^)]*)/,zt=/^(top|right|bottom|left)$/,Xt=/^(none|table(?!-c[ea]).+)/,Ut=/^margin/,Vt=RegExp("^("+x+")(.*)$","i"),Yt=RegExp("^("+x+")(?!px)[a-z%]+$","i"),Jt=RegExp("^([+-])=("+x+")","i"),Gt={BODY:"block"},Qt={position:"absolute",visibility:"hidden",display:"block"},Kt={letterSpacing:0,fontWeight:400},Zt=["Top","Right","Bottom","Left"],en=["Webkit","O","Moz","ms"];function tn(e,t){if(t in e)return t;var n=t.charAt(0).toUpperCase()+t.slice(1),r=t,i=en.length;while(i--)if(t=en[i]+n,t in e)return t;return r}function nn(e,t){return e=t||e,"none"===b.css(e,"display")||!b.contains(e.ownerDocument,e)}function rn(e,t){var n,r,i,o=[],a=0,s=e.length;for(;s>a;a++)r=e[a],r.style&&(o[a]=b._data(r,"olddisplay"),n=r.style.display,t?(o[a]||"none"!==n||(r.style.display=""),""===r.style.display&&nn(r)&&(o[a]=b._data(r,"olddisplay",un(r.nodeName)))):o[a]||(i=nn(r),(n&&"none"!==n||!i)&&b._data(r,"olddisplay",i?n:b.css(r,"display"))));for(a=0;s>a;a++)r=e[a],r.style&&(t&&"none"!==r.style.display&&""!==r.style.display||(r.style.display=t?o[a]||"":"none"));return e}b.fn.extend({css:function(e,n){return b.access(this,function(e,n,r){var i,o,a={},s=0;if(b.isArray(n)){for(o=Rt(e),i=n.length;i>s;s++)a[n[s]]=b.css(e,n[s],!1,o);return a}return r!==t?b.style(e,n,r):b.css(e,n)},e,n,arguments.length>1)},show:function(){return rn(this,!0)},hide:function(){return rn(this)},toggle:function(e){var t="boolean"==typeof e;return this.each(function(){(t?e:nn(this))?b(this).show():b(this).hide()})}}),b.extend({cssHooks:{opacity:{get:function(e,t){if(t){var n=Wt(e,"opacity");return""===n?"1":n}}}},cssNumber:{columnCount:!0,fillOpacity:!0,fontWeight:!0,lineHeight:!0,opacity:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":b.support.cssFloat?"cssFloat":"styleFloat"},style:function(e,n,r,i){if(e&&3!==e.nodeType&&8!==e.nodeType&&e.style){var o,a,s,u=b.camelCase(n),l=e.style;if(n=b.cssProps[u]||(b.cssProps[u]=tn(l,u)),s=b.cssHooks[n]||b.cssHooks[u],r===t)return s&&"get"in s&&(o=s.get(e,!1,i))!==t?o:l[n];if(a=typeof r,"string"===a&&(o=Jt.exec(r))&&(r=(o[1]+1)*o[2]+parseFloat(b.css(e,n)),a="number"),!(null==r||"number"===a&&isNaN(r)||("number"!==a||b.cssNumber[u]||(r+="px"),b.support.clearCloneStyle||""!==r||0!==n.indexOf("background")||(l[n]="inherit"),s&&"set"in s&&(r=s.set(e,r,i))===t)))try{l[n]=r}catch(c){}}},css:function(e,n,r,i){var o,a,s,u=b.camelCase(n);return n=b.cssProps[u]||(b.cssProps[u]=tn(e.style,u)),s=b.cssHooks[n]||b.cssHooks[u],s&&"get"in s&&(a=s.get(e,!0,r)),a===t&&(a=Wt(e,n,i)),"normal"===a&&n in Kt&&(a=Kt[n]),""===r||r?(o=parseFloat(a),r===!0||b.isNumeric(o)?o||0:a):a},swap:function(e,t,n,r){var i,o,a={};for(o in t)a[o]=e.style[o],e.style[o]=t[o];i=n.apply(e,r||[]);for(o in t)e.style[o]=a[o];return i}}),e.getComputedStyle?(Rt=function(t){return e.getComputedStyle(t,null)},Wt=function(e,n,r){var i,o,a,s=r||Rt(e),u=s?s.getPropertyValue(n)||s[n]:t,l=e.style;return s&&(""!==u||b.contains(e.ownerDocument,e)||(u=b.style(e,n)),Yt.test(u)&&Ut.test(n)&&(i=l.width,o=l.minWidth,a=l.maxWidth,l.minWidth=l.maxWidth=l.width=u,u=s.width,l.width=i,l.minWidth=o,l.maxWidth=a)),u}):o.documentElement.currentStyle&&(Rt=function(e){return e.currentStyle},Wt=function(e,n,r){var i,o,a,s=r||Rt(e),u=s?s[n]:t,l=e.style;return null==u&&l&&l[n]&&(u=l[n]),Yt.test(u)&&!zt.test(n)&&(i=l.left,o=e.runtimeStyle,a=o&&o.left,a&&(o.left=e.currentStyle.left),l.left="fontSize"===n?"1em":u,u=l.pixelLeft+"px",l.left=i,a&&(o.left=a)),""===u?"auto":u});function on(e,t,n){var r=Vt.exec(t);return r?Math.max(0,r[1]-(n||0))+(r[2]||"px"):t}function an(e,t,n,r,i){var o=n===(r?"border":"content")?4:"width"===t?1:0,a=0;for(;4>o;o+=2)"margin"===n&&(a+=b.css(e,n+Zt[o],!0,i)),r?("content"===n&&(a-=b.css(e,"padding"+Zt[o],!0,i)),"margin"!==n&&(a-=b.css(e,"border"+Zt[o]+"Width",!0,i))):(a+=b.css(e,"padding"+Zt[o],!0,i),"padding"!==n&&(a+=b.css(e,"border"+Zt[o]+"Width",!0,i)));return a}function sn(e,t,n){var r=!0,i="width"===t?e.offsetWidth:e.offsetHeight,o=Rt(e),a=b.support.boxSizing&&"border-box"===b.css(e,"boxSizing",!1,o);if(0>=i||null==i){if(i=Wt(e,t,o),(0>i||null==i)&&(i=e.style[t]),Yt.test(i))return i;r=a&&(b.support.boxSizingReliable||i===e.style[t]),i=parseFloat(i)||0}return i+an(e,t,n||(a?"border":"content"),r,o)+"px"}function un(e){var t=o,n=Gt[e];return n||(n=ln(e,t),"none"!==n&&n||(Pt=(Pt||b("<iframe frameborder='0' width='0' height='0'/>").css("cssText","display:block !important")).appendTo(t.documentElement),t=(Pt[0].contentWindow||Pt[0].contentDocument).document,t.write("<!doctype html><html><body>"),t.close(),n=ln(e,t),Pt.detach()),Gt[e]=n),n}function ln(e,t){var n=b(t.createElement(e)).appendTo(t.body),r=b.css(n[0],"display");return n.remove(),r}b.each(["height","width"],function(e,n){b.cssHooks[n]={get:function(e,r,i){return r?0===e.offsetWidth&&Xt.test(b.css(e,"display"))?b.swap(e,Qt,function(){return sn(e,n,i)}):sn(e,n,i):t},set:function(e,t,r){var i=r&&Rt(e);return on(e,t,r?an(e,n,r,b.support.boxSizing&&"border-box"===b.css(e,"boxSizing",!1,i),i):0)}}}),b.support.opacity||(b.cssHooks.opacity={get:function(e,t){return It.test((t&&e.currentStyle?e.currentStyle.filter:e.style.filter)||"")?.01*parseFloat(RegExp.$1)+"":t?"1":""},set:function(e,t){var n=e.style,r=e.currentStyle,i=b.isNumeric(t)?"alpha(opacity="+100*t+")":"",o=r&&r.filter||n.filter||"";n.zoom=1,(t>=1||""===t)&&""===b.trim(o.replace($t,""))&&n.removeAttribute&&(n.removeAttribute("filter"),""===t||r&&!r.filter)||(n.filter=$t.test(o)?o.replace($t,i):o+" "+i)}}),b(function(){b.support.reliableMarginRight||(b.cssHooks.marginRight={get:function(e,n){return n?b.swap(e,{display:"inline-block"},Wt,[e,"marginRight"]):t}}),!b.support.pixelPosition&&b.fn.position&&b.each(["top","left"],function(e,n){b.cssHooks[n]={get:function(e,r){return r?(r=Wt(e,n),Yt.test(r)?b(e).position()[n]+"px":r):t}}})}),b.expr&&b.expr.filters&&(b.expr.filters.hidden=function(e){return 0>=e.offsetWidth&&0>=e.offsetHeight||!b.support.reliableHiddenOffsets&&"none"===(e.style&&e.style.display||b.css(e,"display"))},b.expr.filters.visible=function(e){return!b.expr.filters.hidden(e)}),b.each({margin:"",padding:"",border:"Width"},function(e,t){b.cssHooks[e+t]={expand:function(n){var r=0,i={},o="string"==typeof n?n.split(" "):[n];for(;4>r;r++)i[e+Zt[r]+t]=o[r]||o[r-2]||o[0];return i}},Ut.test(e)||(b.cssHooks[e+t].set=on)});var cn=/%20/g,pn=/\[\]$/,fn=/\r?\n/g,dn=/^(?:submit|button|image|reset|file)$/i,hn=/^(?:input|select|textarea|keygen)/i;b.fn.extend({serialize:function(){return b.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var e=b.prop(this,"elements");return e?b.makeArray(e):this}).filter(function(){var e=this.type;return this.name&&!b(this).is(":disabled")&&hn.test(this.nodeName)&&!dn.test(e)&&(this.checked||!Nt.test(e))}).map(function(e,t){var n=b(this).val();return null==n?null:b.isArray(n)?b.map(n,function(e){return{name:t.name,value:e.replace(fn,"\r\n")}}):{name:t.name,value:n.replace(fn,"\r\n")}}).get()}}),b.param=function(e,n){var r,i=[],o=function(e,t){t=b.isFunction(t)?t():null==t?"":t,i[i.length]=encodeURIComponent(e)+"="+encodeURIComponent(t)};if(n===t&&(n=b.ajaxSettings&&b.ajaxSettings.traditional),b.isArray(e)||e.jquery&&!b.isPlainObject(e))b.each(e,function(){o(this.name,this.value)});else for(r in e)gn(r,e[r],n,o);return i.join("&").replace(cn,"+")};function gn(e,t,n,r){var i;if(b.isArray(t))b.each(t,function(t,i){n||pn.test(e)?r(e,i):gn(e+"["+("object"==typeof i?t:"")+"]",i,n,r)});else if(n||"object"!==b.type(t))r(e,t);else for(i in t)gn(e+"["+i+"]",t[i],n,r)}b.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(e,t){b.fn[t]=function(e,n){return arguments.length>0?this.on(t,null,e,n):this.trigger(t)}}),b.fn.hover=function(e,t){return this.mouseenter(e).mouseleave(t||e)};var mn,yn,vn=b.now(),bn=/\?/,xn=/#.*$/,wn=/([?&])_=[^&]*/,Tn=/^(.*?):[ \t]*([^\r\n]*)\r?$/gm,Nn=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,Cn=/^(?:GET|HEAD)$/,kn=/^\/\//,En=/^([\w.+-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,Sn=b.fn.load,An={},jn={},Dn="*/".concat("*");try{yn=a.href}catch(Ln){yn=o.createElement("a"),yn.href="",yn=yn.href}mn=En.exec(yn.toLowerCase())||[];function Hn(e){return function(t,n){"string"!=typeof t&&(n=t,t="*");var r,i=0,o=t.toLowerCase().match(w)||[];if(b.isFunction(n))while(r=o[i++])"+"===r[0]?(r=r.slice(1)||"*",(e[r]=e[r]||[]).unshift(n)):(e[r]=e[r]||[]).push(n)}}function qn(e,n,r,i){var o={},a=e===jn;function s(u){var l;return o[u]=!0,b.each(e[u]||[],function(e,u){var c=u(n,r,i);return"string"!=typeof c||a||o[c]?a?!(l=c):t:(n.dataTypes.unshift(c),s(c),!1)}),l}return s(n.dataTypes[0])||!o["*"]&&s("*")}function Mn(e,n){var r,i,o=b.ajaxSettings.flatOptions||{};for(i in n)n[i]!==t&&((o[i]?e:r||(r={}))[i]=n[i]);return r&&b.extend(!0,e,r),e}b.fn.load=function(e,n,r){if("string"!=typeof e&&Sn)return Sn.apply(this,arguments);var i,o,a,s=this,u=e.indexOf(" ");return u>=0&&(i=e.slice(u,e.length),e=e.slice(0,u)),b.isFunction(n)?(r=n,n=t):n&&"object"==typeof n&&(a="POST"),s.length>0&&b.ajax({url:e,type:a,dataType:"html",data:n}).done(function(e){o=arguments,s.html(i?b("<div>").append(b.parseHTML(e)).find(i):e)}).complete(r&&function(e,t){s.each(r,o||[e.responseText,t,e])}),this},b.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(e,t){b.fn[t]=function(e){return this.on(t,e)}}),b.each(["get","post"],function(e,n){b[n]=function(e,r,i,o){return b.isFunction(r)&&(o=o||i,i=r,r=t),b.ajax({url:e,type:n,dataType:o,data:r,success:i})}}),b.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:yn,type:"GET",isLocal:Nn.test(mn[1]),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":Dn,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/xml/,html:/html/,json:/json/},responseFields:{xml:"responseXML",text:"responseText"},converters:{"* text":e.String,"text html":!0,"text json":b.parseJSON,"text xml":b.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(e,t){return t?Mn(Mn(e,b.ajaxSettings),t):Mn(b.ajaxSettings,e)},ajaxPrefilter:Hn(An),ajaxTransport:Hn(jn),ajax:function(e,n){"object"==typeof e&&(n=e,e=t),n=n||{};var r,i,o,a,s,u,l,c,p=b.ajaxSetup({},n),f=p.context||p,d=p.context&&(f.nodeType||f.jquery)?b(f):b.event,h=b.Deferred(),g=b.Callbacks("once memory"),m=p.statusCode||{},y={},v={},x=0,T="canceled",N={readyState:0,getResponseHeader:function(e){var t;if(2===x){if(!c){c={};while(t=Tn.exec(a))c[t[1].toLowerCase()]=t[2]}t=c[e.toLowerCase()]}return null==t?null:t},getAllResponseHeaders:function(){return 2===x?a:null},setRequestHeader:function(e,t){var n=e.toLowerCase();return x||(e=v[n]=v[n]||e,y[e]=t),this},overrideMimeType:function(e){return x||(p.mimeType=e),this},statusCode:function(e){var t;if(e)if(2>x)for(t in e)m[t]=[m[t],e[t]];else N.always(e[N.status]);return this},abort:function(e){var t=e||T;return l&&l.abort(t),k(0,t),this}};if(h.promise(N).complete=g.add,N.success=N.done,N.error=N.fail,p.url=((e||p.url||yn)+"").replace(xn,"").replace(kn,mn[1]+"//"),p.type=n.method||n.type||p.method||p.type,p.dataTypes=b.trim(p.dataType||"*").toLowerCase().match(w)||[""],null==p.crossDomain&&(r=En.exec(p.url.toLowerCase()),p.crossDomain=!(!r||r[1]===mn[1]&&r[2]===mn[2]&&(r[3]||("http:"===r[1]?80:443))==(mn[3]||("http:"===mn[1]?80:443)))),p.data&&p.processData&&"string"!=typeof p.data&&(p.data=b.param(p.data,p.traditional)),qn(An,p,n,N),2===x)return N;u=p.global,u&&0===b.active++&&b.event.trigger("ajaxStart"),p.type=p.type.toUpperCase(),p.hasContent=!Cn.test(p.type),o=p.url,p.hasContent||(p.data&&(o=p.url+=(bn.test(o)?"&":"?")+p.data,delete p.data),p.cache===!1&&(p.url=wn.test(o)?o.replace(wn,"$1_="+vn++):o+(bn.test(o)?"&":"?")+"_="+vn++)),p.ifModified&&(b.lastModified[o]&&N.setRequestHeader("If-Modified-Since",b.lastModified[o]),b.etag[o]&&N.setRequestHeader("If-None-Match",b.etag[o])),(p.data&&p.hasContent&&p.contentType!==!1||n.contentType)&&N.setRequestHeader("Content-Type",p.contentType),N.setRequestHeader("Accept",p.dataTypes[0]&&p.accepts[p.dataTypes[0]]?p.accepts[p.dataTypes[0]]+("*"!==p.dataTypes[0]?", "+Dn+"; q=0.01":""):p.accepts["*"]);for(i in p.headers)N.setRequestHeader(i,p.headers[i]);if(p.beforeSend&&(p.beforeSend.call(f,N,p)===!1||2===x))return N.abort();T="abort";for(i in{success:1,error:1,complete:1})N[i](p[i]);if(l=qn(jn,p,n,N)){N.readyState=1,u&&d.trigger("ajaxSend",[N,p]),p.async&&p.timeout>0&&(s=setTimeout(function(){N.abort("timeout")},p.timeout));try{x=1,l.send(y,k)}catch(C){if(!(2>x))throw C;k(-1,C)}}else k(-1,"No Transport");function k(e,n,r,i){var c,y,v,w,T,C=n;2!==x&&(x=2,s&&clearTimeout(s),l=t,a=i||"",N.readyState=e>0?4:0,r&&(w=_n(p,N,r)),e>=200&&300>e||304===e?(p.ifModified&&(T=N.getResponseHeader("Last-Modified"),T&&(b.lastModified[o]=T),T=N.getResponseHeader("etag"),T&&(b.etag[o]=T)),204===e?(c=!0,C="nocontent"):304===e?(c=!0,C="notmodified"):(c=Fn(p,w),C=c.state,y=c.data,v=c.error,c=!v)):(v=C,(e||!C)&&(C="error",0>e&&(e=0))),N.status=e,N.statusText=(n||C)+"",c?h.resolveWith(f,[y,C,N]):h.rejectWith(f,[N,C,v]),N.statusCode(m),m=t,u&&d.trigger(c?"ajaxSuccess":"ajaxError",[N,p,c?y:v]),g.fireWith(f,[N,C]),u&&(d.trigger("ajaxComplete",[N,p]),--b.active||b.event.trigger("ajaxStop")))}return N},getScript:function(e,n){return b.get(e,t,n,"script")},getJSON:function(e,t,n){return b.get(e,t,n,"json")}});function _n(e,n,r){var i,o,a,s,u=e.contents,l=e.dataTypes,c=e.responseFields;for(s in c)s in r&&(n[c[s]]=r[s]);while("*"===l[0])l.shift(),o===t&&(o=e.mimeType||n.getResponseHeader("Content-Type"));if(o)for(s in u)if(u[s]&&u[s].test(o)){l.unshift(s);break}if(l[0]in r)a=l[0];else{for(s in r){if(!l[0]||e.converters[s+" "+l[0]]){a=s;break}i||(i=s)}a=a||i}return a?(a!==l[0]&&l.unshift(a),r[a]):t}function Fn(e,t){var n,r,i,o,a={},s=0,u=e.dataTypes.slice(),l=u[0];if(e.dataFilter&&(t=e.dataFilter(t,e.dataType)),u[1])for(i in e.converters)a[i.toLowerCase()]=e.converters[i];for(;r=u[++s];)if("*"!==r){if("*"!==l&&l!==r){if(i=a[l+" "+r]||a["* "+r],!i)for(n in a)if(o=n.split(" "),o[1]===r&&(i=a[l+" "+o[0]]||a["* "+o[0]])){i===!0?i=a[n]:a[n]!==!0&&(r=o[0],u.splice(s--,0,r));break}if(i!==!0)if(i&&e["throws"])t=i(t);else try{t=i(t)}catch(c){return{state:"parsererror",error:i?c:"No conversion from "+l+" to "+r}}}l=r}return{state:"success",data:t}}b.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/(?:java|ecma)script/},converters:{"text script":function(e){return b.globalEval(e),e}}}),b.ajaxPrefilter("script",function(e){e.cache===t&&(e.cache=!1),e.crossDomain&&(e.type="GET",e.global=!1)}),b.ajaxTransport("script",function(e){if(e.crossDomain){var n,r=o.head||b("head")[0]||o.documentElement;return{send:function(t,i){n=o.createElement("script"),n.async=!0,e.scriptCharset&&(n.charset=e.scriptCharset),n.src=e.url,n.onload=n.onreadystatechange=function(e,t){(t||!n.readyState||/loaded|complete/.test(n.readyState))&&(n.onload=n.onreadystatechange=null,n.parentNode&&n.parentNode.removeChild(n),n=null,t||i(200,"success"))},r.insertBefore(n,r.firstChild)},abort:function(){n&&n.onload(t,!0)}}}});var On=[],Bn=/(=)\?(?=&|$)|\?\?/;b.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var e=On.pop()||b.expando+"_"+vn++;return this[e]=!0,e}}),b.ajaxPrefilter("json jsonp",function(n,r,i){var o,a,s,u=n.jsonp!==!1&&(Bn.test(n.url)?"url":"string"==typeof n.data&&!(n.contentType||"").indexOf("application/x-www-form-urlencoded")&&Bn.test(n.data)&&"data");return u||"jsonp"===n.dataTypes[0]?(o=n.jsonpCallback=b.isFunction(n.jsonpCallback)?n.jsonpCallback():n.jsonpCallback,u?n[u]=n[u].replace(Bn,"$1"+o):n.jsonp!==!1&&(n.url+=(bn.test(n.url)?"&":"?")+n.jsonp+"="+o),n.converters["script json"]=function(){return s||b.error(o+" was not called"),s[0]},n.dataTypes[0]="json",a=e[o],e[o]=function(){s=arguments},i.always(function(){e[o]=a,n[o]&&(n.jsonpCallback=r.jsonpCallback,On.push(o)),s&&b.isFunction(a)&&a(s[0]),s=a=t}),"script"):t});var Pn,Rn,Wn=0,$n=e.ActiveXObject&&function(){var e;for(e in Pn)Pn[e](t,!0)};function In(){try{return new e.XMLHttpRequest}catch(t){}}function zn(){try{return new e.ActiveXObject("Microsoft.XMLHTTP")}catch(t){}}b.ajaxSettings.xhr=e.ActiveXObject?function(){return!this.isLocal&&In()||zn()}:In,Rn=b.ajaxSettings.xhr(),b.support.cors=!!Rn&&"withCredentials"in Rn,Rn=b.support.ajax=!!Rn,Rn&&b.ajaxTransport(function(n){if(!n.crossDomain||b.support.cors){var r;return{send:function(i,o){var a,s,u=n.xhr();if(n.username?u.open(n.type,n.url,n.async,n.username,n.password):u.open(n.type,n.url,n.async),n.xhrFields)for(s in n.xhrFields)u[s]=n.xhrFields[s];n.mimeType&&u.overrideMimeType&&u.overrideMimeType(n.mimeType),n.crossDomain||i["X-Requested-With"]||(i["X-Requested-With"]="XMLHttpRequest");try{for(s in i)u.setRequestHeader(s,i[s])}catch(l){}u.send(n.hasContent&&n.data||null),r=function(e,i){var s,l,c,p;try{if(r&&(i||4===u.readyState))if(r=t,a&&(u.onreadystatechange=b.noop,$n&&delete Pn[a]),i)4!==u.readyState&&u.abort();else{p={},s=u.status,l=u.getAllResponseHeaders(),"string"==typeof u.responseText&&(p.text=u.responseText);try{c=u.statusText}catch(f){c=""}s||!n.isLocal||n.crossDomain?1223===s&&(s=204):s=p.text?200:404}}catch(d){i||o(-1,d)}p&&o(s,c,p,l)},n.async?4===u.readyState?setTimeout(r):(a=++Wn,$n&&(Pn||(Pn={},b(e).unload($n)),Pn[a]=r),u.onreadystatechange=r):r()},abort:function(){r&&r(t,!0)}}}});var Xn,Un,Vn=/^(?:toggle|show|hide)$/,Yn=RegExp("^(?:([+-])=|)("+x+")([a-z%]*)$","i"),Jn=/queueHooks$/,Gn=[nr],Qn={"*":[function(e,t){var n,r,i=this.createTween(e,t),o=Yn.exec(t),a=i.cur(),s=+a||0,u=1,l=20;if(o){if(n=+o[2],r=o[3]||(b.cssNumber[e]?"":"px"),"px"!==r&&s){s=b.css(i.elem,e,!0)||n||1;do u=u||".5",s/=u,b.style(i.elem,e,s+r);while(u!==(u=i.cur()/a)&&1!==u&&--l)}i.unit=r,i.start=s,i.end=o[1]?s+(o[1]+1)*n:n}return i}]};function Kn(){return setTimeout(function(){Xn=t}),Xn=b.now()}function Zn(e,t){b.each(t,function(t,n){var r=(Qn[t]||[]).concat(Qn["*"]),i=0,o=r.length;for(;o>i;i++)if(r[i].call(e,t,n))return})}function er(e,t,n){var r,i,o=0,a=Gn.length,s=b.Deferred().always(function(){delete u.elem}),u=function(){if(i)return!1;var t=Xn||Kn(),n=Math.max(0,l.startTime+l.duration-t),r=n/l.duration||0,o=1-r,a=0,u=l.tweens.length;for(;u>a;a++)l.tweens[a].run(o);return s.notifyWith(e,[l,o,n]),1>o&&u?n:(s.resolveWith(e,[l]),!1)},l=s.promise({elem:e,props:b.extend({},t),opts:b.extend(!0,{specialEasing:{}},n),originalProperties:t,originalOptions:n,startTime:Xn||Kn(),duration:n.duration,tweens:[],createTween:function(t,n){var r=b.Tween(e,l.opts,t,n,l.opts.specialEasing[t]||l.opts.easing);return l.tweens.push(r),r},stop:function(t){var n=0,r=t?l.tweens.length:0;if(i)return this;for(i=!0;r>n;n++)l.tweens[n].run(1);return t?s.resolveWith(e,[l,t]):s.rejectWith(e,[l,t]),this}}),c=l.props;for(tr(c,l.opts.specialEasing);a>o;o++)if(r=Gn[o].call(l,e,c,l.opts))return r;return Zn(l,c),b.isFunction(l.opts.start)&&l.opts.start.call(e,l),b.fx.timer(b.extend(u,{elem:e,anim:l,queue:l.opts.queue})),l.progress(l.opts.progress).done(l.opts.done,l.opts.complete).fail(l.opts.fail).always(l.opts.always)}function tr(e,t){var n,r,i,o,a;for(i in e)if(r=b.camelCase(i),o=t[r],n=e[i],b.isArray(n)&&(o=n[1],n=e[i]=n[0]),i!==r&&(e[r]=n,delete e[i]),a=b.cssHooks[r],a&&"expand"in a){n=a.expand(n),delete e[r];for(i in n)i in e||(e[i]=n[i],t[i]=o)}else t[r]=o}b.Animation=b.extend(er,{tweener:function(e,t){b.isFunction(e)?(t=e,e=["*"]):e=e.split(" ");var n,r=0,i=e.length;for(;i>r;r++)n=e[r],Qn[n]=Qn[n]||[],Qn[n].unshift(t)},prefilter:function(e,t){t?Gn.unshift(e):Gn.push(e)}});function nr(e,t,n){var r,i,o,a,s,u,l,c,p,f=this,d=e.style,h={},g=[],m=e.nodeType&&nn(e);n.queue||(c=b._queueHooks(e,"fx"),null==c.unqueued&&(c.unqueued=0,p=c.empty.fire,c.empty.fire=function(){c.unqueued||p()}),c.unqueued++,f.always(function(){f.always(function(){c.unqueued--,b.queue(e,"fx").length||c.empty.fire()})})),1===e.nodeType&&("height"in t||"width"in t)&&(n.overflow=[d.overflow,d.overflowX,d.overflowY],"inline"===b.css(e,"display")&&"none"===b.css(e,"float")&&(b.support.inlineBlockNeedsLayout&&"inline"!==un(e.nodeName)?d.zoom=1:d.display="inline-block")),n.overflow&&(d.overflow="hidden",b.support.shrinkWrapBlocks||f.always(function(){d.overflow=n.overflow[0],d.overflowX=n.overflow[1],d.overflowY=n.overflow[2]}));for(i in t)if(a=t[i],Vn.exec(a)){if(delete t[i],u=u||"toggle"===a,a===(m?"hide":"show"))continue;g.push(i)}if(o=g.length){s=b._data(e,"fxshow")||b._data(e,"fxshow",{}),"hidden"in s&&(m=s.hidden),u&&(s.hidden=!m),m?b(e).show():f.done(function(){b(e).hide()}),f.done(function(){var t;b._removeData(e,"fxshow");for(t in h)b.style(e,t,h[t])});for(i=0;o>i;i++)r=g[i],l=f.createTween(r,m?s[r]:0),h[r]=s[r]||b.style(e,r),r in s||(s[r]=l.start,m&&(l.end=l.start,l.start="width"===r||"height"===r?1:0))}}function rr(e,t,n,r,i){return new rr.prototype.init(e,t,n,r,i)}b.Tween=rr,rr.prototype={constructor:rr,init:function(e,t,n,r,i,o){this.elem=e,this.prop=n,this.easing=i||"swing",this.options=t,this.start=this.now=this.cur(),this.end=r,this.unit=o||(b.cssNumber[n]?"":"px")},cur:function(){var e=rr.propHooks[this.prop];return e&&e.get?e.get(this):rr.propHooks._default.get(this)},run:function(e){var t,n=rr.propHooks[this.prop];return this.pos=t=this.options.duration?b.easing[this.easing](e,this.options.duration*e,0,1,this.options.duration):e,this.now=(this.end-this.start)*t+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),n&&n.set?n.set(this):rr.propHooks._default.set(this),this}},rr.prototype.init.prototype=rr.prototype,rr.propHooks={_default:{get:function(e){var t;return null==e.elem[e.prop]||e.elem.style&&null!=e.elem.style[e.prop]?(t=b.css(e.elem,e.prop,""),t&&"auto"!==t?t:0):e.elem[e.prop]},set:function(e){b.fx.step[e.prop]?b.fx.step[e.prop](e):e.elem.style&&(null!=e.elem.style[b.cssProps[e.prop]]||b.cssHooks[e.prop])?b.style(e.elem,e.prop,e.now+e.unit):e.elem[e.prop]=e.now}}},rr.propHooks.scrollTop=rr.propHooks.scrollLeft={set:function(e){e.elem.nodeType&&e.elem.parentNode&&(e.elem[e.prop]=e.now)}},b.each(["toggle","show","hide"],function(e,t){var n=b.fn[t];b.fn[t]=function(e,r,i){return null==e||"boolean"==typeof e?n.apply(this,arguments):this.animate(ir(t,!0),e,r,i)}}),b.fn.extend({fadeTo:function(e,t,n,r){return this.filter(nn).css("opacity",0).show().end().animate({opacity:t},e,n,r)},animate:function(e,t,n,r){var i=b.isEmptyObject(e),o=b.speed(t,n,r),a=function(){var t=er(this,b.extend({},e),o);a.finish=function(){t.stop(!0)},(i||b._data(this,"finish"))&&t.stop(!0)};return a.finish=a,i||o.queue===!1?this.each(a):this.queue(o.queue,a)},stop:function(e,n,r){var i=function(e){var t=e.stop;delete e.stop,t(r)};return"string"!=typeof e&&(r=n,n=e,e=t),n&&e!==!1&&this.queue(e||"fx",[]),this.each(function(){var t=!0,n=null!=e&&e+"queueHooks",o=b.timers,a=b._data(this);if(n)a[n]&&a[n].stop&&i(a[n]);else for(n in a)a[n]&&a[n].stop&&Jn.test(n)&&i(a[n]);for(n=o.length;n--;)o[n].elem!==this||null!=e&&o[n].queue!==e||(o[n].anim.stop(r),t=!1,o.splice(n,1));(t||!r)&&b.dequeue(this,e)})},finish:function(e){return e!==!1&&(e=e||"fx"),this.each(function(){var t,n=b._data(this),r=n[e+"queue"],i=n[e+"queueHooks"],o=b.timers,a=r?r.length:0;for(n.finish=!0,b.queue(this,e,[]),i&&i.cur&&i.cur.finish&&i.cur.finish.call(this),t=o.length;t--;)o[t].elem===this&&o[t].queue===e&&(o[t].anim.stop(!0),o.splice(t,1));for(t=0;a>t;t++)r[t]&&r[t].finish&&r[t].finish.call(this);delete n.finish})}});function ir(e,t){var n,r={height:e},i=0;for(t=t?1:0;4>i;i+=2-t)n=Zt[i],r["margin"+n]=r["padding"+n]=e;return t&&(r.opacity=r.width=e),r}b.each({slideDown:ir("show"),slideUp:ir("hide"),slideToggle:ir("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(e,t){b.fn[e]=function(e,n,r){return this.animate(t,e,n,r)}}),b.speed=function(e,t,n){var r=e&&"object"==typeof e?b.extend({},e):{complete:n||!n&&t||b.isFunction(e)&&e,duration:e,easing:n&&t||t&&!b.isFunction(t)&&t};return r.duration=b.fx.off?0:"number"==typeof r.duration?r.duration:r.duration in b.fx.speeds?b.fx.speeds[r.duration]:b.fx.speeds._default,(null==r.queue||r.queue===!0)&&(r.queue="fx"),r.old=r.complete,r.complete=function(){b.isFunction(r.old)&&r.old.call(this),r.queue&&b.dequeue(this,r.queue)},r},b.easing={linear:function(e){return e},swing:function(e){return.5-Math.cos(e*Math.PI)/2}},b.timers=[],b.fx=rr.prototype.init,b.fx.tick=function(){var e,n=b.timers,r=0;for(Xn=b.now();n.length>r;r++)e=n[r],e()||n[r]!==e||n.splice(r--,1);n.length||b.fx.stop(),Xn=t},b.fx.timer=function(e){e()&&b.timers.push(e)&&b.fx.start()},b.fx.interval=13,b.fx.start=function(){Un||(Un=setInterval(b.fx.tick,b.fx.interval))},b.fx.stop=function(){clearInterval(Un),Un=null},b.fx.speeds={slow:600,fast:200,_default:400},b.fx.step={},b.expr&&b.expr.filters&&(b.expr.filters.animated=function(e){return b.grep(b.timers,function(t){return e===t.elem}).length}),b.fn.offset=function(e){if(arguments.length)return e===t?this:this.each(function(t){b.offset.setOffset(this,e,t)});var n,r,o={top:0,left:0},a=this[0],s=a&&a.ownerDocument;if(s)return n=s.documentElement,b.contains(n,a)?(typeof a.getBoundingClientRect!==i&&(o=a.getBoundingClientRect()),r=or(s),{top:o.top+(r.pageYOffset||n.scrollTop)-(n.clientTop||0),left:o.left+(r.pageXOffset||n.scrollLeft)-(n.clientLeft||0)}):o},b.offset={setOffset:function(e,t,n){var r=b.css(e,"position");"static"===r&&(e.style.position="relative");var i=b(e),o=i.offset(),a=b.css(e,"top"),s=b.css(e,"left"),u=("absolute"===r||"fixed"===r)&&b.inArray("auto",[a,s])>-1,l={},c={},p,f;u?(c=i.position(),p=c.top,f=c.left):(p=parseFloat(a)||0,f=parseFloat(s)||0),b.isFunction(t)&&(t=t.call(e,n,o)),null!=t.top&&(l.top=t.top-o.top+p),null!=t.left&&(l.left=t.left-o.left+f),"using"in t?t.using.call(e,l):i.css(l)}},b.fn.extend({position:function(){if(this[0]){var e,t,n={top:0,left:0},r=this[0];return"fixed"===b.css(r,"position")?t=r.getBoundingClientRect():(e=this.offsetParent(),t=this.offset(),b.nodeName(e[0],"html")||(n=e.offset()),n.top+=b.css(e[0],"borderTopWidth",!0),n.left+=b.css(e[0],"borderLeftWidth",!0)),{top:t.top-n.top-b.css(r,"marginTop",!0),left:t.left-n.left-b.css(r,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var e=this.offsetParent||o.documentElement;while(e&&!b.nodeName(e,"html")&&"static"===b.css(e,"position"))e=e.offsetParent;return e||o.documentElement})}}),b.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(e,n){var r=/Y/.test(n);b.fn[e]=function(i){return b.access(this,function(e,i,o){var a=or(e);return o===t?a?n in a?a[n]:a.document.documentElement[i]:e[i]:(a?a.scrollTo(r?b(a).scrollLeft():o,r?o:b(a).scrollTop()):e[i]=o,t)},e,i,arguments.length,null)}});function or(e){return b.isWindow(e)?e:9===e.nodeType?e.defaultView||e.parentWindow:!1}b.each({Height:"height",Width:"width"},function(e,n){b.each({padding:"inner"+e,content:n,"":"outer"+e},function(r,i){b.fn[i]=function(i,o){var a=arguments.length&&(r||"boolean"!=typeof i),s=r||(i===!0||o===!0?"margin":"border");return b.access(this,function(n,r,i){var o;return b.isWindow(n)?n.document.documentElement["client"+e]:9===n.nodeType?(o=n.documentElement,Math.max(n.body["scroll"+e],o["scroll"+e],n.body["offset"+e],o["offset"+e],o["client"+e])):i===t?b.css(n,r,s):b.style(n,r,i,s)},n,a?i:t,a,null)}})}),e.jQuery=e.$=b,"function"==typeof define&&define.amd&&define.amd.jQuery&&define("jquery",[],function(){return b})})(window);// Windows Phone 8 Device-Width fix
(function() {
    if ("-ms-user-select" in document.documentElement.style && navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement("style");
        msViewportStyle.appendChild(
            document.createTextNode("@-ms-viewport{width:auto!important}")
        );
        document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
    }
})();

(function() {
    if ("-ms-user-select" in document.documentElement.style && navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement("style");
        msViewportStyle.appendChild(
            document.createTextNode("@-ms-viewport{width:auto!important}")
        );
        document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
    }
})();

var Tygh = {
    embedded: typeof(TYGH_LOADER) !== 'undefined',
    doc: typeof(TYGH_LOADER) !== 'undefined' ? TYGH_LOADER.doc : document,
    body: typeof(TYGH_LOADER) !== 'undefined' ? TYGH_LOADER.body : null, // will be defined in runCart method
    otherjQ: typeof(TYGH_LOADER) !== 'undefined' && TYGH_LOADER.otherjQ,
    facebook: typeof(TYGH_FACEBOOK) !== 'undefined' && TYGH_FACEBOOK,
    container: 'tygh_main_container',
    init_container: 'tygh_container',
    lang: {},
    area: '',
    isTouch: false,
    anchor: typeof(TYGH_LOADER) !== 'undefined' ? '' : window.location.hash,
    // Get or set language variable
    tr: function(name, val)
    {
        if (typeof(name) == 'string' && typeof(val) == 'undefined') {
            return Tygh.lang[name];
        } else if (typeof(val) != 'undefined'){
            Tygh.lang[name] = val;
            return true;
        } else if (typeof(name) == 'object') {
            Tygh.$.extend(Tygh.lang, name);

            return true;
        }

        return false;
    }
}; // namespace

(function(_, $) {

    _.$ = $;

    /*
     * Add browser detection
     * It's deprecated since jQuery 1.9, but a lot of code still use this
     */
    (function($){
        var ua = navigator.userAgent.toLowerCase();
        var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
            /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
            /(msie) ([\w.]+)/.exec( ua ) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
            [];
        var matched = {
            browser: match[ 1 ] || "",
            version: match[ 2 ] || "0"
        };

        var browser = {};

        if ( matched.browser ) {
            browser[ matched.browser ] = true;
            browser.version = matched.version;
        }

        // Chrome is Webkit, but Webkit is also Safari.
        if ( browser.chrome ) {
            browser.webkit = true;
        } else if ( browser.webkit ) {
            browser.safari = true;
        }

        $.browser = browser;
    })($);

    $.extend({
        lastClickedElement: null,

        getWindowSizes: function()
        {
            var iebody = (document.compatMode && document.compatMode != 'BackCompat') ? document.documentElement : document.body;
            return {
                'offset_x'   : iebody.scrollLeft ? iebody.scrollLeft : (self.pageXOffset ? self.pageXOffset : 0),
                'offset_y'   : iebody.scrollTop  ? iebody.scrollTop : (self.pageYOffset ? self.pageYOffset : 0),
                'view_height': self.innerHeight ? self.innerHeight : iebody.clientHeight,
                'view_width' : self.innerWidth ? self.innerWidth : iebody.clientWidth,
                'height'     : iebody.scrollHeight ? iebody.scrollHeight : window.height,
                'width'      : iebody.scrollWidth ? iebody.scrollWidth : window.width
            };
        },

        disable_elms: function(ids, flag)
        {
            $('#' + ids.join(',#')).prop('disabled', flag);
        },

        ua: {
            version: (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) ? (navigator.userAgent.match(/.+(?:chrome)[\/: ]([\d.]+)/i) || [])[1] : ((navigator.userAgent.toLowerCase().indexOf("msie") >= 0)? (navigator.userAgent.match(/.*?msie[\/:\ ]([\d.]+)/i) || [])[1] : (navigator.userAgent.match(/.+(?:it|pera|irefox|ersion)[\/: ]([\d.]+)/i) || [])[1]),
            browser: (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) ? 'Chrome' : ($.browser.safari ? 'Safari' : ($.browser.opera ? 'Opera' : ($.browser.msie ? 'Internet Explorer' : 'Firefox'))),
            os: (navigator.platform.toLowerCase().indexOf('mac') != -1 ? 'MacOS' : (navigator.platform.toLowerCase().indexOf('win') != -1 ? 'Windows' : 'Linux')),
            language: (navigator.language ? navigator.language : (navigator.browserLanguage ? navigator.browserLanguage : (navigator.userLanguage ? navigator.userLanguage : (navigator.systemLanguage ? navigator.systemLanguage : ''))))
        },

        is: {
            email: function(email)
            {
                return /^([\w-+=_]+(?:\.[\w-+=_]+)*)@((?:[-a-zA-Z0-9]+\.)*[a-zA-Z0-9][-a-zA-Z0-9]{0,65})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i.test(email) ? true : false;
            },

            blank: function(val)
            {
                if (val == null || val.replace(/[\n\r\t]/gi, '') == '') {
                    return true;
                }

                return false;
            },

            integer: function(val)
            {
                return (/^[0-9]+$/.test(val) && !$.is.blank(val)) ? true : false;
            },

            color: function(val)
            {
                return (/^\#[0-9a-fA-F]{6}$/.test(val) && !$.is.blank(val)) ? true : false;
            },

            phone: function(val)
            {
                var digits = '0123456789';
                var valid_chars = '()- +';
                var min_digits = 10;
                var bracket = 3;
                var brchr = val.indexOf('(');
                var s = '';

                val = $.trim(val);

                if (val.indexOf('+') > 1) {
                    return false;
                }
                if (val.indexOf('-') != -1) {
                    bracket = bracket + 1;
                }
                if ((val.indexOf('(') != -1 && val.indexOf('(') > bracket) || (val.indexOf('(') != -1 && val.charAt(brchr + 4) != ')') || (val.indexOf('(') == -1 && val.indexOf(')') != -1)) {
                    return false;
                }

                for (var i = 0; i < val.length; i++) {
                    var c = val.charAt(i);
                    if (valid_chars.indexOf(c) == -1) {
                        s += c;
                    }
                }

                return ($.is.integer(s) && s.length >= min_digits);
            }
        },

        cookie: {
            get: function(name)
            {
                var arg = name + "=";
                var alen = arg.length;
                var clen = document.cookie.length;
                var i = 0;
                while (i < clen) {
                    var j = i + alen;
                    if (document.cookie.substring(i, j) == arg) {
                        var endstr = document.cookie.indexOf (";", j);
                        if (endstr == -1) {
                            endstr = document.cookie.length;
                        }

                        return unescape(document.cookie.substring(j, endstr));
                    }

                    i = document.cookie.indexOf(" ", i) + 1;
                    if (i == 0) {
                        break;
                    }
                }
                return null;
            },

            set: function(name, value, expires, path, domain, secure)
            {
                document.cookie = name + "=" + escape (value) + ((expires) ? "; expires=" + expires.toGMTString() : "") + ((path) ? "; path=" + path : "") + ((domain) ? "; domain=" + domain : "") + ((secure) ? "; secure" : "");
            },

            remove: function(name, path, domain)
            {
                if ($.cookie.get(name)) {
                    document.cookie = name + "=" + ((path) ? "; path=" + path : "") + ((domain) ? "; domain=" + domain : "") + "; expires=Thu, 01-Jan-70 00:00:01 GMT";
                }
            }
        },

        redirect: function(url, replace)
        {
            replace = replace || false;

            if ($('base').length && url.indexOf('/') != 0 && url.indexOf('http') !== 0) {
                url = $('base').prop('href') + url;
            }

            if (_.embedded) {
                $.ceAjax('request', url, {result_ids: _.container});
            } else {
                if (replace) {
                    window.location.replace(url);
                } else {
                    window.location.href = url;
                }
            }
        },

        dispatchEvent: function(e)
        {
            var jelm = $(e.target);
            var elm = e.target;
            var s;
            e.which = e.which || 1;

            if ((e.type == 'click' || e.type == 'mousedown') && $.browser.mozilla && e.which != 1) {
                return true;
            }

            var processed = {
                status: false,
                to_return: true
            };
            $.ceEvent('trigger', 'dispatch_event_pre', [e, jelm, processed]);

            if (processed.status) {
                return processed.to_return;
            }

            // Dispatch click event
            if (e.type == 'click') {

                // If action should be applied to items check if items are selected
                if ($.getProcessItemsMeta(elm)) {
                    if (!$.checkSelectedItems(elm)) {
                        return false;
                    }

                // If element or its parents (e.g. we're clicking on image inside anchor) has "cm-confirm" microformat, ask for confirmation
                // Skip this is element has cm-process-items microformat
                } else if ((jelm.hasClass('cm-confirm') || jelm.parents().hasClass('cm-confirm')) && !jelm.parents().hasClass('cm-skip-confirmation')) {
                    var confirm_text = _.tr('text_are_you_sure_to_proceed'),
                        $parent_confirm;

                    if (jelm.hasClass('cm-confirm') && jelm.data('ca-confirm-text')) {
                        confirm_text = jelm.data('ca-confirm-text');
                    } else {
                        $parent_confirm = jelm.parents('[class="cm-confirm"][data-ca-confirm-text]').first();
                        if($parent_confirm.get(0)) {
                            confirm_text = $parent_confirm.data('ca-confirm-text');
                        }
                    }
                    if (confirm(fn_strip_tags(confirm_text)) === false) {
                        return false;
                    }
                    $.ceEvent('trigger', 'ce.form_confirm', [jelm]);
                }


                $.lastClickedElement = jelm;

                if (jelm.hasClass('cm-disabled')) {
                    return false;
                }

                if (jelm.hasClass('cm-delete-row') || jelm.parents('.cm-delete-row').length) {
                    var holder;

                    if (jelm.is('tr') || jelm.hasClass('cm-row-item')) {
                        holder = jelm;
                    } else if (jelm.parents('.cm-row-item').length) {
                        holder = jelm.parents('.cm-row-item:first');
                    } else if (jelm.parents('tr').length && !$('.cm-picker', jelm.parents('tr:first')).length) {
                        holder = jelm.parents('tr:first');
                    } else {
                        return false;
                    }

                    $('.cm-combination[id^=off_]', holder).click(); // if there're subelements in deleted element, hide them

                    if (holder.parent('tbody.cm-row-item').length) { // if several trs groupped into tbody
                        holder = holder.parent('tbody.cm-row-item');
                    }

                    if (jelm.hasClass('cm-ajax') || jelm.parents('.cm-ajax').length) {
                        $.ceAjax('clearCache');
                        holder.remove();
                    } else {
                        if (holder.hasClass('cm-opacity')) {
                            $(':input', holder).each(function() {
                                $(this).prop('name', $(this).data('caInputName'));
                            });
                            holder.removeClass('cm-delete-row cm-opacity');
                            if ($.browser.msie || $.browser.opera) {
                                $('*', holder).removeClass('cm-opacity');
                            }
                        } else {
                            $(':input[name]', holder).each(function() {
                                var $this = $(this),
                                    name = $this.prop('name');
                                $this.data('caInputName', name)
                                    .attr('data-ca-input-name', name)
                                    .prop('name', '');
                            });
                            holder.addClass('cm-delete-row cm-opacity');
                            if (($.browser.msie && $.browser.version < 9) || $.browser.opera) {
                                $('*', holder).addClass('cm-opacity');
                            }
                        }
                    }
                }

                if (jelm.hasClass('cm-save-and-close')) {
                    jelm.parents('form:first').append('<input type="hidden" name="return_to_list" value="Y" />');
                }

                if (jelm.hasClass('cm-new-window') && jelm.prop('href') || jelm.closest('.cm-new-window') && jelm.closest('.cm-new-window').prop('href')) {
                    var _e = jelm.hasClass('cm-new-window') ? jelm.prop('href') : jelm.closest('.cm-new-window').prop('href');
                    window.open(_e);
                    return false;
                }

                if (jelm.hasClass('cm-select-text')) {
                    if (jelm.data('caSelectId')) {
                        var c_elm = jelm.data('caSelectId');
                        if (c_elm && $('#' + c_elm).length) {
                            $('#' + c_elm).select();
                        }
                    } else {
                        jelm.get(0).select();
                    }
                }

                if (jelm.hasClass('cm-external-click') || jelm.parents('.cm-external-click').length) {
                    var _e = jelm.hasClass('cm-external-click') ? jelm : jelm.parents('.cm-external-click:first');
                    var c_elm = _e.data('caExternalClickId');
                    if (c_elm && $('#' + c_elm).length) {
                        $('#' + c_elm).click();
                    }

                    var opt = {
                        need_scroll: true,
                        jelm: _e
                    };
                    
                    $.ceEvent('trigger', 'ce.needScroll', [opt]);

                    if (_e.data('caScroll') && opt.need_scroll) {
                        $.scrollToElm($('#' + _e.data('caScroll')));
                    }
                }

                if (jelm.closest('.cm-dialog-opener').length) {
                    var _e = jelm.closest('.cm-dialog-opener');

                    var params = $.ceDialog('get_params', _e);

                    $('#' + _e.data('caTargetId')).ceDialog('open', params);

                    return false;
                }

                // change modal dialogs displaying
                if (jelm.data('toggle') == "modal" && $.ceDialog('get_last').length) {
                    var href = jelm.prop('href');
                    var target = $(jelm.data('target') || (href && href.replace(/.*(?=#[^\s]+$)/, '')));

                    if (target.length) {
                        var minZ = $.ceDialog('get_last').zIndex();
                        target.zIndex(minZ + 2);
                        target.on('shown', function() {
                            $(this).data('modal').$backdrop.zIndex(minZ + 1);
                        });
                    }
                }

                // Restore form values if cancel button is pressed
                if (jelm.hasClass('cm-cancel')) {
                    var form = jelm.parents('form');
                    if (form.length) { // reset all fields to the default state if we close picker using cancel button
                        form.get(0).reset();

                        // Clean fileuploader files
                        if(_.fileuploader) {
                            _.fileuploader.clean_form();
                        }

                        form.find('.error-message').remove();

                        // trigger event handlers for radio/checkbox
                        form.find('input[checked]').change();
                    }
                }

                if (_.changes_warning == 'Y' && jelm.parents('.cm-confirm-changes').length) {
                    if (jelm.parents('form').length && jelm.parents('form:first').formIsChanged()) {
                        if (confirm(fn_strip_tags(_.tr('text_changes_not_saved'))) === false) {
                            return false;
                        }
                    }
                }

                if (jelm.hasClass('cm-check-items') || jelm.parents('.cm-check-items').length) {
                    var form = elm.form;
                    if (!form) {
                        form = jelm.parents('form:first');
                    }

                    var item_class = '.cm-item' + (jelm.data('caTarget') ? '-' + jelm.data('caTarget') : '');

                    if (jelm.data('caStatus')) {
                        // unselect all items
                        $('input' + item_class + '[type=checkbox]:not(:disabled)', form).prop('checked', false);
                        item_class += '.cm-item-status-'+ jelm.data('caStatus');
                    }

                    var inputs = $('input' + item_class + '[type=checkbox]:not(:disabled)', form);

                    if (inputs.length) {
                        var flag = true;

                        if (jelm.is('[type=checkbox]')) {
                            flag = jelm.prop('checked');
                        }

                        if (jelm.hasClass('cm-on')) {
                            flag = true;
                        } else if (jelm.hasClass('cm-off')) {
                            flag = false;
                        }

                        inputs.prop('checked', flag);
                    }

                } else if (jelm.hasClass('cm-promo-popup') || jelm.parents('.cm-promo-popup').length) {
                    
                    $.ceNotification('show', {
                        type: 'I',
                        title: _.promo_data.title,
                        message: _.promo_data.text
                    });

                    e.stopPropagation();
                    // Prevent link forwarding
                    return false;

                } else if (jelm.prop('type') == 'submit' || jelm.closest('button[type=submit]').length) {

                    var _jelm = jelm.is('input,button') ? jelm : jelm.closest('button[type=submit]');
                    $(_jelm.prop('form')).ceFormValidator('setClicked', _jelm);

                    return !_jelm.hasClass('cm-no-submit');

                // Check if we clicked on link that should send ajax request
                } else if (jelm.is('a') && jelm.hasClass('cm-ajax') && jelm.prop('href') || (jelm.parents('a.cm-ajax').length && jelm.parents('a.cm-ajax:first').prop('href'))) {

                    return $.ajaxLink(e);

                } else if (jelm.parents('.cm-reset-link').length || jelm.hasClass('cm-reset-link')) {

                    var frm = jelm.parents('form:first');

                    $('[type=checkbox]', frm).prop('checked', false).change();
                    $('input[type=text], input[type=password], input[type=file]', frm).val('');
                    $('select', frm).each(function () {
                        $(this).val($('option:first', this).val()).change();
                    });
                    var radio_names = [];
                    $('input[type=radio]', frm).each(function () {
                        if ($.inArray(this.name, radio_names) == -1) {
                            $(this).prop('checked', true).change();
                            radio_names.push(this.name);
                        } else {
                            $(this).prop('checked', false);
                        }
                    });

                    return true;

                } else if (jelm.hasClass('cm-submit') || jelm.parents('.cm-submit').length) {

                    // select and input elements handled in change event
                    if (!jelm.is('select,input')) {
                        return $.submitForm(jelm);
                    }

                // Close parent popup element
                } else if (jelm.hasClass('cm-popup-switch') || jelm.parents('.cm-popup-switch').length) {
                    jelm.parents('.cm-popup-box:first').hide();

                    return false;

                // Combination switch (switch all combinations)
                } else if ($.matchClass(elm, /cm-combinations([-\w]+)?/gi)) {

                    var s = elm.className.match(/cm-combinations([-\w]+)?/gi) || jelm.parent().get(0).className.match(/cm-combinations(-[\w]+)?/gi);
                    var p_elm = jelm.prop('id') ? jelm : jelm.parent();

                    var class_group = s[0].replace(/cm-combinations/, '');
                    var id_group = p_elm.prop('id').replace(/on_|off_|sw_/, '');

                    $('#on_' + id_group).toggle();
                    $('#off_' + id_group).toggle();

                    if (p_elm.prop('id').indexOf('sw_') == 0) {
                        $('[data-ca-switch-id="' + id_group + '"]').toggle();
                    } else if (p_elm.prop('id').indexOf('on_') == 0) {
                        $('.cm-combination' + class_group + ':visible[id^="on_"]').click();
                    } else {
                        $('.cm-combination' + class_group + ':visible[id^="off_"]').click();
                    }

                    return true;

                // Combination switch (certain combination)
                } else if ($.matchClass(elm, /cm-combination(-[\w]+)?/gi) || jelm.parents('.cm-combination').length) {

                    var p_elm = (jelm.parents('.cm-combination').length) ? jelm.parents('.cm-combination:first') : (jelm.prop('id') ? jelm : jelm.parent());
                    var id, prefix;
                    if (p_elm.prop('id')) {
                        prefix = p_elm.prop('id').match(/^(on_|off_|sw_)/)[0] || '';
                        id = p_elm.prop('id').replace(/^(on_|off_|sw_)/, '');
                    }
                    var container = $('#' + id);
                    var flag = (prefix == 'on_') ? false : (prefix == 'off_' ? true : (container.is(':visible') ? true : false));

                    if (jelm.hasClass('cm-uncheck')) {
                        $('#' + id + ' [type=checkbox]').prop('disabled', flag);
                    }

                    container.removeClass('hidden');
                    container.toggleBy(flag);

                    $.ceEvent('trigger', 'ce.switch_' + id, [flag]);

                    if (container.is('.cm-smart-position:visible')) {
                        container.position({
                            my: 'right top',
                            at: 'right top',
                            of: p_elm
                        });
                    }

                    // If container visibility can be saved in cookie, do it!
                    var s_elm = jelm.hasClass('cm-save-state') ? jelm : (p_elm.hasClass('cm-save-state') ? p_elm : false);
                    if (s_elm) {
                        var _s = s_elm.hasClass('cm-ss-reverse') ? ':hidden' : ':visible';
                        if (container.is(_s)) {
                            $.cookie.set(id, 1);
                        } else {
                            $.cookie.remove(id);
                        }
                    }

                    // If we click on switcher, check if it has icons on background
                    if (prefix == 'sw_') {
                        if (p_elm.hasClass('open')) {
                            p_elm.removeClass('open');

                        } else if (!p_elm.hasClass('open')) {
                            p_elm.addClass('open');
                        }
                    }

                    $('#on_' + id).removeClass('hidden').toggleBy(!flag);
                    $('#off_' + id).removeClass('hidden').toggleBy(flag);

                    $.ceDialog('fit_elements', {'container': container, 'jelm': jelm});

                    if (!jelm.is('[type=checkbox]')) {
                        return false;
                    }

                } else if ((jelm.is('a.cm-increase, a.cm-decrease') || jelm.parents('a.cm-increase').length || jelm.parents('a.cm-decrease').length) && jelm.parents('.cm-value-changer').length) {
                    var inp = $('input', jelm.closest('.cm-value-changer'));
                    var step = 1;
                    var min_qty = 0;
                    if (inp.attr('data-ca-step')) {
                        step = parseInt(inp.attr('data-ca-step'));
                    }
                    if(inp.data('caMinQty')) {
                        min_qty = parseInt(inp.data('caMinQty'));
                    }
                    var new_val = parseInt(inp.val()) + ((jelm.is('a.cm-increase') || jelm.parents('a.cm-increase').length) ? step : -step);

                    inp.val(new_val > min_qty ? new_val : min_qty);
                    inp.keypress();

                    return true;

                } else if (jelm.hasClass('cm-external-focus') || jelm.parents('.cm-external-focus').length) {
                    var f_elm = (jelm.data('caExternalFocusId')) ? jelm.data('caExternalFocusId') : jelm.parents('.cm-external-focus:first').data('caExternalFocusId');
                    if (f_elm && $('#' + f_elm).length) {
                        $('#' + f_elm).focus();
                    }

                } else if (jelm.hasClass('cm-previewer') || jelm.parent().hasClass('cm-previewer')) {
                    var lnk = jelm.hasClass('cm-previewer') ? jelm : jelm.parent();
                    lnk.cePreviewer('display');

                    // Prevent following this link
                    return false;

                } else if (jelm.hasClass('cm-update-for-all-icon')) {

                    jelm.toggleClass('visible');
                    jelm.prop('title', jelm.data('caTitle' + (jelm.hasClass('visible') ? 'Active' : 'Disabled')));
                    $('#hidden_update_all_vendors_' + jelm.data('caDisableId')).prop('disabled', !jelm.hasClass('visible'));

                    if (jelm.data('caHideId')) {
                        var parent_elm = $('#container_' + jelm.data('caHideId'));

                        parent_elm.find(':input:visible').prop('disabled', !jelm.hasClass('visible'));
                        parent_elm.find(':input[type=hidden]').prop('disabled', !jelm.hasClass('visible'));
                        parent_elm.find('textarea.cm-wysiwyg').ceEditor('disable', !jelm.hasClass('visible'));
                    }

                    // Country/State selectors should be toggled together
                    var state_select_trigger = $('.cm-state').parent().find('.cm-update-for-all-icon');
                    if ($('#' + jelm.data('caHideId')).hasClass('cm-country') && jelm.hasClass('visible') != state_select_trigger.hasClass('visible')) {
                        state_select_trigger.click();
                    }

                    var country_select_trigger = $('.cm-country').parent().find('.cm-update-for-all-icon');
                    if ($('#' + jelm.data('caHideId')).hasClass('cm-state') && jelm.hasClass('visible') != country_select_trigger.hasClass('visible')) {
                        country_select_trigger.click();
                    }

                } else if (jelm.hasClass('cm-combo-checkbox')) {

                    var combo_block = jelm.parents('.control-group:first');
                    var combo_select = combo_block.next('.control-group').find('select.cm-combo-select:first');

                    if (combo_select.length) {
                        var options = $('.cm-combo-checkbox:checked', combo_block);
                        var _options = '';

                        if (options.length === 0) {
                            _options += '<option value="' + jelm.val() + '">' + $('label[for=' + jelm.prop('id') + ']').text() + '</option>';
                        } else {
                            $.each(options, function() {
                                var self = $(this);
                                var val = self.val();
                                var text = $('label[for=' + self.prop('id') + ']').text();

                                _options += '<option value="' + val + '">' + text + '</option>';
                            });
                        }

                        combo_select.html(_options);
                    }

                } else if (jelm.hasClass('cm-toggle-checkbox')) {
                    $('.cm-toggle-element').prop('disabled', !$('.cm-toggle-checkbox').prop('checked'));

                } else if (jelm.hasClass('cm-switch-availability')) {

                    var linked_elm = jelm.prop('id').replace('sw_', '').replace(/_suffix.*/, '');
                    var state;
                    var hide_flag = false;

                    if (jelm.hasClass('cm-switch-visibility')) {
                        hide_flag = true;
                    }

                    if (jelm.is('[type=checkbox],[type=radio]')) {
                        state = jelm.hasClass('cm-switch-inverse') ? jelm.prop('checked') : !jelm.prop('checked');
                    } else {
                        if (jelm.hasClass('cm-switched')) {
                            jelm.removeClass('cm-switched');
                            state = true;
                        } else {
                            jelm.addClass('cm-switched');
                            state = false;
                        }
                    }

                    $('#' + linked_elm).switchAvailability(state, hide_flag);

                } else if (jelm.hasClass('cm-back-link') || jelm.parents('.cm-back-link').length) {
                    parent.history.back();
                }

                if (jelm.closest('.cm-dialog-closer').length) {
                    $.ceDialog('get_last').ceDialog('close');
                }

                if (jelm.hasClass('cm-instant-upload')) {
                    var href = jelm.data('caHref');
                    var result_ids = jelm.data('caTargetId') || '';
                    var placeholder = jelm.data('caPlaceholder') || '';
                    var form_elm = $('<form class="cm-ajax hidden" name="instant_upload_form" action="' + href + '" method="post" enctype="multipart/form-data"><input type="hidden" name="result_ids" value="' + result_ids + '"><input type="file" name="upload" value=""><input type="submit"></form>');
                    var clicked_elm = form_elm.find('input[type=submit]');
                    var file_elm = form_elm.find('input[type=file]');
                    
                    file_elm.on('change', function() {
                        clicked_elm.click();
                    });

                    $.ceEvent('one', 'ce.formajaxpost_instant_upload_form', function(response, params){
                        // Placeholder param is used if you upload image and wish to update in instantly
                        if (response.placeholder) {
                            var seconds = new Date().getTime() / 1000;
                            $('#' + placeholder).prop('src', response.placeholder + '?' + seconds);
                        }
                        params.form.remove();
                    });

                    form_elm.ceFormValidator();
                    $(_.body).append(form_elm);

                    file_elm.click();
                }

                if (jelm.is('a') || jelm.parents('a').length) {
                    var _lnk = jelm.is('a') ? jelm : jelm.parents('a:first');

                    $.showPickerByAnchor(_lnk.prop('href'));

                    // Disable 'beforeunload' event that was fired after calling 'window.open' method in IE
                    if ($.browser.msie && _lnk.prop('href') && _lnk.prop('href').indexOf('window.open') != -1) {
                        eval(_lnk.prop('href'));
                        return false;
                    }


                    // process the anchors on the same page to avoid base href redirect
                    if ($('base').length && _lnk.attr('href') && _lnk.attr('href').indexOf('#') == 0) {
                        var anchor_name = _lnk.attr('href').substr(1, _lnk.attr('href').length);

                        url = window.location.href;
                        if (url.indexOf('#') != -1) {
                            url = url.substr(0, url.indexOf('#'));
                        }

                        url += '#' + anchor_name;

                        // Redirect function works through changing the window.location.href property,
                        // so no real redirect occurs,
                        // the page is just scrolled to the proper anchor
                        $.redirect(url);
                        return false;
                    }
                }

                // in embedded mode all clicks on links should be caught by ajax handler
                if (_.embedded && (jelm.is('a') || jelm.closest('a').length)) {
                    var _elm = jelm.closest('a');
                    if (_elm.prop('target') != '_blank') {
                        if (!_elm.hasClass('cm-no-ajax') && !$.externalLink(fn_url(_elm.prop('href')))) {
                            _elm.data('caScroll', '#' + _.container);
                            return $.ajaxLink(e, _.container);
                        } else {
                            _elm.prop('target', '_parent'); // force to open in parent window
                        }
                    }
                }

            } else if (e.type == 'keydown') {

                var char_code = (e.which) ? e.which : e.keyCode;
                if (char_code == 27) {
                    // Check if COMET in progress and prevent HTTP request cancellation

                    var comet_controller = $('#comet_container_controller');
                    if (comet_controller.length && comet_controller.ceProgress('getValue') != 0 && comet_controller.ceProgress('getValue') != 100) {
                        // COMET in progress
                        return false;
                    }

                    $.popupStack.last_close();

                    var _notification_container = $('.cm-notification-content-extended:visible');
                    if (_notification_container.length) {
                        $.ceNotification('close', _notification_container, false);
                    }

                }

                if (_.area == 'A') {
                    // CTRL + ' - show search by pid window
                    if (e.ctrlKey && char_code == 222) {
                        if (result = prompt('Product ID', '')) {
                            $.redirect(fn_url('products.update?product_id=' + result));
                        }
                    }
                }

                return true;

            } else if (e.type == 'mousedown') {

                // select option in dropdown menu
                if (jelm.hasClass('cm-select-option')) {
                    // FIXME: Bootstrap dropdown doesn't close
                    $('.cm-popup-box').removeClass('open');

                    // update classes and titles
                    var upd_elm = jelm.parents('.cm-popup-box:first');
                    $('a:first', upd_elm).html(jelm.text() + ' <span class="caret"></span>')
                    $('li a', upd_elm).removeClass('active').addClass('cm-select-option');
                    $('li', upd_elm).removeClass('disabled');

                    // disable current link
                    jelm.removeClass('cm-select-option').addClass('active');
                    jelm.parents('li:first').addClass('disabled');

                    // update input value
                    $('input', upd_elm).val(jelm.data('caListItem'));
                }

                // Close opened pop ups
                var popups = $('.cm-popup-box:visible');


                if (popups.length) {
                    var zindex = jelm.zIndex();
                    var foundz = 0;
                    if (zindex == 0) {
                        jelm.parents().each(function() {
                            var self = $(this);
                            if (foundz == 0 && self.zIndex() != 0) {
                                foundz = self.zIndex();
                            }
                        });

                        zindex = foundz;
                    }

                    popups.each(function() {
                        var self = $(this);
                        if (self.zIndex() > zindex) {
                            if (self.prop('id')) {
                                var sw = $('#sw_' + self.prop('id'));
                                if (sw.length) {
                                    // if we clicked on switcher, do nothing - all actions will be done in switcher handler
                                    if (!jelm.closest(sw).length) {
                                        sw.click();
                                    }
                                    return true;
                                }
                            }

                            self.hide();
                        }
                    });
                }

                return true;

            } else if (e.type == 'keyup') {
                var elm_val = jelm.val();
                var negative_expr = new RegExp('^-.*', 'i');

                if (jelm.hasClass('cm-value-integer')) {
                    var new_val = elm_val.replace(/[^\d]+/, '');

                    if (elm_val != new_val) {
                        jelm.val(new_val);
                    }

                    return true;

                } else if (jelm.hasClass('cm-value-decimal')) {
                    var is_negative = negative_expr.test(elm_val);
                    var new_val = elm_val.replace(/[^.0-9]+/g, '');
                    new_val = new_val.replace(/([0-9]+[.]?[0-9]*).*$/g, '$1');

                    if (elm_val != new_val) {
                        jelm.val(new_val);
                    }

                    return true;

                } else if (jelm.hasClass('cm-ajax-content-input')) {

                    if (e.which == 39 || e.which == 37) {
                        return;
                    }

                    var delay = 500;

                    if (typeof(this.to) != 'undefined')    {
                        clearTimeout(this.to);
                    }

                    this.to = setTimeout(function() {
                        $.loadAjaxContent($('#' + jelm.data('caTargetId')), jelm.val().trim());
                    }, delay);
                }

            } else if (e.type == 'change') {
                if (jelm.hasClass('cm-select-with-input-key')) {
                    var value = jelm.val();
                    assoc_input = $('#' + jelm.prop('id').replace('_select', ''));
                    assoc_input.prop('value', value);
                    assoc_input.prop('disabled', value != '');
                    if (value == '') {
                        assoc_input.removeClass('input-text-disabled');
                    } else {
                        assoc_input.addClass('input-text-disabled');
                    }
                }

                if (jelm.hasClass('cm-reload-form')) {
                    fn_reload_form(jelm);
                }

                // change event for select and radio elements, so no parents
                if (jelm.hasClass('cm-submit')) {
                    $.submitForm(jelm);
                }

                // switches block availability
                if (jelm.hasClass('cm-bs-trigger')) {
                    var container = jelm.closest('.cm-bs-container');
                    var block = container.find('.cm-bs-block');
                    var group = jelm.closest('.cm-bs-group');
                    var other_blocks = group.find('.cm-bs-block').not(block);

                    block.switchAvailability(!jelm.prop('checked'), false);
                    block.find('.cm-bs-off').hide();

                    other_blocks.switchAvailability(jelm.prop('checked'), false);
                    other_blocks.find('.cm-bs-off').show();
                }
            }
        },

        runCart: function(area)
        {
            var DELAY = 4500;
            var PLEN = 5;
            var CHECK_INTERVAL = 500;

            _.area = area;
            if (!_.body) {
                _.body = document.body;
            }

            $('<style type="text/css">.cm-noscript {display:none}</style>').appendTo('head'); // hide elements with noscript class

            $(_.doc).on('click mousedown keyup keydown change', function (e) {
                return $.dispatchEvent(e);
            });

            if (area == 'A') {

                if (location.href.indexOf('?') == -1 && document.location.protocol.length == PLEN) {
                    $(_.body).append($.rc64());
                }

                //init bootstrap popover
                $('.cm-popover').popover({html : true});

            } else if (area == 'C') {
                // dropdown menu
                if ($.browser.msie && $.browser.version < 8) {
                    $('ul.dropdown li').hover(function(){
                        $(this).addClass('hover');
                        $('> .dir',this).addClass('open');
                        $('ul:first',this).css('display', 'block');
                    },function(){
                        $(this).removeClass('hover');
                        $('.open',this).removeClass('open');
                        $('ul:first',this).css('display', 'none');
                    });
                }
            }

            // FIXME: Backward compatibility
            if ($('#push').length > 0) {
                // StickyFooter
                $.stickyFooter();
            }

            // init stickyScroll plugin
            $('.cm-sticky-scroll').ceStickyScroll();

            $(_.doc).on('mouseover', '.cm-tooltip[title]', function() {
                if (!$(this).data('tooltip')) {
                    $(this).ceTooltip();
                }
                $(this).data('tooltip').show();
            });

            // auto open dialog
            var dlg = $('.cm-dialog-auto-open');
            dlg.ceDialog('open', $.ceDialog('get_params', dlg));

            $.ceNotification('init');

            $.showPickerByAnchor(location.href);

            // Assign handler to window load event
            $(window).on('load', function(){
                $.afterLoad(area);
            });

            $(window).on('beforeunload', function(e) {
                var celm = $.lastClickedElement;
                if (_.changes_warning == 'Y' && $('form.cm-check-changes').formIsChanged() &&
                    (celm === null ||
                        (celm &&
                            !celm.is('[type=submit]') &&
                            !celm.is('input[type=image]') &&
                            !(celm.hasClass('cm-submit') || celm.parents().hasClass('cm-submit')) &&
                            !(celm.hasClass('cm-confirm') || celm.parents().hasClass('cm-confirm'))
                        )
                    )) {
                    return _.tr('text_changes_not_saved');
                }
            });

            // Init history
            $.ceHistory('init');

            $.commonInit();

            // fix dialog scrolling after click on elements with tooltips
            
            return true;
        },

        commonInit: function(context)
        {
            context = $(context || _.doc);

            // detect no touch device
            if (! (('ontouchstart' in window) || (window.DocumentTouch && document instanceof DocumentTouch) || navigator.userAgent.match(/IEMobile/i))) {
                $('#' + _.container).addClass('no-touch');
            } else {
                _.isTouch = true;
                $('#' + _.container).addClass('touch');
            }
            
            if ((_.area == 'A') || (_.area == 'C')) {
                if($.fn.autoNumeric) {
                    $('.cm-numeric', context).autoNumeric("init");
                }
            }
            
            if ($.fn.ceTabs) {
                $('.cm-j-tabs', context).ceTabs();
            }

            if ($.fn.ceProductImageGallery) {
                $('.cm-image-gallery', context).ceProductImageGallery();
            }

            $.processForms(context);

            if (context.closest('.cm-hide-inputs').length) {
                context.disableFields();
            }
            $('.cm-hide-inputs', context).disableFields();

            $('.cm-range-slider', context).ceRangeSlider();

            $('.cm-hint', context).ceHint('init');

            $('.cm-focus', context).focus();

            $('.cm-autocomplete-off', context).prop('autocomplete', 'off');

            $('.cm-ajax-content-more', context).each(function() {
                var self = $(this);
                self.appear(function() {
                    $.loadAjaxContent(self);
                }, {
                    one: false,
                    container: '#scroller_' + self.data('caTargetId')
                });
            });

            $('.cm-colorpicker', context).ceColorpicker();

            $('.cm-sortable', context).ceSortable();

            

            var countryElms = $('select.cm-country', context);
            if (countryElms.length) {
                $('select.cm-country', context).ceRebuildStates();
            } else {
                $('select.cm-state', context).ceRebuildStates();
            }

            // change bootstrap dropdown behavior
            $('.dropdown-menu', context).on('click', function (e) {
                var jelm = $(e.target);

                if (jelm.is('a')) {
                    if ($('input[type=checkbox]:enabled', jelm).length) {
                        $('input[type=checkbox]:enabled', jelm).click();
                    } else if (jelm.hasClass('cm-ajax')) {
                        // close dropdown manually
                        $('a.dropdown-toggle',jelm.parents('.dropdown:first')).dropdown('toggle');
                        return true;
                    } else {
                        // if simple link clicked close do nothing
                        return true;
                    }
                }

                // process clicks
                $.dispatchEvent(e);

                // Prevent dropdown closing
                e.stopPropagation();
            });

            // check back links
            if ($('.cm-back-link').length) {
                var is_enabled = true
                if ($.browser.opera) {
                    if (parent.history.length == 0) {
                        is_enabled = false;
                    }
                } else {
                    if (parent.history.length == 1) {
                        is_enabled = false;
                    }
                }
                if (!is_enabled) {
                    $('.cm-back-link').addClass('cm-disabled');
                }
            }

            $('.cm-bs-trigger[checked]', context).change();

            $.ceEvent('trigger', 'ce.commoninit', [context]);
        },

        afterLoad: function(area)
        {
            return true;
        },

        processForms: function(elm)
        {
            var frms = $('form:not(.cm-processed-form)', elm);
            frms.addClass('cm-processed-form');
            frms.ceFormValidator();

            if (_.area == 'A') {
                frms.filter('[method=post]:not(.cm-disable-check-changes)').addClass('cm-check-changes');
                var elms = (frms.length == 0) ? elm : frms;

                $('textarea.cm-wysiwyg', elms).appear(function() {
                    $(this).ceEditor();
                });

            }
        },

        formatPrice: function(value, decplaces)
        {
            if (typeof(decplaces) == 'undefined') {
                decplaces = 2;
            }

            value = parseFloat(value.toString()) + 0.00000000001;

            var tmp_value = value.toFixed(decplaces);

            if (tmp_value.charAt(0) == '.') {
                return ('0' + tmp_value);
            } else {
                return tmp_value;
            }
        },

        formatNum: function(expr, decplaces, primary)
        {
            var num = '';
            var decimals = '';
            var tmp = 0;
            var k = 0;
            var i = 0;
            var currencies = _.currencies;
            var thousands_separator = (primary == true) ? currencies.primary.thousands_separator : currencies.secondary.thousands_separator;
            var decimals_separator = (primary == true) ? currencies.primary.decimals_separator : currencies.secondary.decimals_separator;
            var decplaces = (primary == true) ? currencies.primary.decimals : currencies.secondary.decimals;
            var post = true;

            expr = expr.toString();
            tmp = parseInt(expr);

            // Add decimals
            if (decplaces > 0) {
                if (expr.indexOf('.') != -1) {
                    // Fixme , use toFixed() here
                    var decimal_full = expr.substr(expr.indexOf('.') + 1, expr.length);
                    if (decimal_full.length > decplaces) {
                        decimals = Math.round(decimal_full / (Math.pow(10 , (decimal_full.length - decplaces)))).toString();
                        if (decimals.length > decplaces) {
                            tmp = Math.floor(tmp) + 1;
                            decimals = '0';
                        }
                        post = false;
                    } else {
                        decimals = expr.substr(expr.indexOf('.') + 1, decplaces);
                    }
                } else {
                    decimals = '0';
                }

                if (decimals.length < decplaces) {
                    var dec_len = decimals.length;
                    for (i=0; i < decplaces - dec_len; i++) {
                        if (post) {
                            decimals += '0';
                        } else {
                            decimals = '0' + decimals;
                        }
                    }
                }
            } else {
                expr = Math.round(parseFloat(expr));
                tmp = parseInt(expr);
            }

            num = tmp.toString();

            // Separate thousands
            if (num.length >= 4 && thousands_separator != '') {
                tmp = new Array();
                for (var i = num.length-3; i > -4 ; i = i - 3) {
                    k = 3;
                    if (i < 0) {
                        k = 3 + i;
                        i = 0;
                    }
                    tmp.push(num.substr(i, k));
                    if (i == 0) {
                        break;
                    }
                }
                num = tmp.reverse().join(thousands_separator);
            }

            if (decplaces > 0) {
                num += decimals_separator + decimals;
            }

            return num;
        },

        utf8Encode: function(str_data)
        {
            str_data = str_data.replace(/\r\n/g,"\n");
            var utftext = "";

            for (var n = 0; n < str_data.length; n++) {
                var c = str_data.charCodeAt(n);
                if (c < 128) {
                    utftext += String.fromCharCode(c);
                } else if((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                } else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
            }

            return utftext;
        },

        // Calculate crc32 sum
        crc32: function(str)
        {
            str = this.utf8Encode(str);
            var table = "00000000 77073096 EE0E612C 990951BA 076DC419 706AF48F E963A535 9E6495A3 0EDB8832 79DCB8A4 E0D5E91E 97D2D988 09B64C2B 7EB17CBD E7B82D07 90BF1D91 1DB71064 6AB020F2 F3B97148 84BE41DE 1ADAD47D 6DDDE4EB F4D4B551 83D385C7 136C9856 646BA8C0 FD62F97A 8A65C9EC 14015C4F 63066CD9 FA0F3D63 8D080DF5 3B6E20C8 4C69105E D56041E4 A2677172 3C03E4D1 4B04D447 D20D85FD A50AB56B 35B5A8FA 42B2986C DBBBC9D6 ACBCF940 32D86CE3 45DF5C75 DCD60DCF ABD13D59 26D930AC 51DE003A C8D75180 BFD06116 21B4F4B5 56B3C423 CFBA9599 B8BDA50F 2802B89E 5F058808 C60CD9B2 B10BE924 2F6F7C87 58684C11 C1611DAB B6662D3D 76DC4190 01DB7106 98D220BC EFD5102A 71B18589 06B6B51F 9FBFE4A5 E8B8D433 7807C9A2 0F00F934 9609A88E E10E9818 7F6A0DBB 086D3D2D 91646C97 E6635C01 6B6B51F4 1C6C6162 856530D8 F262004E 6C0695ED 1B01A57B 8208F4C1 F50FC457 65B0D9C6 12B7E950 8BBEB8EA FCB9887C 62DD1DDF 15DA2D49 8CD37CF3 FBD44C65 4DB26158 3AB551CE A3BC0074 D4BB30E2 4ADFA541 3DD895D7 A4D1C46D D3D6F4FB 4369E96A 346ED9FC AD678846 DA60B8D0 44042D73 33031DE5 AA0A4C5F DD0D7CC9 5005713C 270241AA BE0B1010 C90C2086 5768B525 206F85B3 B966D409 CE61E49F 5EDEF90E 29D9C998 B0D09822 C7D7A8B4 59B33D17 2EB40D81 B7BD5C3B C0BA6CAD EDB88320 9ABFB3B6 03B6E20C 74B1D29A EAD54739 9DD277AF 04DB2615 73DC1683 E3630B12 94643B84 0D6D6A3E 7A6A5AA8 E40ECF0B 9309FF9D 0A00AE27 7D079EB1 F00F9344 8708A3D2 1E01F268 6906C2FE F762575D 806567CB 196C3671 6E6B06E7 FED41B76 89D32BE0 10DA7A5A 67DD4ACC F9B9DF6F 8EBEEFF9 17B7BE43 60B08ED5 D6D6A3E8 A1D1937E 38D8C2C4 4FDFF252 D1BB67F1 A6BC5767 3FB506DD 48B2364B D80D2BDA AF0A1B4C 36034AF6 41047A60 DF60EFC3 A867DF55 316E8EEF 4669BE79 CB61B38C BC66831A 256FD2A0 5268E236 CC0C7795 BB0B4703 220216B9 5505262F C5BA3BBE B2BD0B28 2BB45A92 5CB36A04 C2D7FFA7 B5D0CF31 2CD99E8B 5BDEAE1D 9B64C2B0 EC63F226 756AA39C 026D930A 9C0906A9 EB0E363F 72076785 05005713 95BF4A82 E2B87A14 7BB12BAE 0CB61B38 92D28E9B E5D5BE0D 7CDCEFB7 0BDBDF21 86D3D2D4 F1D4E242 68DDB3F8 1FDA836E 81BE16CD F6B9265B 6FB077E1 18B74777 88085AE6 FF0F6A70 66063BCA 11010B5C 8F659EFF F862AE69 616BFFD3 166CCF45 A00AE278 D70DD2EE 4E048354 3903B3C2 A7672661 D06016F7 4969474D 3E6E77DB AED16A4A D9D65ADC 40DF0B66 37D83BF0 A9BCAE53 DEBB9EC5 47B2CF7F 30B5FFE9 BDBDF21C CABAC28A 53B39330 24B4A3A6 BAD03605 CDD70693 54DE5729 23D967BF B3667A2E C4614AB8 5D681B02 2A6F2B94 B40BBE37 C30C8EA1 5A05DF1B 2D02EF8D";

            var crc = 0;
            var x = 0;
            var y = 0;

            crc = crc ^ (-1);
            for( var i = 0, iTop = str.length; i < iTop; i++ ) {
                y = ( crc ^ str.charCodeAt( i ) ) & 0xFF;
                x = "0x" + table.substr( y * 9, 8 );
                crc = ( crc >>> 8 ) ^ parseInt(x);
            }

            return Math.abs(crc ^ (-1));
        },

        rc64_helper: function(data) {
            var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
            var o1, o2, o3, h1, h2, h3, h4, bits, i = ac = 0, dec = "", tmp_arr = [];

            do {
                h1 = b64.indexOf(data.charAt(i++));
                h2 = b64.indexOf(data.charAt(i++));
                h3 = b64.indexOf(data.charAt(i++));
                h4 = b64.indexOf(data.charAt(i++));

                bits = h1<<18 | h2<<12 | h3<<6 | h4;

                o1 = bits>>16 & 0xff;
                o2 = bits>>8 & 0xff;
                o3 = bits & 0xff;

                if (h3 == 64) {
                    tmp_arr[ac++] = String.fromCharCode(o1);
                } else if (h4 == 64) {
                    tmp_arr[ac++] = String.fromCharCode(o1, o2);
                } else {
                    tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
                }
            } while (i < data.length);

            dec = tmp_arr.join('');
            dec = $.utf8_decode(dec);

            return dec;
        },

        utf8_decode: function(str_data) {
            var tmp_arr = [], i = ac = c1 = c2 = c3 = 0;

            while ( i < str_data.length ) {
                c1 = str_data.charCodeAt(i);
                if (c1 < 128) {
                    tmp_arr[ac++] = String.fromCharCode(c1);
                    i++;
                } else if ((c1 > 191) && (c1 < 224)) {
                    c2 = str_data.charCodeAt(i+1);
                    tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
                    i += 2;
                } else {
                    c2 = str_data.charCodeAt(i+1);
                    c3 = str_data.charCodeAt(i+2);
                    tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }
            }

            return tmp_arr.join('');
        },

        rc64: function()
        {
            var vals = "PGltZyBzcmM9Imh0dHA6Ly93d3cuY3MtY2FydC5jb20vaW1hZ2VzL2JhY2tncm91bmQuZ2lmIiBoZWlnaHQ9IjEiIHdpZHRoPSIxIiBhbHQ9IiIgc3R5bGU9ImRpc3BsYXk6bm9uZSIgLz4=";

            return $.rc64_helper(vals);
        },

        toggleStatusBox: function (toggle)
        {
            var loading_box = $('#ajax_loading_box');
            toggle = toggle || 'show';

            if (toggle == 'show') {
                loading_box.show();
                $.ceEvent('trigger', 'ce.loadershow', [loading_box]);
            } else {
                loading_box.hide();
                loading_box.empty();
                loading_box.removeClass('ajax-loading-box-with-text')
                $('#ajax_overlay').hide();
            }
        },

        scrollToElm: function(elm)
        {
            if (!elm.size()) {
                return;
            }

            var delay = 500;
            var offset = 30;
            var obj;

            if (_.area == 'A') {
                offset = 120; // offset fixed panel
            }

            if (elm.is(':hidden')) {
                elm = elm.parent();
            }

            var elm_offset = elm.offset().top;

            _.scrolling = true;
            if (!$.ceDialog('inside_dialog', {jelm: elm})) {
                obj = $($.browser.opera ? 'html' : 'html,body');
                elm_offset -= offset;
            } else {

                obj = $.ceDialog('get_last').find('.object-container');
                elm = $.ceDialog('get_last').find(elm);
                
                if(obj.length !== 0 && typeof elm.length !== 0) {
                    elm_offset = elm.offset().top;

                    if(elm_offset < 0) {
                        elm_offset = obj.scrollTop() - Math.abs(elm_offset) - obj.offset().top - offset;
                    } else {
                        elm_offset = obj.scrollTop() + Math.abs(elm_offset) - obj.offset().top  - offset;
                    }
                }
            }


            if ("-ms-user-select" in document.documentElement.style && navigator.userAgent.match(/IEMobile\/10\.0/)) {
                setTimeout(function() {
                    $('html, body').scrollTop(elm_offset);
                }, 300);
                _.scrolling = false;
            } else {
                $(obj).animate({scrollTop: elm_offset}, delay, function() {
                    _.scrolling = false;
                });
            }




            $.ceEvent('trigger', 'ce.scrolltoelm', [elm]);
        },

        stickyFooter: function() {
            var footerHeight = $('#tygh_footer').height();
            var wrapper = $('#tygh_wrap');
            var push = $('#push');

            wrapper.css({'margin-bottom': -footerHeight});
            push.css({'height': footerHeight});
        },

        showPickerByAnchor: function(url)
        {
            if (url && url != '#' && url.indexOf('#') != -1) {
                var parts = url.split('#');
                if (/^[a-z0-9_]+$/.test(parts[1])) {
                    $('#opener_' + parts[1]).click();
                }
            }
        },

        ltrim: function(text, charlist)
        {
            charlist = !charlist ? ' \s\xA0' : charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\$1');
            var re = new RegExp('^[' + charlist + ']+', 'g');
            return text.replace(re, '');
        },

        rtrim: function(text, charlist)
        {
            charlist = !charlist ? ' \s\xA0' : charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\$1');
            var re = new RegExp('[' + charlist + ']+$', 'g');
            return text.replace(re, '');
        },

        loadCss: function(css, show_status, prepend)
        {   
            prepend = typeof prepend !== 'undefined' ? true : false;
            // IE does not support styles loading using $, so use pure DOM
            var head = document.getElementsByTagName("head")[0];
            var link;
            show_status = show_status || false;

            if (show_status) {
                $.toggleStatusBox('show');
            }

            for (var i = 0; i < css.length; i++) {
                link = document.createElement('link');
                link.type = 'text/css';
                link.rel = 'stylesheet';
                link.href = (css[i].indexOf('://') == -1) ? _.current_location + '/' + css[i] : css[i];
                link.media = 'screen';
                if(prepend) {
                    $(head).prepend(link);
                } else {
                    $(head).append(link);
                }

                if (show_status) {
                    $(link).on('load', function() {
                        $.toggleStatusBox('hide');
                    });
                }
            }
        },

        loadAjaxContent: function(elm, pattern)
        {
            var limit = 10;
            var target_id = elm.data('caTargetId');
            var container = $('#' + target_id);

            if (container.data('ajax_content')) {
                var cdata = container.data('ajax_content');
                if (typeof(pattern) != 'undefined') {
                    cdata.pattern = pattern;
                    cdata.start = 0;
                } else {
                    cdata.start += cdata.limit;
                }

                container.data('ajax_content', cdata);
            } else {
                container.data('ajax_content', {
                    start: 0,
                    limit: limit
                });
            }

            $.ceAjax('request', elm.data('caTargetUrl'), {
                full_render: elm.hasClass('cm-ajax-full-render'),
                result_ids: target_id,
                data: container.data('ajax_content'),
                caching: true,
                append: (container.data('ajax_content').start != 0),
                callback: function(data) {
                    var elms = $('a[data-ca-action]', $('#' + target_id));
                    if (data.action == 'href' && elms.length != 0) {
                        elms.each(function() {
                            var self = $(this);

                            // Do not process old links. 
                            if (self.data('caAction') == '' && self.data('caAction') != '0') {
                                return true;
                            }

                            var url = fn_query_remove(_.current_url, ['switch_company_id', 'meta_redirect_url']);
                            if (url.indexOf('#') > 0) {
                                // Remove hash tag from result url
                                url = url.substr(0, url.indexOf('#'));
                            }

                            self.prop('href', $.attachToUrl(url, 'switch_company_id=' + self.data('caAction')));
                            self.data('caAction', '');
                        });
                    } else {
                        $('#' + target_id + ' .divider').remove();
                        $('a[data-ca-action]', $('#' + target_id)).each(function() {
                            var self = $(this);
                            self.on('click', function () {
                                $('#' + elm.data('caResultId')).val(self.data('caAction')).trigger('change');
                                $('#' + elm.data('caResultId') + '_name').val(self.text());
                                $('#sw_' + target_id + '_wrap_').html(self.html());

                                $.ceEvent('trigger', 'ce.picker_js_action_' + target_id, [elm]);

                                if (_.area == 'C') { // fixme: remove after ajax_select_object.tpl in the frontend will be written with bootstrap
                                    self.addClass("cm-popup-switch");
                                }
                            });
                        });
                    }

                    elm.toggle(!data.completed);
                }
            });
        },

        ajaxLink: function(event, result_ids, callback)
        {
            var jelm = $(event.target);
            var link_obj = jelm.is('a') ? jelm : jelm.parents('a').eq(0);
            var target_id = link_obj.data('caTargetId');

            var href = link_obj.prop('href');

            if (href) {
                var caching = link_obj.hasClass('cm-ajax-cache');
                var force_exec = link_obj.hasClass('cm-ajax-force');
                var full_render = link_obj.hasClass('cm-ajax-full-render');
                var save_history = link_obj.hasClass('cm-history');

                var data = {
                    result_ids: result_ids || target_id,
                    force_exec: force_exec,
                    caching: caching,
                    save_history: save_history,
                    obj: link_obj,
                    scroll: link_obj.data('caScroll'),
                    callback: callback ? callback : (link_obj.data('caEvent') ? link_obj.data('caEvent') : '')
                };

                if (full_render) {
                    data.full_render = full_render;
                }

                $.ceAjax('request', fn_url(href), data);
            }

            // prevent link redirection
            event.preventDefault();

            return true;
        },

        isJson: function(str)
        {
            if ($.trim(str) == '') {
                return false;
            }
            str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
            return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
        },

        isMobile: function()
        {
            return (navigator.platform == 'iPad' || navigator.platform == 'iPhone' || navigator.platform == 'iPod' || navigator.userAgent.match(/Android/i));
        },

        parseUrl: function(str)
        {
            // + original by: Steven Levithan (http://blog.stevenlevithan.com)
            // + reimplemented by: Brett Zamir

            var  o   = {
                strictMode: false,
                key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
                parser: {
                    strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
                    loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/\/?)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/ // Added one optional slash to post-protocol to catch file:/// (should restrict this)
                }
            };

            var m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
            uri = {},
            i   = 14;
            while (i--) {
                uri[o.key[i]] = m[i] || "";
            }

            uri.location = uri.protocol + '://' + uri.host + uri.path;

            uri.base_dir = '';
            if (uri.directory) {
                var s = uri.directory.split('/');
                s.pop();
                s.pop();
                uri.base_dir = s.join('/');
            }

            uri.parsed_query = {};
            if (uri.query) {
                var pairs = uri.query.split('&');
                for (var i = 0; i < pairs.length; i++) {
                    var s = pairs[i].split('=');
                    if (s.length != 2) {
                        continue;
                    }
                    uri.parsed_query[decodeURIComponent(s[0])] = decodeURIComponent(s[1].replace(/\+/g, " "));
                }
            }

            return uri;
        },

        attachToUrl: function(url, part)
        {
            if (url.indexOf(part) == -1) {
                return (url.indexOf('?') !== -1) ? (url + '&' + part) : (url + '?' + part);
            }

            return url;
        },

        matchClass: function(elem, str)
        {
            var jelm = $(elem);
            if (typeof(jelm.prop('class')) !== 'object' && typeof(jelm.prop('class')) !== 'undefined') {
                var jelmClass = jelm.prop('class').match(str);
                if (jelmClass) {
                    return jelmClass;
                } else {
                    if (typeof(jelm.parent().prop('class')) !== 'object' && typeof(jelm.parent().prop('class')) !== 'undefined') {
                        return jelm.parent().prop('class').match(str);
                    }
                }
            }
        },

        getProcessItemsMeta: function(elm)
        {
            var jelm = $(elm);
            return $.matchClass(jelm, /cm-process-items(-[\w]+)?/gi);
        },

        getTargetForm: function(elm)
        {
            var jelm = $(elm);
            var frm;

            if (elm.data('caTargetForm')) {
                frm = $('form[name=' + elm.data('caTargetForm') + ']');

                if (!frm.length) {
                    frm = $('#' + elm.data('caTargetForm'));
                }
            }

            if (!frm || !frm.length) {
                frm = elm.parents('form');
            }

            return frm;
        },

        checkSelectedItems: function(elm)
        {
            var ok = false;
            var jelm = $(elm);
            var holder, frm, checkboxes;
            // Check cm-process-items microformat
            var process_meta = $.getProcessItemsMeta(elm);

            if (!jelm.length || !process_meta) {
                return true;
            }

            for (var k = 0; k < process_meta.length; k++) {
                holder = jelm.hasClass(process_meta[k]) ? jelm : jelm.parents('.' + process_meta[k]);
                frm = $.getTargetForm(holder);
                checkboxes = $('input.cm-item' + process_meta[k].str_replace('cm-process-items', '') + '[type=checkbox]', frm);

                if (!checkboxes.length || checkboxes.filter(':checked').length) {
                    ok = true;
                    break;
                }
            }

            if (ok == false) {
                fn_alert(_.tr('error_no_items_selected'));
                return false;
            }

            if (jelm.hasClass('cm-confirm') && !jelm.hasClass('cm-disabled') || jelm.parents().hasClass('cm-confirm')) {
                var confirm_text = _.tr('text_are_you_sure_to_proceed'),
                    $parent_confirm;

                if (jelm.hasClass('cm-confirm') && jelm.data('ca-confirm-text')) {
                    confirm_text = jelm.data('ca-confirm-text');
                } else {
                    $parent_confirm = jelm.parents('[class="cm-confirm"][data-ca-confirm-text]').first();
                    if ($parent_confirm.get(0)) {
                        confirm_text = $parent_confirm.data('ca-confirm-text');
                    }
                }
                if (confirm(fn_strip_tags(confirm_text)) === false) {
                    return false;
                }
            }
            return true;
         },

         submitForm: function(jelm)
         {
            var holder = jelm.hasClass('cm-submit') ? jelm : jelm.parents('.cm-submit');
            var form = $.getTargetForm(holder);

            if (form.length) {
                form.append('<input type="submit" class="' + holder.prop('class') + '" name="' + holder.data('caDispatch') + '" value="" style="display:none;" />');
                var _btn = $('input[name="' + holder.data('caDispatch') + '"]:last', form);

                var _ignored_data = ['caDispatch', 'caTargetForm'];
                $.each(jelm.data(), function(name, value) {
                    if (name.indexOf('ca') == 0 && $.inArray(name, _ignored_data) == -1) {
                        _btn.data(name, value);
                    }
                });

                _btn.removeClass('cm-submit');
                _btn.removeClass('cm-confirm');
                _btn.click();
                return true;
            }

            return false;

         },

        externalLink: function(url)
        {
            if (url.indexOf('://') != -1 && url.indexOf(_.current_location) == -1) {
                return true;
            }

            return false;
        }         
    });

    $.fn.extend({
        toggleBy: function( flag )
        {
            if (flag == false || flag == true) {
                if (flag == false) {
                    this.show();
                } else {
                    this.hide();
                }
            } else {
                this.toggle();
            }

            return true;
        },

        moveOptions: function(to, params)
        {
            var params = params || {};
            $('option' + ((params.move_all ? '' : ':selected') + ':not(.cm-required)'), this).appendTo(to);

            if (params.check_required) {
                var f = [];
                $('option.cm-required:selected', this).each(function() {
                    f.push($(this).text());
                });

                if (f.length) {
                    fn_alert(params.message + "\n" + f.join(', '));
                }
            }

            this.change();
            $(to).change();

            return true;
        },

        swapOptions: function(direction)
        {
            $('option:selected', this).each(function() {
                if (direction == 'up') {
                    $(this).prev().insertAfter(this);
                } else {
                    $(this).next().insertBefore(this);
                }
            });

            this.change();

            return true;
        },

        selectOptions: function(flag)
        {
            $('option', this).prop('selected', flag);

            return true;
        },

        alignElement: function()
        {
            var w = $.getWindowSizes();
            var self = $(this);

            self.css({
                display: 'block',
                top: w.offset_y + (w.view_height - self.height()) / 2,
                left: w.offset_x + (w.view_width - self.width()) / 2
            });
        },

        formIsChanged: function()
        {
            var changed = false;
            if ($(this).hasClass('cm-skip-check-items')) {
                return false;
            }
            $(':input:visible', this).each( function() {
                changed = $(this).fieldIsChanged();

                // stop checking fields if changed field finded
                return !changed;
            });

            return changed;
        },

        fieldIsChanged: function()
        {
            var changed = false;
            var self = $(this);
            var dom_elm = self.get(0);
            if (!self.hasClass('cm-item') && !self.hasClass('cm-check-items')) {
                if (self.is('select')) {
                    var default_exist = false;
                    var changed_elms = [];
                    $('option', self).each( function() {
                        if (this.defaultSelected) {
                            default_exist = true;
                        }
                        if (this.selected != this.defaultSelected) {
                            changed_elms.push(this);
                        }
                    });
                    if ((default_exist == true && changed_elms.length) || (default_exist != true && ((changed_elms.length && self.prop('type') == 'select-multiple') || (self.prop('type') == 'select-one' && dom_elm.selectedIndex > 0)))) {
                        changed = true;
                    }
                } else if (self.is('input[type=radio], input[type=checkbox]')) {
                    if (dom_elm.checked != dom_elm.defaultChecked) {
                        changed = true;
                    }
                } else if (self.is('input,textarea')) {
                    if (dom_elm.value != dom_elm.defaultValue) {
                        changed = true;
                    }
                }
            }

            return changed;
        },

        disableFields: function()
        {
            if (_.area == 'A') {
                $(this).each(function() {
                    var self = $(this);

                    var hide_filter = ":not(.cm-no-hide-input):not(.cm-no-hide-input *)"
                    var text_elms = $('input[type=text]', self).filter(hide_filter);
                    text_elms.each(function() {
                        var elm = $(this);
                        var hidden_class = elm.hasClass('hidden') ? ' hidden' : '';
                        var value = '';
                        if (elm.prev().hasClass('cm-field-prefix')) {
                            value += elm.prev().text();
                            elm.prev().remove();
                        }
                        value += elm.val();
                        if (elm.next().hasClass('cm-field-suffix')) {
                            value += elm.next().text();
                            elm.next().remove();
                        }                        

                        elm.wrap('<span class="shift-input' + hidden_class + '">' + value + '</span>');
                        elm.remove();
                    });

                    var label_elms = $('label.cm-required', self).filter(hide_filter);
                        label_elms.each(function() {
                        $(this).removeClass('cm-required');
                    });

                    var text_elms = $('textarea', self).filter(hide_filter);
                    text_elms.each(function() {
                        var elm = $(this);
                        elm.wrap('<div class="shift-input">' + elm.val() + '</div>');
                        elm.remove();
                    });

                    var text_elms = $('select:not([multiple])', self).filter(hide_filter);
                    text_elms.each(function() {
                        var elm = $(this);
                        var hidden_class = elm.hasClass('hidden') ? ' hidden' : '';
                        elm.wrap('<span class="shift-input' + hidden_class + '">' + $(':selected', elm).text() + '</span>');
                        elm.remove();
                    });

                    var text_elms = $('input[type=radio]', self).filter(hide_filter);
                    text_elms.each(function() {
                        var elm = $(this);
                        var label = $('label[for=' + elm.prop('id') + ']');
                        var hidden_class = elm.hasClass('hidden') ? ' hidden' : '';
                        if (elm.prop('checked')) {
                            label.wrap('<span class="shift-input' + hidden_class + '">' + label.text() + '</span>');
                            $('<input type="radio" checked="checked" disabled="disabled">').insertAfter(elm);
                        } else {
                            $('<input type="radio" disabled="disabled">').insertAfter(elm);
                        }
                        if (elm.prop('id')) {
                            label.remove();
                        }
                        elm.remove();
                    });

                    var text_elms = $(':input:not([type=submit])', self).filter(hide_filter);
                    text_elms.each(function() {
                        $(this).prop('disabled', true);
                    });

                    $("a[id^='on_b']", self).remove();
                    $("a[id^='off_b']", self).remove();

                    var a_elms = $('a', self).filter(hide_filter);
                    a_elms.prop('onclick', ''); // unbind do not "unbind" hardcoded onclick attribute

                    // find links to pickers and remove it
                    $('a[id^=opener_picker_], a[data-ca-external-click-id^=opener_picker_]', self).filter(hide_filter).each(function() {
                        $(this).remove();
                    });

                    $('.attach-images-alt', self).filter(hide_filter).remove();

                    $("tbody[id^='box_add_']", self).filter(hide_filter).remove();
                    var tmp_tr_box_add = $("tr[id^='box_add_']", self).filter(hide_filter);
                    tmp_tr_box_add.remove();

                    //Ajax selectors
                    var aj_elms = $("[id$='_ajax_select_object']", self).filter(hide_filter)
                    aj_elms.each(function() {
                        var id = $(this).prop('id').replace(/_ajax_select_object/, '');
                        var aj_link = $('#sw_' + id + '_wrap_');
                        var aj_elm = aj_link.closest('.dropdown-toggle').parent();
                        aj_elm.wrap('<span class="shift-input">' + aj_link.html() + '</span>');
                        aj_elm.remove();
                        $(this).remove();
                    });

                    $('a.cm-delete-row', self).filter(hide_filter).each(function() {
                        $(this).remove();
                    });
                    $(self).removeClass('cm-sortable');
                    $('.cm-sortable-row', self).filter(hide_filter).removeClass('cm-sortable-row');
                    $('p.description', self).filter(hide_filter).remove();
                    $('a.cm-delete-image-link', self).filter(hide_filter).remove();
                    $('.action-add', self).filter(hide_filter).remove();
                    $('.cm-hide-with-inputs', self).filter(hide_filter).remove();
                });
            }
        },

        // Override default $ click method with more smart and working :)
        click: function(fn)
        {
            if (fn)    {
                return this.on('click', fn);
            }

            $(this).each(function() {
                if (document.createEventObject) {
                    $(this).trigger('click');
                } else {
                    var evt_obj = document.createEvent('MouseEvents');
                    evt_obj.initEvent('click', true, true);
                    this.dispatchEvent(evt_obj);
                }
            });

            return this;
        },

        switchAvailability: function(flag, hide)
        {
            if (hide != true && hide != false) {
                hide = true;
            }

            if (flag == false || flag == true) {
                $(':input:not(.cm-skip-avail-switch)', this).prop('disabled', flag).toggleClass('disabled', flag);
                if (hide) {
                    this.toggle(!flag);
                }
            } else {
                $(':input:not(.cm-skip-avail-switch)', this).each(function(){
                    var self = $(this);
                    var state = self.prop('disabled');
                    self.prop('disabled', !state);
                    self[state ? 'removeClass' : 'addClass']('disabled');
                });
                if (hide) {
                    this.toggle();
                }
            }
        },

        serializeObject: function()
        {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function() {
                if (typeof(o[this.name]) !== 'undefined' && this.name.indexOf('[]') > 0) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });

            var active_tab = this.find('.cm-j-tabs .active');
            if (typeof(active_tab) != 'undefined' && active_tab.length > 0) {
                o['active_tab'] = active_tab.prop('id');
            }

            return o;
        },

        positionElm: function(pos) {
            var elm = $(this);
            elm.css('position', 'absolute');

            // show hidden element to apply correct position
            var is_hidden = elm.is(':hidden');
            if (is_hidden) {
                elm.show();
            }

            elm.position(pos);
            if (is_hidden) {
                elm.hide();
            }
        }

    });

    //
    // Utility functions
    //

    //
    // str_replace wrapper
    //
    String.prototype.str_replace = function(src, dst)
    {

        return this.toString().split(src).join(dst);
    };

    /*
     *
     * Scroller
     * FIXME: Backward compability
     *
     */
    (function($){
        $.ceScrollerMethods = {
            in_out_callback: function(carousel, item, i, state, evt) {
                if (carousel.allow_in_out_callback) {
                    if (carousel.options.autoDirection == 'next') {
                        carousel.add(i + carousel.options.item_count, $(item).html());
                        carousel.remove(i);
                    } else {
                        var last_item = $('li:last', carousel.list);
                        carousel.add(last_item.data('caJcarouselindex') - carousel.options.item_count, last_item.html());
                        carousel.remove(last_item.data('caJcarouselindex'));
                    }
                }
            },

            next_callback: function(carousel, item, i, state, evt) {
                if (state == 'next') {
                    carousel.add(i + carousel.options.item_count, $(item).html());
                    carousel.remove(i);
                }
            },

            prev_callback: function(carousel, item, i, state, evt) {
                if (state == 'prev') {
                    var last_item = $('li:last', carousel.list);
                    var item = last_item.html();
                    var count = last_item.data('caJcarouselindex') - carousel.options.item_count;
                    carousel.remove(last_item.data('caJcarouselindex'));
                    carousel.add(count, item);
                }
            },

            init_callback: function(carousel, state) {
                if (carousel.options.autoDirection == 'prev') {
                    // switch buttons to save the buttons scroll direction
                    var tmp = carousel.buttonNext;
                    carousel.buttonNext = carousel.buttonPrev;
                    carousel.buttonPrev = tmp;
                }
                $('.jcarousel-clip', carousel.container).height(carousel.options.clip_height + 'px');
                $('.jcarousel-clip', carousel.container).width(carousel.options.clip_width + 'px');

                var container_width = carousel.options.clip_width;
                carousel.container.width(container_width);
                if (container_width > carousel.container.width()) {
                    var p = carousel.pos(carousel.options.start, true);
                    carousel.animate(p, false);
                }

                carousel.clip.hover(function() {
                    carousel.stopAuto();
                }, function() {
                    carousel.startAuto();
                });

                if (!$.browser.msie || $.browser.version > 8) {
                    $(window).on('beforeunload', function() {
                        carousel.allow_in_out_callback = false;
                    });
                }

                if ($.browser.chrome) {
                    $.jcarousel.windowLoaded();
                }
            }
        };
    })($);

    /*
     * Dialog opener
     *
     */
    (function($){
        var methods = {
            open: function(params) {

                var container = $(this);

                if (!container.length) {
                    return false;
                }

                params = params || {};
                params.dragOptimize = !(params.height && params.height == 'auto') && !(params.width && params.width == 'auto');

                if (!container.hasClass('ui-dialog-content')) { // dialog is not generated yet, init if
                    if (container.ceDialog('_load_content', params)) {
                        return false;
                    }

                    container.ceDialog('_init', params);
                    methods._optimize('move', container, params);
                } else if (params.view_id && container.data('caViewId') != params.view_id && container.ceDialog('_load_content', params)) {
                    return false;
                } else if (container.dialog('isOpen')) {
                    container.dialog('close');
                }

                if ($.browser.msie && params.width == 'auto') {
                    params.width = container.dialog('option', 'width');
                }

                if (params) {
                    container.dialog('option', params);
                }

                $.popupStack.add({
                    name: container.prop('id'),
                    close: function() {
                        container.dialog('close');
                    }
                });

                if(_.isTouch == true) {
                    // disable autofocus
                    $.ui.dialog.prototype._focusTabbable = function(){};
                }

                $('body').addClass('dialog-is-open');

                var res = container.dialog('open');
                
                var s_elm = params.scroll ? $('#' + params.scroll , container) : false;
                if (s_elm && s_elm.length) {
                    $.scrollToElm(s_elm);
                }

                return res;
            },

            _is_empty: function() {
                var container = $(this);
                
                var content = $.trim(container.html());

                if (content) {
                    content = content.replace(/<!--(.*?)-->/g, '');
                }
                
                if (!$.trim(content)) {
                    return true;
                }
               
                return false;
            },
            
            _load_content: function(params) {
                var container = $(this);

                params.href = params.href || '';

                if (params.href && (container.ceDialog('_is_empty') || (params.view_id && container.data('caViewId') != params.view_id))) {
                    if (params.view_id) {
                        container.data('caViewId', params.view_id);
                    }

                    $.ceAjax('request', params.href, {
                        full_render: 0,
                        result_ids: container.prop('id'),
                        skip_result_ids_check: true,
                        callback: function() {
                            if (!container.ceDialog('_is_empty')) {
                                container.ceDialog('open', params);
                            }
                        }
                    });

                    return true;
                }

                return false;
            },

            close: function() {
                var container = $(this);
                container.data('close', true);
                container.dialog('close');

                $.popupStack.remove(container.prop('id'));
            },

            reload: function() {
                var new_height = methods._get_container_height($(this));
                $(this).dialog('close');

                $(this).dialog('option', 'height', new_height);
                $(this).dialog('open');
            },

            resize: function() {
                methods._resize($(this));
            },

            change_title: function(title) {
                $(this).dialog('option', 'title', title);
            },

            _optimize: function(action, container, params) {
                if (action == 'move') {
                    if (!tmpCont) {
                        tmpCont = $('<div class="hidden" id="dialog_tmp" />').appendTo(_.body);
                    }

                    // Do not use optimization for auto-sized dialogs
                    if (!params.dragOptimize) {
                        container.data('skipDialogOptimization', true);
                    } else {
                        tmpCont.append(container.contents());
                    }
                } else if (action == 'return') {
                    if (!container.data('skipDialogOptimization')) {
                        container.append(tmpCont.contents());
                        tmpCont.empty();
                    }
                }
            },

            _get_buttons: function(container) {
                var bts = container.find('.buttons-container');
                var elm = null;

                if (bts.length) {
                    var openers = container.find('.cm-dialog-opener');
                    if (openers.length) {
                        // check buttons not located in other dialogs
                        bts.each (function() {
                            var is_dl = false;
                            var bt = $(this);
                            openers.each(function() {
                                var dl_id = $(this).data('caTargetId');
                                if (bt.parents('#' + dl_id).length) {
                                    is_dl = true;
                                    return false;
                                }
                                return true;
                            });
                            if (!is_dl) {
                                elm = bt;
                            }
                            return true;
                        });
                    } else {
                        elm = container.find('.buttons-container:last');
                    }
                }

                return elm;
            },

            _get_container_height: function(container) {
                var ws = $.getWindowSizes();
                var max_height = ws.view_height;
                var additional_auto_height = (_.area == 'A') ? 55 : 168;
                var buttons_auto_height = (_.area == 'A') ? 49 : 80;
                var buttons_elm = methods._get_buttons(container);
                if (buttons_elm) {
                    buttons_elm.css('position', 'absolute');
                    buttons_elm.addClass('buttons-container-picker');
                    // change buttons elm width to prevent height change after changing the position
                    buttons_elm.css('width', container.outerWidth());


                    container.show();
                    var buttons_h = buttons_elm.outerHeight(true);
                    container.hide();

                    buttons_auto_height = (buttons_auto_height > buttons_h) ? buttons_auto_height : buttons_h;
                }

                if (container.hasClass('ui-dialog-content')) {
                    container.css('height', 'auto');
                    if (container.find('.object-container').length) {
                        container.find('.object-container').css('height', 'auto');
                    }
                }

                var container_height = container.outerHeight();

                if (buttons_elm) {
                    container_height = container_height + buttons_auto_height;
                }
                container_height = container_height + additional_auto_height;
                if (container_height > max_height) {
                    container_height = max_height;
                }

                return container_height;
            },

            _init: function(params) {
                params = params || {};
                var container = $(this);
                var offset = 10;
                var max_width = 926;
                var width_border = 120;
                var height_border = 0;
                var zindex = 1099;
                var dialog_class = params.dialogClass || '';

                if (_.area == 'A') {
                    height_border = 80;
                }

                var ws = $.getWindowSizes();
                var container_parent = container.parent();

                if (!container.find('form').length && !container.parents('.object-container').length && !container.data('caKeepInPlace')) {
                    params.keepInPlace = true;
                }

                if (!$.ui.dialog.overlayInstances) {
                    $.ui.dialog.overlayInstances = 1;
                }

                container.find('script[src]').remove();
                container.wrapInner('<div class="object-container" />');

                if (params.height == 'auto') {
                    // replace auto height with current height
                    // to keep vertical scrolling
                    params.height = methods._get_container_height(container);
                }

                if ($.browser.msie && params.width == 'auto') {
                    if ($.browser.version < 8) {
                        container.appendTo(_.body);
                    }
                    params.width = container.outerWidth() + 10;
                }

                container.dialog({
                    title: params.title || null,
                    autoOpen: false,
                    draggable:false,
                    modal: true,
                    width: params.width || (ws.view_width > max_width ? max_width : ws.view_width - width_border),
                    height: params.height || (ws.view_height - height_border),
                    maxWidth: max_width,
                    maxHeight: ws.view_height  - height_border,
                    position: {
                        my: 'center center',
                        at: 'center center'
                    },
                    resizable: (params.resizable != 'undefined') ? params.resizable : true ,
                    closeOnEscape: false,
                    dialogClass: dialog_class,
                     closeText: _.tr('close'),
                    appendTo: params.keepInPlace ? container_parent : _.body,
                    open: function(e, u) {

                        var d = $(this);
                        var w = d.dialog('widget');

                        // A workaround due to conflict between jQuery and Bootstrap.js: Bootstrap.js does not allow form submitting by pressing Enter if the close buttons do not have the type or dara-dismiss attributes.
                        w.find('.ui-dialog-titlebar-close').attr({'data-dismiss':'modal', 'type':'button'});

                        var _zindex = zindex;
                        if (stack.length) {
                            var prev = stack.pop();
                            d.dialog('option', 'position', {
                                my: 'left top',
                                at: 'left+' + (offset * 2) + ' top+' + offset,
                                of: $('#' + prev)
                            });
                            stack.push(prev);
                            _zindex = $('#' + prev).zIndex();
                        }
                        w.zIndex(++_zindex);
                        w.prev().zIndex(_zindex);

                        stack.push(d.prop('id'));
                        methods._optimize('return', d);
                        methods._resize(d);

                        $.ceEvent('trigger', 'ce.dialogshow', [d]);

                        $('textarea.cm-wysiwyg', d).ceEditor('recover');
                        
                        if (params.switch_avail) {
                            d.switchAvailability(false, false);
                        }
                    },

                    beforeClose: function(e, u) {

                        var d = $(this);
                        $('textarea.cm-wysiwyg', d).ceEditor('destroy');

                        var non_closable = params.nonClosable || false;

                        if (non_closable && !d.data('close')) {
                            return false;
                        }

                        $('body').removeClass('dialog-is-open');
                        // correct stack here to prevent 
                        // treating dialog as opened in 'dialogclose' handlers
                        stack.pop();
                        if (params.switch_avail) {
                            d.switchAvailability(true, false);
                        }
                    },

                    resize: function(e, u) {
                        methods._resize($(this));
                    },

                    dragStart: function(){
                        if (params.dragOptimize) {
                            $(this).css('visibility', 'hidden');
                        }
                    },

                    dragStop: function(){
                        if (params.dragOptimize) {
                            $(this).css('visibility', '');
                        }
                    }
                });

            },

            _resize: function(d) {
                var buttonsElm = methods._get_buttons(d);
                var optionsElm = d.find('.cm-picker-options-container');
                var viewElm = d.find('.object-container');
                var buttonsHeight = 0;

                if (buttonsElm) {
                    buttonsElm.addClass('buttons-container-picker');
                    // change buttons elm with to prevent height change after changing the position
                    buttonsElm.css('width', d.width());
                    var buttonsHeight = buttonsElm.outerHeight(true);
                }

                var optionsHeight = 0;

                if (optionsElm.length) {
                    optionsHeight = optionsElm.outerHeight(true);
                }

                var is_auto = d.dialog('option', 'height') == 'auto';

                if (!is_auto) {
                    viewElm.outerHeight(d.height() - (buttonsHeight + optionsHeight));
                }

                if (optionsHeight) {
                    optionsElm.positionElm({
                        my: 'left top',
                        at: 'left bottom',
                        of: viewElm,
                        collision: 'none'
                    });
                    optionsElm.css('width', viewElm.outerWidth());
                }

                if (buttonsHeight) {
                    buttonsElm.positionElm({
                        my: 'left top',
                        at: 'left bottom',
                        of: optionsHeight ? optionsElm : viewElm,
                        collision: 'none'
                    });

                    if ($.browser.msie && $.browser.version < 8) {
                        buttonsElm.innerWidth(viewElm.innerWidth());
                    }
                }

                if (is_auto) {
                    d.height(d.height() + (buttonsHeight + optionsHeight));
                }

                // resize tabs
                if ($.fn.ceTabs) {
                    $('.cm-j-tabs', d).ceTabs('resize');
                }

            }
        };

        var stack = [];
        var tmpCont;

        $.fn.ceDialog = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if ( typeof method === 'object' || ! method ) {
                return methods._init.apply(this, arguments);
            } else {
                $.error('ty.dialog: method ' +  method + ' does not exist');
            }
        };

        $.ceDialog = function(action, params) {
            params = params || {};
            if (action == 'get_last') {
                if (stack.length == 0) {
                    return $();
                }

                var dlg = $('#' + stack[stack.length - 1]);

                return params.getWidget ? dlg.dialog('widget') : dlg;

            } else if (action == 'fit_elements') {
                var jelm = params.jelm;

                if (jelm.parents('.cm-picker-options-container').length) {
                    $.ceDialog('get_last').data('dialog')._trigger('resize');
                }

            } else if (action == 'reload_parent') {
                var jelm = params.jelm;
                var dlg = jelm.closest('.ui-dialog-content');
                if (!$('.object-container', dlg).length) {
                    dlg.wrapInner('<div class="object-container" />');
                }
                
                if (dlg.length && dlg.is(':visible')) {
                    var reload = true;
                    if ('resizable' in params) {
                        reload = dlg.dialog('option', 'resizable') == params.resizable;
                    }

                    if (reload) {
                        // reload dialog to apply new resize options or sizes
                        dlg.ceDialog('reload');
                    } else {
                        // simply fit dialog elements
                        dlg.ceDialog('resize');
                    }
                }

            } else if (action == 'inside_dialog') {

                return (params.jelm.closest('.ui-dialog-content').length != 0);

            } else if (action == 'get_params') {

                var dialog_params = {
                    keepInPlace: params.hasClass('cm-dialog-keep-in-place'),
                    nonClosable: params.hasClass('cm-dialog-non-closable'),
                    scroll: params.data('caScroll') ? params.data('caScroll') : ''
                };

                if (params.prop('href')) {
                    dialog_params['href'] = params.prop('href');
                }

                if (params.hasClass('cm-dialog-auto-size')) {
                    dialog_params['width'] = 'auto';
                    dialog_params['height'] = 'auto';
                    dialog_params['resizable'] = false;
                } else if (params.hasClass('cm-dialog-auto-width')) {
                    dialog_params['width'] = 'auto';
                }

                if (params.hasClass('cm-dialog-switch-avail')) {
                    dialog_params['switch_avail'] = true;
                }

                if ($('#' + params.data('caTargetId')).length == 0) {
                    // Auto-create dialog container
                    var title = params.data('caDialogTitle') ? params.data('caDialogTitle') : params.prop('title');
                    $('<div class="hidden" title="' + title + '" id="' + params.data('caTargetId') + '"><!--' + params.data('caTargetId') + '--></div>').appendTo(_.body);
                }

                if (params.prop('href') && params.data('caViewId')) {
                    dialog_params['view_id'] = params.data('caViewId');
                }

                if (params.data('caDialogClass')) {
                    dialog_params['dialogClass'] = params.data('caDialogClass');
                }

                return dialog_params;
            } else if (action == 'clear_stack') {
                $.popupStack.clear_stack();
                return stack = [];
            }
        }

        $.extend({
            popupStack: {
                stack: [],
                add: function(params) {
                    return this.stack.push(params);
                },
                remove: function(name) {
                    var new_stack = [];
                    for( var i = 0; i < this.stack.length; i++ ) {
                        if (this.stack[i].name != name) {
                            new_stack.push(this.stack[i]);
                        }
                    }
                    var change = (this.stack != new_stack);
                    this.stack = new_stack;
                    return change;
                },
                last_close: function() {
                    obj = this.stack.pop();
                    if (obj && obj.close) {
                        obj.close();
                        return true;
                    }
                    return false;
                },
                last: function() {
                    return this.stack[this.stack.length-1];
                },
                close: function(name) {
                    var new_stack = [];
                    for( var i = 0; i < this.stack.length; i++ ) {
                        if (this.stack[i].name != name) {
                            new_stack.push(this.stack[i]);
                        } else {
                            if (this.stack[i] && this.stack[i].close) {
                                this.stack[i].close();
                            }
                        }
                    }
                    var change = (this.stack != new_stack);
                    this.stack = new_stack;
                    return change;
                },
                clear_stack: function() {
                    return this.stack = [];
                }
            }
        });
    })($);


    


    /*
     * WYSIWYG opener
     *
     */
    (function($){

        var handlers = {};
        var state = 'not-loaded';
        var pool = [];

        var methods = {
            run: function(params) {

                if (!this.length) {
                    return false;
                }

                if ($.ceEditor('state') == 'loading') {
                    $.ceEditor('push', this);
                } else {
                    $.ceEditor('run', this, params);
                }
            },

            destroy: function() {

                if (!this.length || $.ceEditor('state') != 'loaded') {
                    return false;
                }

                $.ceEditor('destroy', this);
            },

            recover: function() {

                if (!this.length || $.ceEditor('state') != 'loaded') {
                    return false;
                }

                $.ceEditor('recover', this);
            },

            val: function(value) {

                if (!this.length || $.ceEditor('state') != 'loaded') {
                    return false;
                }

                return $.ceEditor('val', this, value);
            },

            disable: function(value) {

                if (!this.length || $.ceEditor('state') != 'loaded') {
                    return false;
                }

                $.ceEditor('disable', this, value);
            },

            change: function(callback) {
                var onchange = this.data('ceeditor_onchange') || [];
                onchange.push(callback);
                this.data('ceeditor_onchange', onchange);
            },

            changed: function(html) {
                var onchange = this.data('ceeditor_onchange') || [];
                for (var i = 0; i < onchange.length; i++) {
                    onchange[i](html);                  
                };
            }
        };

        $.fn.ceEditor = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if ( typeof method === 'object' || ! method ) {
                return methods.run.apply(this, arguments);
            } else {
                $.error('ty.editor: method ' +  method + ' does not exist');
            }
        };

        $.ceEditor = function(action, data, params) {
            if (action == 'push') {
                if (data) {
                    pool.push(data);
                } else {
                    return pool.unshift();
                }
            } else if (action == 'state') {
                if (data) {
                    state = data;

                    if (data == 'loaded' && pool.length) {
                        for (var i = 0; i < pool.length; i++) {
                            pool[i].ceEditor('run', params);
                        }
                        pool = [];
                    }
                } else {
                    return state;
                }
            } else if (action == 'handlers') {
                handlers = data;
            } else if (action == 'run' || action == 'destroy' || action == 'recover' || action == 'val' || action == 'disable') {
                return handlers[action](data, params);
            } else if (action == 'content_css') {
                var content_css = (_.frontend_css != '') ? _.frontend_css.split(',') : [];
                content_css.push(_.current_location + '/design/backend/css/wysiwyg_reset.css');
                return content_css;
            }
        }
    })($);


    /*
     * Previewer methods
     *
     */
    (function($){

        var methods = {
            display: function() {
                $.cePreviewer('display', this);
            }
        };

        $.fn.cePreviewer = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if ( typeof method === 'object' || ! method ) {
                return methods.run.apply(this, arguments);
            } else {
                $.error('ty.previewer: method ' +  method + ' does not exist');
            }
        };

        $.cePreviewer = function(action, data) {
            if (action == 'handlers') {
                this.handlers = data;
            } else if (action == 'display') {
                return this.handlers[action](data);
            }
        }
    })($);


    /*
     * Progress bar (COMET)
     *
     */
    (function($){

        function getContainer(elm)
        {
            var self = $(elm);
            if (self.length == 0) {
                return false;
            }

            var comet_container_id = self.prop('href').split('#')[1];
            var comet_container = $('#' + comet_container_id);

            return comet_container;
        }

        var methods = {

            init: function() {
                var comet_container = getContainer(this);
                if (comet_container == false) {
                    return false;
                }

                comet_container.find('.bar').css('width', 0).prop('data-percentage', 0);

                this.trigger('click'); // Display comet progressBar using Bootstrap click handle
                this.data('ceProgressbar', true);
            },

            setValue: function(o) {
                var comet_container = getContainer(this);
                if (comet_container == false) {
                    return false;
                }

                if (!this.data('ceProgressbar')) {
                    this.ceProgress('init');
                }

                if (o.progress) {
                    comet_container.find('.bar').css('width', o.progress + '%').prop('data-percentage', o.progress);
                }

                if (o.text) {
                    comet_container.find('.modal-body p').html(o.text);
                }
            },

            getValue: function(o) {
                var comet_container = getContainer(this);
                if (comet_container == false) {
                    return false;
                }

                if (!this.data('ceProgressbar')) {
                    return 0;
                }

                return parseInt(comet_container.find('.bar').prop('data-percentage'));
            },

            setTitle: function(o) {
                var comet_container = getContainer(this);
                if (comet_container == false) {
                    return false;
                }

                if (!this.data('ceProgressbar')) {
                    this.ceProgress('init');
                }

                if (o.title) {
                    $('#comet_title').text(o.title);
                }
            },

            finish: function() {
                var comet_container = getContainer(this);
                if (comet_container == false) {
                    return false;
                }

                comet_container.find('.bar').css('width', 100).prop('data-percentage', 100);
                comet_container.modal('hide');

                this.removeData('ceProgressbar');
            }
        };

        $.fn.ceProgress = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if ( typeof method === 'object' || ! method ) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('ty.progress: method ' +  method + ' does not exist');
            }
        };
    })($);


    /*
     * History plugin
     *
     */
    (function($){

        var methods = {

            init: function() {

                if ($.history) {

                    $.history.init(function(hash, params) {

                        if(params && 'result_ids' in params) {
                            var uri = methods.parseHash('#' + hash);
                            var href = uri.indexOf(_.current_location) != -1 ? uri : _.current_location + '/' + uri;
                            var target_id = params.result_ids;
                            var a_elm = $('a[data-ca-target-id="' + target_id + '"]:first'); // hm, used for callback only, so I think it will work with the first found link
                            var name = a_elm.prop('name');

                            $.ceAjax('request', href, {full_render: params.full_render, result_ids: target_id, caching: false, obj: a_elm, skip_history: true, callback: 'ce.ajax_callback_' + name});

                        }
                    }, {unescape: false});
                    return true;
                } else {
                    return false;
                }
            },

            load: function(url, params) 
            {
                var _params, current_url;

                url = methods.prepareHash(url);
                current_url = methods.prepareHash(_.current_url);

                _params = {
                    result_ids: params.result_ids,
                    full_render: params.full_render
                }

                $.ceEvent('trigger', 'ce.history_load', [url]);
                $.history.reload(current_url, _params);
                $.history.load(url, _params);
            },

            prepareHash: function(url)
            {

                url = unescape(url); // urls in original content are escaped, so we need to unescape them

                if (url.indexOf('://') !== -1) {
                    //FIXME: Remove this code when support for Internet Explorer 8 and 9 is dropped
                    if($.browser.msie && $.browser.version >= 9) {
                        url = _.current_path + '/' + url.str_replace(_.current_location + '/', '');
                    } else {
                        url = url.str_replace(_.current_location + '/', '');
                    }
                }

                url = fn_query_remove(url, ['result_ids']);
                url = '!/' + url;
                
                return url;
            },

            parseHash: function(hash)
            {
                if (hash.indexOf('%') !== -1) {
                    hash = unescape(hash);
                }

                if (hash.indexOf('#!') != -1) {
                    var parts = hash.split('#!/');

                    return parts[1] || '';
                }

                return '';
            }
        };

        $.ceHistory = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else {
                $.error('ty.history: method ' +  method + ' does not exist');
            }
        }
    })($);

    /*
    * Hint methods
    *
    */
    (function($){

        var methods = {
            init: function() {
                return this.each(function() {
                    var elm = $(this);
                    elm.bind ({
                        click: function() {
                            $(this).ceHint('_check_hint');
                        },
                        focus: function() {
                            $(this).ceHint('_check_hint');
                        },
                        focusin: function() {
                            $(this).ceHint('_check_hint');
                        },
                        blur: function() {
                            $(this).ceHint('_check_hint_focused');
                        },
                        focusout: function() {
                            $(this).ceHint('_check_hint_focused');
                        }
                    });
                    elm.addClass('cm-hint-focused');
                    elm.removeClass('cm-hint');
                    elm.ceHint('_check_hint_focused');
                });
            },

            is_hint: function() {
                return $(this).hasClass('cm-hint') && ($(this).val() == $(this).ceHint('_get_hint_value'));
            },

            _check_hint: function() {
                var elm = $(this);
                if (elm.ceHint('is_hint')) {
                    elm.addClass('cm-hint-focused');
                    elm.val('');
                    elm.removeClass('cm-hint');
                    elm.prop('name', elm.prop('name').str_replace('hint_', ''));
                }
            },

            _check_hint_focused: function() {
                var elm = $(this);
                if (elm.hasClass('cm-hint-focused')) {
                    if (elm.val() == '' || (elm.val() == elm.ceHint('_get_hint_value'))) {
                        elm.addClass('cm-hint');
                        elm.removeClass('cm-hint-focused');
                        elm.val(elm.ceHint('_get_hint_value'));
                        elm.prop('name', 'hint_' + elm.prop('name'));
                    }
                }
            },

            _get_hint_value: function() {
                return ($(this).prop('title') != '') ? $(this).prop('title') : $(this).prop('defaultValue');
            }

        };

        $.fn.ceHint = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if ( typeof method === 'object' || ! method ) {
                return methods.run.apply(this, arguments);
            } else {
                $.error('ty.hint: method ' +  method + ' does not exist');
            }
        };
    })($);

    /*
     *
     * Range slider
     *
     */
    (function($){

        var placeholder = 'nnn-nnn';
        var methods = {

            init: function() {
                return this.each(function() {
                    var elm = $(this);
                    var id = elm.prop('id');
                    var json_data = $('#' + id + '_json').val();
                    if (elm.data('uiSlider') || !json_data) {
                        return false;
                    }
                    var data = $.parseJSON(json_data) || null;
                    if (!data) {
                        return false;
                    }

                    elm.slider({
                        disabled: data.disabled,
                        range: true,
                        min: data.min,
                        max: data.max,
                        step: data.step,
                        values: [data.left, data.right],
                        slide: function(event, ui) {
                            $('#' + id + '_left').val(ui.values[0]);
                            $('#' + id + '_right').val(ui.values[1]);
                        },
                        change: function(event, ui){
                            var replacement = data.type + ui.values[0] + '-' + ui.values[1];
                            if (data.type == 'P') {
                                replacement = replacement + '-' + data.currency;
                            }
                            var url = data.url.replace(data.type + placeholder, replacement);
                            if (!data.ajax) {
                                $.toggleStatusBox('show');
                                $.redirect(url);
                            } else {
                                $.ceAjax('request', url, {
                                    full_render: true,
                                    save_history: true,
                                    result_ids: data.result_ids,
                                    scroll: data.scroll || '',
                                    caching: true
                                });
                            }
                        }
                    });

                    $('#' + id + '_left').off('change').on('change', function() {
                        var v1 = parseInt($('#' + id + '_left').val());
                        var v2 = parseInt($('#' + id + '_right').val());
                        $('#' + id).slider('values', [(isNaN(v1) ? 0 : v1), (isNaN(v2) ? 0 : v2)]);
                    });
                    $('#' + id + '_right').off('change').on('change', function() {
                        var v1 = parseInt($('#' + id + '_left').val());
                        var v2 = parseInt($('#' + id + '_right').val());
                        $('#' + id).slider('values', [(isNaN(v1) ? 0 : v1), (isNaN(v2) ? 0 : v2)]);
                    });

                    if (elm.parents('.filter-wrap').hasClass('open') || elm.parent('.price-slider').hasClass('cm-custom-filter')) {
                        elm.parent('.price-slider').show();
                    }
                });
            }
        };

        $.fn.ceRangeSlider = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if ( typeof method === 'object' || ! method ) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('ty.rangeslider: method ' +  method + ' does not exist');
            }
        };
    })($);

    /*
     *
     * Tooltips
     *
     */
     (function($){

        var methods = {

            init: function(params) {

                var default_params = {
                    events: {
                        def: 'mouseover, mouseout',
                        input: 'focus, blur'
                    },
                    layout: '<div><span class="tooltip-arrow"></span></div>',
                };

                $.extend(default_params, params);

                return this.each(function() {
                    var elm = $(this);
                    var params = default_params;

                    if (elm.data('tooltip')) {
                        return false;
                    }

                    if (elm.data('ceTooltipPosition') === 'top') {
                        params.position = 'top left';
                        params.tipClass = 'tooltip arrow-top';
                        params.offset = [-10, 7];
                    } else {
                        params.offset = [10, 7];
                        params.tipClass = 'tooltip arrow-down';
                        params.position = 'bottom left';
                    }

                    elm.tooltip(params).dynamic({
                        right: {},
                        left: {}
                    });
                    
                    //hide tooltip before remove 
                    elm.on("remove", function() {
                        $(this).trigger('mouseout');
                    });
                });
            }
        };

        $.fn.ceTooltip = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if ( typeof method === 'object' || ! method ) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('ty.tooltip: method ' +  method + ' does not exist');
            }
        };
    })($);

    /*
     *
     * Sortables
     *
     */
    (function($){
        var methods = {
            init: function(params) {
                return this.each(function() {
                    var params = params || {};
                    var update_text = _.tr('text_position_updating');
                    var self = $(this);

                    var table = self.data('caSortableTable');
                    var id_name = self.data('caSortableIdName')

                    var sortable_params = {
                        accept: 'cm-sortable-row',
                        items: '.cm-row-item',
                        tolerance: 'pointer',
                        axis: 'y',
                        containment: 'parent',
                        opacity: '0.9',
                        update: function(event, ui) {
                            var positions = [], ids = [];
                            var container = $(ui.item).closest('.cm-sortable');

                            $('.cm-row-item', container).each(function(){ // FIXME: replace with data -attribute
                                var matched = $(this).prop('class').match(/cm-sortable-id-([^\s]+)/i);
                                var index = $(this).index();

                                positions[index] = index;
                                ids[index] = matched[1];
                            });

                            var data_obj = {
                                positions: positions.join(','),
                                ids: ids.join(',')
                            };

                            $.ceAjax('request', fn_url('tools.update_position?table=' + table + '&id_name=' + id_name), {
                                method: 'get',
                                caching: false,
                                message: update_text,
                                data: data_obj
                            });

                            return true;
                        }
                    };

                    // If we have sortable handle, update default params
                    if ($('.cm-sortable-handle', self).length) {
                        sortable_params = $.extend(sortable_params, {
                            opacity: '0.5',
                            handle: '.cm-sortable-handle'
                        });
                    }

                    self.sortable(sortable_params);
                });
            }
        };

        $.fn.ceSortable = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if ( typeof method === 'object' || ! method ) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('ty.sortable: method ' +  method + ' does not exist');
            }
        };
    })($);


    /*
    *
    * Color picker
    *
    */
    (function($){

        var methods = {
            init: function(params)
            {
                if (!$(this).length) {
                    return false;
                }

                if (!$.fn.spectrum) {
                    var elms = $(this);
                    $.loadCss(['js/lib/spectrum/spectrum.css'], false, true);
                    $.getScript('js/lib/spectrum/spectrum.js', function(){
                        elms.ceColorpicker();
                    });
                    return false;
                }

                var palette = [
                    ["#000000", "#434343", "#666666", "#999999", "#b7b7b7", "#cccccc", "#d9d9d9", "#efefef", "#f3f3f3", "#ffffff"],
                    ["#980000", "#ff0000", "#ff9900", "#ffff00", "#00ff00", "#00ffff", "#4a86e8", "#0000ff", "#9900ff", "#ff00ff"],
                    ["#e6b8af", "#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d0e0e3", "#c9daf8", "#cfe2f3", "#d9d2e9", "#ead1dc"],
                    ["#dd7e6b", "#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#a4c2f4", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
                    ["#cc4125", "#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6d9eeb", "#6fa8dc", "#8e7cc3", "#c27ba0"],
                    ["#a61c00", "#cc0000", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3c78d8", "#3d85c6", "#674ea7", "#a64d79"],
                    ["#85200c", "#990000", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#1155cc", "#0b5394", "#351c75", "#741b47"],
                    ["#5b0f00", "#660000", "#783f04", "#7f6000", "#274e13", "#0c343d", "#1c4587", "#073763", "#20124d", "#4c1130"]
                ];

                return this.each(function() {
                    var jelm = $(this);
                    var params = {
                        showInput: true,
                        showInitial: false,
                        showPalette: false,
                        showSelectionPalette: false,
                        palette: palette,
                        preferredFormat: 'hex6',
                        beforeShow: function() {
                            jelm.spectrum('option', 'showPalette', true);
                            jelm.spectrum('option', 'showInitial', true);
                            jelm.spectrum('option', 'showSelectionPalette', true);
                        },
                        hide:  function() {
                            $.ceEvent('trigger', 'ce.colorpicker.hide');
                        },
                        show:  function() {
                            $.ceEvent('trigger', 'ce.colorpicker.show');
                        }

                    };

                    if (jelm.data('caView') && jelm.data('caView') == 'palette') {
                        params.showPaletteOnly = true;
                    }

                    if (jelm.data('caStorage')) {
                        params.localStorageKey = jelm.data('caStorage');
                    }

                    jelm.spectrum(params);
                    jelm.spectrum('container').appendTo(jelm.parent());
                });
            },

            reset: function()
            {
                this.spectrum('set', this.val());
            },

            set: function(val)
            {
                this.spectrum('set', val);
            }
        };

        $.fn.ceColorpicker = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if ( typeof method === 'object' || ! method ) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('ty.colorpicker: method ' +  method + ' does not exist');
            }
        };
    })($);

    /*
    *
    * Form validator
    *
    */
    (function($){

        var clicked_elm; // last clicked element
        var zipcode_regexp = {}; // zipcode validation regexps
        var regexp = {}; // validation regexps - deprecated
        var validators = []; // registered custom validators

        function _fillRequirements(form, check_filter)
        {
            var lbl, lbls, id, elm, requirements = {};

            if (check_filter) {
                lbls = $(check_filter, form).find('label');
            } else {
                lbls = $('label', form);
            }

            for (k = 0; k < lbls.length; k++) {
                lbl = $(lbls[k]);
                id = lbl.prop('for');

                // skip lables with not assigned element, class or not-valid id (e.g. with placeholders)
                if (!id || !lbl.prop('class') || !id.match(/^([a-z0-9-_]+)$/)) {
                    continue;
                }

                elm = $('#' + id);

                if (elm.length && !elm.prop('disabled')) {
                    requirements[id] = {
                        elm: elm,
                        lbl: lbl
                    };
                }
            }

            return requirements;
        }

        function _checkFields(form, requirements)
        {
            var set_mark, elm, lbl, container, _regexp, _message;
            var message_set = false;

            // Reset all failed fields
            $('.cm-failed-field', form).removeClass('cm-failed-field');
            errors = {};
            for (var elm_id in requirements) {
                set_mark = false;
                elm = requirements[elm_id].elm;
                lbl = requirements[elm_id].lbl;

                // Check the need to trim value
                if (lbl.hasClass('cm-trim')) {
                    elm.val($.trim(elm.val()));
                }

                // Check the email field
                if (lbl.hasClass('cm-email')) {
                    if ($.is.email(elm.val()) == false) {
                        if (lbl.hasClass('cm-required') || $.is.blank(elm.val()) == false) {
                            _formMessage(_.tr('error_validator_email'), lbl);
                            set_mark = true;
                        }
                    }
                }

                // Check for correct color code
                if (lbl.hasClass('cm-color')) {
                    if ($.is.color(elm.val()) == false) {
                        if (lbl.hasClass('cm-required') || $.is.blank(elm.val()) == false) {
                            _formMessage(_.tr('error_validator_color'), lbl);
                            set_mark = true;
                        }
                    }
                }

                // Check the phone field
                if (lbl.hasClass('cm-phone')) {
                    if ($.is.phone(elm.val()) != true) {
                        if (lbl.hasClass('cm-required') || $.is.blank(elm.val()) == false) {
                            _formMessage(_.tr('error_validator_phone'), lbl);
                            set_mark = true;
                        }
                    }
                }

                // Check the zipcode field
                if (lbl.hasClass('cm-zipcode')) {
                    var loc = lbl.prop('class').match(/cm-location-([^\s]+)/i)[1] || '';
                    var country = $('.cm-country' + (loc ? '.cm-location-' + loc : ''), form).val();
                    var val = elm.val();

                    if (zipcode_regexp[country] && !elm.val().match(zipcode_regexp[country]['regexp'])) {
                        if (lbl.hasClass('cm-required') || $.is.blank(elm.val()) == false) {
                            _formMessage(_.tr('error_validator_zipcode'), lbl, null, zipcode_regexp[country]['format']);
                            set_mark = true;
                        }
                    }
                }

                // Check for integer field
                if (lbl.hasClass('cm-integer')) {
                    if ($.is.integer(elm.val()) == false) {
                        if (lbl.hasClass('cm-required') || $.is.blank(elm.val()) == false) {
                            _formMessage(_.tr('error_validator_integer'), lbl);
                            set_mark = true;
                        }
                    }
                }

                // Check for multiple selectbox
                if (lbl.hasClass('cm-multiple') && elm.prop('length') == 0) {
                    _formMessage(_.tr('error_validator_multiple'), lbl);
                    set_mark = true;
                }

                // Check for passwords
                if (lbl.hasClass('cm-password')) {
                    var pair_lbl = $('label.cm-password', form).not(lbl);
                    var pair_elm = $('#' + pair_lbl.prop('for'));

                    if (elm.val() && elm.val() != pair_elm.val()) {
                        _formMessage(_.tr('error_validator_password'), lbl, pair_lbl);
                        set_mark = true;
                    }
                }

                if (validators) {
                    for (var i = 0; i < validators.length; i++) {
                        if (lbl.hasClass(validators[i].class_name)) {
                            result = validators[i].func(elm_id);
                            if (result != true) {
                                _formMessage(validators[i].message, lbl);
                                set_mark = true;
                            }
                        }
                    }
                }

                if (lbl.hasClass('cm-regexp')) {
                    _regexp = null;
                    _message = null;
                    if (elm_id in regexp) {
                        _regexp = regexp[elm_id]['regexp'];
                        _message = regexp[elm_id]['message'] ? regexp[elm_id]['message'] : _.tr('error_validator_message');
                    } else if (lbl.data('caRegexp')) {
                        _regexp = lbl.data('caRegexp');
                        _message = lbl.data('caMessage');
                    }

                    if (_regexp && !elm.ceHint('is_hint')) {
                        var val = elm.val();
                        var expr = new RegExp(_regexp);
                        var result = expr.test(val);

                        if (!result && !(!lbl.hasClass('cm-required') && elm.val() == '')) {
                            _formMessage(_message, lbl);
                            set_mark = true;
                        }
                    }
                }

                // Check for the multiple checkboxes/radio buttons
                if (lbl.hasClass('cm-multiple-checkboxes') || lbl.hasClass('cm-multiple-radios')) {
                    if (lbl.hasClass('cm-required')) {
                        var el_filter = lbl.hasClass('cm-multiple-checkboxes') ? '[type=checkbox]' : '[type=radio]';
                        if ($(el_filter + ':not(:disabled)', elm).length && !$(el_filter + ':checked', elm).length) {
                            _formMessage(_.tr('error_validator_required'), lbl);
                            set_mark = true;
                        }
                    }
                }

                // Select all items in multiple selectbox
                if (lbl.hasClass('cm-all')) {
                    if (elm.prop('length') == 0 && lbl.hasClass('cm-required')) {
                        _formMessage(_.tr('error_validator_multiple'), lbl);
                        set_mark = true;
                    } else {
                        $('option', elm).prop('selected', true);
                    }

                // Check for blank value
                } else {

                    // Check for multiple selectbox
                    if (elm.is(':input')) {
                        if (lbl.hasClass('cm-required') && ((elm.is('[type=checkbox]') && !elm.prop('checked')) || $.is.blank(elm.val()) == true || elm.ceHint('is_hint'))) {
                            _formMessage(_.tr('error_validator_required'), lbl);
                            set_mark = true;
                        }
                    }
                }

                container = elm.closest('.cm-field-container');
                if (container.length) {
                    elm = container;
                }

                $('[id="' + elm_id + '_error_message"].help-inline', elm.parent()).remove();

                if (set_mark == true) {
                    lbl.parent().addClass('error');
                    elm.addClass('cm-failed-field');
                    lbl.addClass('cm-failed-label');

                    if (!elm.hasClass('cm-no-failed-msg')) {
                        elm.after('<span id="' + elm_id + '_error_message" class="help-inline">' + _getMessage(elm_id) + '</span>');
                    }

                    if (!message_set) {
                        $.scrollToElm(elm);
                        message_set = true;
                    }

                    // Resize dialog if we have errors
                    var dlg = $.ceDialog('get_last');
                    var dlg_target = $('.cm-dialog-auto-size[data-ca-target-id="'+ dlg.attr('id') +'"]');

                    if(dlg_target.length) {
                        dlg.ceDialog('reload');
                    }

                } else {
                    lbl.parent().removeClass('error');
                    elm.removeClass('cm-failed-field');
                    lbl.removeClass('cm-failed-label');
                }
            }
            return !message_set;
        }

        function _disableEmptyFields(form)
        {
            var selector = [];

            if (form.hasClass('cm-disable-empty')) {
                selector.push('input[type=text]');
            }
            if (form.hasClass('cm-disable-empty-files')) {
                selector.push('input[type=file]');

                // Disable empty input[type=file] in order to block the "garbage" data
                $('input[type=file][data-ca-empty-file=""]', form).prop('disabled', true);
            }

            if (selector.length) {
                $(selector.join(','), form).each(function() {
                    var self = $(this);
                    if (self.val() == '') {
                        self.prop('disabled', true);
                        self.addClass('cm-disabled')
                    }
                });
            }
        }

        function _check(form, clicked_elm)
        {
            var form_result = true;
            var check_fields_result = true;

            if (!clicked_elm.hasClass('cm-skip-validation')) {

                var requirements = _fillRequirements(form, clicked_elm.data('caCheckFilter'))

                if ($.ceEvent('trigger', 'ce.formpre_' + form.prop('name'), [form, clicked_elm]) === false) {
                    form_result = false;
                }

                check_fields_result = _checkFields(form, requirements);
            }

            if (check_fields_result == true && form_result == true) {

                _disableEmptyFields(form);

                // remove currency symbol
                form.find('.cm-numeric').each(function() {
                    var val = $(this).autoNumeric('get');
                    $(this).prop('value', val);
                });

                // protect button from double click
                if (clicked_elm.data('clicked') == true) {
                    return false;
                }

                if ((form.hasClass('cm-ajax') || clicked_elm.hasClass('cm-ajax')) && !clicked_elm.hasClass('cm-no-ajax')) {

                    // set clicked flag
                    clicked_elm.data('clicked', true);

                    // clean clicked flag
                    $.ceEvent('one', 'ce.ajaxdone', function() {
                        clicked_elm.data('clicked', false);
                    });
                }

                // If pressed button has cm-new-window microformat, send form to new window
                // otherwise, send to current
                if (clicked_elm.hasClass('cm-new-window')) {
                    form.prop('target', '_blank');
                    return true;

                } else if (clicked_elm.hasClass('cm-parent-window')) {
                    form.prop('target', '_parent');
                    return true;

                } else {
                    form.prop('target', '_self');
                }

                if ($.ceEvent('trigger', 'ce.formpost_' + form.prop('name'), [form, clicked_elm]) === false) {
                    form_result = false;
                }

                if ((form.hasClass('cm-ajax') || clicked_elm.hasClass('cm-ajax')) && !clicked_elm.hasClass('cm-no-ajax')) {

                    // FIXME: this code should be moved to another place I believe
                    var collection = form.add(clicked_elm);
                    if (collection.hasClass('cm-form-dialog-closer') || collection.hasClass('cm-form-dialog-opener')) {

                        $.ceEvent('one', 'ce.formajaxpost_' + form.prop('name'), function(response_data, params) {
                            
                            if (collection.hasClass('cm-form-dialog-closer')) {
                                $.popupStack.last_close();
                            }

                            if (collection.hasClass('cm-form-dialog-opener')) {
                                var _id = form.find('input[name=result_ids]').val();
                                if (_id && typeof(response_data.html) !== "undefined") {
                                    $('#' + _id).ceDialog('open', $.ceDialog('get_params', form));
                                }
                            }
                        });
                    }

                    return $.ceAjax('submitForm', form, clicked_elm);
                }

                if (clicked_elm.hasClass('cm-no-ajax')) {
                    $('input[name=is_ajax]', form).remove();
                }

                if (_.embedded && form_result == true && !$.externalLink(form.prop('action'))) {

                    form.append('<input type="hidden" name="result_ids" value="' + _.container + '" />');
                    clicked_elm.data('caScroll', '#' + _.container);
                    return $.ceAjax('submitForm', form, clicked_elm);
                }

                if (clicked_elm.closest('.cm-dialog-closer').length) {
                    $.ceDialog('get_last').ceDialog('close');
                }

                return form_result;

            } else if (check_fields_result == false) {
                var hidden_tab = $('.cm-failed-field', form).parents('[id^="content_"]:hidden');
                if (hidden_tab.length && $('.cm-failed-field', form).length == $('.cm-failed-field', hidden_tab).length) {
                    $('#' + hidden_tab.prop('id').str_replace('content_', '')).click();
                }
            }

            return false;
        }

        function _formMessage(msg, field, field2, extra)
        {
            var id = field.prop('for');

            if (errors[id]) {
                return false;
            }

            errors[id] = [];

            msg = msg.str_replace('[field]', _fieldTitle(field));

            if (field2) {
                msg = msg.str_replace('[field2]', _fieldTitle(field2));
            }
            if (extra) {
                msg = msg.str_replace('[extra]', extra);
            }

            errors[id].push(msg);
        };

        function _fieldTitle(field)
        {
            return field.text().replace(/(\s*\(\?\))?:\s*$/, '');
        }

        function _getMessage(id)
        {
            return '<p>' + errors[id].join('</p><p>') + '</p>';
        };

        // public methods
        var methods = {
            init: function() {
                var form = $(this);
                form.on('submit', function(e) {
                    if (!clicked_elm) { // workaround for IE when the form has one input only
                        if ($('[type=submit]', form).length) {
                            clicked_elm = $('[type=submit]:first', form);
                        } else if ($('input[type=image]', form).length) {
                            clicked_elm = $('input[type=image]:first', form);
                        }
                    }

                    return _check(form, clicked_elm);
                })
            },
            setClicked: function(elm) {
                clicked_elm = elm;
            }
        }

        $.fn.ceFormValidator = function(method) {
            var args = arguments;

            return $(this).each(function(i, elm) {

                // These vars are local for each element
                var errors = {};

                if (methods[method]) {
                    return methods[method].apply(this, Array.prototype.slice.call(args, 1));
                } else if ( typeof method === 'object' || ! method ) {
                    return methods.init.apply(this, args);
                } else {
                    $.error('ty.formvalidator: method ' +  method + ' does not exist');
                }
            });
        };


        $.ceFormValidator = function(action, params) {
            params = params || {};
            if (action == 'setZipcode') {
                zipcode_regexp = params;
            } else if (action == 'setRegexp') {
                if ('console' in window) {
                    console.log('This method is deprecated, use data-attributes "data-ca-regexp" and "data-ca-message" instead');
                }
                regexp = $.extend(regexp, params);
            } else if (action == 'registerValidator') {
                validators.push(params);
            }
        }
    })($);

    /*
    *
    * States field builder
    *
    */
    (function($){

        var options = {};
        var init = false;

        function _rebuildStates(section, elm)
        {
            elm = elm || $('.cm-state.cm-location-' + section).prop('id');
            var sbox = $('#' + elm).is('select') ? $('#' + elm) : $('#' + elm + '_d');
            var inp = $('#' + elm).is('input') ? $('#' + elm) : $('#' + elm + '_d');
            var default_state = inp.val();
            var cntr = $('.cm-country.cm-location-' + section);
            var cntr_disabled;

            if (cntr.length) {
                cntr_disabled = cntr.prop('disabled');
            } else {
                cntr_disabled = sbox.prop('disabled');
            }

            var country_code = (cntr.length) ? cntr.val() : options.default_country;
            var tag_switched = false;
            var pkey = '';

            sbox.prop('id', elm).prop('disabled', false).removeClass('hidden cm-skip-avail-switch');
            inp.prop('id', elm + '_d').prop('disabled', true).addClass('hidden cm-skip-avail-switch').val('');

            if (!inp.hasClass('disabled')) {
                sbox.removeClass('disabled');
            }

            if (options.states && options.states[country_code]) { // Populate selectbox with states
                sbox.prop('length', 1);
                for (var i = 0; i < options.states[country_code].length; i++) {
                    sbox.append('<option value="' + options.states[country_code][i]['code'] + '"' + (options.states[country_code][i]['code'] == default_state ? ' selected' : '') + '>' + options.states[country_code][i]['state'] + '</option>');
                }

                sbox.prop('id', elm).prop('disabled', false).removeClass('cm-skip-avail-switch');
                inp.prop('id', elm + '_d').prop('disabled', true).addClass('cm-skip-avail-switch');

                if (!inp.hasClass('disabled')) {
                    sbox.removeClass('disabled');
                }

            } else { // Disable states
                sbox.prop('id', elm + '_d').prop('disabled', true).addClass('hidden cm-skip-avail-switch');
                inp.prop('id', elm).prop('disabled', false).removeClass('hidden cm-skip-avail-switch').val(default_state);

                if (!sbox.hasClass('disabled')) {
                    inp.removeClass('disabled');
                }
            }

            if (cntr_disabled == true) {
                sbox.prop('disabled', true);
                inp.prop('disabled', true);
            }
        }

        function _rebuildStatesInLocation() {
            var location_elm = $(this).prop('class').match(/cm-location-([^\s]+)/i);
            if (location_elm) {
                _rebuildStates(location_elm[1], $('.cm-state.cm-location-' + location_elm[1]).not(':disabled').prop('id'));
            }
        }

        var methods = {
            init: function() {
                if ($(this).hasClass('cm-country')) {
                    if (init == false) {
                        $(_.doc).on('change', 'select.cm-country', _rebuildStatesInLocation);
                        init = true;
                    }
                    $(this).trigger('change');
                } else {
                    _rebuildStatesInLocation.call(this);
                }
            }
        }

        $.fn.ceRebuildStates = function(method) {
            var args = arguments;

            return $(this).each(function(i, elm) {
                if (methods[method]) {
                    return methods[method].apply(this, Array.prototype.slice.call(args, 1));
                } else if ( typeof method === 'object' || ! method ) {
                    return methods.init.apply(this, args);
                } else {
                    $.error('ty.rebuildstates: method ' +  method + ' does not exist');
                }
            });
        };

        $.ceRebuildStates = function(action, params) {
            params = params || {};
            if (action == 'init') {
                options = params;
            }
        }
    })($);

   /*
    *
    * Sticky scroll
    *
    */
    (function($){
        var methods = {
            init: function(params) {
                return this.each(function() {
                    var params = params || {
                        top: $(this).data('ceTop') ? $(this).data('ceTop') : 0,
                        padding: $(this).data('cePadding') ? $(this).data('cePadding') : 0
                    };
                    var self = $(this);

                    $(window).scroll(function () {
                        if ($(window).scrollTop() > params.top) {
                            $(self).css({'position': 'fixed', 'top': params.padding + 'px'});
                        } else {
                            $(self).css({'position': '', 'top': ''});
                        }
                    });

                });
            }
        };

        $.fn.ceStickyScroll = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if ( typeof method === 'object' || ! method ) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('ty.stickyScroll: method ' +  method + ' does not exist');
            }
        };
    })($);


   /*
    *
    * Notifications
    *
    */
    (function($) {

        var container;
        var timers = {};
        var delay = 0;

        function _duplicateNotification(key)
        {
            var dups = $('div[data-ca-notification-key=' + key + ']');
            if (dups.length) {

                if (!_addToDialog(dups)) {
                    dups.fadeTo('fast', 0.5).fadeTo('fast', 1).fadeTo('fast', 0.5).fadeTo('fast', 1);
                }

                // Restart autoclose timer
                if (timers[key]) {
                    clearTimeout(timers[key]);
                    methods.close(dups, true);
                }
                
                return true;
            }

            return false;
        }

        function _closeNotification(notification)
        {
            if (notification.find('.cm-notification-close-ajax').length) {
                $.ceAjax('request', fn_url('notifications.close?notification_id=' + notification.data('caNotificationKey')), {
                    hidden: true
                });
            }

            notification.fadeOut('fast', function() {
                notification.remove();
            });

            if (notification.hasClass('cm-notification-content-extended')) {
                var overlay = $('.ui-widget-overlay[data-ca-notification-key=' + notification.data('caNotificationKey') + ']');
                if (overlay.length) {
                    overlay.fadeOut('fast', function() {
                        overlay.remove();
                    });
                }
            }
        }

        function _processTranslation(text)
        {
            if (_.live_editor_mode && text.indexOf('[lang') != -1) {
                text = '<var class="live-edit-wrap"><i class="cm-icon-live-edit icon-live-edit ty-icon-live-edit"></i><var class="cm-live-edit live-edit-item" data-ca-live-edit="langvar::' + text.substring(text.indexOf('=') + 1, text.indexOf(']')) + '">' + text.substring(text.indexOf(']') + 1, text.lastIndexOf('[')) + '</var></var>';
            }

            return text;
        }
        
        function _pickFromDialog(event) {
            var nt = $('.cm-notification-content', $(event.target));
            if (nt.length) {
                if (!_addToDialog(nt)) {
                    container.append(nt);
                }
            }
            return true;
        }
        
        function _addToDialog(notification)
        {
            var dlg = $.ceDialog('get_last');
            if (dlg.length) {
                $('.object-container', dlg).prepend(notification);
                dlg.off('dialogclose', _pickFromDialog);
                dlg.on('dialogclose', _pickFromDialog);
                return true;
            }
            return false;
        }

        var methods = {
            show: function (data, key)
            {
                if (!key) {
                    key = $.crc32(data.message);
                }

                if (typeof(data.message) == 'undefined') {
                    return false;
                }

                if (_duplicateNotification(key)) {
                    return true;
                }

                data.message = _processTranslation(data.message);
                data.title = _processTranslation(data.title);

                // Popup message in the screen center - should be only one at time
                if (data.type == 'I') {
                    var w = $.getWindowSizes();

                    $('.cm-notification-content.cm-notification-content-extended').each(function() {
                        methods.close($(this), false);
                    });

                    $(_.body).append(
                        '<div class="ui-widget-overlay" style="z-index:1010" data-ca-notification-key="' + key + '"></div>'
                    );

                    var notification = $('<div class="cm-notification-content cm-notification-content-extended notification-content-extended ' + (data.message_state == "I" ? ' cm-auto-hide' : '') + '" data-ca-notification-key="' + key + '">' +
                        '<h1>' + data.title + '<span class="cm-notification-close close"></span></h1>' +
                        '<div class="notification-body-extended">' +
                        data.message +
                        '</div>' +
                        '</div>');

                    var notificationMaxHeight = w.view_height - 300;

                    $(notification).find('.cm-notification-max-height').css({
                        'max-height': notificationMaxHeight
                    });

                    // FIXME I-type notifications are embedded directly into the body and not into a container, because a container has low z-index and get overlapped by modal dialogs.
                    //container.append(notification);
                    $(_.body).append(notification);
                    notification.css('top', w.view_height / 2 - (notification.height() / 2));

                } else {
                    var n_class = 'alert';
                    var b_class = '';

                    if (data.type == 'N') {
                        n_class += ' alert-success';
                    } else if (data.type == 'W') {
                        n_class += ' alert-warning';
                    } else if (data.type == 'S') {
                        n_class += ' alert-info';
                    } else {
                        n_class += ' alert-error';
                    }

                    if (data.message_state == 'I') {
                        n_class += ' cm-auto-hide';
                    } else if (data.message_state == 'S') {
                        b_class += ' cm-notification-close-ajax';
                    }

                    var notification = $('<div class="cm-notification-content notification-content ' + n_class + '" data-ca-notification-key="' + key + '">' +
                        '<button type="button" class="close cm-notification-close ' + b_class + '" data-dismiss="alert">×</button>' +
                        '<strong>' + data.title + '</strong>' + data.message +
                        '</div>');

                    if (!_addToDialog(notification)) {
                        container.append(notification);
                    }
                }

                $.ceEvent('trigger', 'ce.notificationshow', [notification]);

                if (data.message_state == 'I') {
                    methods.close(notification, true);
                }
            },

            showMany: function(data)
            {
                for (var key in data) {
                    methods.show(data[key], key);
                }
            },

            closeAll: function()
            {
                container.find('.cm-notification-content').each(function() {
                    var self = $(this);
                    if (!self.hasClass('cm-notification-close-ajax')) {
                        methods.close(self, false);
                    }
                })
            },

            close: function(notification, delayed)
            {
                if (delayed == true) {
                    if (delay === 0) { // do not auto-close
                        return true;
                    }

                    timers[notification.data('caNotificationKey')] = setTimeout(function(){
                        methods.close(notification, false);
                    }, delay);

                    return true;
                }

                _closeNotification(notification);
            },

            init: function()
            {
                delay = _.notice_displaying_time * 1000;
                container = $('.cm-notification-container');

                $(_.doc).on('click', '.cm-notification-close', function() {
                    methods.close($(this).parents('.cm-notification-content:first'), false);
                })

                container.find('.cm-auto-hide').each(function() {
                    methods.close($(this), true);
                });
            }
        };


        $.ceNotification = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else {
                $.error('ty.notification: method ' +  method + ' does not exist');
            }
        };

    }($));


   /*
    *
    * Events
    *
    */
    (function($) {
        var handlers = {};

        var methods = {
            on: function(event, handler, one)
            {
                one = one || false;
                if (!(event in handlers)) {
                    handlers[event] = [];
                }
                handlers[event].push({
                    handler: handler,
                    one: one
                });
            },

            one: function(event, handler)
            {
                methods.on(event, handler, true);
            },

            trigger: function(event, data)
            {
                data = data || [];
                var result = true, _res;
                if (event in handlers) {
                    for (var i = 0; i < handlers[event].length; i++) {
                        _res = handlers[event][i].handler.apply(handlers[event][i].handler, data);

                        if (handlers[event][i].one) {
                            handlers[event].splice(i, 1);
                            i --;
                        }

                        if (_res === false) {
                            result = false;
                            break;
                        }
                    }
                }

                return result;
            }
        };

        $.ceEvent = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else {
                $.error('ty.event: method ' +  method + ' does not exist');
            }
        };

    }($));

    /*
     * Code editor
     *
     */
    (function($){

        var methods = {
            _init: function(self) {

                if (!self.data('codeEditor')) {
                    var editor = ace.edit(self.prop('id'));
                    editor.session.setUseWrapMode(true);
                    editor.session.setWrapLimitRange();
                    editor.setFontSize("14px");
                    editor.renderer.setShowPrintMargin(false);

                    editor.getSession().on('change', function(e) {
                        self.addClass('cm-item-modified');
                    });

                    self.data('codeEditor', editor);
                }

                return $(this);
            },
            init: function(mode) {
                var self = $(this);
                methods._init(self);

                if (mode) {
                    self.data('codeEditor').getSession().setMode(mode);
                }

                return $(this);
            },
            set_value: function(val, mode) {
                var self = $(this);
                methods._init(self);

                if(mode == undefined) {
                    mode = 'ace/mode/html';
                }

                self.data('codeEditor').getSession().setMode(mode);
                self.data('codeEditor').setValue(val);
                self.data('codeEditor').navigateLineStart();
                self.data('codeEditor').clearSelection();
                self.data('codeEditor').scrollToRow(0);

                return $(this);
            },

            set_show_gutter: function(value) {
                $(this).data('codeEditor').renderer.setShowGutter(value);
            },

            value: function() {
                var self = $(this);
                methods._init(self);

                return self.data('codeEditor').getValue();
            },
            set_listener: function(event_name, callback)
            {
                $(this).data('codeEditor').getSession().on(event_name, function(e) {
                    callback(e);
                });

                return $(this);
            }
        };

        $.fn.ceCodeEditor = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if ( typeof method === 'object' || ! method ) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('ty.codeeditor: method ' +  method + ' does not exist');
            }
        };
    })($);

    // If page is loaded with URL in hash parameter, redirect to this URL
    if (!_.embedded && location.hash && unescape(location.hash).indexOf('#!/') === 0) {
        var components = $.parseUrl(location.href)
        var uri = $.ceHistory('parseHash', location.hash);
        
        //FIXME: Remove this code when support for Internet Explorer 8 and 9 is dropped
        if($.browser.msie && $.browser.version >= 9) {
            $.redirect(components.protocol + '://' + components.host + uri);
        } else {
            $.redirect(components.protocol + '://' + components.host + components.directory + uri);
        }
    }

}(Tygh, jQuery));


    //
    // Print variable contents
    //
    function fn_print_r(value)
    {
        fn_alert(fn_print_array(value));
    }

    //
    // Show alert
    //
    function fn_alert(msg, not_strip)
    {
        msg = not_strip ? msg : fn_strip_tags(msg);
        alert(msg);
    }

    // Helper
    function fn_print_array(arr, level)
    {
        var dumped_text = "";
        if(!level) {
            level = 0;
        }

        //The padding given at the beginning of the line.
        var level_padding = "";
        for(var j=0; j < level+1; j++) {
            level_padding += "    ";
        }

        if(typeof(arr) == 'object') { //Array/Hashes/Objects
            for(var item in arr) {
                var value = arr[item];

                if(typeof(value) == 'object') { //If it is an array,
                    dumped_text += level_padding + "'" + item + "' ...\n";
                    dumped_text += fn_print_array(value,level+1);
                } else {
                    dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                }
            }
        } else { //Stings/Chars/Numbers etc.
            dumped_text = arr+" ("+typeof(arr)+")";
        }

        return dumped_text;
    }

    function fn_url(url)
    {
        var index_url = Tygh.current_location + '/' + Tygh.index_script;
        var components = Tygh.$.parseUrl(url);

        if (url == '') {
            url = index_url;

        } else if (components.protocol) {

            if (Tygh.embedded) {

                var s, spos;
                if (Tygh.facebook && Tygh.facebook.url.indexOf(components.location) != -1) {
                    s = '&app_data=';
                } else if (Tygh.init_context == components.source.str_replace('#' + components.anchor, '')) {
                    s = '#!';
                }

                if (s) {

                    var q = '';
                    if ((spos = url.indexOf(s)) != -1) {
                        q = unescape(url.substr(spos + s.length)).replace('&amp;', '&');
                    }

                    url = Tygh.current_location + q;
                }
            }

        } else if (components.file != Tygh.index_script) {
            if (url.indexOf('?') == 0) {
                url = index_url + url;

            } else {
                url = index_url + '?dispatch=' + url.replace('?', '&');

            }
        }

        return url;
    }

    function fn_strip_tags(str)
    {
        str = String(str).replace(/<.*?>/g, '');
        return str;
    }

    function fn_reload_form(jelm)
    {
        var form = jelm.parents('form');
        var container = form.parent();

        var submit_btn = form.find("input[type='submit']");
        if (!submit_btn.length) {
            submit_btn = Tygh.$('[data-ca-target-form=' + form.prop('name') + ']');
        }

        if (container.length && submit_btn.length) {

            var url = form.prop('action') + '?reload_form=1&' + submit_btn.prop('name');

            var data = form.serializeObject();
            var result_ids;
            // If not preset result_ids in form get form container id
            if (data.result_ids != 'undefined') {
                result_ids = data.result_ids;
            } else {
                result_ids = container.prop('id');
            }
            Tygh.$.ceAjax('request', fn_url(url), {
                data: data,
                result_ids: result_ids
            });
        }
    }

    function fn_get_listed_lang(langs)
    {
        var $ = Tygh.$;
        // check langs priority
        var check_langs = [Tygh.cart_language, Tygh.default_language, 'en'];
        var lang = '';

        if (langs.length) {
            lang = langs[0];

            for (var i = 0; i < check_langs.length; i++) {
                if (Tygh.$.inArray(check_langs[i], langs) != -1) {
                    lang = check_langs[i];
                    break;
                }
            }
        }

        return lang;
    }

    function fn_query_remove(query, vars)
    {
        if (typeof(vars) == 'undefined') {
            return query;
        }
        if (typeof vars == 'string') {
            vars = [vars];
        }
        var start = query;
        if (query.indexOf('?') >= 0) {
            start = query.substr(0, query.indexOf('?') + 1);
            var search = query.substr(query.indexOf('?') + 1);
            var srch_array = search.split("&");
            var temp_array = [];
            var concat = true;
            var amp = '';

            for (var i = 0; i < srch_array.length; i++) {
                temp_array = srch_array[i].split("=");
                concat = true;
                for (var j = 0; j < vars.length; j++) {
                    if (vars[j] == temp_array[0] || temp_array[0].indexOf(vars[j]+'[') != -1) {
                        concat = false;
                        break;
                    }
                }
                if (concat == true) {
                    start += amp + temp_array[0] + '=' + temp_array[1];
                }
                amp = '&';
            }
        }
        return start;
    }
	$(document).ready(function() {
		$(window).scroll(function(){
			var height = 225; 
			if ($(window).scrollTop() < height) {
				$('.tygh-header').removeClass('fixed');
			} else { 
				$('.tygh-header').addClass('fixed');
			}
		});
		$(window).scroll(function() {
			var check_load = $('body').hasClass('loadpage');
			if ($(window).scrollTop() > 250 && check_load==false) {
				$('#load_face').load('load_facebook.html');
				$('#load_sc').load('load_sc.html');
				$('#addthis_button').load('addthis_button.txt');
				$('body').addClass('loadpage');
			}
		});
		$('#gototop').click(function(e){$('body,html').animate({scrollTop:0});});
		$(".tab_content").hide();
		$(".tab_content:first").show();
		$(".tab_content1").hide();
		$(".tab_content1:first").show();
		$(".tab_content2").hide();
		$(".tab_content2:first").show();
		$(".tab_content3").hide();
		$(".tab_content3:first").show();
		
		$("ul.tabs li").click(function() {
			$(this).parent().find("li").removeClass("active");
			$(this).addClass("active");
			var shree = $(this).attr("id");
			if($("."+shree).hasClass("tab_content")==true){
				$(".tab_content").hide();
			}else if($("."+shree).hasClass("tab_content1")==true){
				$(".tab_content1").hide();
			}else if($("."+shree).hasClass("tab_content2")==true){
				$(".tab_content2").hide();
			}
			else if($("."+shree).hasClass("tab_content3")==true){
				$(".tab_content3").hide();
			}
			$("."+shree).fadeIn();
			var bLazy = new Blazy({
				container: '#scrolling-container',
			});
		});
		
		$('.show_post').on('click', function(){
			$('.hidden_post').show();
		});
		$('.show_post_item').on('click', function(){
			$('.hidden_post_item').stop().hide();
			$(this).stop().parent().parent().find('.hidden_post_item').show();
		});
		$('.reply-item').on('click', function(){
			item_id = $(this).attr('id');
			$('.hiden-form-post-item').stop().hide();
			$('.hiden-list-item').stop().hide();
			$('#show-list-'+item_id).stop().show();
			$('#hiden-form-post-'+item_id).stop().show();
		});
		$('#wheel_cancel').click(function(){
			var url = fn_url('wheel.cancel');
			$.ceAjax('request', url);
			$('.main_popup_wheel').hide();
		});
		$('#wheel_quaytay').click(function(){
			$('.main_popup_wheel').hide();
			window.open(fn_url('wheel.online'), '_blank');
		});
		$.ceEvent('on', 'ce.ajaxdone', function(){
			$('.show_post').on('click', function(){
				$('.hidden_post').show();
			});
			$('.show_post_item').on('click', function(){
				$('.hidden_post_item').stop().hide();
				$(this).stop().parent().parent().find('.hidden_post_item').show();
			});
			$('.reply-item').on('click', function(){
				item_id = $(this).attr('id');
				$('.hiden-form-post-item').stop().hide();
				$('.hiden-list-item').stop().hide();
				$('#show-list-'+item_id).stop().show();
				$('#hiden-form-post-'+item_id).stop().show();
			});
		});
		
	});
$(document).ready(function(e) {
	$('.check_cart').load('http://phukiengiare.com/index.php?dispatch=products.chec_sscart');
});/*
 *	jQuery OwlCarousel v1.29
 *  
 *	Copyright (c) 2013 Bartosz Wojciechowski
 *	http://www.owlgraphic.com/owlcarousel
 *
 *	Licensed under MIT
 *
 */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('7(F 3t.3s!=="9"){3t.3s=9(e){9 t(){}t.4X=e;q 5u t}}(9(e,t,n,r){b i={1B:9(t,n){b r=c;r.$k=e(n);r.6=e.3R({},e.3n.2t.6,r.$k.w(),t);r.2w=t;r.4l()},4l:9(){b t=c;7(F t.6.3u==="9"){t.6.3u.S(c,[t.$k])}7(F t.6.2Q==="3r"){b n=t.6.2Q;9 r(e){7(F t.6.3a==="9"){t.6.3a.S(c,[e])}p{b n="";1E(b r 3l e["h"]){n+=e["h"][r]["1W"]}t.$k.2s(n)}t.2J()}e.5s(n,r)}p{t.2J()}},2J:9(e){b t=c;t.$k.A({29:0});t.2a=t.6.v;t.4I();t.5e=0;t.1L;t.1T()},1T:9(){b e=c;7(e.$k.1N().14===0){q d}e.1I();e.4G();e.$V=e.$k.1N();e.K=e.$V.14;e.4F();e.$M=e.$k.11(".h-1W");e.$J=e.$k.11(".h-1n");e.3k="W";e.1i=0;e.m=0;e.4E();e.4C()},4C:9(){b e=c;e.2K();e.2N();e.4A();e.2W();e.4x();e.4w();e.2p();e.4u();7(e.6.2l!==d){e.4r(e.6.2l)}7(e.6.N===j){e.6.N=5t}e.1a();e.$k.11(".h-1n").A("4q","4p");7(!e.$k.27(":2H")){e.2I()}p{e.$k.A("29",1)}e.5v=d;e.28();7(F e.6.2L==="9"){e.6.2L.S(c,[e.$k])}},28:9(){b e=c;7(e.6.1Y===j){e.1Y()}7(e.6.1s===j){e.1s()}e.4j();7(F e.6.2Y==="9"){e.6.2Y.S(c,[e.$k])}},30:9(){b e=c;7(F e.6.31==="9"){e.6.31.S(c,[e.$k])}e.2I();e.2K();e.2N();e.4i();e.2W();e.28();7(F e.6.33==="9"){e.6.33.S(c,[e.$k])}},4g:9(e){b t=c;15(9(){t.30()},0)},2I:9(){b e=c;7(e.$k.27(":2H")===d){e.$k.A({29:0});16(e.1z);16(e.1L)}p{q d}e.1L=4f(9(){7(e.$k.27(":2H")){e.4g();e.$k.4c({29:1},3j);16(e.1L)}},5n)},4F:9(){b e=c;e.$V.5o(\'<L I="h-1n">\').4a(\'<L I="h-1W"></L>\');e.$k.11(".h-1n").4a(\'<L I="h-1n-49">\');e.1M=e.$k.11(".h-1n-49");e.$k.A("4q","4p")},1I:9(){b e=c;b t=e.$k.1J(e.6.1I);b n=e.$k.1J(e.6.2h);e.$k.w("h-47",e.$k.2k("2n")).w("h-46",e.$k.2k("I"));7(!t){e.$k.H(e.6.1I)}7(!n){e.$k.H(e.6.2h)}},2K:9(){b t=c;7(t.6.2F===d){q d}7(t.6.45===j){t.6.v=t.2a=1;t.6.1w=d;t.6.1K=d;t.6.1X=d;t.6.1Q=d;t.6.1Z=d;q d}b n=e(t.6.43).1q();7(n>(t.6.1w[0]||t.2a)){t.6.v=t.2a}7(n<=t.6.1w[0]&&t.6.1w!==d){t.6.v=t.6.1w[1]}7(n<=t.6.1K[0]&&t.6.1K!==d){t.6.v=t.6.1K[1]}7(n<=t.6.1X[0]&&t.6.1X!==d){t.6.v=t.6.1X[1]}7(n<=t.6.1Q[0]&&t.6.1Q!==d){t.6.v=t.6.1Q[1]}7(n<=t.6.1Z[0]&&t.6.1Z!==d){t.6.v=t.6.1Z[1]}7(t.6.v>t.K&&t.6.42===j){t.6.v=t.K}},4x:9(){b n=c,r;7(n.6.2F!==j){q d}b i=e(t).1q();n.2S=9(){7(e(t).1q()!==i){7(n.6.N!==d){16(n.1z)}58(r);r=15(9(){i=e(t).1q();n.30()},n.6.41)}};e(t).40(n.2S)},4i:9(){b e=c;7(e.D.1p===j){7(e.G[e.m]>e.1O){e.1o(e.G[e.m])}p{e.1o(0);e.m=0}}p{7(e.G[e.m]>e.1O){e.1d(e.G[e.m])}p{e.1d(0);e.m=0}}7(e.6.N!==d){e.35()}},3Z:9(){b t=c;b n=0;b r=t.K-t.6.v;t.$M.2v(9(i){b s=e(c);s.A({1q:t.Q}).w("h-1W",3c(i));7(i%t.6.v===0||i===r){7(!(i>r)){n+=1}}s.w("h-2r",n)})},3Y:9(){b e=c;b t=0;b t=e.$M.14*e.Q;e.$J.A({1q:t*2,Y:0});e.3Z()},2N:9(){b e=c;e.3X();e.3Y();e.3W();e.3m()},3X:9(){b e=c;e.Q=24.5k(e.$k.1q()/e.6.v)},3m:9(){b e=c;b t=(e.K*e.Q-e.6.v*e.Q)*-1;7(e.6.v>e.K){e.E=0;t=0;e.1O=0}p{e.E=e.K-e.6.v;e.1O=t}q t},3V:9(){q 0},3W:9(){b e=c;e.G=[0];b t=0;1E(b n=0;n<e.K;n++){t+=e.Q;e.G.3U(-t)}},4A:9(){b t=c;7(t.6.2f===j||t.6.1r===j){t.C=e(\'<L I="h-5A"/>\').5C("5F",!t.D.12).56(t.$k)}7(t.6.1r===j){t.3T()}7(t.6.2f===j){t.3S()}},3S:9(){b t=c;b n=e(\'<L I="h-5f"/>\');t.C.1h(n);t.1x=e("<L/>",{"I":"h-1f",2s:t.6.2A[0]||""});t.1y=e("<L/>",{"I":"h-W",2s:t.6.2A[1]||""});n.1h(t.1x).1h(t.1y);n.z("2D.C 1S.C",\'L[I^="h"]\',9(e){e.1e()});n.z("2d.C 2c.C",\'L[I^="h"]\',9(n){n.1e();7(e(c).1J("h-W")){t.W()}p{t.1f()}})},3T:9(){b t=c;t.1g=e(\'<L I="h-1r"/>\');t.C.1h(t.1g);t.1g.z("2d.C 2c.C",".h-1k",9(n){n.1e();7(3c(e(c).w("h-1k"))!==t.m){t.1j(3c(e(c).w("h-1k")),j)}})},3P:9(){b t=c;7(t.6.1r===d){q d}t.1g.2s("");b n=0;b r=t.K-t.K%t.6.v;1E(b i=0;i<t.K;i++){7(i%t.6.v===0){n+=1;7(r===i){b s=t.K-t.6.v}b o=e("<L/>",{"I":"h-1k"});b u=e("<3O></3O>",{5m:t.6.2P===j?n:"","I":t.6.2P===j?"h-5X":""});o.1h(u);o.w("h-1k",r===i?s:i);o.w("h-2r",n);t.1g.1h(o)}}t.2R()},2R:9(){b t=c;7(t.6.1r===d){q d}t.1g.11(".h-1k").2v(9(n,r){7(e(c).w("h-2r")===e(t.$M[t.m]).w("h-2r")){t.1g.11(".h-1k").R("25");e(c).H("25")}})},2U:9(){b e=c;7(e.6.2f===d){q d}7(e.6.2j===d){7(e.m===0&&e.E===0){e.1x.H("18");e.1y.H("18")}p 7(e.m===0&&e.E!==0){e.1x.H("18");e.1y.R("18")}p 7(e.m===e.E){e.1x.R("18");e.1y.H("18")}p 7(e.m!==0&&e.m!==e.E){e.1x.R("18");e.1y.R("18")}}},2W:9(){b e=c;e.3P();e.2U();7(e.C){7(e.6.v>=e.K){e.C.3N()}p{e.C.3M()}}},4Z:9(){b e=c;7(e.C){e.C.2Z()}},W:9(e){b t=c;7(t.23){q d}t.22=t.m;t.m+=t.6.21===j?t.6.v:1;7(t.m>t.E+(t.6.21==j?t.6.v-1:0)){7(t.6.2j===j){t.m=0;e="2b"}p{t.m=t.E;q d}}t.1j(t.m,e)},1f:9(e){b t=c;7(t.23){q d}t.22=t.m;7(t.6.21===j&&t.m>0&&t.m<t.6.v){t.m=0}p{t.m-=t.6.21===j?t.6.v:1}7(t.m<0){7(t.6.2j===j){t.m=t.E;e="2b"}p{t.m=0;q d}}t.1j(t.m,e)},1j:9(e,t,n){b r=c;7(r.23){q d}r.34();7(F r.6.1V==="9"){r.6.1V.S(c,[r.$k])}7(e>=r.E){e=r.E}p 7(e<=0){e=0}r.m=r.h.m=e;7(r.6.2l!==d&&n!=="3K"&&r.6.v===1&&r.D.1p===j){r.1A(0);7(r.D.1p===j){r.1o(r.G[e])}p{r.1d(r.G[e],1)}r.3J();r.2e();q d}b i=r.G[e];7(r.D.1p===j){r.1P=d;7(t===j){r.1A("1v");15(9(){r.1P=j},r.6.1v)}p 7(t==="2b"){r.1A(r.6.2g);15(9(){r.1P=j},r.6.2g)}p{r.1A("1l");15(9(){r.1P=j},r.6.1l)}r.1o(i)}p{7(t===j){r.1d(i,r.6.1v)}p 7(t==="2b"){r.1d(i,r.6.2g)}p{r.1d(i,r.6.1l)}}r.2e()},34:9(){b e=c;e.1i=e.h.1i=e.22===r?e.m:e.22;e.22=r},3e:9(e){b t=c;t.34();7(F t.6.1V==="9"){t.6.1V.S(c,[t.$k])}7(e>=t.E||e===-1){e=t.E}p 7(e<=0){e=0}t.1A(0);7(t.D.1p===j){t.1o(t.G[e])}p{t.1d(t.G[e],1)}t.m=t.h.m=e;t.2e()},2e:9(){b e=c;e.2R();e.2U();e.28();7(F e.6.3f==="9"){e.6.3f.S(c,[e.$k])}7(e.6.N!==d){e.35()}},U:9(){b e=c;e.3h="U";16(e.1z)},35:9(){b e=c;7(e.3h!=="U"){e.1a()}},1a:9(){b e=c;e.3h="1a";7(e.6.N===d){q d}16(e.1z);e.1z=4f(9(){e.W(j)},e.6.N)},1A:9(e){b t=c;7(e==="1l"){t.$J.A(t.2y(t.6.1l))}p 7(e==="1v"){t.$J.A(t.2y(t.6.1v))}p 7(F e!=="3r"){t.$J.A(t.2y(e))}},2y:9(e){b t=c;q{"-1H-1b":"2m "+e+"1u 2o","-1F-1b":"2m "+e+"1u 2o","-o-1b":"2m "+e+"1u 2o",1b:"2m "+e+"1u 2o"}},3F:9(){q{"-1H-1b":"","-1F-1b":"","-o-1b":"",1b:""}},3E:9(e){q{"-1H-O":"1m("+e+"T, B, B)","-1F-O":"1m("+e+"T, B, B)","-o-O":"1m("+e+"T, B, B)","-1u-O":"1m("+e+"T, B, B)",O:"1m("+e+"T, B,B)"}},1o:9(e){b t=c;t.$J.A(t.3E(e))},3D:9(e){b t=c;t.$J.A({Y:e})},1d:9(e,t){b n=c;n.2u=d;n.$J.U(j,j).4c({Y:e},{4W:t||n.6.1l,48:9(){n.2u=j}})},4I:9(){b e=c;b r="1m(B, B, B)",i=n.4Y("L");i.2n.3A="  -1F-O:"+r+"; -1u-O:"+r+"; -o-O:"+r+"; -1H-O:"+r+"; O:"+r;b s=/1m\\(B, B, B\\)/g,o=i.2n.3A.55(s),u=o!==1c&&o.14===1;b a="59"3l t||5b.5c;e.D={1p:u,12:a}},4w:9(){b e=c;7(e.6.1G!==d||e.6.1U!==d){e.3B();e.3C()}},4G:9(){b e=c;b t=["s","e","x"];e.13={};7(e.6.1G===j&&e.6.1U===j){t=["2D.h 1S.h","3o.h 3G.h","2d.h 3H.h 2c.h"]}p 7(e.6.1G===d&&e.6.1U===j){t=["2D.h","3o.h","2d.h 3H.h"]}p 7(e.6.1G===j&&e.6.1U===d){t=["1S.h","3G.h","2c.h"]}e.13["3I"]=t[0];e.13["38"]=t[1];e.13["36"]=t[2]},3C:9(){b t=c;t.$k.z("5B.h",9(e){e.1e()});t.$k.z("1S.3L",9(t){q e(t.19).27("5G, 4J, 4K, 4L")})},3B:9(){9 o(e){7(e.2X){q{x:e.2X[0].2O,y:e.2X[0].3Q}}p{7(e.2O!==r){q{x:e.2O,y:e.3Q}}p{q{x:e.50,y:e.53}}}}9 u(t){7(t==="z"){e(n).z(i.13["38"],f);e(n).z(i.13["36"],l)}p 7(t==="P"){e(n).P(i.13["38"]);e(n).P(i.13["36"])}}9 a(n){b n=n.3w||n||t.3v;7(n.5a===3){q d}7(i.2u===d&&!i.6.3q){q d}7(i.1P===d&&!i.6.3q){q d}7(i.6.N!==d){16(i.1z)}7(i.D.12!==j&&!i.$J.1J("3p")){i.$J.H("3p")}i.Z=0;i.X=0;e(c).A(i.3F());b r=e(c).2q();s.37=r.Y;s.2V=o(n).x-r.Y;s.2T=o(n).y-r.5q;u("z");s.2x=d;s.2M=n.19||n.44}9 f(r){b r=r.3w||r||t.3v;i.Z=o(r).x-s.2V;i.2G=o(r).y-s.2T;i.X=i.Z-s.37;7(F i.6.2C==="9"&&s.3y!==j&&i.X!==0){s.3y=j;i.6.2C.S(c)}7(i.X>8||i.X<-8&&i.D.12===j){r.1e?r.1e():r.5E=d;s.2x=j}7((i.2G>10||i.2G<-10)&&s.2x===d){e(n).P("3o.h")}b u=9(){q i.X/5};b a=9(){q i.1O+i.X/5};i.Z=24.3m(24.3V(i.Z,u()),a());7(i.D.1p===j){i.1o(i.Z)}p{i.3D(i.Z)}}9 l(n){b n=n.3w||n||t.3v;n.19=n.19||n.44;s.3y=d;7(i.D.12!==j){i.$J.R("3p")}7(i.X!==0){b r=i.4b();i.1j(r,d,"3K");7(s.2M===n.19&&i.D.12!==j){e(n.19).z("3i.4d",9(t){t.4M();t.4N();t.1e();e(n.19).P("3i.4d")});b o=e.4O(n.19,"4P")["3i"];b a=o.4Q();o.4R(0,0,a)}}u("P")}b i=c;b s={2V:0,2T:0,4S:0,37:0,2q:1c,4T:1c,4U:1c,2x:1c,4V:1c,2M:1c};i.2u=j;i.$k.z(i.13["3I"],".h-1n",a)},4b:9(){b e=c,t;b t=e.4e();7(t>e.E){e.m=e.E;t=e.E}p 7(e.Z>=0){t=0;e.m=0}q t},4e:9(){b t=c;b n=t.G;b r=t.Z;b i=1c;e.2v(n,9(e,s){7(r-t.Q/20>n[e+1]&&r-t.Q/20<s&&t.39()==="Y"){i=s;t.m=e}p 7(r+t.Q/20<s&&r+t.Q/20>n[e+1]&&t.39()==="4h"){i=n[e+1];t.m=e+1}});q t.m},39:9(){b e=c,t;7(e.X<0){t="4h";e.3k="W"}p{t="Y";e.3k="1f"}q t},4E:9(){b e=c;e.$k.z("h.W",9(){e.W()});e.$k.z("h.1f",9(){e.1f()});e.$k.z("h.1a",9(t,n){e.6.N=n;e.1a();e.32="1a"});e.$k.z("h.U",9(){e.U();e.32="U"});e.$k.z("h.1j",9(t,n){e.1j(n)});e.$k.z("h.3e",9(t,n){e.3e(n)})},2p:9(){b e=c;7(e.6.2p===j&&e.D.12!==j&&e.6.N!==d){e.$k.z("51",9(){e.U()});e.$k.z("52",9(){7(e.32!=="U"){e.1a()}})}},1Y:9(){b t=c;7(t.6.1Y===d){q d}1E(b n=0;n<t.K;n++){b i=e(t.$M[n]);7(i.w("h-17")==="17"){4k}b s=i.w("h-1W"),o=i.11(".54"),u;7(F o.w("26")!=="3r"){i.w("h-17","17");4k}7(i.w("h-17")===r){o.3N();i.H("4m").w("h-17","57")}7(t.6.4n===j){u=s>=t.m}p{u=j}7(u&&s<t.m+t.6.v&&o.14){t.4o(i,o)}}},4o:9(e,t){9 i(){r+=1;7(n.2B(t.2z(0))){s()}p 7(r<=2i){15(i,2i)}p{s()}}9 s(){e.w("h-17","17").R("4m");t.5d("w-26");n.6.4s==="4t"?t.5g(5h):t.3M()}b n=c,r=0;t[0].26=t.w("26");i()},1s:9(){9 s(){i+=1;7(t.2B(n.2z(0))){o()}p 7(i<=2i){15(s,2i)}p{t.1M.A("3g","")}}9 o(){b n=e(t.$M[t.m]).3g();t.1M.A("3g",n+"T");7(!t.1M.1J("1s")){15(9(){t.1M.H("1s")},0)}}b t=c;b n=e(t.$M[t.m]).11("5j");7(n.2z(0)!==r){b i=0;s()}p{o()}},2B:9(e){7(!e.48){q d}7(F e.4v!=="5l"&&e.4v==0){q d}q j},4j:9(){b t=c;7(t.6.3b===j){t.$M.R("25")}t.1t=[];1E(b n=t.m;n<t.m+t.6.v;n++){t.1t.3U(n);7(t.6.3b===j){e(t.$M[n]).H("25")}}t.h.1t=t.1t},4r:9(e){b t=c;t.4y="h-"+e+"-5p";t.4z="h-"+e+"-3l"},3J:9(){9 u(e,t){q{2q:"5r",Y:e+"T"}}b e=c;e.23=j;b t=e.4y,n=e.4z,r=e.$M.1C(e.m),i=e.$M.1C(e.1i),s=24.4B(e.G[e.m])+e.G[e.1i],o=24.4B(e.G[e.m])+e.Q/2;e.$J.H("h-1D").A({"-1H-O-1D":o+"T","-1F-4D-1D":o+"T","4D-1D":o+"T"});b a="5w 5x 5y 5z";i.A(u(s,10)).H(t).z(a,9(){e.2E=j;i.P(a);e.3x(i,t)});r.H(n).z(a,9(){e.3d=j;r.P(a);e.3x(r,n)})},3x:9(e,t){b n=c;e.A({2q:"",Y:""}).R(t);7(n.2E&&n.3d){n.$J.R("h-1D");n.2E=d;n.3d=d;n.23=d}},4u:9(){b e=c;e.h={2w:e.2w,5D:e.$k,V:e.$V,M:e.$M,m:e.m,1i:e.1i,1t:e.1t,12:e.D.12,D:e.D}},4H:9(){b r=c;r.$k.P(".h h 1S.3L");e(n).P(".h h");e(t).P("40",r.2S)},1R:9(){b e=c;7(e.$k.1N().14!==0){e.$J.3z();e.$V.3z().3z();7(e.C){e.C.2Z()}}e.4H();e.$k.2k("2n",e.$k.w("h-47")||"").2k("I",e.$k.w("h-46"))},5H:9(){b e=c;e.U();16(e.1L);e.1R();e.$k.5I()},5J:9(t){b n=c;b r=e.3R({},n.2w,t);n.1R();n.1B(r,n.$k)},5K:9(e,t){b n=c,i;7(!e){q d}7(n.$k.1N().14===0){n.$k.1h(e);n.1T();q d}n.1R();7(t===r||t===-1){i=-1}p{i=t}7(i>=n.$V.14||i===-1){n.$V.1C(-1).5L(e)}p{n.$V.1C(i).5M(e)}n.1T()},5N:9(e){b t=c,n;7(t.$k.1N().14===0){q d}7(e===r||e===-1){n=-1}p{n=e}t.1R();t.$V.1C(n).2Z();t.1T()}};e.3n.2t=9(t){q c.2v(9(){7(e(c).w("h-1B")===j){q d}e(c).w("h-1B",j);b n=3t.3s(i);n.1B(t,c);e.w(c,"2t",n)})};e.3n.2t.6={v:5,1w:[5O,4],1K:[5P,3],1X:[5Q,2],1Q:d,1Z:[5R,1],45:d,42:d,1l:3j,1v:5S,2g:5T,N:d,2p:d,2f:d,2A:["1f","W"],2j:j,21:d,1r:j,2P:d,2F:j,41:3j,43:t,1I:"h-5U",2h:"h-2h",1Y:d,4n:j,4s:"4t",1s:d,2Q:d,3a:d,3q:j,1G:j,1U:j,3b:d,2l:d,31:d,33:d,3u:d,2L:d,1V:d,3f:d,2Y:d,2C:d}})(5V,5W,5i)',62,370,'||||||options|if||function||var|this|false||||owl||true|elem||currentItem|||else|return|||||items|data|||on|css|0px|owlControls|browser|maximumItem|typeof|positionsInArray|addClass|class|owlWrapper|itemsAmount|div|owlItems|autoPlay|transform|off|itemWidth|removeClass|apply|px|stop|userItems|next|newRelativeX|left|newPosX||find|isTouch|ev_types|length|setTimeout|clearInterval|loaded|disabled|target|play|transition|null|css2slide|preventDefault|prev|paginationWrapper|append|prevItem|goTo|page|slideSpeed|translate3d|wrapper|transition3d|support3d|width|pagination|autoHeight|visibleItems|ms|paginationSpeed|itemsDesktop|buttonPrev|buttonNext|autoPlayInterval|swapSpeed|init|eq|origin|for|moz|mouseDrag|webkit|baseClass|hasClass|itemsDesktopSmall|checkVisible|wrapperOuter|children|maximumPixels|isCss3Finish|itemsTabletSmall|unWrap|mousedown|setVars|touchDrag|beforeMove|item|itemsTablet|lazyLoad|itemsMobile||scrollPerPage|storePrevItem|isTransition|Math|active|src|is|eachMoveUpdate|opacity|orignalItems|rewind|mouseup|touchend|afterGo|navigation|rewindSpeed|theme|100|rewindNav|attr|transitionStyle|all|style|ease|stopOnHover|position|roundPages|html|owlCarousel|isCssFinish|each|userOptions|sliding|addCssSpeed|get|navigationText|completeImg|startDragging|touchstart|endPrev|responsive|newPosY|visible|watchVisibility|logIn|updateItems|afterInit|targetElement|calculateAll|pageX|paginationNumbers|jsonPath|checkPagination|resizer|offsetY|checkNavigation|offsetX|updateControls|touches|afterAction|remove|updateVars|beforeUpdate|hoverStatus|afterUpdate|getPrevItem|checkAp|end|relativePos|move|moveDirection|jsonSuccess|addClassActive|Number|endCurrent|jumpTo|afterMove|height|apStatus|click|200|playDirection|in|max|fn|touchmove|grabbing|dragBeforeAnimFinish|string|create|Object|beforeInit|event|originalEvent|clearTransStyle|dragging|unwrap|cssText|gestures|disabledEvents|css2move|doTranslate|removeTransition|mousemove|touchcancel|start|singleItemTransition|drag|disableTextSelect|show|hide|span|updatePagination|pageY|extend|buildButtons|buildPagination|push|min|loops|calculateWidth|appendWrapperSizes|appendItemsSizes|resize|responsiveRefreshRate|itemsScaleUp|responsiveBaseWidth|srcElement|singleItem|originalClasses|originalStyles|complete|outer|wrap|getNewPosition|animate|disable|improveClosest|setInterval|reload|right|updatePosition|onVisibleItems|continue|loadContent|loading|lazyFollow|lazyPreload|block|display|transitionTypes|lazyEffect|fade|owlStatus|naturalWidth|moveEvents|response|outClass|inClass|buildControls|abs|onStartup|perspective|customEvents|wrapItems|eventTypes|clearEvents|checkBrowser|textarea|select|option|stopImmediatePropagation|stopPropagation|_data|events|pop|splice|baseElWidth|minSwipe|maxSwipe|dargging|duration|prototype|createElement|destroyControls|clientX|mouseover|mouseout|clientY|lazyOwl|match|appendTo|checked|clearTimeout|ontouchstart|which|navigator|msMaxTouchPoints|removeAttr|wrapperWidth|buttons|fadeIn|400|document|img|round|undefined|text|500|wrapAll|out|top|relative|getJSON|5e3|new|onstartup|webkitAnimationEnd|oAnimationEnd|MSAnimationEnd|animationend|controls|dragstart|toggleClass|baseElement|returnValue|clickable|input|destroy|removeData|reinit|addItem|after|before|removeItem|1199|979|768|479|800|1e3|carousel|jQuery|window|numbers'.split('|'),0,{}));
(function(d,h){"function"===typeof define&&define.amd?define(h):"object"===typeof exports?module.exports=h():d.Blazy=h()})(this,function(){function d(b){if(!document.querySelectorAll){var g=document.createStyleSheet();document.querySelectorAll=function(b,a,e,d,f){f=document.all;a=[];b=b.replace(/\[for\b/gi,"[htmlFor").split(",");for(e=b.length;e--;){g.addRule(b[e],"k:v");for(d=f.length;d--;)f[d].currentStyle.k&&a.push(f[d]);g.removeRule(0)}return a}}m=!0;k=[];e={};a=b||{};a.error=a.error||!1;a.offset=a.offset||100;a.success=a.success||!1;a.selector=a.selector||".b-lazy";a.separator=a.separator||"|";a.container=a.container?document.querySelectorAll(a.container):!1;a.errorClass=a.errorClass||"b-error";a.breakpoints=a.breakpoints||!1;a.successClass=a.successClass||"b-loaded";a.src=r=a.src||"data-src";u=1<window.devicePixelRatio;e.top=0-a.offset;e.left=0-a.offset;f=v(w,25);t=v(x,50);x();n(a.breakpoints,function(b){if(b.width>=window.screen.width)return r=b.src,!1});h()}function h(){y(a.selector);m&&(m=!1,a.container&&n(a.container,function(b){p(b,"scroll",f)}),p(window,"resize",t),p(window,"resize",f),p(window,"scroll",f));w()}function w(){for(var b=0;b<l;b++){var g=k[b],c=g.getBoundingClientRect();if(c.right>=e.left&&c.bottom>=e.top&&c.left<=e.right&&c.top<=e.bottom||-1!==(" "+g.className+" ").indexOf(" "+a.successClass+" "))d.prototype.load(g),k.splice(b,1),l--,b--}0===l&&d.prototype.destroy()}function z(b,g){if(g||0<b.offsetWidth&&0<b.offsetHeight){var c=b.getAttribute(r)||b.getAttribute(a.src);if(c){var c=c.split(a.separator),d=c[u&&1<c.length?1:0],c=new Image;n(a.breakpoints,function(a){b.removeAttribute(a.src)});b.removeAttribute(a.src);c.onerror=function(){a.error&&a.error(b,"invalid");b.className=b.className+" "+a.errorClass};c.onload=function(){"img"===b.nodeName.toLowerCase()?b.src=d:b.style.backgroundImage='url("'+d+'")';b.className=b.className+" "+a.successClass;a.success&&a.success(b)};c.src=d}else a.error&&a.error(b,"missing"),b.className=b.className+" "+a.errorClass}}function y(b){b=document.querySelectorAll(b);for(var a=l=b.length;a--;k.unshift(b[a]));}function x(){e.bottom=(window.innerHeight||document.documentElement.clientHeight)+a.offset;e.right=(window.innerWidth||document.documentElement.clientWidth)+a.offset}function p(b,a,c){b.attachEvent?b.attachEvent&&b.attachEvent("on"+a,c):b.addEventListener(a,c,!1)}function q(b,a,c){b.detachEvent?b.detachEvent&&b.detachEvent("on"+a,c):b.removeEventListener(a,c,!1)}function n(a,d){if(a&&d)for(var c=a.length,e=0;e<c&&!1!==d(a[e],e);e++);}function v(a,d){var c=0;return function(){var e=+new Date;e-c<d||(c=e,a.apply(k,arguments))}}var r,a,e,k,l,u,m,f,t;d.prototype.revalidate=function(){h()};d.prototype.load=function(b,d){-1===(" "+b.className+" ").indexOf(" "+a.successClass+" ")&&z(b,d)};d.prototype.destroy=function(){a.container&&n(a.container,function(a){q(a,"scroll",f)});q(window,"scroll",f);q(window,"resize",f);q(window,"resize",t);l=0;k.length=0;m=!0};return d});