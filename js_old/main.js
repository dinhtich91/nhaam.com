$(document).ready(function() {

// if ($('#list-order').length > 0) {
//     $('#list-order').niceScroll({
//         cursorborder: 'none',
//         cursorcolor: '#000'
//     });
// }

    /*$(document)[0].oncontextmenu = function() {
     return false;
     }
     $(document).mousedown(function(e) {
     if (e.button == 2) {
     alert('Copyright gauconflower.com');
     return false;
     } else {
     return true;
     }
     });
     document.onkeypress = function(event) {
     event = (event || window.event);
     if (event.keyCode == 123) {
     return false;
     }
     }
     document.onmousedown = function(event) {
     event = (event || window.event);
     if (event.keyCode == 123) {
     return false;
     }
     }
     document.onkeydown = function(event) {
     event = (event || window.event);
     if (event.keyCode == 123) {
     return false;
     }
     }*/
    $('.main-nav .search a').click(function() {
        if (!$(this).parent('.search').hasClass('active')) {
            $(this).parent('.search').addClass('active');
        } else {
            $(this).parent('.search').removeClass('active');
        }
        if ($('.main-nav .search form').is(':hidden')) {
            $('.main-nav .search form').show();
        } else if ($('.main-nav .search form').is(':visible')) {
            $('.main-nav .search form').hide();
        }
    });
    $('.button-nav').click(function() {
        if (!$(this).hasClass('active')) {
            $(this).addClass('active');
            $('.button-nav a img').attr('src', 'img/icon-menu-ac.png');
            $('.main-nav ul').stop().slideDown();
        } else {
            $(this).removeClass('active');
            $('.button-nav a img').attr('src', 'img/icon-menu-noac.png');
            $('.main-nav ul').stop().slideUp();
        }
    });
    var toTop = $("#toTopHover");
    function clickToTop() {
        toTop.on("click", function() {
            $('html,body').animate({
                scrollTop: 0
            }, 1500);
        });
        return false;
    }
    clickToTop();
    $(window).scroll(function() {
        if ($(window).scrollTop() > 500) {
            toTop.css("opacity", 1);
        } else {
            toTop.css("opacity", 0);
        }
    });
    /* NAVIGATION */
    $('.navigation ul li:not(:last) a').append("<i class='fa fa-angle-right'></i>");
    /* CHI TIẾT SẢN PHẨM */
    $('.product-info-in').children('div').not(':first').hide();
    $('.product-info-tab li').click(function() {
        $('.product-info-tab li').removeClass('active');
        $(this).addClass('active');
        $('.product-info-in').children('div').hide();
        $($(this).children('a').attr('href')).fadeIn();
        return false;
    });
    $('.album').not(':first').hide();
    $('.album-tab li').click(function() {
        $('.album-tab li').removeClass('active');
        $(this).addClass('active');
        $('.album').hide();
        $($(this).children('a').attr('href')).fadeIn();
        return false;
    });
});
function checkOrder() {
    var email = $(".form-ktdh #email").val();
    $.ajax({
        type: "POST",
        url: "checkorder",
        data: {email: email},
        cache: true,
        beforeSend: function() {
            $("#list-order").html("<center><img src='img/loading.gif'/></center>");
        },
        success: function(b) {
            $("#list-order").html(b);
        }
    });
}

function forgetPassword() {
    var email = $(".form-quenmk #email").val();
    $.ajax({
        type: "POST",
        url: "forgetpassword",
        data: {email: email},
        cache: true,
        beforeSend: function() {
            $("#responde").html("<center><img src='img/loading.gif'/> Đang xử lý, vui lòng chờ</center>");
        },
        success: function(b) {
            $("#responde").html(b);
        }
    });
}
