/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $(".search_field").click(function () {
        $(".search_field").val("");
    })
    $("#main-banner").owlCarousel({
        autoplay: true,
        loop: true,
        dots: true,
        nav: true,
        items: 1,
        navText: ['<i class="icon-moon-arrow-2"></i>', '<i class="icon-moon-arrow"></i>'],
    });
    $(".second-banner").owlCarousel({
        autoplay: true,
        loop: true,
        nav: true,
        items: 6,
        navText: ['<i class="icon-moon-arrow-2"></i>', '<i class="icon-moon-arrow"></i>'],
        slideSpeed: 100,
    });
    $(".third-banner").owlCarousel({
        autoplay: true,
        loop: true,
        nav: true,
        items: 4,
        navText: ['<i class="icon-moon-arrow-2"></i>', '<i class="icon-moon-arrow"></i>'],
        slideSpeed: 100,
    });
    $(".foruth-banner").owlCarousel({
        autoplay: true,
        loop: true,
        nav: true,
        items: 4,
        navText: ['<i class="icon-moon-arrow-2"></i>', '<i class="icon-moon-arrow"></i>'],
        slideSpeed: 100,
    });
    $(".fith-banner").owlCarousel({
        autoplay: true,
        loop: true,
        nav: true,
        items: 2,
        navText: ['<i class="icon-moon-arrow-2"></i>', '<i class="icon-moon-arrow"></i>'],
        slideSpeed: 100,
    });
    $(".sixth-banner").owlCarousel({
        autoplay: true,
        loop: true,
        nav: true,
        items: 2,
        navText: ['<i class="icon-moon-arrow-2"></i>', '<i class="icon-moon-arrow"></i>'],
        slideSpeed: 100,
    });
    $(".ty-menu__item ").click(function(){
        if($(".ty-menu__items ").find("li").hasClass("fkme-active")){
            $(".ty-menu__items").find("li").removeClass("fkme-active");
            $(this).addClass("fkme-active");
        }
    })
    $(".product-owl").owlCarousel({
        autoplay: true,
        loop: true,
        nav: true,
        items: 6,
        navText: ['<i class="icon-moon-arrow-2"></i>', '<i class="icon-moon-arrow"></i>'],
        lazyLoad: true,
        slideSpeed: 400,
        autoplayHoverPause: true,
    });
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    window._fbq = window._fbq || [];
    window._fbq.push(['track', 'PixelInitialized', {}]);
    (function (_, $) {
        $.ceEvent('on', 'ce.commoninit', function (context) {
            var elm = context.find('#scroll_list_50');
            if (elm.length) {
                elm.owlCarousel({
                    items: 6,
                    scrollPerPage: true,
                    autoPlay: '3000',
                    slideSpeed: 400,
                    stopOnHover: true,
                    navigation: true,
                    navigationText: ['<i class="icon-moon-arrow-2"></i>', '<i class="icon-moon-arrow"></i>'],
                    pagination: false,
                    lazyLoad: true
                });
            }
        });
    }(Tygh, Tygh.$));
    (function (_, $) {
        $.ceEvent('on', 'ce.commoninit', function (context) {

        });

    }(Tygh, Tygh.$));


    (function (_, $) {
        _.tr({
            addons_tags_add_a_tag: 'add a tag'
        });
    }(Tygh, Tygh.$));

    $(".main-img").elevateZoom({
        gallery: 'gallery-zoom',
        cursor: 'pointer',
        imageCrossfade: true,
        zoomEnabled: false,
        galleryActiveClass: 'active-image',
    });

    $(".ty-menu__item").click(function () {
        if ($(".ty-menu__items").find("li").hasClass('ty-menu__item-active')) {
            $(".ty-menu__items").find("li").removeClass('ty-menu__item-active');
            $(this).addClass("ty-menu__item-active");
        }
        else {
            $(this).addClass("ty-menu__item-active");
        }
    })
})
