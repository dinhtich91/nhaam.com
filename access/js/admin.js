var admin = {
    addPhoto: function(url) {
        $.ajax({
            type: "GET",
            url: PATH_FOLDER_ADMIN + "/common/addPhoto",
            cache: false,
            beforeSend: function() {
                jQuery("#addPhoto").html('<img src="access/image/loading.gif"/>')
            },
            success: function(b) {
                $('#addPhoto').before(b);
                jQuery("#addPhoto").html("");
            }
        });
    },
    addAlbum: function() {
        $.ajax({
            type: "GET",
            url: PATH_FOLDER_ADMIN + "/common/addAlbum",
            cache: false,
            beforeSend: function() {
                jQuery("#addPhoto").html('<img src="access/image/loading.gif"/>')
            },
            success: function(b) {
                $('#addPhoto').before(b);
                jQuery("#addPhoto").html("");
            }
        });
    },
    addDanDung: function() {
        $.ajax({
            type: "GET",
            url: PATH_FOLDER_ADMIN + "/common/addDanDung",
            cache: false,
            beforeSend: function() {
                jQuery("#addPhoto").html('<img src="access/image/loading.gif"/>')
            },
            success: function(b) {
                $('#addPhoto').before(b);
                jQuery("#addPhoto").html("");
            }
        });
    },
    getImgThumb: function(id_product) {
        $.ajax({
            type: "GET",
            url: PATH_FOLDER_ADMIN + "/products/getImgThumb/" + id_product,
            cache: false,
            beforeSend: function() {
                jQuery("#addPhoto").html('<img src="access/image/loading.gif"/>');
            },
            success: function(b) {
                $('#addPhoto').before(b);
                jQuery("#addPhoto").html("");
            }, error: function() {
                alert("Có lỗi xảy ra. Không load được dữ liệu !");
            }
        });
    },
    ajaxThuongHieu: function(id_cat, id) {
        $.ajax({
            type: "GET",
            url: PATH_FOLDER_ADMIN + "/products/ajaxThuongHieu/" + id_cat + "/" + id,
            cache: false,
            beforeSend: function() {
                jQuery("#loading_trademark").html('<img src="access/image/loading.gif"/>');
            },
            success: function(b) {
                jQuery("#trademark").html(b);
                jQuery("#loading_trademark").html("");
            }, error: function() {
                alert("Có lỗi xảy ra. Không load được dữ liệu !");
            }
        });
    },
    ajax_VolumePrice: function(id_product) {
        $.ajax({
            type: "GET",
            url: PATH_FOLDER_ADMIN + "/products/ajax_VolumePrice/" + id_product,
            cache: false,
            beforeSend: function() {
                jQuery(".addVP").html('<img src="access/image/loading.gif"/>');
            },
            success: function(b) {
                $('.addVP').before(b);
                jQuery(".addVP").html("");
            }, error: function() {
                alert("Có lỗi xảy ra. Không load được dữ liệu !");
            }
        });
    },
    removePhoto: function(id) {
        var img = $("#imageHidden_" + id).val();
        $.ajax({
            type: "POST",
            url: PATH_FOLDER_ADMIN + "/common/delPhoto",
            data: {img: img},
            cache: false,
            beforeSend: function() {
//jQuery("#addPhoto").html('<b style="color:#fff;">Loading...<img src="access/image/ajax-loader.gif" alt="Đang tải dữ liệu ..." /></b>')
            },
            success: function(b) {
                jQuery("#" + id).hide("600");
                jQuery("#" + id).remove();
            }
        });
    },
    removeAblums: function(id) {
        var img = $("#imageHidden_" + id).val();
        jQuery("#" + id).hide("600");
        jQuery("#" + id).remove();
    },
    uploadPhoto: function(cache) {
        /**
         *@description: Sử dụng cho nút upload
         *@memberOf : Huỳnh Văn Được - 20130103
         **/
        var btnUpload = $('#upload_' + cache);
        var status = $('#status_' + cache);
        new AjaxUpload(btnUpload, {
            action: PATH_FOLDER_ADMIN + '/common/uploadPhoto',
            name: 'uploadfile',
            onSubmit: function(file, ext) {
                if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
// extension is not allowed 
//status.text('Only JPG, PNG or GIF files are allowed');
                    alert("Only JPG, PNG or GIF files are allowed");
                    return false;
                }
                status.html('<img src="' + IMG_LOADING + '"/>');
            },
            onComplete: function(file, response) {
//On completion clear the status
                status.text('');
                //Add uploaded file to list
                if (response != "error") {
//alert(response);            
                    $("#upload_" + cache + " img").attr("src", response);
                    $("#imageHidden_" + cache).val(response);
                    //var files = $("#files").val();
                    //files += response + ",";
                    //$("#files").val(files);
                    //$('<li></li>').appendTo('#list_files').html(file).addClass('success');
                } else {
//$('<li></li>').appendTo('#list_files').text(file).addClass('error');
                    alert("Fail Upload !");
                }
            }

        });
    },
    uploadPhotoThumb: function(cache) {
        /**
         *@description: Sử dụng cho nút upload
         *@memberOf : Huỳnh Văn Được - 20130103
         **/
        var btnUpload = $('#upload_' + cache);
        var status = $('#status_' + cache);
        new AjaxUpload(btnUpload, {
            action: PATH_FOLDER_ADMIN + '/common/uploadPhotoThumb',
            name: 'uploadfile',
            onSubmit: function(file, ext) {
                if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
// extension is not allowed 
//status.text('Only JPG, PNG or GIF files are allowed');
                    alert("Only JPG, PNG or GIF files are allowed");
                    return false;
                }
                status.html('<img src="' + IMG_LOADING + '"/>');
            },
            onComplete: function(file, response) {
//On completion clear the status
                status.text('');
                //Add uploaded file to list
                if (response != "error") {
//alert(response);            
                    $("#upload_" + cache + " img").attr("src", response);
                    $("#imageHidden_" + cache).val(response);
                    //var files = $("#files").val();
                    //files += response + ",";
                    //$("#files").val(files);
                    //$('<li></li>').appendTo('#list_files').html(file).addClass('success');
                } else {
//$('<li></li>').appendTo('#list_files').text(file).addClass('error');
                    alert("Fail Upload !");
                }
            }

        });
    },
    uploadPhotoAlbum: function(cache) {
        /**
         *@description: Sử dụng cho nút upload
         *@memberOf : Huỳnh Văn Được - 20130103
         **/
        var btnUpload = $('#upload_' + cache);
        var status = $('#status_' + cache);
        new AjaxUpload(btnUpload, {
            action: PATH_FOLDER_ADMIN + '/common/uploadPhotoAlbum',
            name: 'uploadfile',
            onSubmit: function(file, ext) {
                if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
// extension is not allowed 
//status.text('Only JPG, PNG or GIF files are allowed');
                    alert("Only JPG, PNG or GIF files are allowed");
                    return false;
                }
                status.html('<img src="' + IMG_LOADING + '"/>');
            },
            onComplete: function(file, response) {
//On completion clear the status
                status.text('');
                //Add uploaded file to list
                if (response != "error") {
//alert(response);            
                    $("#upload_" + cache + " img").attr("src", response);
                    $("#imageHidden_" + cache).val(response);
                    //var files = $("#files").val();
                    //files += response + ",";
                    //$("#files").val(files);
                    //$('<li></li>').appendTo('#list_files').html(file).addClass('success');
                } else {
//$('<li></li>').appendTo('#list_files').text(file).addClass('error');
                    alert("Fail Upload !");
                }
            }

        });
    },
    uploadLogo: function() {
        /**
         *@description: Sử dụng cho nút upload
         *@memberOf : Huỳnh Văn Được - 20130103
         **/
        var btnUpload = $('#uploadLogo');
        var status = $('#loadAjax');
        new AjaxUpload(btnUpload, {
            action: PATH_FOLDER_ADMIN + '/common/uploadLogo',
            name: 'uploadfile',
            onSubmit: function(file, ext) {
                if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
// extension is not allowed 
//status.text('Only JPG, PNG or GIF files are allowed');
                    alert("Only JPG, PNG or GIF files are allowed");
                    return false;
                }
                status.html('<img src="' + IMG_LOADING + '"/>');
            },
            onComplete: function(file, response) {
//On completion clear the status
                status.text('');
                //Add uploaded file to list
                if (response != "error") {
                    $("#uploadLogo img").attr("src", response);
                    $("#uploadLogo img").attr("width", 100);
                    $("#hiddenLogo").val(response);
                } else {
                    alert("Fail Upload !");
                }
            }

        });
    }, // Upload avatar
    uploadAvatarAlbums: function() {
        /**
         *@description: Sử dụng cho nút upload
         *@memberOf : Huỳnh Văn Được - 20130103
         **/
        var btnUpload = $('#uploadLogo');
        var status = $('#loadAjax');
        new AjaxUpload(btnUpload, {
            action: PATH_FOLDER_ADMIN + '/common/uploadAvatarAlbums',
            name: 'uploadfile',
            onSubmit: function(file, ext) {
                if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
// extension is not allowed 
//status.text('Only JPG, PNG or GIF files are allowed');
                    alert("Only JPG, PNG or GIF files are allowed");
                    return false;
                }
                status.html('<img src="' + IMG_LOADING + '"/>');
            },
            onComplete: function(file, response) {
//On completion clear the status
                status.text('');
                //Add uploaded file to list
                if (response != "error") {
                    $("#uploadLogo img").attr("src", response);
                    $("#uploadLogo img").attr("width", 100);
                    $("#hiddenLogo").val(response);
                } else {
                    alert("Fail Upload !");
                }
            }

        });
    }, // Upload avatar
    uploadAvatar: function() {
        /**
         *@description: Sử dụng cho nút upload
         *@memberOf : Huỳnh Văn Được - 20130103
         **/
        var btnUpload = $('#uploadAvatar');
        var status = $('#loadAjax');
        new AjaxUpload(btnUpload, {
            action: PATH_FOLDER_ADMIN + '/common/uploadAvatar',
            name: 'uploadfile',
            onSubmit: function(file, ext) {
                if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
// extension is not allowed 
//status.text('Only JPG, PNG or GIF files are allowed');
                    alert("Only JPG, PNG or GIF files are allowed");
                    return false;
                }
                status.html('<img src="' + IMG_LOADING + '"/>');
            },
            onComplete: function(file, response) {
//On completion clear the status
                status.text('');
                //Add uploaded file to list
                if (response != "error") {
                    $("#uploadAvatar img").attr("src", response);
                    $("#uploadAvatar img").attr("width", 100);
                    $("#hiddenAvatar").val(response);
                } else {
                    alert("Fail Upload !");
                }
            }

        });
    },
    uploadAvatarEvent: function() {
        /**
         *@description: Sử dụng cho nút upload
         *@memberOf : Huỳnh Văn Được - 20130103
         **/
        var btnUpload = $('#uploadAvatar');
        var status = $('#loadAjax');
        new AjaxUpload(btnUpload, {
            action: PATH_FOLDER_ADMIN + '/common/uploadAvatarEvent',
            name: 'uploadfile',
            onSubmit: function(file, ext) {
                if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
// extension is not allowed 
//status.text('Only JPG, PNG or GIF files are allowed');
                    alert("Only JPG, PNG or GIF files are allowed");
                    return false;
                }
                status.html('<img src="' + IMG_LOADING + '"/>');
            },
            onComplete: function(file, response) {
//On completion clear the status
                status.text('');
                //Add uploaded file to list
                if (response != "error") {
                    $("#uploadAvatar img").attr("src", response);
                    $("#uploadAvatar img").attr("width", 100);
                    $("#hiddenAvatar").val(response);
                } else {
                    alert("Fail Upload !");
                }
            }

        });
    },
    uploadAvatarResource: function() {
        /**
         *@description: Sử dụng cho nút upload
         *@memberOf : Huỳnh Văn Được - 20130103
         **/
        var btnUpload = $('#uploadAvatar');
        var status = $('#loadAjax');
        new AjaxUpload(btnUpload, {
            action: PATH_FOLDER_ADMIN + '/common/uploadAvatarResource',
            name: 'uploadfile',
            onSubmit: function(file, ext) {
                if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
// extension is not allowed 
//status.text('Only JPG, PNG or GIF files are allowed');
                    alert("Only JPG, PNG or GIF files are allowed");
                    return false;
                }
                status.html('<img src="' + IMG_LOADING + '"/>');
            },
            onComplete: function(file, response) {
//On completion clear the status
                status.text('');
                //Add uploaded file to list
                if (response != "error") {
                    $("#uploadAvatar img").attr("src", response);
                    $("#uploadAvatar img").attr("width", 100);
                    $("#hiddenAvatar").val(response);
                } else {
                    alert("Fail Upload !");
                }
            }

        });
    },
    uploadNews: function() {
        /**
         *@description: Sử dụng cho nút upload
         *@memberOf : Huỳnh Văn Được - 20130103
         **/
        var btnUpload = $('#uploadNews');
        var status = $('#loadAjax');
        new AjaxUpload(btnUpload, {
            action: PATH_FOLDER_ADMIN + '/common/uploadNews',
            name: 'uploadfile',
            onSubmit: function(file, ext) {
                if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
// extension is not allowed 
//status.text('Only JPG, PNG or GIF files are allowed');
                    alert("Only JPG, PNG or GIF files are allowed");
                    return false;
                }
                status.html('<img src="' + IMG_LOADING + '"/>');
            },
            onComplete: function(file, response) {
//On completion clear the status
                status.text('');
                //Add uploaded file to list
                if (response != "error") {
                    $("#uploadNews img").attr("src", response);
                    $("#uploadNews img").attr("width", 100);
                    $("#hiddenNews").val(response);
                } else {
                    alert("Fail Upload !");
                }
            }

        });
    }, // Upload avatar
    uploadCaseStudies: function() {
        /**
         *@description: Sử dụng cho nút upload
         *@memberOf : Huỳnh Văn Được - 20130103
         **/
        var btnUpload = $('#uploadNews');
        var status = $('#loadAjax');
        new AjaxUpload(btnUpload, {
            action: PATH_FOLDER_ADMIN + '/common/uploadCaseStudies',
            name: 'uploadfile',
            onSubmit: function(file, ext) {
                if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
// extension is not allowed 
//status.text('Only JPG, PNG or GIF files are allowed');
                    alert("Only JPG, PNG or GIF files are allowed");
                    return false;
                }
                status.html('<img src="' + IMG_LOADING + '"/>');
            },
            onComplete: function(file, response) {
//On completion clear the status
                status.text('');
                //Add uploaded file to list
                if (response != "error") {
                    $("#uploadNews img").attr("src", response);
                    $("#uploadNews img").attr("width", 100);
                    $("#hiddenNews").val(response);
                } else {
                    alert("Fail Upload !");
                }
            }

        });
    }, // Upload avatar
};