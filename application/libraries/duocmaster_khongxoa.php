<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CI_Duocmaster {

    /**
     * @todo   : tạo thư mục theo URL
     * @param  : Chuổi truyền URL
     * @author : Huỳnh Văn Được - 20121106
     */
    public function createFolder($path) {
        if (!is_dir($path)) {
            mkdir($path);
            chmod($path, 0777);
        }
    }
    
    /**
     * @todo   : Hủy tất cả các thẻ a trong chuỗi truyền vào
     * @param  : Chuổi truyền vào
     * @uses   : $this->duocomaster->removeTag_a($string);
     * @return : Trả về chuổi không chứa thẻ a
     * @author : Huỳnh Văn Được - 20121106
     */
    public function removeTag_a($string) {
        $string = preg_replace("/<a[^>]+\>/i", "", $string);
        return $string;
    }

    /**
     * @todo   : Hủy tất cả các thẻ img trong chuỗi truyền vào
     * @param  : Chuổi truyền vào
     * @return : Trả về chuổi không chứa thẻ img
     * @uses   : $this->duocmaster->removeTag_img($string);     
     * @author : Huỳnh Văn Được - 20121106
     */
    public function removeTag_img($string) {
        $string = preg_replace("/<img[^>]+\>/i", "", $string);
        return $string;
    }

    /**
     * @todo   : Kiểm tra đường dẫn của URL xem có hợp lệ không (chứa http: || https hay không)
     * @param  : Truyền vào URL
     * @return : True nếu URL hợp lệ, ngược lại trả về FALSE
     * @uses   : $this->duocmaster->isValidURL('http://yume.vn');
     * @author :  Huỳnh Văn Được - 20121106
     */
    public function isValidURL($url) {
        return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
    }

    /**
     * @todo   : Kiểm tra email có hợp lệ không
     * @param  : Truyền vào email
     * @return : True nếu email hợp lệ, ngược lại trả về FALSE
     * @uses   : $this->duocmaster->isValidEmail('duoc.huynh@dpassion.com');
     * @author : Huỳnh Văn Được - 20121106
     */
    public function isValidEmail($email) {
        return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email)) ? FALSE : TRUE;
    }

    /**
     * @todo   : Download hình từ server xuống thư mục
     * @param  : Truyền vào đường dẫn hình trên server và đường dẫn hình cần lưu     
     * @uses   : $this->duocmaster->saveImage('http://domain.com/i.jpg','upload/1.jpg');
     * @author : Huỳnh Văn Được - 20121106
     */
    public function saveImage($inPath, $outPath) {
        $in = fopen($inPath, "rb");
        $out = fopen($outPath, "wb");
        while ($chunk = fread($in, 8192)) {
            fwrite($out, $chunk, 8192);
        }
        fclose($in);
        fclose($out);
    }

    /**
     * @todo   : Cắt chuỗi theo độ dài nhật định
     * @param  : Truyền vào chuỗi cần cắt, số kí tự cần lấy
     * @return : Số kí tự của chuỗi truyền vào, nếu dài hơn số kí tự cần lấy sẽ thay bằng dấu ...
     * @uses   : $this->duocmaster->cut_string('duocmaster',1);
     * @author : Huỳnh Văn Được - 20121106
     */
    public function cut_string($string, $max_length) {
        if ($string && $max_length) {
            if (strlen($string) > $max_length) {
                $string = substr($string, 0, $max_length);
                $pos = strrpos($string, " ");
                if ($pos === false) {
                    return substr($string, 0, $max_length) . "...";
                }
                return substr($string, 0, $pos) . "...";
            } else {
                return $string;
            }
        }
    }

    /**
     * Upload và resize hình sử dụng Browse
     * @param : Truyền đường dẫn hình, width resize, height resize
     * @return : Trả về đường dẫn hình, nếu xãy ra lỗi trả về FALSE
     * @uses : <input type="file" name="userfile" size="20" />
     * @author : Huỳnh Văn Được - 20121127
     */
    public function uploadResize($path, $width=200, $height=200) {
        $CI = &get_instance();    
        $CI->load->library('image_lib');
        $config['upload_path']   = $path;
        $config['allowed_types'] = 'gif|jpg|png'; // Định dạng file
        $config['max_size']      = '400';         // Kích thước lớn nhất của hình
        $config['max_width']     = '1024';        
        $config['max_height']    = '800';
        $CI->load->library('upload', $config);
        if (!$CI->upload->do_upload()) {
            //$error = array('error' => $this->upload->display_errors());
            return FALSE;
        } else {
            //$data = array('upload_data' => $this->upload->data());
            $config['image_library']  = 'gd2';
            $config['source_image']   = $CI->upload->upload_path . $CI->upload->file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width']          = $width;
            $config['height']         = $height;
            $config['remove_spaces']  = TRUE;
            $config['quality'] = '90';
            //$CI->load->library('image_lib', $config);
            //$CI->image_lib->fit(); // fit,resize,crop    
            $CI->image_lib->clear(); // Phải có dòng này upload nhiều hình sẽ chạy ổn định
            $CI->image_lib->initialize($config);            
            if (!$CI->image_lib->fit())
                return FALSE;
            else
                return $config['upload_path'] . $CI->upload->file_name;
        }
    }
    /**
     * resise hình, thư mục resize nằm trong thư mục upload/resize
     * @param : Truyền vào đường dẫn hình, width, hright
     * @return :Trả về đường dẫn hình nếu thành công, ngược lại trả về FALSE;
     */
    public function resize($path, $width=200, $height=200){        
        $CI = &get_instance();
        $ROOT_UPLOAD = FOLDER_UPLOAD;        
        $ROOT_RESIZE = FOLDER_RESIZE;          
        $CI->load->library('image_moo');        
        $CI->load->helper('string');  
        $path             = trim_slashes($path);   
        $resize_path      = FOLDER_UPLOAD."/".FOLDER_RESIZE."/".trim_slashes($path);   
        $real_path        = FOLDER_UPLOAD."/".trim_slashes($path);
        $array       = explode("/",$resize_path);
        $n           = count($array);
        $folder      = "";   
        for($i=0;$i<$n-1;$i++){            
            $folder.= $array[$i]."/";            
            $this->createFolder($folder);            
        }        
        $CI->image_moo->load($real_path)->resize_crop($width,$height)->save($resize_path,true);          
        return $path;
    }

}
?>