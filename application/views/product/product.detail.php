<div class="container-fluid disable-padding">
            <div class="fromm-product-details-info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 disable-padding">
                            <div class="menu-moblie-product">
                               <button type="button" class="bt-menu">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button> 
                                <ul class="menu-for-product">
                                    <?php
                                        if(isset($list) && !empty($list)){
                                            foreach ($list as $key => $value) {
                                                ?>
                                                    <li class="<?php echo $value['id']==$detail_sanpham['id']?'active':'' ?>"><a href="<?php echo base_url('san-pham/'.$value['tag']) ?>"><?php echo $value['title_'.$lang] ?></a></li>
                                                <?php
                                            }
                                        }
                                    ?>
                                </ul>
                            </div>
                            <h3 class="contact-heading-background open-san-semibold">
                            <?php 
                                if(isset($detail_sanpham['title_'.$lang]))
                                    echo $detail_sanpham['title_'.$lang];
                            ?>
                            </h3>
                        </div>
                    </div>
                    <div class="row" style="max-width:1170px; display: flex;">
                        <div class="col-sm-3 col-md-3 sidebar disable-padding-left clearfix">
                            <ul class="nav nav-sidebar clearfix">
                                <li class="product-menu-background open-san-semibold clearfix">
                                    <a class='disable-padding-top' href="javascript:void(0)"><?php echo $this->lang->line('sanpham'); ?> <span class="sr-only">(current)</span></a>
                                </li>
                            </ul>
                            <ul class="sub-menu-left clearfix open-san-regular main-menu-left">
                                <?php
                                    if(isset($list) && !empty($list)){
                                        foreach ($list as $key => $value) {
                                            ?>
                                                <li class="<?php echo $value['id']==$detail_sanpham['id']?'active':'' ?>"><a href="<?php echo base_url('san-pham/'.$value['tag']) ?>"><?php echo $value['title_'.$lang] ?></a></li>
                                            <?php
                                        }
                                    }
                                ?>
                            </ul>                           
                        </div>
                        <div class="col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-2 main disable-padding get-height" style="margin-left:0px;">
                            <div class="row disable-margin product-details-box">
                                <div class="col-md-12 disable-padding product-details-details" style="padding-top:26px; padding-left:58px; padding-right:58px;">
                                    <ul class="nav nav-tabs">
                                        <li class="active product-details-tabs tabs-active"><a class='text-uppercase open-san-semibold' data-toggle="tab" href="#home"><?php echo $this->lang->line('thongtinsanpham'); ?></a></li>
                                        <li class='product-details-tabs'><a class='text-uppercase open-san-semibold' data-toggle="tab" href="#menu1"><?php echo $this->lang->line('thongso'); ?></a></li>
                                        <li class='product-details-tabs'><a class='text-uppercase open-san-semibold' data-toggle="tab" href="#menu2"><?php echo $this->lang->line('guiyeucau'); ?></a></li>
                                    </ul>

                                    <div class="tab-content">
                                        <div id="home" class="tab-pane fade in active">
                                            <?php
                                                if(isset($detail_sanpham['detail_'.$lang]))
                                                    echo stripslashes($detail_sanpham['detail_'.$lang]);
                                            ?>
                                        </div>
                                        <div id="menu1" class="tab-pane fade">
                                            <?php
                                                if(isset($detail_sanpham['thongso_'.$lang]))
                                                    echo stripslashes($detail_sanpham['thongso_'.$lang]);
                                            ?>
                                        </div>
                                        <div id="menu2" class="tab-pane fade">
                                            <h4 class="title-product">
                                                <?php 
                                                    if(isset($detail_sanpham['title_'.$lang]))
                                                        echo $detail_sanpham['title_'.$lang];
                                                ?>
                                            </h4>
                                            <form method="post" class="fromm-form-product">
                                                <input type="hidden" name="name_product" value="<?php echo $detail_sanpham['title_vn']; ?>"></input>
                                                <input type="hidden" name="link_product" value="<?php echo current_url(); ?>"></input>
                                                <div class="row open-san-regular" style="font-weight: 300;">
                                                    <div class="col-md-12 disable-padding mg-input clearfix" style="margin-top: 10px;">
                                                        <label class="col-md-2 open-san-regular-small"><?php echo $this->lang->line('giaban'); ?>:</label>
                                                        <label class="col-md-10 product-price open-san-regular-small">
                                                            <?php 
                                                                if(isset($detail_sanpham['price_'.$lang]))
                                                                    echo number_format((int)$detail_sanpham['price_'.$lang]);
                                                                echo $this->lang->line('tiente');
                                                            ?>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-12 disable-padding mg-input clearfix">
                                                        <label class="col-md-2 open-san-regular-small"><?php echo $this->lang->line('soluong'); ?>:</label>
                                                        <input type="text" name="soluong" class="form-product col-md-10 open-san-regular-small" />
                                                    </div>
                                                    <div class="col-md-12 disable-padding mg-input clearfix">
                                                        <label class="col-md-2 open-san-regular-small"><?php echo $this->lang->line('hoten'); ?>:</label>
                                                        <input type="text" name="hoten" class="form-product col-md-10 open-san-regular-small" />
                                                    </div>
                                                    <div class="col-md-12 disable-padding mg-input clearfix">
                                                        <label class="col-md-2 open-san-regular-small"><?php echo $this->lang->line('diachi'); ?>:</label>
                                                        <input type="text" name="diachi" class="form-product col-md-10 open-san-regular-small" />
                                                    </div>
                                                    <div class="col-md-12 disable-padding mg-input clearfix">
                                                        <label class="col-md-2 open-san-regular-small"><?php echo $this->lang->line('dienthoai'); ?>:</label>
                                                        <input type="text" name="dienthoai" class="form-product col-md-10 open-san-regular-small" />
                                                    </div>
                                                    <div class="col-md-12 disable-padding mg-input clearfix">
                                                        <label class="col-md-2 open-san-regular-small"><?php echo $this->lang->line('email'); ?>:</label>
                                                        <input type="text" name="email" class="form-product col-md-10 open-san-regular-small" />
                                                    </div>
                                                    <div class="col-md-12 disable-padding mg-input clearfix">
                                                        <label class="col-md-2 open-san-regular-small"><?php echo $this->lang->line('tinnhan'); ?>:</label>
                                                        <textarea class="form-product col-md-10 open-san-regular-small text-area-noidung" name="tinnhan">
                                                        </textarea>
                                                    </div>
                                                                                      
                                                    
                                                    <div class="col-md-12 disable-padding clearfix">
                                                        <button class="btn-product-contact"><?php echo $this->lang->line('gui'); ?></button>
                                                    </div>                                           
                                                    
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>                                
                            </div>                           
                        </div>
                </div>
            </div>            
        </div> <!-- /container -->