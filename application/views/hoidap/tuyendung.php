<div class="container-fluid disable-padding">
            <div class="fromm-product-details-info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 disable-padding">
                            <h3 class="contact-heading-background open-san-semibold">
                                <?php
                                    if(isset($title_cate)){
                                        echo $title_cate['title_'.$lang];
                                    }   
                                ?>
                            </h3>
                        </div>
                    </div>
                    <div class="row" style="max-width:1170px; display: flex;">
                        <div class="col-sm-3 col-md-3 sidebar disable-padding-left clearfix">
                            <ul class="nav nav-sidebar clearfix">
                                <li class="product-menu-background open-san-semibold clearfix">
                                    <a class='disable-padding-top' href="#"><?php echo $this->lang->line('tuyendung'); ?><span class="sr-only">(current)</span></a>
                                </li>
                            </ul>
                            <ul class="sub-menu-left clearfix open-san-regular main-menu-left">
                                <?php
                                    if(isset($list) && !empty($list)){
                                        foreach ($list as $key => $value) {
                                            ?>
                                                <li class="<?php
                                                    if(isset($title_cate['tag']) && $title_cate['tag'] == $value['tag']){
                                                        echo 'active';
                                                    }
                                                 ?>
                                                 "><a href="<?php echo site_url('tuyen-dung/'.$value['tag']) ?>"><?php echo $value['title_'.$lang] ?></a></li>
                                            <?php
                                        }
                                    }
                                ?>
                            </ul>                           
                        </div>
                        <div class="col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-2 main disable-padding get-height" style="margin-left:0px; ">
                            <div class="row disable-margin product-details-box" style="padding-bottom: 30px;">
                                <?php
                                    if(isset($list_new_cate) && !empty($list_new_cate)){
                                        var_dump($list_new_cate);
                                        foreach ($list_new_cate as $key => $value) {
                                            ?>
                                                

                                            <?php
                                        }
                                    }
                                ?>
                                

                                
                                <div class="phan-trang pull-right">
                                    <ul class="pagination">
                                      <?php echo $this->pagination->create_links(); ?>
                                    </ul>
                                </div>                                 
                            </div>                           
                        </div>
                    </div>
                </div>
            </div>            
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Large Modal</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>This is a large modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
        </div> <!-- /container -->