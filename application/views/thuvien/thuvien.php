<div class="container-fluid disable-padding">
            <div class="fromm-product-details-info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 disable-padding">
                            <div class="menu-moblie-product">
                               <button type="button" class="bt-menu">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button> 
                                <ul class="menu-for-product">
                                    <?php
                                        if(isset($list) && !empty($list)){
                                            foreach ($list as $key => $value) {
                                                ?>
                                                    <li class="<?php
                                                        if(isset($title_cate['tag']) && $title_cate['tag'] == $value['tag']){
                                                            echo 'active';
                                                        }
                                                     ?>
                                                     "><a href="<?php echo site_url('thu-vien/'.$value['tag']) ?>"><?php echo $value['title_'.$lang] ?></a></li>
                                                <?php
                                            }
                                        }
                                    ?>
                                </ul>
                            </div>
                            <h3 class="contact-heading-background open-san-semibold">
                                <?php
                                    if(isset($title_cate)){
                                        echo $title_cate['title_'.$lang];
                                    }   
                                ?>
                            </h3>
                        </div>
                    </div>
                    <div class="row" style="max-width:1170px; display: flex;">
                        <div class="col-sm-3 col-md-3 sidebar disable-padding-left clearfix">
                            <ul class="nav nav-sidebar clearfix">
                                <li class="product-menu-background open-san-semibold clearfix">
                                    <a class='disable-padding-top' href="javascript:void(0)"><?php echo $this->lang->line('thuvien'); ?><span class="sr-only">(current)</span></a>
                                </li>
                            </ul>
                            <ul class="sub-menu-left clearfix open-san-regular main-menu-left">
                                <?php
                                    if(isset($list) && !empty($list)){
                                        foreach ($list as $key => $value) {
                                            ?>
                                                <li class="<?php
                                                    if(isset($title_cate['tag']) && $title_cate['tag'] == $value['tag']){
                                                        echo 'active';
                                                    }
                                                 ?>
                                                 "><a href="<?php echo site_url('thu-vien/'.$value['tag']) ?>"><?php echo $value['title_'.$lang] ?></a></li>
                                            <?php
                                        }
                                    }
                                ?>
                            </ul>                           
                        </div>
                        <div class="col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-2 main disable-padding get-height" style="margin-left:0px; ">
                            <div class="row disable-margin product-details-box" style="padding-bottom: 30px;">
                                <div style="max-width: 750px; margin: 0px auto; margin-top: 30px;">
                                    <?php
                                        if(isset($list_new_cate) && !empty($list_new_cate)){
                                    ?>
                                        <div id='gallery_temp'>
                                           <div class="grid">
                                            <?php
                                                if(isset($list_new_cate[0])){
                                            ?>
                                                <div class="grid-item grid-item--width1 grid-item--height1">
                                                    <a href="upload/album/lagre/<?php echo isset($list_new_cate[0])?$list_new_cate[0]['image']:'' ?>">
                                                        <img src="upload/album/lagre/<?php echo isset($list_new_cate[0])?$list_new_cate[0]['image']:'' ?>" title="photo1 title">
                                                    </a>
                                                </div>
                                            <?php
                                            }
                                            ?>
                                            <?php
                                                if(isset($list_new_cate[1])){
                                            ?>
                                                <div class="grid-item grid-item--width2 grid-item--height2">
                                                    <a href="upload/album/lagre/<?php echo isset($list_new_cate[1])?$list_new_cate[1]['image']:'' ?>">
                                                        <img src="upload/album/lagre/<?php echo isset($list_new_cate[1])?$list_new_cate[1]['image']:'' ?>" title="photo1 title">
                                                    </a>
                                                </div>

                                            <?php
                                        }
                                                if(isset($list_new_cate[2])){
                                            ?>
                                                <div class="grid-item grid-item--width3 grid-item--height3">
                                                    <a href="upload/album/lagre/<?php echo isset($list_new_cate[2])?$list_new_cate[2]['image']:'' ?>">
                                                        <img src="upload/album/lagre/<?php echo isset($list_new_cate[2])?$list_new_cate[2]['image']:'' ?>" title="photo1 title">
                                                    </a>
                                                </div>


                                            <?php
                                        }
                                                if(isset($list_new_cate[3])){
                                            ?>
                                                <div class="grid-item grid-item--width4 grid-item--height3">
                                                    <a href="upload/album/lagre/<?php echo isset($list_new_cate[3])?$list_new_cate[3]['image']:'' ?>">
                                                        <img src="upload/album/lagre/<?php echo isset($list_new_cate[3])?$list_new_cate[3]['image']:'' ?>" title="photo1 title">
                                                    </a>
                                                </div>

                                            <?php
                                        }
                                                if(isset($list_new_cate[4])){
                                            ?>
                                                <div class="grid-item grid-item--width5 grid-item--height4">
                                                    <a href="upload/album/lagre/<?php echo isset($list_new_cate[4])?$list_new_cate[4]['image']:'' ?>">
                                                        <img src="upload/album/lagre/<?php echo isset($list_new_cate[4])?$list_new_cate[4]['image']:'' ?>" title="photo1 title">
                                                    </a>
                                                </div>
                                            <?php
                                            }
                                            ?>
                                            <?php
                                                if(isset($list_new_cate[5])){
                                            ?>
                                                <div class="grid-item grid-item--width6 grid-item--height4">
                                                    <a href="upload/album/lagre/<?php echo isset($list_new_cate[5])?$list_new_cate[5]['image']:'' ?>">
                                                        <img src="upload/album/lagre/<?php echo isset($list_new_cate[5])?$list_new_cate[5]['image']:'' ?>" title="photo1 title">
                                                    </a>
                                                </div>
                                            <?php
                                            }
                                            ?>


                                            <?php
                                                if(isset($list_new_cate[6])){
                                            ?>
                                                <div class="grid-item grid-item--width1 grid-item--height1">
                                                    <a href="upload/album/lagre/<?php echo isset($list_new_cate[6])?$list_new_cate[6]['image']:'' ?>">
                                                        <img src="upload/album/lagre/<?php echo isset($list_new_cate[6])?$list_new_cate[6]['image']:'' ?>" title="photo1 title">
                                                    </a>
                                                </div>
                                            <?php
                                            }
                                            ?>
                                            <?php
                                                if(isset($list_new_cate[7])){
                                            ?>
                                                <div class="grid-item grid-item--width2 grid-item--height2">
                                                    <a href="upload/album/lagre/<?php echo isset($list_new_cate[7])?$list_new_cate[7]['image']:'' ?>">
                                                        <img src="upload/album/lagre/<?php echo isset($list_new_cate[7])?$list_new_cate[7]['image']:'' ?>" title="photo1 title">
                                                    </a>
                                                </div>

                                            <?php
                                        }
                                                if(isset($list_new_cate[8])){
                                            ?>
                                                <div class="grid-item grid-item--width3 grid-item--height3">
                                                    <a href="upload/album/lagre/<?php echo isset($list_new_cate[8])?$list_new_cate[8]['image']:'' ?>">
                                                        <img src="upload/album/lagre/<?php echo isset($list_new_cate[8])?$list_new_cate[8]['image']:'' ?>" title="photo1 title">
                                                    </a>
                                                </div>


                                            <?php
                                        }
                                                if(isset($list_new_cate[9])){
                                            ?>
                                                <div class="grid-item grid-item--width4 grid-item--height3">
                                                    <a href="upload/album/lagre/<?php echo isset($list_new_cate[9])?$list_new_cate[9]['image']:'' ?>">
                                                        <img src="upload/album/lagre/<?php echo isset($list_new_cate[9])?$list_new_cate[9]['image']:'' ?>" title="photo1 title">
                                                    </a>
                                                </div>

                                            <?php
                                        }
                                                if(isset($list_new_cate[10])){
                                            ?>
                                                <div class="grid-item grid-item--width5 grid-item--height4">
                                                    <a href="upload/album/lagre/<?php echo isset($list_new_cate[10])?$list_new_cate[10]['image']:'' ?>">
                                                        <img src="upload/album/lagre/<?php echo isset($list_new_cate[10])?$list_new_cate[10]['image']:'' ?>" title="photo1 title">
                                                    </a>
                                                </div>
                                            <?php
                                            }
                                            ?>
                                            <?php
                                                if(isset($list_new_cate[11])){
                                            ?>
                                                <div class="grid-item grid-item--width6 grid-item--height4">
                                                    <a href="upload/album/lagre/<?php echo isset($list_new_cate[11])?$list_new_cate[11]['image']:'' ?>">
                                                        <img src="upload/album/lagre/<?php echo isset($list_new_cate[11])?$list_new_cate[11]['image']:'' ?>" title="photo1 title">
                                                    </a>
                                                </div>
                                            <?php
                                            }
                                            ?>
                                            
                                            
                                          </div>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                      
                                 </div>
                              </div>                           
                        </div>
                    </div>
                </div>
            </div>            
        </div> <!-- /container -->

<style type="text/css">
 
</style>
<script type="text/javascript">
    $('.grid').masonry({
      // options
      itemSelector: '.grid-item',
      columnWidth: 1
    });
</script>
<script>

    // applying photobox on a `gallery` element which has lots of thumbnails links.
      // Passing options object as well:
      //-----------------------------------------------
      $('#gallery_temp').photobox('a',{ time:0 });

      // using a callback and a fancier selector
      //----------------------------------------------
      $('#gallery_temp').photobox('li > a.family',{ time:0 }, callback);
      function callback(){
         console.log('image has been loaded');
      }

      // // destroy the plugin on a certain gallery:
      // //-----------------------------------------------
      // $('#gallery_temp').photobox('destroy');

      // // re-initialize the photbox DOM (does what Document ready does)
      // //-----------------------------------------------
      // $('#gallery_temp').photobox('prepareDOM');
  </script>