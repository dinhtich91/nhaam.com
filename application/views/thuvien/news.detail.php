<div class="container-fluid disable-padding">
            <div class="fromm-product-details-info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 disable-padding">
                            <h3 class="contact-heading-background open-san-semibold">
                                <?php
                                    if(isset($title_cate)){
                                        echo $title_cate['title_'.$lang];
                                    }   
                                ?>
                            </h3>
                        </div>
                    </div>
                    <div class="row" style="max-width:1170px; display: flex;">
                        <div class="col-sm-3 col-md-3 sidebar disable-padding-left clearfix">
                            <ul class="nav nav-sidebar clearfix">
                                <li class="product-menu-background open-san-semibold clearfix">
                                    <a class='disable-padding-top' href="#"><?php echo $this->lang->line('tintuc'); ?><span class="sr-only">(current)</span></a>
                                </li>
                            </ul>
                            <ul class="sub-menu-left clearfix open-san-regular main-menu-left">
                                <?php
                                    if(isset($list) && !empty($list)){
                                        foreach ($list as $key => $value) {
                                            ?>
                                                <li class="<?php
                                                    if(isset($title_cate['tag']) && $title_cate['tag'] == $value['tag']){
                                                        echo 'active';
                                                    }
                                                 ?>
                                                 "><a href="<?php echo site_url('tin-tuc/'.$value['tag']) ?>"><?php echo $value['title_'.$lang] ?></a></li>
                                            <?php
                                        }
                                    }
                                ?>
                            </ul>                           
                        </div>
                        <div class="col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-2 main disable-padding get-height" style="margin-left:0px; ">
                            <div class="row disable-margin product-details-box" style="padding-bottom: 30px;">
                                <?php
                                    if(isset($list_new_cate) && !empty($list_new_cate)){
                                        $content = strip_tags($list_new_cate['detail_'.$lang]);
                                        ?>
                                        <p class="open-san-semibold title-new-detail"><?php echo $list_new_cate['title_'.$lang] ?></p>
                                        <div class="content-new">
                                            <?php echo $content; ?>
                                        </div>
                                        <?php
                                    }
                                ?>
                                <div class="tin-lien-quan">
                                    <p class="open-san-semibold title-new-detail"><?php echo $this->lang->line('tinlienquan') ?></p>

                                    <?php
                                    if(isset($tinlienquan) && !empty($tinlienquan)){
                                        foreach ($tinlienquan as $key => $value) {
                                            ?>
                                                <div class="col-md-12 disable-padding product-details-details" style="padding-top:26px; padding-left:58px; padding-right:58px;">
                                                    <div class="col-md-3 img-tintuc">
                                                        <img src="<?php echo $value['image'] ?>">
                                                    </div>
                                                    <div class="col-md-9 des-tintuc">
                                                        <div class="title-tintuc">
                                                            <h4 class="title-product">
                                                                <?php
                                                                    echo $value['title_'.$lang];
                                                                ?>
                                                            </h4>
                                                        </div>
                                                        <div class="tintuc-des">
                                                            <?php
                                                                $content = strip_tags($value['detail_'.$lang]);
                                                                echo $this->function->cut_string($content, 300);

                                                            ?>
                                                        </div>
                                                        <div class="footer-tintuc pull-right">
                                                            <a href="<?php echo base_url('tin-tuc-chi-tiet/'.$value['tag']) ?>"><span class="btn-list-tintuc"><?php echo $this->lang->line('xemchitiet') ?></span></a>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php
                                        }
                                    }
                                ?>

                                </div>                               
                            </div>  
                                                    
                        </div>
                    </div>
                </div>
            </div>            
        </div> <!-- /container -->