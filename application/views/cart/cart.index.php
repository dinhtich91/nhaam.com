<section class="content">
    <div class="container">
        <div class="navigation clearfix">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <div class="quatang-in clearfix">
            <div class="quatang-w clearfix">
                <form action="cart/update" method="post" accept-charset="utf-8">
                    <table class="tb_order_info table margin_top_15" border="0" cellpadding="0" cellspacing="1">
                        <tbody>
                            <tr>
                                <th style="width: 120px;" align="center">
                                    Image
                                </th>
                                <th align="center">
                                    Tên sản phẩm
                                </th>
                                <th style="width: 15%;" align="center">
                                    Đơn giá
                                </th>
                                <th style="width: 15%;" align="center">
                                    Số lượng
                                </th>
                                <th style="width: 15%;" align="center">
                                    Tạm tính
                                </th>
                            </tr>
                            <?php
                            $j = 0;
                            foreach ($this->cart->contents() as $items):
                                echo form_hidden($j . '[rowid]', $items['rowid']);
                                $detailProduct = $this->gaucon->detailProductByID($items['id']);
                                $title = $detailProduct['title'];
                                ?>
                            <input type="hidden" value="<?php echo $items['id']; ?>" name="list_product[<?= $items['id']; ?>]"/>
                            <tr>
                                <td align="center">
                                    <img alt="<?php echo $title; ?>" title="<?php echo $title; ?>" src="<?php echo $detailProduct['avatar']; ?>" style="width:100px;border-width:0px;">
                                </td>
                                <td align="left" >
                                    <div style="position: relative; min-height: 120px">
                                        Tên sản phẩm: <?php echo $title; ?><br/>Mã: <?php echo $items['id']; ?>
                                        <a href="<?php echo site_url("xoa-san-pham/" . $items['rowid']); ?>" class="del-product">Xóa sản phẩm</a>
                                    </div>
                                </td>
                                <td align="center">
                                    <?php echo number_format($items['price']) ?> vnđ
                                </td>
                                <td align="center">
                                    <select name="<?= $j . '[qty]'; ?>" onchange="$('form').submit();">
                                        <?php
                                        for ($i = 1; $i <= 20; $i++) {
                                            ?>
                                            <option <?= $items['qty'] == $i ? "selected=selected" : ""; ?> value="<?= $i; ?>"><?= $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td align="center">
                                    <?php echo number_format($items['subtotal']) ?> vnđ
                                </td>
                            </tr>
                            <?php
                            $j++;
                        endforeach;
                        ?>

                        <?php
                        if (count($this->cart->contents()) > 0) {
                            ?>
                            <tr>
                                <td align="right" colspan="5">
                                    <span class="total">Tổng cộng:</span> <?php echo number_format($this->cart->total()); ?> vnđ
                                </td>
                            </tr>
                            <tr class="gio-hang-button">
                                <td align="right" colspan="5">
                                    <a class="btn-tt" href="<?php echo $callback; ?>">Tiếp tục mua hàng</a>
                                    <a class="btn-tt" href="cart/step1">Thanh toán ngay</a>
                                </td>
                            </tr>
                        <?php } else { ?>
                            <tr>
                                <td colspan="5">Giỏ hàng trống, vui lòng chọn sản phẩm!</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </form>
                <p style="margin-bottom: 10px;font-style: italic;color: #03f;">Gấu Con sẽ Free Ship ở các khu vực : QUẬN 1, QUẬN 3, QUẬN 10 và với giá trị đơn hàng trên 500.000 vnđ. Và đối với những khu vực lân cận sẽ tùy theo đơn hàng và tùy theo khu vực Gấu Con sẽ đưa ra mức phí phù hợp nhất, để đảm bảo quyền lợi của khách hàng.</p>
            </div>
        </div>
    </div>
</section>