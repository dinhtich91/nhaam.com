<section class="content">
    <div class="container">
        <div class="navigation clearfix">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <div class="quatang-in clearfix">
            <div class="step clearfix">
                <ul class="clearfix">
                    <li class="active">
                        <a href="javascript:void(0);">1</a>
                        <span>Quà tặng kèm</span>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);">2</a>
                        <span>Thông tin liên lạc </span>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);">3</a>
                        <span>Thông tin người nhận</span>
                    </li>
                    <li>
                        <a href="javascript:void(0);">4</a>
                        <span>Lời nhắn </span>
                    </li>
                    <li>
                        <a href="javascript:void(0);">5</a>
                        <span>Thanh toán</span>
                    </li>
                    <li>
                        <a href="javascript:void(0);">6</a>
                        <span>Hoàn tất</span>
                    </li>
                </ul>
            </div>
            <div class="quatang-w clearfix">
                <div class="quatang-l clearfix">
                    <form action="cart/update" method="post" accept-charset="utf-8">
                        <div class="block-giohang">
                            <h2>GIỎ HÀNG</h2>
                            <?php
                            $j = 0;
                            foreach ($this->cart->contents() as $items):
                                echo form_hidden($j . '[rowid]', $items['rowid']);
                                $detailProduct = $this->gaucon->detailProductByID($items['id']);
                                $title = $detailProduct['title'];
                                $link = $this->gaucon->permartlinkProduct($detailProduct['id']);
                                ?>
                                <div class="item-giohang clearfix">
                                    <div class="item-giohang-i">
                                        <a href="<?php echo $link; ?>" target="_blank"><img src="<?php echo $detailProduct['avatar']; ?>" alt=""></a>
                                    </div>
                                    <div class="item-giohang-t">
                                        <h4><a href="<?php echo $link; ?>" target="_blank"><?php echo $title; ?></a></h4>
                                        <p><span>Số lượng:</span>
                                            <select name="<?= $j . '[qty]'; ?>" onchange="$('form').submit();">
                                                <?php
                                                for ($i = 1; $i <= 20; $i++) {
                                                    ?>
                                                    <option <?= $items['qty'] == $i ? "selected=selected" : ""; ?> value="<?= $i; ?>"><?= $i; ?></option>
                                                <?php } ?>
                                            </select>
                                        </p>
                                        <p><span>Đơn giá:</span> <?php echo number_format($items['subtotal']) ?> vnđ</p>
                                    </div>
                                    <a href="<?php echo site_url("xoa-san-pham/" . $items['rowid']); ?>" class="item-giohang-c"><img src="img/close.png" height="12" width="9" alt=""></a>
                                </div>
                                <?php
                                $j++;
                            endforeach;
                            ?>
                            <?php
                            if (count($this->cart->contents()) > 0) {
                                ?>
                                <div class="item-giohang-total clearfix">
                                    <div class="item-giohang-t">
                                        <p>Gấu Con sẽ Free Ship ở các khu vực : QUẬN 1, QUẬN 3, QUẬN 10 và với giá trị đơn hàng trên 500.000 vnđ. Và đối với những khu vực lân cận sẽ tùy theo đơn hàng và tùy theo khu vực Gấu Con sẽ đưa ra mức phí phù hợp nhất, để đảm bảo quyền lợi của khách hàng.</p>
                                        <p><span>Tổng :</span> <?php echo number_format($this->cart->total()); ?> vnđ</p>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <em>Giỏ hàng trồng, vui lòng chọn sản phẩm!</em>
                            <?php } ?>
                        </div>
                    </form>
                </div>
                <div class="quatang-r clearfix">
                    <div class="block-thongtin">
                        <div class="nn">
                            <h2>THÔNG TIN NGƯỜI NHẬN</h2>
                            <form action="" method="post">
                                <div class="msg_box_error">
                                    <?php echo validation_errors('<div class="error">', '</div>'); ?>
                                </div>
                                <p>
                                    <label>Thời gian giao hàng: </label>
                                    <select name="day" class="date">
                                        <?php
                                        $dayActive = $this->session->userdata("day") ? $this->session->userdata("day") : date("d");
                                        for ($i = 1; $i <= 31; $i++) {
                                            ?>
                                            <option <?= $dayActive == $i ? "selected" : ""; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select name="month" class="date">
                                        <?php
                                        $monthActive = $this->session->userdata("month") ? $this->session->userdata("month") : date("m");
                                        for ($i = 1; $i <= 12; $i++) {
                                            ?>
                                            <option <?= $monthActive == $i ? "selected" : ""; ?> value="<?php echo $i; ?>"><?php echo "Tháng " . $i; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select name="year" class="date">
                                        <?php
                                        $yearActive = $this->session->userdata("year") ? $this->session->userdata("year") : date("Y");
                                        for ($i = date("Y"); $i <= date("Y") + 1; $i++) {
                                            ?>
                                            <option <?= $yearActive == $i ? "selected" : ""; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select name="time" class="date">
                                        <?php
                                        $timeActive = $this->session->userdata("time") ? $this->session->userdata("time") : "";
                                        for ($i = 5; $i <= 22; $i++) {
                                            ?>
                                            <option <?= $timeActive == $i ? "selected" : ""; ?> value="<?php echo $i; ?>"><?php echo $i; ?> h</option>
                                        <?php } ?>
                                    </select>
                                </p>
                                <?php
                                $is_hide = $this->session->userdata("is_hide") ? $this->session->userdata("is_hide") : 0;
                                ?>
                                <p><label>Dấu tên người gửi?: </label><input type="checkbox" name="is_hide" <?= $is_hide == 1 ? "checked" : ""; ?> value="1"></p>
                                <p><button type="button" onclick="itme();">Tôi là người nhận hoa</button></p>
                                <div class="msg_box_error">
                                    <?php echo validation_errors('<div class="error">', '</div>'); ?>
                                </div>
                                <p class="clearfix"><span>Người nhận <span>*</span></span><input type="text" name="name_nguoinhan" id="name_nguoinhan" value="<?= $name_nguoinhan != "" ? $name_nguoinhan : set_value('name_nguoinhan'); ?>"></p>
                                <p class="clearfix"><span>Điện thoại <span>*</span></span><input type="text" name="phone_nguoinhan" id="phone_nguoinhan" value="<?= $phone_nguoinhan != "" ? $phone_nguoinhan : set_value('phone_nguoinhan'); ?>"></p>
                                <p class="clearfix"><span>Địa chỉ <span>*</span></span><input type="text" name="address_nguoinhan" id="address_nguoinhan" value="<?= $address_nguoinhan != "" ? $address_nguoinhan : set_value('address_nguoinhan'); ?>"></p>
                                <p class="clearfix">
                                    <span style="width: 27.5%">Tỉnh/ Thành  <span>*</span></span>
                                    <select name="city" onchange="changeCity(this.value, 0);">
                                        <?php
                                        $listCity = $this->gaucon->listCity();
                                        foreach ($listCity as $row) {
                                            $cityActive = $this->session->userdata("city") ? $this->session->userdata("city") : 69
                                            ?>
                                            <option <?= $row['id'] == $cityActive ? "selected" : ""; ?> value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                                <p class="clearfix">
                                    <span style="width: 27.5%">Quận huyện <span>*</span></span>
                                    <select name="district" id="district">
                                        <option value="0">-- Quận/huyện --</option>
                                    </select>
                                </p>
                                <p class="clearfix temp"><input type="submit" name="" value="Tiếp tục" class="submit-form"><i class="fa fa-angle-double-right"></i></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
                                                $(document).ready(function() {
                                                    var city = '<?= $this->session->userdata("city") ? $this->session->userdata("city") : 69 ?>';
                                                    var district = '<?= $this->session->userdata("district") ? $this->session->userdata("district") : 0 ?>';
                                                    changeCity(city, district);
                                                })
                                                function itme() {
                                                    $("#name_nguoinhan").val('<?php echo $name; ?>');
                                                    $("#phone_nguoinhan").val('<?php echo $phone; ?>');
                                                    $("#address_nguoinhan").val('<?php echo $address; ?>');
                                                }

                                                function changeCity(id_city, district) {
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "change-city/" + id_city + "/" + district,
                                                        beforeSend: function() {

                                                        },
                                                        success: function(b) {
                                                            $("#district").html(b);
                                                        }
                                                    });
                                                }
</script>