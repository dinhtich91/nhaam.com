<link rel="stylesheet" href="css/jquery.fancybox.css" />
<link rel="stylesheet" href="css/jquery.fancybox-thumbs.css" />
<script src="js/jquery.fancybox.js"></script>
<script src="js/jquery.fancybox-thumbs.js"></script>
<section class="content">
    <div class="container">
        <div class="navigation clearfix">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <ul class="album-tab clearfix">
            <li class="active"><a href="#album-1">Album mẫu tiệc cưới</a></li>
            <li><a href="#album-2">Album khác</a></li>
        </ul>
        <div class="albums-in clearfix">
            <div class="group-album">
                <div id="album-1" class="album">
                    <ul class="clearfix">
                        <?php
                        foreach ($album_tieccuoi as $row) {
                            $imgChil = $this->gaucon->listImageByAlbum($row['id']);
                            ?>
                            <li>
                                <div>
                                    <a href="<?= site_url('thu-vien-chi-tiet/' . $row['tag'] . "-" . $row['id']); ?>" class="fancybox-img" rel="gallery<?= $row['id']; ?>"><img class="img-responsive" src="<?php echo $row['avatar']; ?>" alt=""></a>
                                </div>
                                <a href="<?= site_url('thu-vien-chi-tiet/' . $row['tag'] . "-" . $row['id']); ?>">
                                    <h3 class="album-title"><?php echo $row['title']; ?></h3>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div id="album-2" class="album">
                    <ul class="clearfix">
                        <?php
                        foreach ($album_hinhkhac as $row) {
                            $imgChil = $this->gaucon->listImageByAlbum($row['id']);
                            ?>
                            <li>
                                <div>
                                    <a href="<?= site_url('thu-vien-chi-tiet/' . $row['tag'] . "-" . $row['id']); ?>" class="fancybox-img" rel="gallery<?= $row['id']; ?>"><img class="img-responsive" src="<?php echo $row['avatar']; ?>" alt=""></a>
                                </div>
                                <a href="<?= site_url('thu-vien-chi-tiet/' . $row['tag'] . "-" . $row['id']); ?>">
                                    <h3 class="album-title"><?php echo $row['title']; ?></h3>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>