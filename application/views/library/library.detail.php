<link rel="stylesheet" href="css/jquery.fancybox.css" />
<link rel="stylesheet" href="css/jquery.fancybox-thumbs.css" />
<script src="js/jquery.fancybox.js"></script>
<script src="js/jquery.fancybox-thumbs.js"></script>
<section class="content">
    <div class="container">
        <div class="navigation clearfix">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <ul class="album-tab clearfix">
            <li class="active"><a href="#album-1"><?=$title_header;?></a></li>
        </ul>
        <div class="albums-in clearfix">
            <div class="group-album">
                <div id="album-1" class="album">
                    <ul class="clearfix">
                        <?php
                        $imgChil = $this->gaucon->listImageByAlbum($detailAlbum['id']);
                        foreach ($imgChil as $row) {
                            ?>
                            <li>
                                <div>
                                    <a href="upload/album/lagre/<?php echo basename($row['image']); ?>" class="fancybox-img" rel="gallery1"><img class="img-responsive" src="upload/album/thumb/<?php echo $row['image']; ?>" alt=""></a>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.fancybox-img').fancybox({
                    padding: 0,
                    prevEffect: 'fade',
                    nextEffect: 'fade',
                    helpers: {
                        title: {
                            type: 'outside'
                        },
                        thumbs: {
                            width: 100,
                            height: 70
                        }
                    }
                });

                $('.album-title').matchHeight({
                    byRow: true,
                    property: 'height',
                    target: null,
                    remove: false
                });
            });
        </script>
    </div>
</section>