<div class="container-fluid disable-padding">
    <!-- Example row of columns -->
    <div class="row disable-margin category-box-desktop">
        <div class="col-md-12 disable-padding">
            <div class="category-menu-box">
                <div class="father-category">
                    <div class="the-ball">
                        <img src="img/fromm-ball.png" />
                    </div>
                    <div class="industry-medicine">
                        <div class="industry-medicine-box five-parts">
                            <a class="hyper-link-box clearfix" href="<?php echo site_url('san-pham/dau-dong-co-xang-diesel') ?>"><span class="industry-title animated bounceInLeft">industry & medicine</span></a>                                
                        </div>
                        <div class="cars-box five-parts">
                            <a class="hyper-link-box clearfix" href="<?php echo site_url('san-pham/nhot-xe-o-to') ?>"><span class="cars-title animated bounceInRight">cars</span> </a>                                
                        </div>
                        <div class="ships-box five-parts">
                            <a class="hyper-link-box clearfix" href="<?php echo site_url('san-pham/dau-dong-co-hang-hai') ?>"><span class="ships-title  animated bounceInRight">ships</span></a>                                
                        </div>
                        <div class="bikes-box five-parts">
                            <a class="hyper-link-box clearfix" href="<?php echo site_url('san-pham/nhot-xe-may') ?>"><span class="motobike-title animated bounceInLeft">motorbikes</span></a>                                
                        </div>
                        <div class="trucks-box five-parts">
                            <a class="hyper-link-box clearfix" href="<?php echo site_url('san-pham/nhot-nganh-van-tai') ?>"><span class="trucks-title animated bounceInLeft">trucks</span> </a>                                
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row disable-margin category-box-mobile">
        <div class="col-md-12 disable-padding">
            <div class="category-menu-box category-menu-box-mobile">
                <div class="industry-medicine">
                    <div class="industry-medicine-box-mobile clearfix">
                        <span class="text-uppercase category-soft-box">industry medicine</span>
                        <a class="hyper-link-box medicine-mobile clearfix" href="<?php echo site_url('san-pham/dau-dong-co-xang-diesel') ?>"><img class="full-size" src="img/medicine-mobile.png" /></a>
                        <a class="hyper-link-box medicine-ipad clearfix" href="<?php echo site_url('san-pham/dau-dong-co-xang-diesel') ?>"><img class="full-size" src="img/medicine-ipad.png" /></a>
                    </div>
                    <div class="cars-box-mobile clearfix">
                        <span class="text-uppercase category-soft-box">cars</span>
                        <a class="hyper-link-box cars-mobile clearfix" href="<?php echo site_url('san-pham/nhot-xe-o-to') ?>"><img class="full-size" src="img/cars-mobile.png" /></a>
                        <a class="hyper-link-box cars-ipad clearfix" href="<?php echo site_url('san-pham/nhot-xe-o-to') ?>"><img class="full-size" src="img/cars-ipad.png" /></a>                                
                    </div>
                    <div class="ships-box-mobile clearfix">
                        <span class="text-uppercase category-soft-box">ships</span>
                        <a class="hyper-link-box ships-mobile clearfix" href="<?php echo site_url('san-pham/dau-dong-co-hang-hai') ?>"><img class="full-size" src="img/ships-mobile.png" /></a>                                
                        <a class="hyper-link-box ships-ipad clearfix" href="<?php echo site_url('san-pham/dau-dong-co-hang-hai') ?>"><img class="full-size" src="img/shipping-business-investment.png" /></a>                                
                    </div>
                    <div class="bikes-box-mobile clearfix">
                        <span class="text-uppercase category-soft-box">motorbikes</span>
                        <a class="hyper-link-box bikes-mobile clearfix" href="<?php echo site_url('san-pham/nhot-xe-may') ?>"><img class="full-size" src="img/bikes-mobile.png" /></a>   
                        <a class="hyper-link-box bikes-ipad clearfix" href="<?php echo site_url('san-pham/nhot-xe-may') ?>"><img class="full-size" src="img/bikes-ipad.png" /></a>                                
                    </div>
                    <div class="trucks-box-mobile clearfix">
                        <span class="text-uppercase category-soft-box">trucks</span>
                        <a class="hyper-link-box trucks-mobile clearfix" href="<?php echo site_url('san-pham/nhot-nganh-van-tai') ?>"><img class="full-size" src="img/trucks-mobile.png" /></a>    
                        <a class="hyper-link-box trucks-ipad clearfix" href="<?php echo site_url('san-pham/nhot-nganh-van-tai') ?>"><img class="full-size" src="img/trucks-ipad-1.png" /></a>                                
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- /container -->