<div class="container-fluid disable-padding">
            <div class="fromm-product-details-info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 disable-padding">
                            <div class="menu-moblie-product">
                               <button type="button" class="bt-menu">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button> 
                                <ul class="menu-for-product">
                                    <?php
                                        if(isset($list) && !empty($list)){
                                            foreach ($list as $key => $value) {
                                                ?>
                                                  <li class="<?php
                                                      if(isset($title_cate['tag']) && $title_cate['tag'] == $value['tag']){
                                                          echo 'active';
                                                      }
                                                   ?>
                                                   "><a href="<?php echo site_url('doi-tac/'.$value['tag']) ?>"><?php echo $value['title_'.$lang] ?></a></li>
                                              <?php
                                            }
                                        }
                                    ?>
                                </ul>
                            </div>
                            <h3 class="contact-heading-background open-san-semibold">
                                <?php
                                    if(isset($title_cate)){
                                        echo $title_cate['title_'.$lang];
                                    }   
                                ?>
                            </h3>
                        </div>
                    </div>
                    <div class="row" style="max-width:1170px; display: flex;">
                        <div class="col-sm-3 col-md-3 sidebar disable-padding-left clearfix">
                            <ul class="nav nav-sidebar clearfix">
                                <li class="product-menu-background open-san-semibold clearfix">
                                    <a class='disable-padding-top' href="javascript:void(0)"><?php echo $this->lang->line('khachhang_doitac'); ?><span class="sr-only">(current)</span></a>
                                </li>
                            </ul>
                            <ul class="sub-menu-left clearfix open-san-regular main-menu-left">
                                <?php
                                    if(isset($list) && !empty($list)){
                                        foreach ($list as $key => $value) {
                                            ?>
                                                <li class="<?php
                                                    if(isset($title_cate['tag']) && $title_cate['tag'] == $value['tag']){
                                                        echo 'active';
                                                    }
                                                 ?>
                                                 "><a href="<?php echo site_url('doi-tac/'.$value['tag']) ?>"><?php echo $value['title_'.$lang] ?></a></li>
                                            <?php
                                        }
                                    }
                                ?>
                            </ul>                           
                        </div>
                        <div class="col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-2 main disable-padding get-height" style="margin-left:0px; ">
                            <div class="row disable-margin product-details-box" style="padding-bottom: 30px;">
                                <div class="col-sm-12">
                                    <?php
                                        if(isset($list_new_cate) && !empty($list_new_cate)){
                                            foreach ($list_new_cate as $key => $value) {
                                               ?>
                                                <div class="col-sm-4">
                                                    <div class="img-doitac">
                                                      <a href="<?php echo $value['link']; ?>" target="_blank">
                                                        <img src="<?php echo $value['avatar'] ?>">
                                                      </a>
                                                    </div>
                                                    <div class="title-doitac">
                                                       <a href="<?php echo $value['link']; ?>" target="_blank">
                                                        <h4><?php echo $value['title_'.$lang]; ?></h4>
                                                        </a>
                                                    </div>
                                                </div>
                                               <?php
                                            }
                                        }
                                        else{
                                          ?>
                                            <div class="updating">
                                              <?php echo $this->lang->line('dangcapnhat'); ?>
                                            </div>
                                          <?php
                                        }
                                    ?>
                                </div>                                     
                            </div>
                    </div>
                </div>
            </div>            
        </div> <!-- /container -->

<style type="text/css">
    .product-details-box{
        padding: 30px 40px;
    }
    
    .img-doitac{
        text-align: center;
    }
    .title-doitac{
        text-align: center;
        text-transform: uppercase;
    }
    .title-doitac a{
      color: #000;
    }

</style>

<script>

    // applying photobox on a `gallery` element which has lots of thumbnails links.
      // Passing options object as well:
      //-----------------------------------------------
      $('#gallery_temp').photobox('a',{ time:0 });

      // using a callback and a fancier selector
      //----------------------------------------------
      $('#gallery_temp').photobox('li > a.family',{ time:0 }, callback);
      function callback(){
         console.log('image has been loaded');
      }

      // // destroy the plugin on a certain gallery:
      // //-----------------------------------------------
      // $('#gallery_temp').photobox('destroy');

      // // re-initialize the photbox DOM (does what Document ready does)
      // //-----------------------------------------------
      // $('#gallery_temp').photobox('prepareDOM');
  </script>