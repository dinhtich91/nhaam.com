<section class="content">
    <div class="container">
        <div class="navigation clearfix">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <div class="news-page-in clearfix">
            <?php
            $i = 0;
            foreach ($list as $row) {
                ?>
                <?php
                if ($i == 0) {
                    ?>
                    <div class="hot-new-page clearfix">
                        <div class="hot-new-page-img">
                            <a href="<?= site_url("tin-tuc/" . $row['tag']); ?>">
                                <img src="<?php echo $row['avatar']; ?>" alt="<?php echo $row['title']; ?>" />
                            </a>
                        </div>
                        <div class="hot-new-page-desc">
                            <h1><a href="<?= site_url("tin-tuc/" . $row['tag']); ?>"><?php echo $row['title']; ?></a></h1>
                            <?php echo $row['sumary']; ?>
                            <a href="<?= site_url("tin-tuc/" . $row['tag']); ?>" class="more">Xem thêm <i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="news-page-item">
                        <a class="item-in" href="<?= site_url("tin-tuc/" . $row['tag']); ?>">
                            <img src="<?php echo $row['avatar']; ?>" alt="<?php echo $row['title']; ?>" />
                            <h2><?php echo $row['title']; ?></h2>
                            <p>Xem chi tiết <i class="fa fa-angle-double-right"></i></p>
                        </a>
                    </div>
                <?php } ?>
                <?php
                $i++;
            }
            ?>
        </div>
        <script type="text/javascript">
            $('.item-in h2').matchHeight({
                byRow: true,
                property: 'height',
                target: null,
                remove: false
            });
        </script>
        <div class="page clearfix">
            <ul>
                <?php echo $this->pagination->create_links(); ?>
            </ul>
        </div>
    </div>
</section>