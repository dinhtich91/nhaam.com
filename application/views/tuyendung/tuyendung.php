<div id="lang" value="<?php echo $lang ?>">
    
</div>
<div class="container-fluid disable-padding">
            <div class="fromm-product-details-info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 disable-padding">
                            <div class="menu-moblie-product">
                               <button type="button" class="bt-menu">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button> 
                                <ul class="menu-for-product">
                                    <?php
                                        if(isset($list) && !empty($list)){
                                            foreach ($list as $key => $value) {
                                                ?>
                                                    <li class="<?php
                                                        if(isset($title_cate['tag']) && $title_cate['tag'] == $value['tag']){
                                                            echo 'active';
                                                        }
                                                     ?>
                                                     "><a href="<?php echo site_url('tuyen-dung/'.$value['tag']) ?>"><?php echo $value['title_'.$lang] ?></a></li>
                                                <?php
                                            }
                                        }
                                    ?>
                                </ul>
                            </div>
                            <h3 class="contact-heading-background open-san-semibold">
                                <?php
                                    if(isset($title_cate)){
                                        echo $title_cate['title_'.$lang];
                                    }   
                                ?>
                            </h3>
                        </div>
                    </div>
                    <div class="row" style="max-width:1170px; display: flex;">
                        <div class="col-sm-3 col-md-3 sidebar disable-padding-left clearfix">
                            <ul class="nav nav-sidebar clearfix">
                                <li class="product-menu-background open-san-semibold clearfix">
                                    <a class='disable-padding-top' href="javascript:void(0)"><?php echo $this->lang->line('tuyendung'); ?><span class="sr-only">(current)</span></a>
                                </li>
                            </ul>
                            <ul class="sub-menu-left clearfix open-san-regular main-menu-left">
                                <?php
                                    if(isset($list) && !empty($list)){
                                        foreach ($list as $key => $value) {
                                            ?>
                                                <li class="<?php
                                                    if(isset($title_cate['tag']) && $title_cate['tag'] == $value['tag']){
                                                        echo 'active';
                                                    }
                                                 ?>
                                                 "><a href="<?php echo site_url('tuyen-dung/'.$value['tag']) ?>"><?php echo $value['title_'.$lang] ?></a></li>
                                            <?php
                                        }
                                    }
                                ?>
                            </ul>                           
                        </div>
                        <div class="col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-2 main disable-padding get-height" style="margin-left:0px; ">
                            <div class="row disable-margin product-details-box" style="padding-bottom: 30px; padding-top:30px; padding-left:20px;">
                                <?php
                                    if(isset($list_new_cate) && !empty($list_new_cate)){
                                        foreach ($list_new_cate as $key => $value) {
                                            ?>
                                                <div class="col-md-12 clearfix" style="margin-bottom: 5px;">
                                                    <div class="col-md-6 clearfix left-tuyendung" >
                                                        <p class="text-title-tuyendung"><?php echo $value['title_'.$lang] ?></p>
                                                    </div>
                                                    <div class="col-md-6 clearfix right-tuyendung" style="float: right;">
                                                    <div class="tuyen-dung-desktop">
                                                    
                                                        <button rel="photobox" onclick="getViec(<?php echo $value['id'] ?>);" class="xem-tuyendung">Xem</button>
                                                    
                                                        <button class="ungtuyen-tuyendung" data-toggle="modal" data-target="#myModal-<?php echo $key ?>" ><?php echo $this->lang->line('ungtuyen'); ?></button>
                                                        </div>
                                                        <div class="modal fade" id="myModal-<?php echo $key ?>" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header" style="border-bottom:none; height:10px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <p class="text-center text-uppercase text-fromm" >Công ty cổ phần công nghệ dầu nhớt đức Fromm</p>
                <p class="text-center text-uppercase text-fromm-2" ><?php echo $value['title_'.$lang] ?></p>
                <div class="luu-y" style="padding-left:26px; font-weight:bold;">
                <p>Lưu ý:</p>
                <p class="tuyendung-text">- Vui lòng điền đầy đủ thông tin bên dưới</p>
                <p class="tuyendung-text">- Tất cả thông tin trên sẽ được giữ bí mật tuyệt đối và chỉ được sử dụng làm cơ sở để công ty sàng lọc ứng viên</p>
                <p class="tuyendung-text">- Hồ sơ sẽ được lưu trong vòng 3 tuần</p>
                </div>
            </div>
        </div>
        <div class="row" style="margin-bottom:60px;">
        <div class="col-md-12" style="margin-bottom: 10px;">
            <form method="post" action="" enctype="multipart/form-data" name="frm-tuyendung" id="frm-tuyendung">
                <div class="col-md-12 clearfix">

                    <input type="hidden" name="id_vitri" value="<?php echo $value['id'] ?>" />
                    <div class="col-md-6">
                        <input type="text" name="hoten" placeholder="Họ tên" style="width:100%; height:50px; margin-bottom:15px; padding-left:10px;" />
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="diachi" placeholder="Địa chỉ" style="width:100%; height:50px; margin-bottom:15px; padding-left:10px;"/>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="email" placeholder="Email"  style="width:100%; height:50px; padding-left:10px; margin-bottom: 15px;"/>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="dienthoai" placeholder="Điện thoại"  style="width:100%; height:50px; padding-left:10px;"/>
                    </div>
                    <div class="col-md-12">
                        <h3 style="text-transform: uppercase; font-weight:bold;">Upload CV</h3>
                        <div class="col-md-6" style="padding-left:0px;">                            
                            <input type="file" name="userfile" id="file-tuyendung" style="height:40px;" />
                        </div>
                        <div class="col-md-6">
                            <input type="submit" id="<?php echo $value['id']; ?>" value="Gửi thông tin"   style="float:right; width:152px; height:40px; background-color:#fdb71e; color:#fff; border:none; font-size:21px; font-weight:bold;" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>
        
      </div>
    </div>
  </div>
  
        </div> <!-- /container -->
                                                    </div>
                                                </div>

                                            <?php
                                        }
                                    }
                                ?>
                                

                                
                                <div class="phan-trang pull-right">
                                    <ul class="pagination">
                                      <?php echo $this->pagination->create_links(); ?>
                                    </ul>
                                </div>                                 
                            </div>                           
                        </div>
                    </div>
                </div>
            </div>            

  <!-- Modal -->
  <script src="js/facebox/facebox.js"></script>
  <script type="text/javascript">
    function getViec(id){
        jQuery.facebox({ajax: 'getWork/'+id});
    }

    function checkValidate(){
    
    var $login_frm   = $('#frm-tuyendung');
    var lang = $('#lang').attr('value');

    $login_frm.submit(function( event ) {
        var errAll      = true;
        if( !$login_frm.find('input[name="hoten"]').val() || $login_frm.find('input[name="hoten"]').val() === undefined ) {

            if(lang == 'vn'){
              alert('Tên không được để trống!');
            }
            else{
              alert('Name is required'); 
            }
            errAll = false;
        }

        else if( !$login_frm.find('input[name="email"]').val() || $login_frm.find('input[name="email"]').val() === undefined ) {

            if(lang == 'vn'){
              alert('Email không được để trống!');
            }
            else{
              alert('Email is required'); 
            }
            errAll = false;
        }

         else if( !$login_frm.find('input[name="diachi"]').val() || $login_frm.find('input[name="diachi"]').val() === undefined ) {

            if(lang == 'vn'){
              alert('Địa chỉ không được để trống!');
            }
            else{
              alert('Address is required'); 
            }
            errAll = false;
        }

        else if( !$login_frm.find('input[name="dienthoai"]').val() || $login_frm.find('input[name="dienthoai"]').val() === undefined || $.isNumeric( $('input[name="dienthoai"]').val()) == false ) {

            if(lang == 'vn'){
              alert('Điện thoại không hợp lệ!');
            }
            else{
              alert('Phone number is valid'); 
            }
            errAll = false;
        }
        else if( document.getElementById("file-tuyendung").files.length == 0 ){
            if(lang == 'vn'){
              alert('File không hợp lệ!');
            }
            else{
              alert('File is valid'); 
            }
            errAll = false;
        }
        

        return errAll;
    });

    $login_frm.find('.ipt').on('focus', function(e){
        e.preventDefault();
        $(this).siblings('.help-block').addClass('hidden');
    });
};
$(document).ready(function() {

    checkValidate();
});
  </script>

  