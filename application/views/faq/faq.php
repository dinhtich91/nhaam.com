
    <div class="banner group">
        <img src="img/doctor/doctor.jpg" alt="doctor">
    </div>
<div class="page-container page-far-container group">
    <div class="block-question-answer group">
        <div class="answer">
            <h3>Hỏi đáp</h3>
            <ul class="nav-FAQ">
                <?php
                $listFAQ = $this->namyangi->listFAQ();
                $i = 1;
                foreach ($listFAQ as $row) {
                    ?>
                    <li>
                        <a href="javascript:void(0);"><img src="img/plus_icon.png" alt="plus_icon"> <?php echo $i.". ".stripslashes($row['answer']); ?></a>
                        <ul style="display: none;" class="sub-FAQ">
                            <li>
                                <?php echo stripslashes($row['question']); ?>
                            </li>
                        </ul>
                    </li>
                    <?php
                    $i++;
                }
                ?>
            </ul>
        </div>
        <div class="question">
            <h3><img src="img/plus.png" alt="plus"> Gửi câu hỏi</h3>
            <div class="new-question">
                <h4>Điền thông tin</h4>
                <form id="frm_faq">
                    <div><label for="">Họ tên</label><input name="name" type="text"/></div>
                    <div><label for="">Địa chỉ </label><input name="address" type="text"/></div>
                    <div><label for="">Email </label><input name="email" type="text"/> </div>
                    <div><label for="">Số điện thoại </label><input name="phone_number" type="text"/></div>
                    <div><label for="">Nội dung </label><textarea name="message"></textarea></div>
                    <div id="loadding" style="text-align: right;"></div>
                    <input type="button" onclick="namyangi.sendFAQ();" name="submit" value="Gửi"/>
                </form>
            </div>
        </div>
    </div>
</div><!--End page-container-->
