<!DOCTYPE html>
<html>
    <?php 
        $config = $this->myconfig->getConfig();
        $slider = $this->fromm->slider();
    ?>
    <head>
        <base href="<?php echo base_url(); ?>"/>
        <meta http-equiv="content-type" content="text/html" charset="utf-8"/>
        <title><?= isset($title_header) ? $title_header : $config['title_web']; ?></title>
        <meta content="index, follow" name="robots"/>
        <meta name="description" content="<?= isset($metaDescription) ? $metaDescription : $config['meta_description']; ?>" />
        <meta name="keywords" content="<?= (isset($metaKeyword) && $metaKeyword != "") ? $metaKeyword : $config['meta_keywords']; ?>" />
        <link rel="canonical" href="<?= current_url(); ?>" />
        <link rel="icon" type="image/png" href="img/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="img/favicon-16x16.png" sizes="16x16" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">              

        <link type="text/css" rel="stylesheet" href="css/flexslider.css">        
        <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">  
        <link type="text/css" rel="stylesheet" href="css/animate.min.css">
        <link type="text/css" rel="stylesheet" href="js/facebox/facebox.css">
        <link type="text/css" rel="stylesheet" href="js/photobox/photobox.css">
        <link type="text/css" rel="stylesheet" href="css/custom.css"> 

        <script src="js/jquery-2.2.3.min.js"></script> 
        <script src="js/photobox/jquery.photobox.js"></script>    
        <script src="js/jquery.flexslider.js"></script>
        <script src="js/masonry.pkgd.js"></script>

        <script type="text/javascript" src="js/custom.js"></script>

        <style type="text/css">
        <?php 
           if($lang == 'en'){
                ?>

                .resize-right-width .nav-partable-right li.client-list a{
                    padding-left: 30px;
                }
                <?php
           }
           else if($lang == 'vn'){ 
                ?>
                .nav-partable li a{
                    padding: 10px 7px;
                }
                .resize-right-width .nav-partable-right li.client-list a{
                    padding-left: 28px;
                }
                .logo-padding img{
                    padding-left: -40px !important;
                }
                @media(max-width:414px) and (min-width:375px){
                    .logo-padding img{
                        padding-left: 0 !important;
                    }
                }
                @media(max-width:375px) and (min-width:320px){
                    .logo-padding img{
                        padding-left: 0 !important;
                    }
                }
                <?php
           }
           else{
                ?>

                .nav-partable li a{
                    padding: 10px 8px;
                }
                .resize-right-width .nav-partable-right li.client-list a{
                    padding-left: 22px;
                }
                <?php
           }
         ?>
        </style>
     
    </head>
    <body class="<?= isset($class_body) ? $class_body : "home"; ?>">

        <div class="container-fluid container-fluid-mobile disable-padding">
            <header class="fromm-header-mobile clearfix">
                <nav class="navbar navbar-inverse navbar-fixed-top fromm-navbar from-navbar-mobile">
                    <div class="container disable-padding banner-height">
                        <a class="navbar-brand logo-mobile" href="<?php echo base_url(); ?>"><img src="img/logo.png" alt="logo" title="logo" /></a>
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed hamburger-btn" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse menu-min-height navbar-mobile">
                            <div class="row disable-margin">
                                <div class="col-md-12 disable-padding">
                                    <ul class="nav nav-pills menu-padding nav-partable menu-mobile">
                                        <li><a class="text-uppercase open-san-regular fromm-li-home"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
                                        <li><a class="text-uppercase open-san-regular" href="<?php  echo site_url('gioi-thieu'); ?>"><?php echo $this->lang->line('gioithieu'); ?></a></li>
                                        <li><a class="text-uppercase open-san-regular" href="<?php  echo site_url('san-pham'); ?>"><?php echo $this->lang->line('sanpham'); ?></a></li>
                                        <li><a class="text-uppercase open-san-regular" href="<?php  echo site_url('dich-vu'); ?>"><?php echo $this->lang->line('dichvu'); ?></a></li>
                                        <li><a class="text-uppercase open-san-regular" href="<?php  echo site_url('tin-tuc'); ?>"><?php echo $this->lang->line('tintuc'); ?></a></li>
                                        <li><a class="text-uppercase open-san-regular" href="<?php  echo site_url('doi-tac'); ?>"><?php echo $this->lang->line('khachhang_doitac'); ?></a></li>
                                        <li><a class="text-uppercase open-san-regular" href="<?php  echo site_url('thu-vien'); ?>"><?php echo $this->lang->line('thuvien'); ?></a></li>
                                        <li><a class="text-uppercase open-san-regular" href="<?php  echo site_url('tuyen-dung'); ?>"><?php echo $this->lang->line('tuyendung'); ?></a></li>
                                        <li><a class="text-uppercase open-san-regular" href="<?php  echo site_url('hoi-dap'); ?>"><?php echo $this->lang->line('hoidap'); ?></a></li>
                                        <li><a class="text-uppercase open-san-regular" href="<?php  echo site_url('lien-he'); ?>"><?php echo $this->lang->line('lienhe'); ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="multilanguage-box clearfix">
                            <ul class="multilanguage-ul clearfix">
                                <li class="clearfix">
                                    
                                    <?php

                                        if($lang == 'vn'){
                                            echo '<span class="vn-flag"></span><a>Tiếng Việt</a>';

                                        }
                                        // else if($lang == 'en'){
                                        //     echo '<span class="usa-flag"></span><a>English</a>';
                                        // }
                                        else{
                                            echo '<span class="de-flag"></span><a>Germany</a>';
                                        }
                                     ?>
                                  <span class="caret caret-icon"></span>
                                    <ul class="clearfix sub-language sub-language-menu-mobile" style="z-index: 99;">
                                        <li class="clearfix"><span class="vn-flag"></span><a href="<?php echo base_url('vn') ?>">Tiếng Việt</a></span></li>
                                        <li class="clearfix"><span class="usa-flag"></span><a href="<?php echo base_url('en') ?>">English</a></span></li>
                                        <li class="clearfix"><span class="de-flag"></span><a href="<?php echo base_url('de') ?>">Germany</a></span></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
            <header class="fromm-header clearfix">
                <nav class="navbar navbar-inverse navbar-fixed-top fromm-navbar">
                    <div class="container disable-padding banner-height">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse menu-min-height nav-gaoranger">
                            <div class="row menu-nav-height disable-margin home-fromm-desktop">
                                <div class="col-md-5 disable-padding menu-nav-height resize-left-width">
                                    <ul class="nav nav-pills menu-padding menu-nav-height nav-partable nav-partable-left">
                                        <li class="<?php echo $active_menu=='home'?'fromm-active':'' ?>"><a class="text-uppercase open-san-regular fromm-li-home" href="<?php echo site_url(); ?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
                                        <li class="<?php echo $active_menu=='gioithieu'?'fromm-active':'' ?>"><a class="text-uppercase open-san-regular" href="<?php  echo site_url('gioi-thieu'); ?>"><?php echo $this->lang->line('gioithieu'); ?></a></li>
                                        <li class="<?php echo $active_menu=='sanpham'?'fromm-active':'' ?>"><a class="text-uppercase open-san-regular" href="<?php  echo site_url('san-pham'); ?>"><?php echo $this->lang->line('sanpham'); ?></a></li>
                                        <li class="<?php echo $active_menu=='dichvu'?'fromm-active':'' ?>"><a class="text-uppercase open-san-regular" href="<?php  echo site_url('dich-vu'); ?>"><?php echo $this->lang->line('dichvu'); ?></a></li>
                                        <li class="news-list <?php echo $active_menu=='tintuc'?'active-tintuc':'' ?>"><a class="text-uppercase open-san-regular"  href="<?php  echo site_url('tin-tuc'); ?>"><?php echo $this->lang->line('tintuc'); ?></a></li>
                                    </ul>
                                </div>
                                <div class="col-md-2 disable-padding menu-nav-height">
                                    <div class="big-logo logo-padding">
                                        <a href="<?php echo base_url(); ?>"><img src="img/logo.png" alt="logo" title="logo" /></a>
                                    </div>
                                </div>
                                <div class="col-md-5 disable-padding menu-nav-height resize-right-width">
                                    <ul class="nav nav-pills menu-padding menu-nav-height nav-partable nav-partable-right">
                                        <li class="client-list <?php echo $active_menu=='doitac'?'active-doitac':'' ?>"><a class="text-uppercase open-san-regular" href="<?php  echo site_url('doi-tac'); ?>"><?php echo $this->lang->line('khachhang_doitac'); ?></a></li>
                                        <li class="<?php echo $active_menu=='thuvien'?'fromm-active':'' ?>"><a class="text-uppercase open-san-regular" href="<?php  echo site_url('thu-vien'); ?>"><?php echo $this->lang->line('thuvien'); ?></a></li>
                                        <li class="<?php echo $active_menu=='tuyendung'?'fromm-active':'' ?>"><a class="text-uppercase open-san-regular" href="<?php  echo site_url('tuyen-dung'); ?>"><?php echo $this->lang->line('tuyendung'); ?></a></li>
                                        <li class="<?php echo $active_menu=='hoidap'?'fromm-active':'' ?>"><a class="text-uppercase open-san-regular" href="<?php  echo site_url('hoi-dap'); ?>"><?php echo $this->lang->line('hoidap'); ?></a></li>
                                        <li class="<?php echo $active_menu=='lienhe'?'fromm-active':'' ?>"><a class="text-uppercase open-san-regular" href="<?php  echo site_url('lien-he') ?>"><?php echo $this->lang->line('lienhe'); ?></a></li>
                                    </ul>
                                </div>
                                <div class="multilanguage-box clearfix">
                                    <ul class="multilanguage-ul clearfix">
                                        <li class="clearfix">
                                            
                                                <?php
                                                    if($lang == 'vn'){
                                                        echo '<span class="vn-flag"></span><a>';
                                                        echo 'Tiếng Việt</a>';
                                                    }
                                                    else if($lang == 'en'){
                                                        echo '<span class="usa-flag"></span><a>';
                                                        echo 'English</a>';
                                                    }
                                                    else{
                                                        echo '<span class="de-flag"></span><a>';
                                                        echo 'Germany</a>';
                                                    }
                                                 ?>
                                            <span class="caret caret-icon"></span>
                                            <ul class="clearfix sub-language">
                                                <li class="clearfix"><span class="vn-flag"></span><a href="<?php echo base_url('vn') ?>">Tiếng Việt</a></span></li>
                                                <li class="clearfix"><span class="usa-flag"></span><a href="<?php echo base_url('en') ?>">English</a></span></li>
                                                <li class="clearfix"><span class="de-flag"></span><a href="<?php echo base_url('de') ?>">Germany</a></span></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </nav>
            </header>
        </div>

        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="container-fluid disable-padding banner-box">
            <div class="row">
                <div class="col-md-12 disable-padding">
                    <div class="fromm-slider">
                        <ul class="slides">
                            <?php
                                if(isset($slider) && !empty($slider)){
                                    foreach ($slider as $key => $value) {
                                        ?>
                                            <li style="position: relative;">
                                                <a href="<?php echo $value['link'] ?>">
                                                    <img class="img-maxsize" src="<?php echo $value['image'] ?>" alt="<?php echo $value['title_'.$lang]; ?>" title="<?php echo $value['title_'.$lang]; ?>" />
                                                    <div class="slide_text">
                                                        <div class="container">
                                                            <div class="slide_title">Sức mạnh từ công nghệ Đức</div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        <?php
                                    }
                                }

                            ?>
                            
                        </ul>
                        
                    </div>
                </div>
            </div>
        </div>




