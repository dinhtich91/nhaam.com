<?php
    $allFooter = $this->fromm->getFooter();
?>
<div class="container-fluid disable-padding">
            <footer class="footer-box">
                <div class="container disable-padding footer-part-1">
                    <div class="row disable-margin">
                        <div class="col-md-6 disable-padding">
                            <div class="address-info">
                                <?php
                                    if(isset($allFooter['title_'.$lang])){
                                        echo $allFooter['title_'.$lang];
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-3 disable-padding">
                            <div class="phone-box">
                                <?php
                                    if(isset($allFooter['footer1_'.$lang])){
                                        echo $allFooter['footer1_'.$lang];
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-3 disable-padding">
                            <div class="mail-box">
                                <?php
                                    if(isset($allFooter['footer2_'.$lang])){
                                        echo $allFooter['footer2_'.$lang];
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container disable-padding border-footer footer-copyright-desktop">
                    <p class="address-parts text-center copyright-footer-box">Copyright © 2016 Fromm.com. All rights reserved | Designed by SGD</p>
                </div>
                <div class="container disable-padding border-footer footer-copyright-mobile">
                    <p class="address-parts text-center copyright-footer-box">Copyright © 2016 Fromm.com. All rights reserved</p><p class="address-parts text-center copyright-footer-box">Designed by SGD</p>
                </div>
            </footer>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>