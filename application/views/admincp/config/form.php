<?php
$title_web = isset($detail['title_web']) ? $detail['title_web'] : "";
$meta_description = isset($detail['meta_description']) ? $detail['meta_description'] : "";
$meta_keywords = isset($detail['meta_keywords']) ? $detail['meta_keywords'] : "";
$email = isset($detail['email']) ? $detail['email'] : "";
$smtp_user = isset($detail['smtp_user']) ? $detail['smtp_user'] : "";
$smtp_pass = isset($detail['smtp_pass']) ? $detail['smtp_pass'] : "";
$phone = isset($detail['phone']) ? $detail['phone'] : "";
$nick_yahoo = isset($detail['nick_yahoo']) ? $detail['nick_yahoo'] : "";
$hotline = isset($detail['hotline']) ? $detail['hotline'] : "";
$facebook = isset($detail['facebook']) ? $detail['facebook'] : "";
$support_phone = isset($detail['support_phone']) ? $detail['support_phone'] : "";
$support_email = isset($detail['support_email']) ? $detail['support_email'] : "";
$address = isset($detail['address']) ? $detail['address'] : "";
$image_home = isset($detail['image_home']) ? $detail['image_home'] : "";
?>

<?= form_open(); ?>
<?= form_close(); ?>
<div id="content">
    <div class="breadcrumb">
        <br />        
    </div>
    <form id="form" action="" onsubmit="return check_input();" method="post" enctype="multipart/form-data" name="LISTFORM">
        <div class="box">
            <div class="left"></div>
            <div class="right"></div>
            <div class="heading">
                <h1 style="background-image: url('access/image/review.png');">
                    <?= $title_header; ?>
                </h1>
                <div class="buttons" style="float:right;">
                    <input type="submit" value="Lưu lại" class="button_v1">              
                </div>
            </div>
            <div class="content">
                <?php
                $messages = $this->messages->get();
                if (is_array($messages)):
                    foreach ($messages as $type => $msgs):
                        if (count($msgs > 0)):
                            foreach ($msgs as $message):
                                echo ('<div id="messages"><div class="' . $type . '">' . $message . '</div></div> ');
                            endforeach;
                        endif;
                    endforeach;
                endif;
                ?>   
                <div id="tabs" class="htabs">
                    <a tab="#tab_general">TỔNG QUAN</a>
                    <a tab="#tab_data">EMAIL</a>
                </div>
                <!--####-->
                <div id="tab_general">                
                    <div id="language1">
                        <table class="form">

                            <tr>
                                <td>Title:
                                    <span class="required">*</span>
                                    <div class="comments">Tiêu đề mặc định</div>
                                </td>
                                <td><input class="width_50" name="title_web" value="<?= $title_web; ?>"/>
                                </td>
                            </tr>

                            <tr>
                                <td>Meta Description:<span class="required">*</span></td>
                                <td><textarea name="meta_description" cols="100" rows="5"><?= $meta_description; ?></textarea></td>
                            </tr>

                            <tr>
                                <td>Meta keyword:
                                    <span class="required">*</span>
                                    <div class="comments">(Sử dụng dấu phẩy nếu có nhiều từ khóa ",")</div>
                                </td>
                                <td><textarea name="meta_keywords" cols="100" rows="5"><?= $meta_keywords; ?></textarea></td>
                            </tr>
                            <tr>
                                <td>Hình nhà máy: <div class="comments">Kích thước: 1628x405 pixels</div></td>
                                <td>
                                    <?php
                                    if (is_file($image_home)) {
                                        ?>
                                        <img src="<?= $image_home; ?>" width="100"/><br/>                                    
                                    <?php } ?>
                                    <input type="file" name="userfile" size="20" />
                                    <input type="hidden" name="temp_img" value="<?= $image_home; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Facebook</td>
                                <td><input class="width_50" name="phone" value="<?= $phone; ?>"/></td>
                            </tr>
                            <tr>
                                <td>G+</td>
                                <td><input class="width_50" name="facebook" value="<?= $facebook; ?>"/></td>
                            </tr>
                            <tr>
                                <td>Địa chỉ:</td>
                                <td><input class="width_50" name="address" value="<?= $address; ?>"/></td>
                            </tr>
                            <tr>
                                <td>Hotline:</td>
                                <td><input class="width_50" name="hotline" value="<?= $hotline; ?>"/></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!--####-->
                <div id="tab_data">
                    <table class="form">

                        <tr>
                            <td>Email nhận:</td>
                            <td><input name="email" value="<?= $email; ?>" size="50"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $.tabs('#tabs a');
    </script>
</div>
<script type="text/javascript" src="access/js/form.js"></script>
<script>

</script>
