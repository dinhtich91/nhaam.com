<li><a href="<?= PATH_FOLDER_ADMIN ?>/gioithieu" class="top">Giới thiệu</a></li>

<li>
    <a class="top">Slider</a>
    <ul>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/slide">Slider</a></li>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/nhatieubieu">Nhà tiêu biểu</a></li>
        <!-- <li><a href="<?= PATH_FOLDER_ADMIN ?>/image_home">Ảnh Trang chủ</a></li> -->
    </ul>
</li>
<li>
    <a class="top">Nhà ấm dân dụng</a>
    <ul>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/nhaamdandung">Nhà ấm dân dụng</a></li>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/nhaamdandung_category">Danh mục nhà dân dụng</a></li>
    </ul>
</li>
<li>
    <a class="top">Nhà ấm công nghiệp</a>
    <ul>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/nhaamcongnghiep">Nhà ấm công nghiệp</a></li>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/nhaamcongnghiep_category">Danh mục nhà ấm CN</a></li>
    </ul>
</li>
<li>
    <a class="top">Thư viện</a>
    <ul>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/thuvien">Thư viện</a></li>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/thuvien_category">Danh mục thư viện</a></li>
    </ul>
</li>
<li>
    <a class="top">Tin Tức</a>
    <ul>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/tintuc">Tin Tức</a></li>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/tintuc_category">Danh mục Tin Tức</a></li>
    </ul>
</li>
<!-- <li><a href="<?= PATH_FOLDER_ADMIN ?>/shortcode" class="top">Shortcode</a></li>
<li>
    <a class="top">Giới thiệu</a>
    <ul>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/slider_about">Slider hình</a></li>                
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/staticbaiviet/edit/gioi-thieu">Giới thiệu</a></li>
    </ul>
</li> -->


<li>
    <a class="top">Khách hàng & Đối tác</a>
    <ul>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/doitac">Khách hàng & Đối tác</a></li>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/doitac_category">Danh mục KH & ĐT</a></li>
    </ul>
</li>


<?php
//$count11 = $this->user->checkUserPermission($this->session->userdata('idAdmin'), "news");
//$count22 = $this->user->checkUserPermission($this->session->userdata('idAdmin'), "news_category");

$count11 = 0;
$count22 = 0;
?>
<li>
    <?php
    if ($count11 + $count22 > 0) {
        ?>
        <a class="top">Tin tức</a>
    <?php } ?>
    <ul>
        <?php
        if ($this->user->checkUserPermission($this->session->userdata('idAdmin'), "news") > 0) {
            ?>
            <li>
                <a>Tin tức</a>
                <ul>
                    <?php
                    $getCatNews = $this->user->getCatNews(0);
                    foreach ($getCatNews as $row) {
                        ?>
                        <li>
                            <a href="<?= PATH_FOLDER_ADMIN ?>/news/p/<?= $row['id']; ?>">
                                <?= $row['title']; ?>
                            </a>
                            <ul>
                                <?php
                                $getCatNews2 = $this->user->getCatNews($row['id']);
                                foreach ($getCatNews2 as $row2) {
                                    ?>
                                    <li><a href="<?= PATH_FOLDER_ADMIN ?>/news/p/<?= $row2['id']; ?>"><?= $row2['title']; ?></a>
                                    <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php
        if ($this->user->checkUserPermission($this->session->userdata('idAdmin'), "news_category") > 0) {
            ?>
            <li><a href="<?= PATH_FOLDER_ADMIN ?>/news_category">Danh mục tin tức</a></li>
        <?php } ?>
    </ul>
</li>

<!--<li><a href="<?= PATH_FOLDER_ADMIN ?>/orders" class="top">Đơn hàng</a></li>-->

<li><a href="<?= PATH_FOLDER_ADMIN ?>/contact" class="top">Liên hệ</a></li>
<!-- <li>
    <a class="top">Pages</a>
    <ul>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/staticbaiviet/edit/intro">Intro</a></li>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/staticbaiviet/edit/gioi-thieu">Giới thiệu</a></li>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/staticbaiviet/edit/phuong-thuc-giao-hang">Phương thức giao hàng</a></li>
        <li><a href="<?= PATH_FOLDER_ADMIN ?>/staticbaiviet/edit/phuong-thuc-thanh-toan">Phương thức thanh toán</a></li>
    </ul>
</li> -->
<?php
if ($this->session->userdata('levelAdmin') == 3) {
    ?>
    <li>
        <a class="top">Cấu hình</a>
        <ul>
            <li><a href="<?= PATH_FOLDER_ADMIN ?>/config">Cấu hình hệ thống</a></li>                
            <li><a href="<?= PATH_FOLDER_ADMIN ?>/administrator">Quản trị hệ thống</a></li>
            <li><a href="<?= PATH_FOLDER_ADMIN ?>/staticbaiviet/edit/cauhinh-footer">Cấu hình footer</a></li>                 
<!--            <li><a href="<?= PATH_FOLDER_ADMIN ?>/permission_group">Nhóm quyền</a></li>-->
<!--            <li><a href="<?= PATH_FOLDER_ADMIN ?>/slider_position">Nhóm Slider</a></li>-->
        </ul>
    </li>
    <?php
}?>