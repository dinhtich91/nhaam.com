<div id="content">
    <div class="box">
        <div class="left"></div>
        <div class="right"></div>
        <div class="heading">
            <h1 style="background-image: url('access/image/review.png');">
                <a href="<?= $task_list; ?>"><?= $title_header; ?> (Total: <?= $total_rows; ?>)</a></h1>
            <div class="buttons" style="float:right;">
                <button onclick="return Question_Delete_All();">Xóa chọn</button>                
            </div>
        </div>
        <div class="content">
            <?php
            $messages = $this->messages->get();
            if (is_array($messages)):
                foreach ($messages as $type => $msgs):
                    if (count($msgs > 0)):
                        foreach ($msgs as $message):
                            echo ('<div id="messages"><div class="' . $type . '">' . $message . '</div></div> ');
                        endforeach;
                    endif;
                endforeach;
            endif;
            ?>   
            <form name="LISTFORM" id="LISTFORM" method="post" action="<?= $action_form; ?>" enctype="multipart/form-data">                
                <table class="list">
                    <thead>
                        <tr>
                            <td width="20" style="text-align: center;">
                                <input onclick="javascript: selectAll(this.checked);" type="checkbox" name="checkall"/>
                            </td>
                            <td class="left" width="150">Tên</td>
                            <td class="left" width="150">Chủ đề</td>                                                                                         
                            <td class="left">Nội dung</td>
                            <td class="center" width="130">Ngày nhận</td>
                            <td class="center" width="40">Duyệt</td>
                            <td class="right"  width="140">Thao tác</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($list as $row) {
                            $id = $row['id'];
                            $name = $row['name'];
                            $subject = $row['subject'];
                            $message = $row['message'];
                            $status = $row['status'];
                            $link = $row['link'];
                            $add_date = date("d/m/Y | H:i A", strtotime($row['add_date']));
                            $img_status = $status == 1 ? STATUS_1 : STATUS_0;

                            $detail = $this->comment->getName("message", "comment", "parent", $id);
                            ?>
                            <tr>
                                <td style="text-align: center;"><input  type="checkbox" name="del[]" value="<?= $id; ?>"> </td>
                                <td class="left"><a href="<?= $task_edit; ?>/<?= $id; ?>"><strong><?= $name; ?></strong></a></td>
                                <td class="left"><?= $subject; ?></td>
                                <td class="left">
                                    <?= $message; ?><br/>
                                    <em><strong>Trả lời: <?= $detail; ?></strong></em><br/><br/>
                                    <strong>Bài viết: </strong>
                                    <a target="_blank" href="<?= $link; ?>"><?= $link; ?></a>
                                </td>
                                <td class="left"><?= $add_date; ?></td>
                                <td class="center"><img id="status" status="<?= $status; ?>" onclick="changeStatus(this, '<?= $id; ?>', 'status');" src="<?= $img_status; ?>"/></td>
                                <td class="right">
                                    [<a class="fancybox fancybox.iframe" href="<?= $task_list; ?>/replay/<?= $id; ?>">Trả lời</a> ] - 
                                    [<a href="<?= $task_edit; ?>/<?= $id; ?>">Sửa</a> ] - 
                                    [<a onclick="Question_Delete('<?= $task_del; ?>/<?= $id; ?>');">Xóa</a> ]
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </form>
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="access/js/form.js"></script>
<script type="text/javascript">
                                        function changeStatus(this_, id, field) {
                                            var status = $(this_).attr("status");
                                            $.ajax({
                                                url: '<?= $task_status; ?>/' + id + '/' + status + '/' + field,
                                                type: "get",
                                                beforeSend: function () {
                                                    $(this_).attr("src", '<?= IMG_LOADING; ?>');
                                                },
                                                success: function (data) {
                                                    $(this_).attr("status", data)
                                                    if (data == 1)
                                                        $(this_).attr("src", '<?= STATUS_1; ?>');
                                                    else if (data == 0)
                                                        $(this_).attr("src", '<?= STATUS_0; ?>');

                                                }
                                            });
                                        }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        // Popup iframe detail products
        $(".fancybox").fancybox({
            fitToView: false,
            width: '90%',
            height: '90%',
            autoSize: false,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none',
            afterClose: function () {
                location.reload();
                return;
            }
        });
    })
</script>
