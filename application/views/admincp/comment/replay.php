<?php
$message = isset($detail['message']) ? $detail['message'] : "";
$link = isset($detail['link']) ? $detail['link'] : "";
$detailReplay = isset($detailReplay['message']) ? $detailReplay['message'] : "";
?>
<style>
    .item-photo{width: 100px;float: left;margin: 5px;border: 1px solid #ccc;padding: 5px;text-align: center;}
</style>
<div id="content">
    <form id="form" action="" onsubmit="return check_input();" method="post" enctype="multipart/form-data" name="LISTFORM">
        <div class="box">
            <div class="left"></div>
            <div class="right"></div>
            <div class="heading">
                <h1 style="background-image: url('access/image/review.png');">
                    <?= $title_header; ?>
                </h1>
                <div class="buttons" style="float:right;">
                    <input type="submit" value="Lưu lại" class="button_v1">   
                    <input onclick="return Question_Cancel('<?= $task_list; ?>');" type="button" value="Hủy bỏ" class="button_v1">
                </div>
            </div>
            <div class="content">
                <?php
                $messages = $this->messages->get();
                if (is_array($messages)):
                    foreach ($messages as $type => $msgs):
                        if (count($msgs > 0)):
                            foreach ($msgs as $message):
                                echo ('<div id="messages"><div class="' . $type . '">' . $message . '</div></div> ');
                            endforeach;
                        endif;
                    endforeach;
                endif;
                ?>
                <div id="tab_general">                   
                    <div id="language1">
                        <table class="form">
                            <input type="hidden" name="parent" value="<?=$parent;?>"/>
                            <tr>
                                <td>Link bài viết:<span class="required">*</span></td>
                                <td><a href="<?=$link;?>" target="_blank"><?= $link; ?></a></td>
                            </tr>
                            <tr>
                                <td>Nội dung câu hỏi:<span class="required">*</span></td>
                                <td><?= $message; ?></td>
                            </tr>
                            <tr>
                                <td>Nội dung trả lời:<span class="required">*</span></td>
                                <td>
                                    <textarea name="message"><?= $detailReplay; ?></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>              
            </div>
        </div>
    </form>
</div>