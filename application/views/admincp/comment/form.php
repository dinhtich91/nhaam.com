<?php
$id_product = isset($detail['id']) ? $detail['id'] : 0;
$name = isset($detail['name']) ? stripslashes($detail['name']) : "";
$status = isset($detail['status']) ? $detail['status'] : 1;
$subject = isset($detail['subject']) ? $detail['subject'] : "";
$email = isset($detail['email']) ? $detail['email'] : "";
$message = isset($detail['message']) ? $detail['message'] : "";
$type = isset($detail['type']) ? $detail['type'] : 0;
?>
<style>
    .item-photo{width: 100px;float: left;margin: 5px;border: 1px solid #ccc;padding: 5px;text-align: center;}
</style>
<div id="content">
    <form id="form" action="" onsubmit="return check_input();" method="post" enctype="multipart/form-data" name="LISTFORM">
        <div class="box">
            <div class="left"></div>
            <div class="right"></div>
            <div class="heading">
                <h1 style="background-image: url('access/image/review.png');">
                    <?= $title_header; ?>
                </h1>
                <div class="buttons" style="float:right;">
                    <input type="submit" value="Lưu lại" class="button_v1">   
                    <input onclick="return Question_Cancel('<?= $task_list; ?>');" type="button" value="Hủy bỏ" class="button_v1">
                </div>
            </div>
            <div class="content">
                <?php
                $messages = $this->messages->get();
                if (is_array($messages)):
                    foreach ($messages as $type => $msgs):
                        if (count($msgs > 0)):
                            foreach ($msgs as $message):
                                echo ('<div id="messages"><div class="' . $type . '">' . $message . '</div></div> ');
                            endforeach;
                        endif;
                    endforeach;
                endif;
                ?>
                <div id="tab_general">                   
                    <div id="language1">
                        <table class="form">
                            <tr>
                                <td>Họ tên:<span class="required">*</span>                                    
                                </td>
                                <td><input name="name" value='<?= $name; ?>' class="width_50"/></td>
                            </tr>
                            <tr>
                                <td>Chủ đề:<span class="required">*</span>                                    
                                </td>
                                <td><input name="subject" value='<?= $subject; ?>' class="width_50"/></td>
                            </tr>
                            <tr>
                                <td>Email:<span class="required">*</span>                                    
                                </td>
                                <td><input name="email" value='<?= $email; ?>' class="width_50"/></td>
                            </tr>
                            <tr>
                                <td>Nội dung:<span class="required">*</span></td>
                                <td>
                                    <textarea name="message"><?=$message;?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>Duyệt:</td>
                                <td>
                                    <label><input type="radio" value="1" name="status" <?= $status == 1 ? "checked='checked'" : "" ?> /> Có</label>
                                    <label><input type="radio" value="0" name="status" <?= $status == 0 ? "checked='checked'" : "" ?>/> Không</label>
                                </td>
                            </tr>  
                        </table>
                    </div>
                </div>              
            </div>
        </div>
    </form>
</div>