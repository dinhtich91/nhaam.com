<?php
$avatar = isset($detail['avatar']) ? $detail['avatar'] : "";
$title_vn = isset($detail['title_vn']) ? $detail['title_vn'] : "";
$title_en = isset($detail['title_en']) ? $detail['title_en'] : "";
$title_ge = isset($detail['title_ge']) ? $detail['title_ge'] : "";

$sumary_vn = isset($detail['sumary_vn']) ? stripslashes($detail['sumary_vn']) : "";
$sumary_en = isset($detail['sumary_en']) ? stripslashes($detail['sumary_en']) : "";
$sumary_ge = isset($detail['sumary_ge']) ? stripslashes($detail['sumary_ge']) : "";

$detail_vn = isset($detail['detail_vn']) ? $detail['detail_vn'] : "";
$detail_en = isset($detail['detail_en']) ? $detail['detail_en'] : "";
$detail_ge = isset($detail['detail_ge']) ? $detail['detail_ge'] : "";

$status = isset($detail['status']) ? $detail['status'] : "1";
$ordering = isset($detail['ordering']) ? $detail['ordering'] : $orderingMax;
// $id_cat = isset($detail['id_cat']) ? $detail['id_cat'] : 0;
$is_hot = isset($detail['is_hot']) ? $detail['is_hot'] : 0;
$is_home = isset($detail['is_home']) ? $detail['is_home'] : 0;

?>
<div id="content">
    <form id="form" action="" onsubmit="return check_input();" method="post" enctype="multipart/form-data" name="LISTFORM">
        <div class="box">
            <div class="left"></div>
            <div class="right"></div>
            <div class="heading">
                <h1 style="background-image: url('access/image/review.png');">
                    <?= $title_header; ?>
                </h1>
                <div class="buttons" style="float:right;">
                    <input type="submit" value="Lưu lại" class="button_v1">               
                    <input onclick="return Question_Cancel('<?= $task_list; ?>');" type="button" value="Hủy bỏ" class="button_v1">
                </div>
            </div>
            

            <div class="content"> 
                <div id="tabs" class="htabs">
                    <a tab="#tab_general_vn"><?= IMG_VN ?></a>
                    <a tab="#tab_general_en"><?= IMG_EN ?></a>
                </div>
                <div id="tab_general_vn">                   
                    <div id="language1">
                        <table class="form">                            
                            <tr>
                                <td>Tiêu đề:</td>
                                <td><input type="text" name="title_vn" value="<?= $title_vn; ?>" size="100" />
                                </td>
                            </tr>
                            <tr>
                                <td>Danh mục:</td>
                                <td>
                                    <select name="id_cat">
                                        <?php
                                        $listCat = $this->news->listCat(0);
                                        foreach ($listCat as $row) {
                                            ?>
                                            <option <?= $id_cat == $row['id'] ? "selected" : ""; ?> value="<?= $row['id']; ?>">
                                                <?= $row['title_vn']; ?> [<?= $row['title_en']; ?>]
                                            </option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Video:
                                    <div class="comments"> ( Áp dụng với Clip )</div>
                                </td>
                                <td><input type="text" name="video" value="<?= $video; ?>" size="100" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Hình ảnh đại diện
                                    <div class="comments">Size: 365X210px (png,jpg)</div>
                                </td>
                                <td>
                                    <?php
                                    if (is_file($avatar)) {
                                        ?>
                                        <img src="<?= $avatar; ?>" width="100"/><br/>
                                    <?php } ?>
                                    <input type="file" name="userfile" size="20" />                                    
                                    <input name="temp_img" type="hidden" value="<?= $avatar; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Mô tả: </td>
                                <td><textarea name="sumary_vn"><?php echo $sumary_vn; ?></textarea></td>
                            </tr>
                            <tr>
                                <td>Nội dung: </td>
                                <td><?= $this->function->display_CKEditor("detail_vn", stripslashes($detail_vn), 250); ?></td>
                            </tr>   
                            <tr>
                                <td> Thứ tự:</td>
                                <td><input name="ordering" value="<?= $ordering; ?>" size="1" />
                                </td>
                            </tr>
                            <tr>
                                <td>Tình trạng:</td>
                                <td>
                                    <label><input type="radio" value="1" name="status" <?= $status == 1 ? "checked='checked'" : "" ?> /> Hiển thị</label>
                                    <label><input type="radio" value="0" name="status" <?= $status == 0 ? "checked='checked'" : "" ?>/> Ẩn</label>
                                </td>
                            </tr>                          
                        </table>
                    </div>
                </div>

                <div id="tab_general_en">                   
                    <div id="language1">
                        <table class="form">                            
                            <tr>
                                <td>Tiêu đề:</td>
                                <td><input name="title_en" value="<?= $title_en; ?>" size="100" />
                                </td>
                            </tr>
                            <tr>
                                <td>Mô tả: </td>
                                <td><textarea name="sumary_en"><?php echo $sumary_en; ?></textarea></td>
                            </tr>
                            <tr>
                                <td>Chi tiết: </td>
                                <td><?= $this->function->display_CKEditor("detail_en", stripslashes($detail_en), 250); ?></td>
                            </tr>                         
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $.tabs('#tabs a');
    </script>
</div>
<script type="text/javascript" src="access/js/form.js"></script>
