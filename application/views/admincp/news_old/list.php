<div id="content">
    <div class="box">
        <div class="left"></div>
        <div class="right"></div>
        <div class="heading">
            <h1 style="background-image: url('access/image/review.png');">
                <a href="<?= $task_list; ?>"><?= $title_header; ?> (Total: <?= $total_rows; ?>)</a></h1>
            <div class="buttons" style="float:right;">
                <button onclick="return Question_Update();">Cập nhật vị trí</button>
                <button onclick="return Question_Delete_All();">Xóa chọn</button>                
                <a href="<?= $task_add; ?>"><button>Thêm</button></a>
            </div>
        </div>
        <div class="content">
            <?php
            $messages = $this->messages->get();
            if (is_array($messages)):
                foreach ($messages as $type => $msgs):
                    if (count($msgs > 0)):
                        foreach ($msgs as $message):
                            echo ('<div id="messages"><div class="' . $type . '">' . $message . '</div></div> ');
                        endforeach;
                    endif;
                endforeach;
            endif;
            ?>   
            <form name="LISTFORM" id="LISTFORM" method="post" action="<?= $action_form; ?>" enctype="multipart/form-data">                
                <table class="list">
                    <thead>
                        <tr>
                            <td width="20" style="text-align: center;">
                                <input onclick="javascript: selectAll(this.checked);" type="checkbox" name="checkall"/>
                            </td>
                            <td class="center" width="50">Vị trí</td>
                            <td class="left" width="50">Hình ảnh</td>
                            <td class="left">Tiêu đề</td>                                                                                         
<!--                            <td class="left" width="110">Chuyên mục</td>-->
                            <td class="center" width="110">Nổi bật</td>
                            <td class="center" width="110">Tình trạng</td> 
                            <td class="right"  width="200">Thao tác</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($list as $row) {
                            $id = $row['id'];
                            $title = $row['title'];
                            $status = $row['status'];
                            $img_status = $status == 1 ? STATUS_1 : STATUS_0;
                            $is_hot = $row['is_hot'];
                            $img_is_hot = $is_hot == 1 ? STATUS_1 : STATUS_0;
                            $avatar = $row['avatar'];
                            $ordering = $row['ordering'];
                            $nameCat = $this->news->getName("title", "news_category", "id", $row['id_cat']);
                            $linkCat = $this->news->getName("tag", "news_category", "id", $row['id_cat']);
                            ?>
                            <tr>
                                <td style="text-align: center;"><input  type="checkbox" name="del[]" value="<?= $id; ?>"> </td>
                                <td class="center"><input type="text" name="ordering[<?= $id; ?>]" value="<?= $ordering; ?>" size="50"/></td>
                                <td class="left"><a href="<?= $task_edit; ?>/<?= $id; ?>"><img src="<?= $avatar; ?>" width="50"/></a></td>
                                <td class="left"><a href="<?= $task_edit; ?>/<?= $id; ?>"><strong><?= $title; ?></strong></a></td>
<!--                                <td class="left"><?= $nameCat; ?></td>-->
                                <td class="center"><img id="is_hot" status="<?= $is_hot; ?>" onclick="changeStatus(this, '<?= $id; ?>', 'is_hot');" src="<?= $img_is_hot; ?>"/></td>
                                <td class="center"><img id="status" status="<?= $status; ?>" onclick="changeStatus(this, '<?= $id; ?>', 'status');" src="<?= $img_status; ?>"/></td>
                                <td class="right">
                                    [ <a target="_blank" href="<?= $linkCat; ?>/<?= $row['tag']; ?>">Xem</a> ] - 
                                    [ <a href="<?= $task_edit; ?>/<?= $id; ?>">Chỉnh sửa</a> ] - 
                                    [ <a onclick="Question_Delete('<?= $task_del; ?>/<?= $id; ?>');">Xóa</a> ]
                                </td>
                            </tr>
                        <? } ?>
                    </tbody>
                </table>
            </form>
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="access/js/form.js"></script>
<script type="text/javascript">
                                        function changeStatus(this_, id, field) {
                                            var status = $(this_).attr("status");
                                            $.ajax({
                                                url: '<?= $task_status; ?>/' + id + '/' + status + '/' + field,
                                                type: "get",
                                                beforeSend: function () {
                                                    $(this_).attr("src", '<?= IMG_LOADING; ?>');
                                                },
                                                success: function (data) {
                                                    $(this_).attr("status", data)
                                                    if (data == 1)
                                                        $(this_).attr("src", '<?= STATUS_1; ?>');
                                                    else if (data == 0)
                                                        $(this_).attr("src", '<?= STATUS_0; ?>');

                                                }
                                            });
                                        }
</script>
