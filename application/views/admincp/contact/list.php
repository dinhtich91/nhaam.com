<div id="content">
    <div class="breadcrumb">
        <br />

    </div>
    <div class="box">
        <div class="left"></div>
        <div class="right"></div>
        <div class="heading">
            <h1 style="background-image: url('access/image/review.png');">
                <a href="<?= $task_list; ?>"><?= $title_header; ?> (Tổng số: <?= $total_rows; ?>)</a></h1>
            <div class="buttons" style="float:right;">
                <!--<button onclick="return Question_Update();">Cập nhật vị trí</button>-->
                <button onclick="return Question_Delete_All();">Xóa chọn</button>                
                <!--<a href="<?= $task_add; ?>"><button>Thêm</button></a>-->
            </div>
        </div>
        <div class="content">
            <?php
            $messages = $this->messages->get();
            if (is_array($messages)):
                foreach ($messages as $type => $msgs):
                    if (count($msgs > 0)):
                        foreach ($msgs as $message):
                            echo ('<div id="messages"><div class="' . $type . '">' . $message . '</div></div> ');
                        endforeach;
                    endif;
                endforeach;
            endif;
            ?>   
            <form name="LISTFORM" id="LISTFORM" method="post" action="<?= $action_form; ?>" enctype="multipart/form-data">                
                <table class="list">
                    <thead>
                        <tr>
                            <td width="1" style="text-align: center;">
                                <input onclick="javascript: selectAll(this.checked);" type="checkbox" name="checkall"/>
                            </td>
                            <td class="left" width="10%">Họ tên</td>
                            <td class="left" width="15%">Email</td>                                                 
                            <td class="center" width="7%">Số điện thoại</td>
                            <td class="center">Địa chỉ</td>
                            <td class="right">Nội dung</td>
                            <td class="right" width="9%;">Ngày</td>
                            <td class="right"  width="9%;">Thao tác</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($list as $row) {
                            $id = $row['id'];
                            $name = $row['name'];
                            $email = $row['email'];
                            $phone = $row['phone'];
                            $title = $row['title'];
                            $contact = $this->function->cut_string($row['contact'], 50);
                            $add_date = date("d-m-Y | H:i", strtotime($row['add_date']));
                            ?>
                            <tr>
                                <td style="text-align: center;"><input  type="checkbox" name="del[]" value="<?= $id; ?>"> </td>
                                <td class="left"><?= $name; ?></td>
                                <td class="left"><?= $email; ?></td>                                
                                <td class="center"><?= $phone; ?></td>
                                <td class="center"><?= $title; ?></td>
                                <td class="right"><?= $contact; ?></td>
                                <td class="right"><?= $add_date; ?></td>
                                <td class="right">[<a onclick="showme('<?= $task_edit; ?>/<?= $id; ?>');">Xem nội dung</a>] - [<a onclick="Question_Delete('<?= $task_del; ?>/<?= $id; ?>');">Xóa</a>]</td>
                            </tr> 
                        <?php } ?>
                    </tbody>
                </table>
            </form>
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="access/js/form.js"></script>
<script type="text/javascript">
                                function changeStatus(this_, id, field) {
                                    var status = $(this_).attr("status");
                                    $.ajax({
                                        url: '<?= $task_status; ?>/' + id + '/' + status + '/' + field,
                                        type: "get",
                                        beforeSend: function () {
                                            $(this_).attr("src", '<?= IMG_LOADING; ?>');
                                        },
                                        success: function (data) {
                                            $(this_).attr("status", data)
                                            if (data == 1)
                                                $(this_).attr("src", '<?= STATUS_1; ?>');
                                            else if (data == 0)
                                                $(this_).attr("src", '<?= STATUS_0; ?>');

                                        }
                                    });
                                }
</script>
