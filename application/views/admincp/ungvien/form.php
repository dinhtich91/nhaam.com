<title>Chi tiết đơn hàng</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<table border="0" cellpadding="0" cellspacing="1" class="tb_order_info table-bordered" style="box-sizing: border-box; border-spacing: 0px; border-collapse: collapse; margin: 20px 0px; padding: 0px; border-style: solid; border-color: rgb(221, 221, 221); font-size: 14px; vertical-align: baseline; clear: both; color: rgb(51, 51, 51); font-family: myriadpro-semiext, Arial, sans-serif; background: rgb(194, 194, 194);width: 100%">
    <tbody style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">
        <tr style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">
            <th align="left" style="box-sizing: border-box; padding: 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: bottom; color: rgb(85, 85, 85); width: 495px; background: rgb(247, 247, 247);" valign="middle">
                <strong style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">M&atilde; đơn h&agrave;ng: 3262163</strong></th>
            <th align="right" style="box-sizing: border-box; padding: 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: bottom; color: rgb(85, 85, 85); width: 494px; background: rgb(247, 247, 247);" valign="top">
                <strong style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">Thời gian mua h&agrave;ng:&nbsp;</strong>07/12/2015&nbsp;<br style="box-sizing: border-box;" />
                <strong style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">Ng&agrave;y giao h&agrave;ng:&nbsp;</strong>12/12/2015 v&agrave;o l&uacute;c 10h</th>
        </tr>
        <tr style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">
            <th align="center" style="box-sizing: border-box; padding: 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: bottom; color: rgb(85, 85, 85); background: rgb(247, 247, 247);" valign="middle">
                Th&ocirc;ng tin li&ecirc;n lạc</th>
            <th align="center" style="box-sizing: border-box; padding: 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: bottom; color: rgb(85, 85, 85); background: rgb(247, 247, 247);" valign="middle">
                Th&ocirc;ng tin người nhận</th>
        </tr>
        <tr style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">
            <td align="left" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);" valign="middle">
                <strong class="title" style="box-sizing: border-box; margin: 0px; padding: 0px 10px 0px 0px; border: 0px; vertical-align: baseline; width: 130px; display: inline-block; float: left; text-align: right; background: transparent;">Họ t&ecirc;n:</strong>Huỳnh Văn Được</td>
            <td align="left" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);" valign="middle">
                <strong class="title" style="box-sizing: border-box; margin: 0px; padding: 0px 10px 0px 0px; border: 0px; vertical-align: baseline; width: 130px; display: inline-block; float: left; text-align: right; background: transparent;">Họ t&ecirc;n:</strong>Hồng Lan</td>
        </tr>
        <tr style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">
            <td align="left" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);" valign="middle">
                <strong class="title" style="box-sizing: border-box; margin: 0px; padding: 0px 10px 0px 0px; border: 0px; vertical-align: baseline; width: 130px; display: inline-block; float: left; text-align: right; background: transparent;">Địa chỉ:</strong>61/7 Đường 10, P Tăng Nhơn Ph&uacute; B, Quận 9</td>
            <td align="left" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);" valign="middle">
                <strong class="title" style="box-sizing: border-box; margin: 0px; padding: 0px 10px 0px 0px; border: 0px; vertical-align: baseline; width: 130px; display: inline-block; float: left; text-align: right; background: transparent;">Địa chỉ:</strong>61/7 Đường 10, P Tăng Nhơn Ph&uacute; B, Quận 9</td>
        </tr>
        <tr style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">
            <td align="left" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);" valign="middle">
                <strong class="title" style="box-sizing: border-box; margin: 0px; padding: 0px 10px 0px 0px; border: 0px; vertical-align: baseline; width: 130px; display: inline-block; float: left; text-align: right; background: transparent;">Điện thoại:</strong>0909 257 034</td>
            <td align="left" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);" valign="middle">
                <strong class="title" style="box-sizing: border-box; margin: 0px; padding: 0px 10px 0px 0px; border: 0px; vertical-align: baseline; width: 130px; display: inline-block; float: left; text-align: right; background: transparent;">Điện thoại:</strong>0909 257 034</td>
        </tr>
        <tr style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">
            <td align="left" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);" valign="middle">
                &nbsp;</td>
            <td align="left" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);" valign="middle">
                <strong class="title" style="box-sizing: border-box; margin: 0px; padding: 0px 10px 0px 0px; border: 0px; vertical-align: baseline; width: 130px; display: inline-block; float: left; text-align: right; background: transparent;">Quận/Huyện:</strong>Q. 9</td>
        </tr>
        <tr style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">
            <td align="left" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);" valign="middle">
                &nbsp;</td>
            <td align="left" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);" valign="middle">
                <strong class="title" style="box-sizing: border-box; margin: 0px; padding: 0px 10px 0px 0px; border: 0px; vertical-align: baseline; width: 130px; display: inline-block; float: left; text-align: right; background: transparent;">Tỉnh/Th&agrave;nh phố:</strong>TP. Hồ Ch&iacute; Minh</td>
        </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="1" class="tb_order_info table-bordered margin_top_15" style="box-sizing: border-box; border-spacing: 0px; border-collapse: collapse; margin: 20px 0px; padding: 0px; border-style: solid; border-color: rgb(221, 221, 221); font-size: 14px; vertical-align: baseline; clear: both; color: rgb(51, 51, 51); font-family: myriadpro-semiext, Arial, sans-serif; background: rgb(194, 194, 194);width: 100%">
    <tbody style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">
        <tr style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">
            <th align="center" style="box-sizing: border-box; padding: 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: bottom; color: rgb(85, 85, 85); width: 20px; background: rgb(247, 247, 247);">
                STT</th>
            <th align="center" style="box-sizing: border-box; padding: 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: bottom; color: rgb(85, 85, 85); background: rgb(247, 247, 247);">
                T&ecirc;n sản phẩm</th>
            <th align="center" style="box-sizing: border-box; padding: 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: bottom; color: rgb(85, 85, 85); width: 120px; background: rgb(247, 247, 247);">
                Image</th>
            <th align="center" style="box-sizing: border-box; padding: 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: bottom; color: rgb(85, 85, 85); width: 148px; background: rgb(247, 247, 247);">
                Đơn gi&aacute;</th>
            <th align="center" style="box-sizing: border-box; padding: 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: bottom; color: rgb(85, 85, 85); width: 148px; background: rgb(247, 247, 247);">
                Số lượng</th>
            <th align="center" style="box-sizing: border-box; padding: 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: bottom; color: rgb(85, 85, 85); width: 148px; background: rgb(247, 247, 247);">
                Tạm t&iacute;nh</th>
        </tr>
        <tr style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">
            <td align="center" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);" valign="middle">
                1</td>
            <td align="left" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);">
                HB-30</td>
            <td align="center" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);">
                <a href="http://localhost/gaucon/hoa-bo/hoa-bo/hb30.html" style="box-sizing: border-box; color: rgb(0, 51, 255); text-decoration: none; margin: 0px; padding: 0px; vertical-align: baseline; -webkit-tap-highlight-color: rgb(252, 215, 0); outline: none; background: transparent;" target="_blank"><img alt="" kasperskylab_antibanner="on" src="http://localhost/gaucon/upload/images/IMG_6679.jpg" style="box-sizing: border-box; border: 0px; vertical-align: top; margin: 0px; padding: 0px; max-width: 100%; background: transparent;" /></a></td>
            <td align="center" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);">
                700,000 vnđ</td>
            <td align="center" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);">
                1</td>
            <td align="right" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);">
                700,000 vnđ</td>
        </tr>
        <tr style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">
            <td align="right" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);" valign="middle">
                <strong style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">Tổng:</strong></td>
            <td align="right" colspan="5" style="box-sizing: border-box; padding: 2px 5px; margin: 0px; border-style: solid; border-color: rgb(221, 221, 221); vertical-align: top; color: rgb(0, 0, 0); background: rgb(255, 255, 255);" valign="middle">
                <strong style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;">700,000 vnđ</strong><br />
                &nbsp;</td>
        </tr>
    </tbody>
</table>



<style>
    .head td{
        background:#C3A684;
        color: white;        
    }
    tr td{
        border-bottom: 1px solid #CCCCCC;
        border-right: 1px solid #CCCCCC;
        padding: 5px 5px;
    }
    table{
        border-left: 1px solid #CCCCCC;
    }
    .color1 td{
        background:#e5e5e5;
    }
    .color2 td{
        background: white;
    }
</style>
<p><hr></p>
<p><strong>Họ tên: </strong><?= $detail['name']; ?></p>
<p><strong>Email: </strong><?= $detail['email']; ?></p>
<p><strong>SĐT: </strong><?= $detail['phone']; ?></p>
<p><strong>Địa chỉ giao: </strong><?= $detail['address1']; ?></p>
<p><strong>Người nhận hàng: </strong><?= $detail['address2'] !== "" ? $detail['address2'] : $detail['name']; ?></p>
<p><strong>Ngày giao hàng: </strong><?= date("d-m-Y", strtotime($detail['date'])) == "01-01-1970" ? "" : date("d-m-Y", strtotime($detail['date'])); ?></p>
<p><strong>Hình thức thanh toán: </strong><?= ($detail['type_payment'] == 0) ? "Trả trước qua ngân hàng" : "Trả khi nhận hàng" ?></p>

<p style="color: #777;"><strong>Ngày đặt hàng: </strong><?= date("d-m-Y | H:i A", strtotime($detail['add_date'])); ?></p>
<p><hr></p>
<p><strong>Danh sách các món hàng: </strong></p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr class="head">
        <td align="center">STT</td>
        <td align="center">Mã số</td>
        <td>Tên hàng</td>
        <td align="center">SL</td>
        <td align="right">Đơn giá</td>
        <td align="right">Thành tiền</td>
        <td>Ghi chú</td>
    </tr>

    <?
    $i = 0;
    $sum_sl = 0;
    $sum_tt = 0;
    foreach ($items as $row) {
        $i++;
        $product = $this->order->getProductByCode($row['code']);
        $code = $row['code'];
        $sl = $row['sl'];
        $note = $row['notes'];
        $price = $product['price'];
        $title = $product['title'];
        $tt = $sl * $price;
        $sum_sl+=$sl;
        $sum_tt+=$tt;
        ?>
        <tr <?= ($i % 2 == 0) ? "class='color1'" : "class='color2'"; ?>>
            <td align="center"><?= $i; ?></td>
            <td align="center"><?= $code; ?></td>
            <td><?= $title; ?></td>
            <td align="center"><?= $sl; ?></td>
            <td align="right"><?= number_format($price, 0, '.', '.'); ?></td>
            <td align="right"><?= number_format($tt, 0, '.', '.'); ?></td>
            <td><?= $note; ?></td>
        </tr>
    <? } ?>
    <tr>
        <td colspan="3" align="center"><strong>Tổng số</strong></td>
        <td align="center"><strong><?= $sum_sl; ?></strong></td>
        <td></td>
        <td align="right"><strong><?= number_format($sum_tt, 0, '.', '.'); ?></strong></td>
        <td></td>
        </td>
</table>
