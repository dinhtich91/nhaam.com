<?php
$image = isset($detail['image']) ? $detail['image'] : "";
$title_vn = isset($detail['title_vn']) ? stripslashes($detail['title_vn']) : "";
$title_en = isset($detail['title_en']) ? stripslashes($detail['title_en']) : "";
$title_de = isset($detail['title_de']) ? stripslashes($detail['title_de']) : "";

$thongso_vn = isset($detail['thongso_vn']) ? stripslashes($detail['thongso_vn']) : "";
$thongso_en = isset($detail['thongso_en']) ? stripslashes($detail['thongso_en']) : "";
$thongso_de = isset($detail['thongso_de']) ? stripslashes($detail['thongso_de']) : "";

$detail_vn = isset($detail['detail_vn']) ? stripslashes($detail['detail_vn']) : "";
$detail_en = isset($detail['detail_en']) ? stripslashes($detail['detail_en']) : "";
$detail_de = isset($detail['detail_de']) ? stripslashes($detail['detail_de']) : "";

$price_vn = isset($detail['price_vn']) ? stripslashes($detail['price_vn']) : "";
$price_en = isset($detail['price_en']) ? stripslashes($detail['price_en']) : "";
$price_de = isset($detail['price_de']) ? stripslashes($detail['price_de']) : "";

$category = isset($detail['id_news_category']) ? stripslashes($detail['id_news_category']) : "";

$desc = isset($detail['desc']) ? stripslashes($detail['desc']) : "";
$link = isset($detail['link']) ? stripslashes($detail['link']) : "#";
$type = isset($detail['type']) ? stripslashes($detail['type']) : "";
$ordering = isset($detail['ordering']) ? $detail['ordering'] : $orderingMax;
$status = isset($detail['status']) ? $detail['status'] : 1;
?>
<div id="content">
    <form id="form" action="" method="post" enctype="multipart/form-data" name="LISTFORM">
        <div class="box">
            <div class="left"></div>
            <div class="right"></div>
            <div class="heading">
                <h1 style="background-image: url('access/image/review.png');">
                    <?= $title_header; ?>
                </h1>
                <div class="buttons" style="float:right;">
                    <input type="submit" value="Lưu lại" class="button_v1">   
                    <input onclick="return Question_Cancel('<?= $task_list; ?>');" type="button" value="Hủy bỏ" class="button_v1">
                </div>
            </div>

            <div class="content"> 
                <?php
                $messages = $this->messages->get();
                if (is_array($messages)):
                    foreach ($messages as $type => $msgs):
                        if (count($msgs > 0)):
                            foreach ($msgs as $message):
                                echo ('<div id="messages"><div class="' . $type . '">' . $message . '</div></div> ');
                            endforeach;
                        endif;
                    endforeach;
                endif;
                ?> 
                <div id="tabs" class="htabs">
                    <a tab="#tab_general_vn"><?= IMG_VN ?></a>
                    <a tab="#tab_general_en"><?= IMG_EN ?></a>
                    <a tab="#tab_general_de"><?= IMG_DE ?></a>
                </div>
                
                <div id="tab_general_vn">                   
                    <div id="language1">
                        <table class="form">                            
                            <tr>
                                <td>Tiêu đề:
                                    <span class="required">*</span>     
                                </td>
                                <td><input type="text" name="title_vn" value="<?= $title_vn; ?>" size="50" />
                                </td>
                            </tr>
                            <tr>
                                <td>Thông tin sản phẩm: </td>
                                <td><?= $this->function->display_CKEditor("detail_vn", stripslashes($detail_vn), 250); ?></td>
                            </tr>
                            <tr>
                                <td>Thông số kỹ thuật: </td>
                                <td><?= $this->function->display_CKEditor("thongso_vn", stripslashes($thongso_vn), 250); ?></td>
                            </tr>
                            <tr>
                                <td>Giá: </td>
                                <td><input type="text" name="price_vn" value="<?= $price_vn; ?>" size="50" />
                                </td>
                            </tr>                               
                            <!-- <tr>
                                <td>Hình: <div class="comments">Kích thước: 180x150 pixels</div></td>
                                <td>
                                    <?php
                                    if (is_file($image)) {
                                        ?>
                                        <img src="<?= $image; ?>" width="100"/><br/>                                    
                                    <?php } ?>
                                    <input type="file" name="userfile" size="20" />
                                    <input type="hidden" name="temp_img" value="<?= $image; ?>"/>
                                </td>
                            </tr> -->
                            <tr>
                                <td> Thứ tự:</td>
                                <td><input name="ordering" value="<?= $ordering; ?>" size="1" />
                                </td>
                            </tr>                           
                            <tr>
                                <td> Trạng thái:</td>
                                <td>
                                    <label><input type="radio" value="1" name="status" <?= $status == 1 ? "checked='checked'" : "" ?> /> Hiển thị </label>
                                    <label><input type="radio" value="0" name="status" <?= $status == 0 ? "checked='checked'" : "" ?>/> Ẩn</label>
                                </td>
                            </tr>                   
                        </table>
                    </div>
                </div>

                <div id="tab_general_en">                   
                    <div id="language1">
                        <table class="form">                            
                            <tr>
                                <td>Tiêu đề:
                                    <span class="required">*</span>     
                                </td>
                                <td><input type="text" name="title_en" value="<?= $title_en; ?>" size="50" />
                                </td>
                            </tr>
                            <tr>
                                <td>Thông tin sản phẩm: </td>
                                <td><?= $this->function->display_CKEditor("detail_en", stripslashes($detail_en), 250); ?></td>
                            </tr>
                            <tr>
                                <td>Thông số kỹ thuật: </td>
                                <td><?= $this->function->display_CKEditor("thongso_en", stripslashes($thongso_en), 250); ?></td>
                            </tr>
                            <tr>
                                <td>Giá: </td>
                                <td><input type="text" name="price_en" value="<?= $price_en; ?>" size="50" />
                                </td>
                            </tr>                       
                        </table>
                    </div>
                </div>

                <div id="tab_general_de">                   
                    <div id="language1">
                        <table class="form">   
                            <tr>
                                <td>Tiêu đề:
                                    <span class="required">*</span>     
                                </td>
                                <td><input type="text" name="title_de" value="<?= $title_de; ?>" size="50" />
                                </td>
                            </tr>
                            <tr>
                                <td>Thông tin sản phẩm: </td>
                                <td><?= $this->function->display_CKEditor("detail_de", stripslashes($detail_de), 250); ?></td>
                            </tr>
                            <tr>
                                <td>Thông số kỹ thuật: </td>
                                <td><?= $this->function->display_CKEditor("thongso_de", stripslashes($thongso_de), 250); ?></td>
                            </tr>
                            <tr>
                                <td>Giá: </td>
                                <td><input type="text" name="price_de" value="<?= $price_de; ?>" size="50" />
                                </td>
                            </tr>                       
                        </table>
                    </div>
                </div>
            </div>
        </div>
            
    </form>
    <script type="text/javascript">
        $.tabs('#tabs a');
    </script>
</div>
<script type="text/javascript" src="access/js/form.js"></script>

