<script type="text/javascript" src="<?= base_url('access/js/ajaxupload.3.5.js'); ?>"></script>
<?php
$id_product = isset($detail['id']) ? $detail['id'] : 0;
$avatar = isset($detail['avatar']) && is_file($detail['avatar']) ? $detail['avatar'] : ICON_UPLOAD_PHOTO;
$title_vn = isset($detail['title_vn']) ? stripslashes($detail['title_vn']) : "";
$title_en = isset($detail['title_en']) ? $detail['title_en'] : "";
$title_de = isset($detail['title_de']) ? $detail['title_de'] : "";
$ordering = isset($detail['ordering']) ? $detail['ordering'] : $orderingMax;
$status = isset($detail['status']) ? $detail['status'] : 1;
$category = isset($detail['cat_id']) ? $detail['cat_id'] : "";
$type = isset($detail['type']) ? $detail['type'] : 0;
$overview = isset($detail['overview']) ? $detail['overview'] : "";
$price = isset($detail['price']) ? $detail['price'] : 0;
$sale = isset($detail['sale']) ? $detail['sale'] : "";
$content = isset($detail['detail']) ? stripslashes($detail['detail']) : "";
$notes = isset($detail['notes']) ? stripslashes($detail['notes']) : "";
$uses = isset($detail['detail']) ? stripslashes($detail['uses']) : "";
$is_popup = isset($detail['is_popup']) ? $detail['is_popup'] : 1;
$is_phukien = isset($detail['is_phukien']) ? $detail['is_phukien'] : 0;
$thanhphan = isset($detail['thanhphan']) ? stripslashes($detail['thanhphan']) : "";
$link = isset($detail['link']) ? stripslashes($detail['link']) : "#";

$detail_vn = isset($detail['detail_vn']) ? stripslashes($detail['detail_vn']) : "";
$detail_en = isset($detail['detail_en']) ? stripslashes($detail['detail_en']) : "";
$detail_de = isset($detail['detail_de']) ? stripslashes($detail['detail_de']) : "";
?>
<style>
    .item-photo{float: left;margin: 5px;border: 1px solid #ccc;padding: 5px;text-align: center;}
</style>
<div id="content">
    <form id="form" action="" onsubmit="return check_input();" method="post" enctype="multipart/form-data" name="LISTFORM">
        <div class="box">
            <div class="left"></div>
            <div class="right"></div>
            <div class="heading">
                <h1 style="background-image: url('access/image/review.png');">
                    <?= $title_header; ?>
                </h1>
                <div class="buttons" style="float:right;">
                    <input type="submit" value="Lưu lại" class="button_v1">   
                    <input onclick="return Question_Cancel('<?= $task_list; ?>');" type="button" value="Hủy bỏ" class="button_v1">
                </div>
            </div>
            <div class="content">
                <div id="tabs" class="htabs">
                    <a tab="#tab_general_vn"><?= IMG_VN ?></a>
                    <a tab="#tab_general_en"><?= IMG_EN ?></a>
                    <a tab="#tab_general_de"><?= IMG_DE ?></a>
                </div>
                <?php
                $messages = $this->messages->get();
                if (is_array($messages)):
                    foreach ($messages as $type => $msgs):
                        if (count($msgs > 0)):
                            foreach ($msgs as $message):
                                echo ('<div id="messages"><div class="' . $type . '">' . $message . '</div></div> ');
                            endforeach;
                        endif;
                    endforeach;
                endif;
                ?>
                <div id="tab_general_vn">                   
                    <div id="language1">
                        <table class="form">
                            <tr>
                                <td>Tiêu đề:
                                    <span class="required">*</span>                                    
                                </td>
                                <td><input type="text" name="title_vn" value="<?= $title_vn; ?>"/></td>
                            </tr>
                            <tr>
                                <td>Danh mục:</td>
                                <td>
                                    <select name="cat_id">        
                                        <option value="0">--None--</option> 
                                        <?php
                                        foreach ($dequydoitac_category as $k => $row) {
                                            ?>                                     
                                            <option value="<?= $row['id'] ?>" <?= $category == $row['id'] ? "selected='selected'" : "" ?>><?= $row['title_vn'] ?></option> 
                                        <?php } ?>  
                                    </select>   
                                </td>
                            </tr>
                            <tr>
                                <td>Link:
                                    <span class="required">*</span>                                    
                                </td>
                                <td><input type="text" name="link" value="<?= $link; ?>"/></td>
                            </tr>
                            <tr>
                                <td>Hình: <div class="comments">Kích thước: 180x150 pixels</div></td>
                                <td>
                                    <?php
                                    if (is_file($avatar)) {
                                        ?>
                                        <img src="<?= $avatar; ?>" width="100"/><br/>                                    
                                    <?php } ?>
                                    <input type="file" name="userfile" size="20" />
                                    <input type="hidden" name="temp_img" value="<?= $avatar; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td> Thứ tự:</td>
                                <td><input name="ordering" value="<?= $ordering; ?>" size="1" />
                                </td>
                            </tr>
                            <tr>
                                <td>Trạng thái:</td>
                                <td>
                                    <label><input type="radio" value="1" name="status" <?= $status == 1 ? "checked='checked'" : "" ?> /> Hiển thị </label>
                                    <label><input type="radio" value="0" name="status" <?= $status == 0 ? "checked='checked'" : "" ?>/> Ẩn</label>
                                </td>
                            </tr>  
                        </table>
                    </div>
                </div>

                <div id="tab_general_en">                   
                    <div id="language1">
                        <table class="form">                            
                            <tr>
                                <td>Tiêu đề:
                                    <span class="required">*</span>     
                                </td>
                                <td><input type="text" name="title_en" value="<?= $title_en; ?>" size="50" />
                                </td>
                            </tr>                        
                        </table>
                    </div>
                </div>

                <div id="tab_general_de">                   
                    <div id="language1">
                        <table class="form">   
                            <tr>
                                <td>Tiêu đề:
                                    <span class="required">*</span>     
                                </td>
                                <td><input type="text" name="title_de" value="<?= $title_de; ?>" size="50" />
                                </td>
                            </tr>                       
                        </table>
                    </div>
                </div>              
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $.tabs('#tabs a');
        admin.uploadLogo();
    </script>
</div>
<script type="text/javascript" src="access/js/form.js"></script>