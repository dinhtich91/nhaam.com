<div id="sidebar-cat">
    <div id="sidebar_top"></div>
    <div id="sidebar_middle">
        <ul class="level_o">
            <?php
            $listCatParent = $this->songkim->listCatParent(0);
            foreach ($listCatParent as $row):
                ?>
                <li class="parent">
                    <a href="<?=  site_url("danh-muc/".$row['v_tag']);?>"><?= $row['v_title']; ?></a>
                    <ul class="submenu">
                        <?php
                        $listCatParent2 = $this->songkim->listCatParent($row['id']);
                        foreach ($listCatParent2 as $row2):
                            ?>
                            <li>
                                <a href="<?=  site_url("danh-muc/".$row2['v_tag']);?>"><?= $row2['v_title']; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div id="sidebar_bottom"></div>
    <div>
        <img src="image/cham-soc-khach-hang.png"/>
    </div>
</div>