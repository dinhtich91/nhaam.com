<div class="container-fluid disable-padding">
        <div class="fromm-lienhe">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-xs-12 disable-padding"><h3 class="contact-heading-background open-san-semibold"><?php echo $this->lang->line('lienhe'); ?></h3></div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12 disable-padding lienhe-mb">
                        <div class="info-box">
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <h3 class="tahoma contact-title"><?php echo $this->lang->line('lienhechungtoi'); ?>:</h3>
                                    <p class="open-san-semibold contact-company">Công ty Cổ phần Công nghệ Dầu nhớt Đức FROMM</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p style="margin-bottom:0px;">KCN Ông Kèo, xã Phước Khánh, Huyện Nhơn Trạch, tỉnh Đồng Nai</p>
                                    <p>Văn phòng: 17B Trần Doãn Khanh, P.Đakao, Q.1, TPHCM</p>
                                </div>
                                <div class="col-md-3">
                                    <p style="margin-bottom:0px;">Tell: (+84) 8 382391537</p>
                                    <p>Web: www.frommoil.com</p>
                                </div>
                                <div class="col-md-3">
                                    <p>Email:  info@frommoil.com</p>
                                </div>
                            </div>
                            <form method="post" class="fromm-form-lienhe">
                                <div class="row">
                                    <div class="col-md-5 disable-padding-right personal-info-box">
                                        <div class="col-md-12 disable-padding">
                                            <input type="text" name="hoten" class="tahoma input-text-fromm" placeholder="<?php echo $this->lang->line('hoten'); ?>" />
                                        </div>
                                        <div class="col-md-12 disable-padding">
                                            <input type="text" name="diachi" class="tahoma input-text-fromm" placeholder="<?php echo $this->lang->line('diachi'); ?>" />
                                        </div>
                                        <div class="col-md-12 disable-padding">
                                            <input type="text" name="dienthoai" class="tahoma input-text-fromm" placeholder="<?php echo $this->lang->line('dienthoai'); ?>" />
                                        </div>
                                        <div class="col-md-12 disable-padding">
                                            <input type="text" name="email" class="tahoma input-text-fromm" placeholder="Email" />
                                        </div>
                                    </div>                                    
                                    <div class="col-md-7 disable-padding-left personal-info-box">
                                        <div class="col-md-12 disable-padding">
                                            <textarea class="tahoma contact-message" name="tinnhan" placeholder="<?php echo $this->lang->line('tinnhan'); ?>"></textarea>
                                            <button style="width:58px; height:30px; background-color:#f1b22a; color:#fff; float:right; border: none; outline: none; padding:0px; margin-top:10px;"><?php echo $this->lang->line('gui'); ?></button>
                                        </div>                                           
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="google-maps-box">
                            <div id="map">
                                
                            </div>
                            <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d64566.36897336761!2d106.85790314926942!3d10.69446056617957!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31753cf74e441925%3A0x2cf506e3da165574!2zw5RuZyBLw6hv!5e0!3m2!1svi!2s!4v1464757919614" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>            
    </div> <!-- /container -->

    <style type="text/css">
        .google-maps-box{
            min-height: 200px;
            width: 100%;
        }
        #map{
            width: 100%;
            height: 400px;
        }
    </style>
  
    <script>

      // This example displays a marker at the center of Australia.
      // When the user clicks the marker, an info window opens.

      function initMap() {
        var uluru = {lat: 10.702111, lng: 106.836996};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: uluru
        });

        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h4 id="firstHeading" class="firstHeading">Công ty Cổ phần Công nghệ Dầu nhớt Đức FROMM</h4>'+
            '<div id="bodyContent">'+
            'KCN Ông Kèo, xã Phước Khánh, Huyện Nhơn Trạch, tỉnh Đồng Nai'+
            '<br >Tell: (+84) 8 382391537 Email:  info@frommoil.com'+
            '</div>'+
            '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          title: 'Uluru (Ayers Rock)'
        });
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
        infowindow.open(map, marker);
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdtCDmGzluQYJzzUoCPknLko3gx7LvK0s&callback=initMap">
    </script>
    <!--   AIzaSyDrq9H_zkJigNkygBmGjDxStCbzZEyaOzM -->

    <style type="text/css">
        @media(max-width:414px) and (min-width:375px){
            #content{
                display: none;
            }
        }
        @media(max-width:375px) and (min-width:320px){
            #content{
                display: none;
            }
        }
    </style>