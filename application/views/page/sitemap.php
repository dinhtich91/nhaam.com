<div class="page-wrap">
    <script type="text/javascript">
    var h_wd, timen, cl = true, bn_animate;
    var h_imgbn;

    function height_banner() {
        h_wd = $(window).height();
        h_imgbn = $('#home-banner .swiper-slide img').eq(0).height();
        if (h_wd < h_imgbn) {
            $('#home-banner').height(h_wd);
        } else {
            $('#home-banner').height(h_imgbn);
        }
    }

    function create_bulet() {
        $('#home-banner .swiper-slide').each(function (index) {
            if (index == 0)
                $('.slider-pagi').append('<span class="swiper-pagination-switch active"></span>')
            else
                $('.slider-pagi').append('<span class="swiper-pagination-switch"></span>')
        })
    }

    function click_slider() {

        $(document).on('click', '.slider-pagi span', function () {
            if (cl) {
                clearTimeout(timen);
                cl = false;

                var $this = $(this);
                var idex = $this.index();

                $('.slider-pagi span').removeClass('active');
                $this.addClass('active');

                $('#home-banner .swiper-slide').stop().animate({'z-index': '0', opacity: 0});
                $('#home-banner .swiper-slide').eq(idex).stop().animate({'z-index': '1', opacity: 1}, function () {
                    //bn_animate.pause(0);
                    //bn_animate = TweenMax.to('.slider' + idex + ' .bn-bg', 70, {scale: 1.5, ease: Linear.easeNone})
                    cl = true;
                    timen = setTimeout(function () {
                        self.auto_click();
                    }, 5000)
                });
            }
        })

        $('.swip-next').click(function () {
            self.auto_click();
        })
        $('.swip-prev').click(function () {
            var num_l = $('.slider-pagi span').length;
            var id = $('.slider-pagi span.active').index();
            if (id == 0) {
                $('.slider-pagi span').eq(num_l - 1).trigger('click');
            } else {
                $('.slider-pagi span.active').prev().trigger('click');
            }
        })
    }

    function auto_click() {
        var num_l = $('.slider-pagi span').length;
        var id = $('.slider-pagi span.active').index();
        if (id == num_l - 1) {
            $('.slider-pagi span').eq(0).trigger('click');
        } else {
            $('.slider-pagi span.active').next().trigger('click');
        }
    }
    $(document).ready(function () {
        create_bulet();
        click_slider();
        $(window).resize(function () {
            height_banner();
        })
        $(window).load(function () {
            height_banner();
            setTimeout(function () {
                $('#loading-page').fadeOut();
            }, 1000)
            setTimeout(function () {
                auto_click();
            }, 5000)
        })

    })

</script>
<div id="home-banner">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?php
            $sliders = $this->namyangi->slider($type_slider);
            $i = 0;
            foreach ($sliders as $row) {
                $image = $row['image'];
                $desc = stripslashes($row['desc']);
                ?>
                <div class="slider<?= $i; ?> swiper-slide">
                    <img class="bn-bg" src="<?= $image; ?>" alt="<?= $row['title']; ?>" />
                    <div class="note-banner">
                        <?php
                        if ((int) $row['is_title'] == 1) {
                            ?>
                            <h3><?= $row['title']; ?></h3>
                        <?php } ?>
                        <?php
                        if ((int) $row['is_desc'] == 1) {
                            ?>
                            <p><?php echo $desc; ?></p>
                        <?php } ?>
                        <?php
                        if ((int) $row['is_link'] == 1) {
                            ?>
                            <a href="<?php echo $row['link']; ?>">Xem thêm</a>
                        <?php } ?>
                    </div>
                </div>
                <?php
                $i++;
            }
            ?>
        </div>
        <?php
        if (count($sliders) > 1) {
            ?>
            <div class="slider-pagi"></div>
            <a href="javascript: void(0);" class="swip-next"></a>
            <a href="javascript: void(0);" class="swip-prev"></a>
        <?php } ?>
    </div>
</div>
    <div class="block-sitemap">
        <h2>Sơ đồ trang</h2>
        <?php echo stripslashes($detail['v_detail']) ?>
    </div>
</div>