<section class="content">
    <div class="container">
        <div class="navigation clearfix">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <h1 class="intro-title"><?php echo $title_header; ?></h1>
        <div class="intro-content">
            <?php echo stripslashes($detail); ?>
        </div>
    </div>
</section>