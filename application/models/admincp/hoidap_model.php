<?php

class Hoidap_model extends CI_Model {

    private $TBL_hoidap = "hoidap";
    private $FOLDER_SILDE = "upload/hoidap/";

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("duocmaster");
        $this->load->library("function");
    }

    /**
     * @todo: Hiển thị tất cả
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function display($num, $offset = 0, $type = "") {
        $table = $this->TBL_hoidap;
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by('ordering', 'desc');
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Lấy theo bảng, trường gì, cột điều kiện, giá trị
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function getImgThumb($id) {
        $select = "default_image,hover_image";
        $table = $this->TBL_hoidap_IMAGE;
        $where = array("id_hoidap" => $id);
        $ordering = "DESC";
        $result = $this->function->getMulSelectTableWhere($select, $table, $where, $ordering, "ASC");
        return $result;
    }

    /**
     * Đếm số thumb của dự án
     */
    public function totalThumb($id) {
        $table = $this->TBL_IMG_hoidap;
        $where = array("id" => $id);
        $result = $this->function->total_rows($table, $where);
        return $result;
    }

    /**
     * @todo: Hiển thị tất cả
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function displaySearch($filter_name = "") {
        $filter_name = $this->function->convertHTML($filter_name);
        $table = $this->TBL_hoidap;
        $this->db->select('*');
        $this->db->from($table);
        if ($filter_name != "")
            $this->db->like('tag', $filter_name);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * @todo: Hiện thị chi tiết theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function getList($id) {
        $table = $this->TBL_hoidap;
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where(array('id' => (int) $id));
        $query = $this->db->get();
        $result = $query->result_array();
        return (isset($result)) ? $result[0] : null;
    }

    /**
     * @todo : Thêm 
     * @author : Huỳnh Văn Được 
     * @copyright : Dpassion
     */
    public function add() {
        $table = $this->TBL_hoidap;
        $params = $this->input->post();
        // unset($params['temp_img']);
        // $params['image'] = $this->duocmaster->uploadResize($this->FOLDER_SILDE,180,150);
        // if ($params['image'] == FALSE)
        //     return FALSE;
        $this->db->insert($table, $params);
        return TRUE;
    }

    /**
     * @todo : Cập nhật theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function insertMulipleImg($listIMG_DEFAULT, $listIMG_HOVER, $id) {
        $this->db->delete($this->TBL_hoidap_IMAGE, array('id_hoidap' => $id));
        foreach ($listIMG_DEFAULT as $i => $default_image) {
            $hover_image = $listIMG_HOVER[$i];
            $params = array(
                'id_hoidap' => $id,
                'default_image' => $default_image,
                'hover_image' => $hover_image,
                'ordering' => $i,
            );
            $this->db->insert($this->TBL_hoidap_IMAGE, $params);
        }
    }

    /**
     * @todo : Cập nhật theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function update($id) {
        $table = $this->TBL_hoidap;
        $params = $this->input->post();
        // $params['image'] = $params['temp_img'];
        // $imgUpload = $this->duocmaster->uploadResize($this->FOLDER_SILDE,180,150);
        // if ($imgUpload) {
        //     $params['image'] = $imgUpload;
        //     @unlink($params['temp_img']);
        // }

        // unset($params['temp_img']);
        $this->db->where(array('id' => $id), NULL, FALSE);
        $this->db->update($table, $params);
    }

    /**
     * @todo : Xóa mẫu tin theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function del($id) {
        $table = $this->TBL_hoidap;
        return $this->function->del($table, $id);
    }

    /**
     * @todo : Bật tắt tình trạng nhanh
     */
    public function status($id = 0, $status = 0, $field = 'status') {
        $table = $this->TBL_hoidap;
        return $this->function->status($table, $id, $status, $field);
    }

    /**
     * Lấy vị trí lớn nhất
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function orderingMax() {
        $table = $this->TBL_hoidap;
        return $this->function->orderingMax($table);
    }

    /**
     * Chức năng xóa tất cả
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function del_all() {
        $table = $this->TBL_hoidap;
        $this->function->del_all($table);
    }

    /**
     * Chức năng sắp xếp nhanh trong danh sách
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function ordering_all() {
        $table = $this->TBL_hoidap;
        $this->function->ordering_all($table);
    }

    /**
     * Chức năng tính tổng số dòng trong phân trang nếu không có
     * điều kiện thì $where = array();
     * Ngược lại, $where = array(
     *                          'status'    =>1
     *                          );
     */
    public function total_rows() {
        $table = $this->TBL_hoidap;
        return $this->function->total_rows($table,array());
    }

    /**
     * Danh mục cha
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function parent($parent = 0) {
    $this->db->select('*');
    $this->db->from($this->TBL_hoidap);
    $this->db->where(array('parent' => (int) $parent));
    $query = $this->db->get();
    if ($query)
        return $query->result_array();
    else
        return NULL;
}

/**
 * Menu đa cấp
 * @author : Huỳnh Văn Được
 * @copyright : Dpassion
 */
public function dequyMenu($cap = 0, $gach = "", $arr = NULL) {
    $title = "v_title";
    $result = $this->parent($cap);
    if (!$arr)
        $arr = array(); //khoi tao 1 array co ten la arr  
    foreach ($result as $row) {
        $arr[] = array('id' => $row['id'], "parent" => $cap, $title => $gach . $row[$title]);
        $arr = $this->dequyMenu($row['id'], $gach . "   -------  ", $arr);
    }
    return $arr;
}

/**
 * Lấy tên danh mục cha
 * @author : Huỳnh Văn Được
 * @copyright : Dpassion
 */
public function getNameParent($parent = 0) {
    $select = "v_title";
    $table = $this->TBL_hoidap;
    $where = array('id' => $parent);
    $result = $this->function->getName($select, $table, $where);
    return $result[$select] ? $result[$select] : "#";
}

/**
 * Lấy tên danh mục cha
 * @author : Huỳnh Văn Được
 * @copyright : Dpassion
 */
public function getName($select, $table, $where, $value) {
    $where = array($where => $value);
    $result = $this->function->getSelectTableWhere($select, $table, $where);
    return $result[$select] ? $result[$select] : "#";
}


public function listGrouphoidapr() {
    return $this->function->getMulSelectTableWhere("*", "hoidapr_position", array(),"id","ASC");
}

public function listDanhMucTinTuc() {
    return $this->function->getMulSelectTableWhere("*", "hoidap_category", array('status' => 1),"id","ASC");
}

}

?>
