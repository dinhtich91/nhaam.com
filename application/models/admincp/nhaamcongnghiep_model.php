<?php

class Nhaamcongnghiep_model extends CI_Model {

    private $TBL_PRODUCTS = "nhaamcongnghiep";
    private $TBL_IMG_PRODUCTS = "nhaamcongnghiep_image";

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("duocmaster");
    }

    /**
     * @todo: Hiển thị tất cả
     * @author : Huỳnh Văn Được
     * @copyright : dpassion
     */
    public function display($num, $offset = 0) {
        $table = $this->TBL_PRODUCTS;
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by('ordering', 'desc');
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Lấy theo bảng, trường gì, cột điều kiện, giá trị
     * @author : Huỳnh Văn Được
     * @copyright : dpassion
     */
    public function getImgThumb($id) {
        $select = "*";
        $table = $this->TBL_IMG_PRODUCTS;
        $where = array("id_nhaamcongnghiep" => $id);
        $result = $this->function->getMulSelectTableWhere($select, $table, $where, "ordering", "ASC");
        return $result ? $result : array();
    }

    /**
     * Lấy theo bảng, trường gì, cột điều kiện, giá trị
     * @author : Huỳnh Văn Được
     * @copyright : dpassion
     */
    public function getVolumePriceProduct($id_nhaamcongnghiep) {
        $select = "*";
        $where = array("id_nhaamcongnghiep" => $id_nhaamcongnghiep);
        $result = $this->function->getMulSelectTableWhere($select, TBL_VOLUME_PRICE, $where, "ordering", "ASC");
        return isset($result) ? $result : array();
    }

    /**
     * Đếm số thumb của dự án
     */
    public function totalThumb($id) {
        $table = $this->TBL_IMG_PRODUCTS;
        $where = array("id_nhaamcongnghiep" => $id);
        $result = $this->function->total_rows($table, $where);
        return $result;
    }

    /**
     * @todo: Hiển thị tất cả
     * @author : Huỳnh Văn Được
     * @copyright : dpassion
     */
    public function displaySearch($filter_name = "") {
        $filter_name = $this->function->convertHTML($filter_name);
        $table = $this->TBL_PRODUCTS;
        $this->db->select('*');
        $this->db->from($table);
        if ($filter_name != "")
            $this->db->like('tag', $filter_name);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * @todo: Hiện thị chi tiết theo id
     * @author : Huỳnh Văn Được
     * @copyright : dpassion
     */
    public function getList($id) {
        $table = $this->TBL_PRODUCTS;
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where(array('id' => (int) $id));
        $query = $this->db->get();
        $result = $query->result_array();
        return (isset($result)) ? $result[0] : null;
    }

    /**
     * @todo : Thêm 
     * @author : Huỳnh Văn Được 
     * @copyright : dpassion
     */
    public function add() {
        $table = $this->TBL_PRODUCTS;        
        $params = $this->input->post();
        $params['tag'] = $this->function->convertHTML($params['title_vn']);
        $listIMG = $params['images'];
        unset($params['images']);
        $this->db->insert($table, $params);
        $id = $this->db->insert_id();
        //insert multi photo
        $this->insertMulipleImg($listIMG, $id);
    }

    /**
     * @todo : Cập nhật theo id
     * @author : Huỳnh Văn Được
     * @copyright : dpassion
     */
    public function update($id) {
        $table = $this->TBL_PRODUCTS;
        $params = $this->input->post();
        $params['tag'] = $this->function->convertHTML($params['title_vn']);
        $listIMG = $params['images'];
        unset($params['images']);
        $this->db->where(array('id' => $id), NULL, FALSE);
        $this->db->update($table, $params);
        //insert multi photo
        $this->insertMulipleImg($listIMG, $id);
    }

    /**
     * del hình resize
     * @author : Huỳnh Văn Được
     * @copyright : dpassion
     */
    public function delImgThumb($id, $main_image) {
        $result = $this->getImgThumb($id);
        @unlink(FOLDER_RESIZE . "/" . $main_image);
        foreach ($result as $image) {
            @unlink(FOLDER_RESIZE . "/" . $image);
        }
    }

    /**
     * @todo : insert nhiều hình
     * @author : Huỳnh Văn Được
     * @copyright : dpassion
     */
    public function insertMulipleImg($listIMG, $id) {
        $this->db->delete($this->TBL_IMG_PRODUCTS, array('id_nhaamcongnghiep' => (int) $id));
        $n = count($listIMG);
        for ($i = 0; $i < $n; $i++) {
            $params = array(
                'id_nhaamcongnghiep' => $id,
                'image' => basename($listIMG[$i]),
                'ordering' => $i
            );
            if (is_file($listIMG[$i])) {
                $this->db->insert($this->TBL_IMG_PRODUCTS, $params);
            }
        }
    }

    /**
     * @todo : insert đối tượng
     * @author : Huỳnh Văn Được
     * @copyright : dpassion
     */
    public function insertMulipleObject($ArrayObject, $id) {
        $this->db->delete("nhaamcongnghiep_image", array('id_nhaamcongnghiep' => (int) $id));
        $n = count($ArrayObject);
        for ($i = 0; $i < $n; $i++) {
            $params = array(
                'id_nhaamcongnghiep' => $id,
                'id_age' => $ArrayObject[$i],
                'ordering' => $i
            );
            $this->db->insert("nhaamcongnghiep_image", $params);
        }
    }

    /**
     * @todo Insert thể tích & gía
     */
    private function insertVolumePrice($arrayVolume, $arrayPrice, $arraySale, $id) {
        $this->db->delete(TBL_VOLUME_PRICE, array('id_nhaamcongnghiep' => (int) $id));
        $n = count($arrayVolume);
        for ($i = 0; $i < $n; $i++) {
            $data['id_nhaamcongnghiep'] = $id;
            $data['volume'] = isset($arrayVolume[$i]) ? $arrayVolume[$i] : 0;
            $data['price'] = isset($arrayPrice[$i]) ? $arrayPrice[$i] : 0;
            $data['sale'] = isset($arraySale[$i]) ? $arraySale[$i] : 0;
            $data['ordering'] = $i;
            if ($data['volume'] != 0 && $data['price'] != 0) {
                $this->db->insert(TBL_VOLUME_PRICE, $data);
            }
        }
    }

    /**
     * @todo : Xóa mẫu tin theo id
     * @author : Huỳnh Văn Được
     * @copyright : dpassion
     */
    public function del($id) {
        $table = $this->TBL_PRODUCTS;
        return $this->function->del($table, $id);
    }

    /**
     * @todo : Bật tắt tình trạng nhanh
     */
    public function status($id = 0, $status = 0, $field = 'status') {
        $table = $this->TBL_PRODUCTS;
        return $this->function->status($table, $id, $status, $field);
    }

    /**
     * Lấy vị trí lớn nhất
     * @author : Huỳnh Văn Được
     * @copyright : dpassion
     */
    public function orderingMax() {
        $table = $this->TBL_PRODUCTS;
        return $this->function->orderingMax($table);
    }

    /**
     * Chức năng xóa tất cả
     * @author : Huỳnh Văn Được
     * @copyright : dpassion
     */
    public function del_all() {
        $table = $this->TBL_PRODUCTS;
        $this->function->del_all($table);
    }

    /**
     * Chức năng sắp xếp nhanh trong danh sách
     * @author : Huỳnh Văn Được
     * @copyright : dpassion
     */
    public function ordering_all() {
        $table = $this->TBL_PRODUCTS;
        $this->function->ordering_all($table);
    }

    /**
     * Chức năng tính tổng số dòng trong phân trang nếu không có
     * điều kiện thì $where = array();
     * Ngược lại, $where = array(
     *                          'status'    =>1
     *                          );
     */
    public function total_rows() {
        $table = $this->TBL_PRODUCTS;
        $where = array();
        return $this->function->total_rows($table, $where);
    }

    /**
     * Danh mục cha
     * @author : Huỳnh Văn Được
     * @copyright : dpassion
     */
    public function parent($parent = 0) {
    $this->db->select('*');
    $this->db->from($this->TBL_PRODUCTS);
    $this->db->where(array('parent' => (int) $parent));
    $query = $this->db->get();
    if ($query)
        return $query->result_array();
    else
        return NULL;
}

/**
 * Menu đa cấp
 * @author : Huỳnh Văn Được
 * @copyright : dpassion
 */
public function dequyMenu($cap = 0, $gach = "", $arr = NULL) {
    $title = "v_title";
    $result = $this->parent($cap);
    if (!$arr)
        $arr = array(); //khoi tao 1 array co ten la arr  
    foreach ($result as $row) {
        $arr[] = array('id' => $row['id'], "parent" => $cap, $title => $gach . $row[$title]);
        $arr = $this->dequyMenu($row['id'], $gach . "   -------  ", $arr);
    }
    return $arr;
}

/**
 * Lấy tên danh mục cha
 * @author : Huỳnh Văn Được
 * @copyright : dpassion
 */
public function getNameParent($parent = 0) {
    $select = "v_title";
    $table = $this->TBL_PRODUCTS;
    $where = array('id' => $parent);
    $result = $this->function->getName($select, $table, $where);
    return $result ? $result[$select] : "";
}

/**
 * Lấy tên danh mục cha
 * @author : Huỳnh Văn Được
 * @copyright : dpassion
 */
public function getName($select, $table, $where, $value) {
    $where = array($where => $value);
    $result = $this->function->getSelectTableWhere($select, $table, $where);
    return $result ? $result[$select] : "";
}

/**
 * @todo: Danh sách đối tượng
 * @author : Huỳnh Văn Được
 * @copyright : dpassion
 */
public function listDoiTuong() {
    $this->db->select('*');
    $this->db->from("age");
    $this->db->where(array('status' => 1));
    $query = $this->db->get();
    return $query->result_array();
}

/**
 * @todo: Danh sách đối tượng
 * @author : Huỳnh Văn Được
 * @copyright : dpassion
 */
public function getDoiTuong($id_nhaamcongnghiep = 0) {
    $this->db->select('*');
    $this->db->from("nhaamcongnghiep_image");
    $this->db->where(array('id_nhaamcongnghiep' => (int) $id_nhaamcongnghiep));
    $query = $this->db->get();
    $result = $query->result_array();
    $array = array();
    foreach ($result as $row) {
        $array[] = $row['id_age'];
    }
    return $array;
}

}
