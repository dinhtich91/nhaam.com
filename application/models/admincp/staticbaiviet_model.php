<?php

class Staticbaiviet_model extends CI_Model {

    private $TBL_STATISBAIVIET = "baiviet_tinh";
    private $TBL_ABOUTUS = "aboutus";
    private $FOLDER_SILDE= "upload/popup/";

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("duocmaster");
    }

    /**
     * @todo: Hiện thị chi tiết theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function getList($type) {
        $table = $this->TBL_STATISBAIVIET;
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where(array('type' => $type));
        $query = $this->db->get();
        $result = $query->result_array();
        return ($result) ? $result[0] : null;
    }

    /**
     * @todo : Thêm 
     * @author : Huỳnh Văn Được 
     * @copyright : Dpassion
     */
    public function add($type) {
        $table = $this->TBL_STATISBAIVIET;
        $params = $this->input->post();
        // $params['v_detail'] = addslashes($params['v_detail']);
        $params['type'] = $type;
        $this->db->insert($table, $params);
    }

    /**
     * @todo : Cập nhật theo type, nếu không có thì insert
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function update($type) {

        $table = $this->TBL_STATISBAIVIET;
        $where = array("type" => $type);
        $count = $this->function->total_rows($table, $where);
        if ($count > 0) {
            $table = $this->TBL_STATISBAIVIET;
            $params = $this->input->post();
            // $params['v_detail'] = addslashes($params['v_detail']);
            $this->db->where(array('type' => $type));
            $this->db->update($table, $params);
        } else if ($count == 0) {
            $this->add($type);
        }
    }

    /**
     * @todo : Cập nhật theo type, nếu không có thì insert
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function aboutus() {
        $table  = $this->TBL_ABOUTUS;
        $params = $this->input->post();
        $params['description'] = addslashes($params['description']);
        $this->db->update($table, $params);
    }
    /**
     * @todo : Cập nhật theo type, nếu không có thì insert
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function getAboutus() {
        $table = $this->TBL_ABOUTUS;
        $this->db->select('*');
        $this->db->from($table);
        $query = $this->db->get();
        $result = $query->result_array();
        return ($result) ? $result[0] : null;
    }
    
    /**
     * @todo Thông báo Popup
     * @author Huỳnh Văn Được 20130328 <duoc.huynh@dpassion.com>
     */
    public function getPopup(){
        $type  = "popup";
        $table = $this->TBL_STATISBAIVIET;
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where(array('type' => $type));
        $query = $this->db->get();
        $result = $query->result_array();
        return ($result) ? $result[0] : null;
    }
    
    public function updatePopup(){
        $table            = $this->TBL_STATISBAIVIET;        
        $params           = $this->input->post();
        $params['v_detail'] = $params['temp_img'];        
        $imgUpload        = $this->duocmaster->uploadResize($this->FOLDER_SILDE,502,396,TRUE);
        if($imgUpload){
            $params['v_detail']   = $imgUpload;
            @unlink($params['temp_img']);
        }        
        unset($params['temp_img']);        
        $this->db->where(array('type' => "popup"));
        $this->db->update($table, $params);
    }

}

?>
