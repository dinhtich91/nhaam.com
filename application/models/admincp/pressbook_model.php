<?php
class Pressbook_model extends CI_Model {
    private $TBL_PRESSBOOK  = "pressbook";    
    private $FOLDER_FILE    = "upload/files/";

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("duocmaster");
        $this->load->library("function");
    }   
    /**
     * @todo : Cập nhật theo id
     * @author : Huỳnh Văn Được
     * @copyright : Dpassion
     */
    public function update() {
        $table            = $this->TBL_PRESSBOOK;        
        $params           = $this->input->post();
        $params['link']   = $params['temp_file'];        
        
        $fileUpload       = $this->duocmaster->uploadFile($this->FOLDER_FILE);
        if($fileUpload){
            $params['link']   = $fileUpload;
            @unlink($params['temp_file']);
        }        
        unset($params['temp_file']);    
        unset($params['uerfile']);     
        $this->db->update($table, $params);
    }
    /**
     * @todo Lấy link getLinkPressBook
     * @author Huỳnh Văn Được 20130306 <duoc.huynh@dpassion.com>
     * @category Dpassion
     */
    public function getLinkPressBook(){
        $select = "link";
        $table  = $this->TBL_PRESSBOOK;        
        $where  = array();
        $result = $this->function->getSelectTableWhere($select, $table, $where);
        return $result[$select] ? $result[$select] : "";
    }

}

?>
