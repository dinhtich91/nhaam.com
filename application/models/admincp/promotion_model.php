<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Promotion_model extends CI_Model {

    private $TBL_SLIDE = "promotion";
    private $FOLDER_SILDE = "upload/images/";

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("duocmaster");
        $this->load->library("function");
    }

    /**
     * @todo: Hiển thị tất cả
     * @author : Huỳnh Văn Được
     * @copyright : songkim.net
     */
    public function display($num, $offset = 0) {
        $table = $this->TBL_SLIDE;
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by('ordering', 'desc');
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Lấy theo bảng, trường gì, cột điều kiện, giá trị
     * @author : Huỳnh Văn Được
     * @copyright : songkim.net
     */
    public function getImgThumb($id) {
        $select = "default_image,hover_image";
        $table = $this->TBL_SLIDE_IMAGE;
        $where = array("id_slide" => $id);
        $ordering = "DESC";
        $result = $this->function->getMulSelectTableWhere($select, $table, $where, $ordering, "ASC");
        return $result;
    }

    /**
     * Đếm số thumb của dự án
     */
    public function totalThumb($id) {
        $table = $this->TBL_IMG_SLIDE;
        $where = array("id" => $id);
        $result = $this->function->total_rows($table, $where);
        return $result;
    }

    /**
     * @todo: Hiển thị tất cả
     * @author : Huỳnh Văn Được
     * @copyright : songkim.net
     */
    public function displaySearch($filter_name = "") {
        $filter_name = $this->function->convertHTML($filter_name);
        $table = $this->TBL_SLIDE;
        $this->db->select('*');
        $this->db->from($table);
        if ($filter_name != "")
            $this->db->like('tag', $filter_name);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * @todo: Hiện thị chi tiết theo id
     * @author : Huỳnh Văn Được
     * @copyright : songkim.net
     */
    public function getList($id) {
        $table = $this->TBL_SLIDE;
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where(array('id' => (int) $id));
        $query = $this->db->get();
        $result = $query->result_array();
        return (isset($result)) ? $result[0] : null;
    }

    /**
     * @todo : Thêm 
     * @author : Huỳnh Văn Được 
     * @copyright : songkim.net
     */
    public function add() {
        $table = $this->TBL_SLIDE;
        $params = $this->input->post();
        $params['tag'] = $this->function->convertHTML($params['title']);
        $avartar = $this->duocmaster->uploadResize($this->FOLDER_SILDE, 245, 160, TRUE);
        if ($avartar) {
            $params['avatar'] = $avartar;
        }
        $params['detail'] = addslashes($params['detail']);
        $params['time'] = date("Y-m-d H:i:s");
        unset($params['temp_img']);
        $this->db->insert($table, $params);
        return TRUE;
    }

    /**
     * @todo : Cập nhật theo id
     * @author : Huỳnh Văn Được
     * @copyright : songkim.net
     */
    public function update($id) {
        $table = $this->TBL_SLIDE;
        $params = $this->input->post();
        $params['tag'] = $this->function->convertHTML($params['title']);
        $params['avatar'] = $params['temp_img'];
        $imgUpload = $this->duocmaster->uploadResize($this->FOLDER_SILDE, 245, 160, TRUE);
        if ($imgUpload) {
            $params['avatar'] = $imgUpload;
            @unlink($params['temp_img']);
        }
        $params['detail'] = addslashes($params['detail']);
        unset($params['temp_img']);
        $this->db->where(array('id' => $id), NULL, FALSE);
        $this->db->update($table, $params);
    }

    /**
     * @todo : Xóa mẫu tin theo id
     * @author : Huỳnh Văn Được
     * @copyright : songkim.net
     */
    public function del($id) {
        $table = $this->TBL_SLIDE;
        return $this->function->del($table, $id);
    }

    /**
     * @todo : Bật tắt tình trạng nhanh
     */
    public function status($id = 0, $status = 0, $field = 'status') {
        $table = $this->TBL_SLIDE;
        return $this->function->status($table, $id, $status, $field);
    }

    /**
     * Lấy vị trí lớn nhất
     * @author : Huỳnh Văn Được
     * @copyright : songkim.net
     */
    public function orderingMax() {
        $table = $this->TBL_SLIDE;
        return $this->function->orderingMax($table);
    }

    /**
     * Chức năng xóa tất cả
     * @author : Huỳnh Văn Được
     * @copyright : songkim.net
     */
    public function del_all() {
        $table = $this->TBL_SLIDE;
        $this->function->del_all($table);
    }

    /**
     * Chức năng sắp xếp nhanh trong danh sách
     * @author : Huỳnh Văn Được
     * @copyright : songkim.net
     */
    public function ordering_all() {
        $table = $this->TBL_SLIDE;
        $this->function->ordering_all($table);
    }

    /**
     * Chức năng tính tổng số dòng trong phân trang nếu không có
     * điều kiện thì $where = array();
     * Ngược lại, $where = array(
     *                          'status'    =>1
     *                          );
     */
    public function total_rows() {
        $table = $this->TBL_SLIDE;
        $where = array();
        return $this->function->total_rows($table, $where);
    }

    /**
     * Danh mục cha
     * @author : Huỳnh Văn Được
     * @copyright : songkim.net
     */
    public function parent($parent = 0) {
        $this->db->select('*');
        $this->db->from($this->TBL_SLIDE);
        $this->db->where(array('parent' => (int) $parent));
        $query = $this->db->get();
        if ($query)
            return $query->result_array();
        else
            return NULL;
    }

    /**
     * Menu đa cấp
     * @author : Huỳnh Văn Được
     * @copyright : songkim.net
     */
    public function dequyMenu($cap = 0, $gach = "", $arr = NULL) {
        $title = "v_title";
        $result = $this->parent($cap);
        if (!$arr)
            $arr = array(); //khoi tao 1 array co ten la arr  
        foreach ($result as $row) {
            $arr[] = array('id' => $row['id'], "parent" => $cap, $title => $gach . $row[$title]);
            $arr = $this->dequyMenu($row['id'], $gach . "   -------  ", $arr);
        }
        return $arr;
    }

    /**
     * Lấy tên danh mục cha
     * @author : Huỳnh Văn Được
     * @copyright : songkim.net
     */
    public function getNameParent($parent = 0) {
        $select = "v_title";
        $table = $this->TBL_SLIDE;
        $where = array('id' => $parent);
        $result = $this->function->getName($select, $table, $where);
        return $result[$select] ? $result[$select] : "#";
    }

    /**
     * Lấy tên danh mục cha
     * @author : Huỳnh Văn Được
     * @copyright : songkim.net
     */
    public function getName($select, $table, $where, $value) {
        $where = array($where => $value);
        $result = $this->function->getSelectTableWhere($select, $table, $where);
        return $result[$select] ? $result[$select] : "#";
    }

}

?>
