<?php

class Fromm_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library("duocmaster");
    }

    /**
     * @todo  Lay san pham moi nhat
     * @since 20160426
     */
    public function listRecentProduct($num) {
        $this->db->limit($num);
        $where = array("status" => 1);
        return $this->function->getMulSelectTableWhere("title, id, avatar, price, sale, tag", "products", $where, "ordering", "DESC");
    }

    /**
     * @todo  Lay danh muc cha
     * @param int $parent ID danh muc cha
     * @since 20160425
     */
    public function listCategoryByParent($parent = 0) {
        
        $where = array("status" => 1, "parent" => (int) $parent);
        return $this->function->getMulSelectTableWhere("id,title,tag, parent", "project_category", $where, "ordering", "ASC");
    }
    /**
     * @todo  Lay danh muc cha theo mang
     * @param array $parent array ID danh muc cha
     * @since 20160428
     */
    public function listCategoryByParentArray($parent = array()) {
        $this->db->select("*");
        $this->db->from('project_category');
        $this->db->where(array("status" => 1));
        $this->db->where_in('parent', $parent);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    /**
     *  @todo Total product by mảng cat_id
     *  @param array $cat_id mảng ID danhmục
     *  @since 20150918
     */
    public function countProductArray($cat_id = array()) {
        $this->db->select("*");
        $this->db->from('products');
        $this->db->where(array("status" => 1));
        $this->db->where_in('cat_id', $cat_id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
     /**
     *  @todo Danh sách sản phẩm phân trang theo mảng id danh mục có phân trang
     *  @param array $cat_id ID danhmục
     *  @since 20150918
     */
    public function listProductByArrayCatID($num, $offset, $cat_id = array()) {
        $this->db->select("*");
        $this->db->from('products');
        $this->db->where(array("status" => 1));
        $this->db->where_in('cat_id', $cat_id);
        $this->db->limit($num, $offset);
        $query = $this->db->get();
       
        return $query->result_array();
    }
    
    /**
     *  @todo Danh sách sản phẩm phân trang theo mảng id danh mục
     *  @param array $cat_id ID danhmục
     *  @since 20150918
     */
    public function listProductByArrayCatID2($cat_id = array()) {
        $this->db->select("*");
        $this->db->from('products');
        $this->db->where(array("status" => 1));
        $this->db->where_in('cat_id', $cat_id);
        $query = $this->db->get();
       
        return $query->result_array();
    }
    
    public function listCategoryByID($id = 0) {
        $where = array("status" => 1, "id" => (int) $id);
        return $this->function->getMulSelectTableWhere("id, title, tag", "project_category", $where, "ordering", "ASC");
    }

    /**
     * @todo  Lay san pham theo danh muc
     * @param array $cat_id ID mảng danh mục
     * @since 20160426
     */
    public function listProductsByCat($cat_id = array()) {
        $this->db->select("*");
        $this->db->from('products');
        $this->db->where(array("status" => 1));
        $this->db->where_in('cat_id', $cat_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function listCategorySide($num) {
        $data = array();
        $where = array("status" => 1, "show_on_sidebox" => 1);
        $this->db->limit($num);
        $category = $this->function->getMulSelectTableWhere("id, title, tag, parent", "project_category", $where, "ordering", "DESC");
        return $category;
    }

    public function slider($type = "") {
        $where = array("status" => 1);
        return $this->function->getMulSelectTableWhere("*", "slide", $where, "ordering", "DESC");
    }

    /**
     *  @todo List Shortcode
     *  @since 20150916
     * @copyright (c) fromm, DuocMaster
     */
    public function shortcode() {
        $where = array("status" => 1);
        return $this->function->getMulSelectTableWhere("id,title,link,image,desc", "shortcode", $where, "ordering", "DESC");
    }

    /**
     *  @todo List Photo About us
     *  @since 20150916
     */
    public function slider_aboutus() {
        $where = array("status" => 1);
        return $this->function->getMulSelectTableWhere("title,image", "slider_about", $where, "ordering", "DESC");
    }

    /**
     *  @todo Chi tiết danh mục sản phẩm theo tag
     *  @param string $tag tag danh mục
     *  @since 20151019
     */
    public function detailCatByTag($tag = "") {
        $where = array("status" => 1, "tag" => $tag);
        return $this->function->getSelectTableWhere("id,title,tag, desc, parent", "project_category", $where);
    }

    /**
     *  @todo List Category By Parent
     *  @param int $parent ID Parent
     *  @since 201510609
     */
    public function listCatByParent($parent = 0) {
        $where = array("status" => 1, "parent" => (int) $parent);
        return $this->function->getMulSelectTableWhere("id,tag,title,avatar", "project_category", $where, "ordering", "ASC");
    }
    
     /**
     *  @todo find parent of category by ID_parent
     *  @param int $id_parent ID Parent
     *  @since 201510609
     */
    public function findParentByID($id_parent = 0) {
        $where = array("status" => 1, "id" => (int) $id_parent);
        return $this->function->getMulSelectTableWhere("id, tag, title, avatar, parent", "project_category", $where, "ordering", "ASC");
    }

    /**
     *  @todo Danh sách sản phẩm theo id danh mục
     *  @param int $cat_id ID danhmục
     *  @since 20150918
     */
    public function listProductByCatID($num, $offset, $cat_id = 0) {
        $where = array("status" => 1, "cat_id" => (int) $cat_id);
        $this->db->limit($num, $offset);
        return $this->function->getMulSelectTableWhere("id,tag,price,sale,title,avatar,overview,cat_id,is_popup", "products", $where, "ordering", "DESC");
    }

    /**
     *  @todo Sản phẩm liên quan theo id danh mục
     *  @param int $cat_id ID danhmục
     *  @since 20150918
     */
    public function listProductRelated($num, $cat_id = 0, $id_product = 0) {
        $where = array("status" => 1, "cat_id" => (int) $cat_id);
        $this->db->where_not_in("id", $id_product);
        $this->db->limit($num, 0);
        return $this->function->getMulSelectTableWhere("id,tag,title,price,sale,avatar,overview,cat_id,is_popup", "products", $where, "ordering", "ASC");
    }

    /**
     *  @todo Total product by cateory
     *  @param int $cat_id ID danhmục
     *  @since 20150918
     */
    public function countProduct($cat_id) {
        $where = array("status" => 1, "cat_id" => (int) $cat_id);
        return $this->function->total_rows("products", $where);
    }
    

    /**
     *  @todo Chi tiết sản phẩm
     *  @param string $tag Tag sản phẩm
     *  @since 201510609
     */
    public function detailProduct($tag = '') {
        $where = array("status" => 1, "tag" => $tag);
        return $this->function->getSelectTableWhere("id,cat_id,price,sale,title,tag,avatar,overview,cat_id,link_album,detail,notes,uses,thanhphan", "products", $where);
    }

    /**
     *  @todo Chi tiết sản phẩm theo ID
     *  @param string $id ID sản phẩm
     *  @since 201510609
     */
    public function detailProductByID($id = 0) {
        $where = array("status" => 1, "id" => $id);
        return $this->function->getSelectTableWhere("id,cat_id,price,sale,title,tag,avatar,overview,cat_id,link_album,detail,notes,uses,thanhphan", "products", $where);
    }

    /**
     *  @todo Danh sách hình \
     *  @param int $product_id ID danhmục
     *  @since 20150918
     */
    public function imgProduct($product_id = 0) {
        $where = array("id_product" => (int) $product_id);
        return $this->function->getMulSelectTableWhere("image", "products_image", $where, "ordering", "ASC");
    }

    /**
     *  @todo Lấy tin tức nổi bật mới nhất theo ID danh mục
     *  @param int $id_cat ID  danh mục
     *  @since 201510608    
     */
    public function getFirstNews($id_cat = 0) {
        $where = array("status" => 1, "id_cat" => (int) $id_cat);
        return $this->function->getSelectTableWhere("title,tag,sumary,avatar", "news", $where, "ordering", "DESC");
    }

    /**
     *  @todo Lấy tin tức nổi bật mới nhất theo ID danh mục
     *  @param int $id_cat ID  danh mục
     *  @since 201510608    
     */
    public function getHotNewsByCat($id_cat = 0) {
        $where = array("status" => 1, "is_hot" => 1, "id_cat" => (int) $id_cat);
        return $this->function->getSelectTableWhere("id,title,tag,sumary,avatar", "news", $where, "ordering", "DESC");
    }

    public function countNews() {
        $where = array("status" => 1);
        return $this->function->total_rows("news", $where);
    }

    public function listNews($num, $offset) {
        $this->db->where(array("status" => 1));
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     *  @todo Lấy tin tức nổi bật mới nhất theo ID danh mục và loại bỏ mảng ID tin
     *  @param int $num Số tin cần lấy
     *  @param int $offset Vị trí cần lấy
     *  @param int $id_cat Danh mục cần lấy
     *  @param int $is_hot Nổi bật hay không
     *  @param array $exinclude Danh sách mảng ID bị loại bỏ
     *  @since 201510608    
     */
    public function listNewsV2($num, $offset, $id_cat = 0, $is_hot = FALSE, $exinclude = array()) {
        if (!empty($exinclude)) {
            $this->db->where_not_in("id", $exinclude);
        }
        $this->db->select('id,tag,title,sumary,avatar,id_cat');
        $this->db->from("news");
        $this->db->order_by('ordering', 'desc');
        if ($is_hot) {
            $this->db->where(array("is_hot" => 1));
        }
        $this->db->where(array("id_cat" => (int) $id_cat, "status" => 1));
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     *  @todo Chi tiết danh mục tin tức theo ID
     *  @param int $idCat ID danh mục
     *  @since 201510609
     */
    public function detailCatNewsByID($idCat = 0) {
        $where = array("status" => 1, "id" => $idCat);
        return $this->function->getSelectTableWhere("id,title,tag,parent,tag", "news_category", $where);
    }

    /**
     *  @todo Chi tiết danh mục tin tức theo Tag
     *  @param int $tag Tag danh mục tin tức
     *  @since 201510609
     */
    public function detailCatNewsByTag($tag = 0) {
        $where = array("status" => 1, "tag" => $tag);
        return $this->function->getSelectTableWhere("id,title,tag,parent,banner,desc,is_title,is_desc", "news_category", $where);
    }

    public function listNewsRelated($num, $id_cat = 0, $exinclude) {
        $this->db->select('id,tag,title,sumary,avatar');
        $this->db->from("news");
        $this->db->order_by('ordering', 'desc');
        $this->db->where(array("id_cat" => (int) $id_cat, "status" => 1));
        $this->db->where_not_in("id", array($exinclude));
        $this->db->limit($num);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     *  @todo Chi tiết tin tức
     *  @param int $tag tin tức
     *  @since 201510608    
     */
    public function detailNews($tag = "") {
        $where = array("status" => 1, "tag" => $tag);
        return $this->function->getSelectTableWhere("id,title,tag,id_cat,sumary,avatar,detail", "news", $where);
    }

    /**
     *  @todo Danh mục sản phẩm
     *  @since 201510609
     */
    public function listCatProducts() {
        $where = array("status" => 1);
        return $this->function->getMulSelectTableWhere("avatar,title,tag,desc,is_title,is_desc", "project_category", $where);
    }

    public function listCatProductFa() {
        $where = array("status" => 1, "parent" => 0);
        return $this->function->getMulSelectTableWhere("title", "project_category", $where);
    }

    /**
     *  @todo Chi tiết danh mục sản phẩm theo ID
     *  @param int $id ID danh mục
     *  @since 201510609
     */
    public function detailCatByID($id = 0) {
        $where = array("status" => 1, "id" => $id);
        return $this->function->getSelectTableWhere("id,title,tag,parent,is_title,is_desc", "project_category", $where);
    }

    /**
     * Lấy theo bảng, trường gì, cột điều kiện, giá trị
     * @author : Huỳnh Văn Được
     * @copyright : dpassion
     */
    public function getImgThumb($id) {
        return $this->function->getMulSelectTableWhere("image", "products_image", array("id_product" => (int) $id), "ordering", "ASC");
    }

    /**
     *  @todo danh sách các đối tượng
     *  @since 201510609
     */
    public function listObject() {
        $where = array("status" => 1);
        return $this->function->getMulSelectTableWhere("id,tag,title", "age", $where);
    }

    /**
     *  @todo Lọc sản phẩm theo danh mục sản phẩm, theo array đối tượng
     *  @param array $listID chuỗi ID
     *  @param int $cat_id ID danh mục sản phẩm
     *  @since 201510609
     */
    public function listProductFilter($listID, $cat_id) {
        $arrayIDProduct = $this->litsProductByAge($listID);
        $this->db->where_in("id", $arrayIDProduct);
        $where = array("cat_id" => (int) $cat_id);
        return $this->function->getMulSelectTableWhere("id,tag,price,title,avatar,overview,cat_id", "products", $where);
    }

    /**
     *  @todo ID sản phẩm theo mảng đối tượng
     *  @param int $listAge ID mảng danhmục
     *  @since 201510609
     */
    public function litsProductByAge($listAge) {
        $array = explode(",", $listAge);
        $this->db->where_in("id_age", $array);
        $result = $this->function->getMulSelectTableWhere("id_product", "products_age", array());
        $out = array(0);
        foreach ($result as $row) {
            $out[] = $row['id_product'];
        }
        return $out;
    }

    /**
     *  @todo Video
     *  @since 2015106010
     */
    public function listVideo($limit = 0) {
        $where = array("status" => 1);
        if ($limit > 0) {
            $this->db->limit($limit);
        }
        return $this->function->getMulSelectTableWhere("title,link", "video", $where);
    }

    /**
     *  @todo Video
     *  @since 2015106010
     */
    public function listAlbum($type = 1) {
        $where = array("status" => 1, "type" => $type);
        return $this->function->getMulSelectTableWhere("id,title,tag,avatar", "albums ", $where, "ordering", "DESC");
    }

    /**
     * @todo Detail Album 
     * @param string $slug
     * @param int $id ID Album
     * @since 20150921
     */
    public function detailAlbum($slug, $id = 0) {
        $where = array("status" => 1, "tag" => trim($slug), "id" => $id);
        return $this->function->getSelectTableWhere("id,title,tag,avatar", "albums ", $where);
    }

    /**
     *  @todo Danh sách hình theo Album
     *  @since 2015106010
     */
    public function listImageByAlbum($id_albums) {
        $where = array("id_albums" => (int) $id_albums);
        return $this->function->getMulSelectTableWhere("image", "albums_image", $where, "ordering", "ASC");
    }

    /**
     *  @todo Hỏi đáp
     *  @since 2015106010
     */
    public function listFAQ() {
        $where = array("status" => 1);
        return $this->function->getMulSelectTableWhere("answer,question", "answer_question", $where, "ordering", "DESC");
    }

    /**
     *  @todo Danh mục tin tức
     * @param int $is_menu Hiện menu hay không
     *  @since 2015106010
     */
    public function listCatNews($is_menu = FALSE, $parent = 0) {
        $where = array("status" => 1);
        if ($is_menu) {
            $this->db->where(array("is_menu" => 1));
        }
        $this->db->where(array("parent" => $parent));
        return $this->function->getMulSelectTableWhere("id,title,tag,is_title,is_desc", "news_category", $where, "ordering", "ASC");
    }

    /**
     * @todo Danh sách hình theo album
     * @param int $id_album ID Album
     * @since 2015106010
     */
    public function listGalleryByAlbum($id_album = 0) {
        $where = array("id_albums" => (int) $id_album);
        return $this->function->getMulSelectTableWhere("image", "albums_image", $where, "ordering", "ASC");
    }

    /**
     * @todo Tin tức theo danh mục
     * @param int $num Số tin lấy
     * @param int $offset Vị trí cần lấy
     * @param int $idCat ID danh mục cha
     * @param int $is_home is trang chủ hay không
     * @param array $excludeID những Array ID news cần loại bỏ
     * @param int $is_hot 1: tin nổi bật, 0: không nổi bật
     * @since 201510701
     */
    public function listNewsByCategory($num, $offset, $idCat = 0, $is_home = FALSE, $excludeID = array(), $is_hot = FALSE) {
        $listCat = $this->listCatNews(FALSE, $idCat);
        $ID = array($idCat);
        foreach ($listCat as $row) {
            $ID[] = $row['id'];
        }

        // hiện trang chủ hay không
        if ($is_home) {
            $this->db->where(array("is_home" => 1));
        }
        $this->db->where_in("id_cat", $ID);

        if (!empty($excludeID) && is_array($excludeID)) {
            $this->db->where_not_in("id", $excludeID);
        }

        // sản phẩm nổi bật
        if ($is_hot) {
            $this->db->where(array("is_hot" => 1));
        }

        $this->db->select('id,tag,title,sumary,avatar');
        $this->db->from("news");
        $this->db->order_by('ordering', 'desc');
        $this->db->where(array("status" => 1));
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function listSearch($num, $offset, $key) {
        if (trim($key != "")) {
            $tag = $this->function->convertHTML($key);
            $this->db->like("tag", $tag, "both");
        }
        $this->db->select('id,tag,title,avatar,price,sale');
        $this->db->from("products");
        $this->db->order_by('ordering', 'desc');
        $this->db->where(array("status" => 1));
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * @todo Đếm tìm kiếm
     * @author Huynh Van Duoc <support@saigondomain.com>  
     * @since 20150703
     */
    public function countSearch($key = '') {
        if (trim($key != "")) {
            $tag = $this->function->convertHTML($key);
            $this->db->like("tag", $tag, "both");
        }
        return $this->function->total_rows("products", array("status" => 1));
    }

    /**
     * @todo Insert comment
     * @author Huynh Van Duoc <support@saigondomain.com>   
     */
    public function submitComment() {
        $params = $this->input->post();
        $data['name'] = strip_tags($params['name']);
        $data['subject'] = strip_tags($params['subject']);
        $data['email'] = strip_tags($params['email']);
        $data['message'] = strip_tags($params['message']);
        $data['link'] = strip_tags($params['link']);
        $data['id_news'] = (int) $params['id_news'];
        $data['parent'] = 0;
        $data["add_date"] = date("Y-m-d H:i:s");
        $this->db->insert("comment", $data);
    }

    /**
     * @todo Display comments by ID
     * @author Huynh Van Duoc <support@saigondomain.com>   
     */
    public function listComments($id_news = 0) {
        $where = array("status" => 1, "parent" => 0, "id_news" => $id_news);
        return $this->function->getMulSelectTableWhere("id,name,subject,message,add_date", "comment", $where, "id", "DESC");
    }

    /**
     * @todo Hiển thị bài viết trả lời
     * @author Huynh Van Duoc <support@saigondomain.com>   
     */
    public function replayComments($parent = 0) {
        $where = array("parent" => $parent);
        return $this->function->getMulSelectTableWhere("message,add_date", "comment", $where, "id", "DESC");
    }

    /**
     * @todo history page
     * @author Huynh Van Duoc <support@saigondomain.com>   
     */
    public function listHistory() {
        $where = array("status" => 1);
        return $this->function->getMulSelectTableWhere("id,year,desc", "history", $where, "ordering", "DESC");
    }

    /**
     * @todo List Photo
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function doitac() {
        return $this->function->getMulSelectTableWhere("id,link,image", "partner", array("status" => 1), "ordering", "DESC");
    }

    /**
     * @todo Tìm kiếm sản phẩm
     * @param string $key Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function listSearchProduct($key = "") {
        $key = $this->function->convertHTML($key);
        if ($key != "") {
            $this->db->like("tag", $key, "both");
        } else {
            $this->db->like("tag", "#######", "both");
        }
        return $this->function->getMulSelectTableWhere("id,tag,title,price,avatar,overview,cat_id,is_popup", "products", array("status" => 1), "ordering", "DESC");
    }

    /**
     * Lấy tên danh mục cha
     * @author : Huỳnh Văn Được
     * @copyright : dpassion
     */
    public function getName($select, $table, $where, $value) {
        $where = array($where => $value);
        $result = $this->function->getSelectTableWhere($select, $table, $where);
        return $result ? $result[$select] : "";
    }

    /**
     * Lưu thông tin đặt hàng
     * @author : Huỳnh Văn Được
     * @copyright : fromm
     */
    public function insertOrder() {
        $data = $this->input->post();
        $params['name'] = $data['hoten'];
        $params['address'] = $data['diachi'];
        $params['phone'] = $data['dienthoai'];
        $params['email'] = $data['email'];
        $params['comment'] = strip_tags($data['tinnhan']);
        $params['quality'] = $data['soluong'];
        $params['name_product'] = $data['name_product'];
        $params['link_product'] = $data['link_product'];
        $params['datetime'] = date("Y-m-d H:i:s");
        $this->db->insert("orders", $params);
    }

    /**
     * Lưu thông tin đặt hàng
     * @author : Huỳnh Văn Được
     * @copyright : fromm
     */
    public function insertContact() {
        $temp = $this->input->post();
        $params['name'] = $temp['hoten'];
        $params['phone'] = $temp['dienthoai'];
        $params['email'] = $temp['email'];
        $params['contact'] = strip_tags($temp['tinnhan']);
        $params['title'] = $temp['diachi'];
        $params['add_date'] = date("Y-m-d H:i:s");
        $this->db->insert("contact", $params);
    }

    /**
     * Lấy đường dẫn chi tiết sản phẩm
     * @author : Huỳnh Văn Được
     * @copyright : fromm
     */
    public function permartlinkProduct($idProduct = 0) {
        $detailProduct = $this->detailProductByID($idProduct);
        $baseLink = $detailProduct['tag'];
        $detailCatRootL2 = $this->detailCatByID($detailProduct['cat_id']);
        $detailCatRootL1 = $this->detailCatByID($detailCatRootL2['id']);
        return site_url($detailCatRootL1['tag'] . "/" . $detailCatRootL2['tag'] . "/" . $baseLink);
    }

    /**
     * @todo List City
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function listCity() {
        return $this->function->getMulSelectTableWhere("id,name", "city", array(), "name", "ASC");
    }

    public function getDistrict($id_city = 0) {
        $select = "id,name";
        $where = array("city" => (int) $id_city);
        $result = $this->function->getMulSelectTableWhere($select, "district", $where);
        return isset($result) ? $result : array();
    }

    /**
     * @todo Lưu đơn hàng
     * @since 20150721
     */
    public function insertOrderV2() {
        $params['code'] = $this->session->userdata("CODE_CART");
        $params['name_nguoigui'] = $this->session->userdata("name");
        $params['diachi_nguoigui'] = $this->session->userdata("address");
        $params['dienthoai_nguoigui'] = $this->session->userdata("phone");
        $params['email_nguoigui'] = $this->session->userdata("email");
        $params['time_order'] = date("Y/m/d H:i:s");
        $params['name_nguoinhan'] = $this->session->userdata("name_nguoinhan");
        $params['dienthoai_nguoinhan'] = $this->session->userdata("phone_nguoinhan");
        $params['diachi_nguoinhan'] = $this->session->userdata("address_nguoinhan");
        $params['time_delivery'] = $this->session->userdata("year") . "/" . $this->session->userdata("month") . "/" . $this->session->userdata("day") . ":" . $this->session->userdata("time") . ":00:00";
        $params['city'] = $this->session->userdata("city");
        $params['district'] = $this->session->userdata("district");
        $params['is_hide'] = $this->session->userdata("is_hide");
        $params['tangcho'] = $this->session->userdata("tangcho");
        $params['nhandip'] = $this->session->userdata("nhandip");
        $params['thongdiep'] = $this->session->userdata("thongdiep");
        $params['msg_fromm'] = $this->session->userdata("msg_fromm");
        $params['method_payment'] = $this->session->userdata("method");
        $params['status'] = 0;
        $this->db->insert("order", $params);
        $id_order = $this->db->insert_id();
        foreach ($this->cart->contents() as $items) {
            $detailProduct = $this->detailProductByID($items['id']);
            $order_products['id_order'] = $id_order;
            $order_products['code'] = $this->session->userdata("CODE_CART");
            $order_products['id_product'] = $items['id'];
            $order_products['qty'] = $items['qty'];
            $order_products['sale'] = $detailProduct['sale'];
            $order_products['price'] = $detailProduct['price'];
            $this->db->insert("order_products", $order_products);
        }
    }

    /**
     * Lưu thông tin đặt hàng
     * @author : Huỳnh Văn Được
     * @copyright : fromm
     */
    public function insertUser() {
        $data = $this->input->post();
        $data['pas'] = md5($data['pas']);
        $this->db->insert("customer", $data);
    }

    /**
     * Đăng nhập
     * @author : Huỳnh Văn Được
     * @copyright : fromm
     */
    public function login($email, $password) {
        $data = array();
        $this->db->select('id,email,name');
        $this->db->from('customer');
        $this->db->where(array('email' => $email, 'pas' => md5($password)));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        }
        $query->free_result();
        return $data;
    }

    /**
     * @todo List City
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function listOrderByEmail($email = "") {
        return $this->function->getMulSelectTableWhere("*", "order", array("email_nguoigui" => trim($email)), "id", "DESC");
    }

    public function forgetPassword($email = "") {
        $pass_news = random_string("numeric", 7);
        $params['pas'] = md5($pass_news);
        $this->db->where(array('email' => trim($email)));
        $this->db->update("customer", $params);
        return $pass_news;
    }

    public function detailCustomerByEmail($email = "") {
        $where = array("email" => $email);
        return $this->function->getSelectTableWhere("name,email", "customer", $where);
    }

    /**
     * @todo sản phẩm phụ kiện
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function fromm() {
        return $this->function->getMulSelectTableWhere("*", "products", array("status" => 1, "is_fromm" => 1), "ordering", "DESC");
    }

    public function getFooter(){
        $where = array("type" => 'cauhinh-footer');
        return $this->function->getSelectTableWhere("*", "baiviet_tinh", $where);
    }

    /**
     * @todo Get about Us
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_aboutus() {
        return $this->function->getMulSelectTableWhere("*", "gioithieu", array("status" => 1), "ordering", "DESC");
    }

    /**
     * @todo Get about Us
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_detail_aboutus($id) {
        if($id == ''){
            return $this->function->getSelectTableWhere("*", "gioithieu", array("status" => 1), "ordering", "DESC");
        }
        else{
            return $this->function->getSelectTableWhere("*", "gioithieu", array("status" => 1, 'tag' => $id), "ordering", "DESC");
        }
    }

    /**
     * @todo Get about Us
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_title_aboutus($id) { 
        if($id == ''){
            return $this->function->getSelectTableWhere("*", "gioithieu", array("status" => 1), "ordering", "DESC");
        }
        return $this->function->getSelectTableWhere("*", "gioithieu", array("status" => 1, 'tag' => $id), "ordering", "DESC");
    }

    /**
     * @todo Get about Us
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_sanpham() {
        return $this->function->getMulSelectTableWhere("*", "sanpham", array("status" => 1), "ordering", "DESC");
    }

    /**
     * @todo Get about Us
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_detail_sanpham($tag) {
        if($tag == ''){
            return $this->function->getSelectTableWhere("*", "sanpham", array("status" => 1), "ordering", "DESC");
        }
        else{
            return $this->function->getSelectTableWhere("*", "sanpham", array("status" => 1, 'tag' => $tag), "ordering", "DESC");
        }
    }

    /**
     * @todo Get about Us
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_dichvu() {
        return $this->function->getMulSelectTableWhere("*", "dichvu", array("status" => 1), "ordering", "DESC");
    }

    /**
     * @todo Get about Us
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_detail_dichvu($tag) {
        if($tag == ''){
            return $this->function->getSelectTableWhere("*", "dichvu", array("status" => 1), "ordering", "DESC");
        }
        else{
            return $this->function->getSelectTableWhere("*", "dichvu", array("status" => 1, 'tag' => $tag), "ordering", "DESC");
        }
    }

    /**
     * @todo Get about Us
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_title_dichvu($tag) {
        if($tag == ''){
            return $this->function->getSelectTableWhere("*", "dichvu", array("status" => 1), "ordering", "DESC");
        }
        else{
            return $this->function->getSelectTableWhere("*", "dichvu", array("status" => 1, 'tag' => $tag), "ordering", "DESC");
        }
        // return $this->function->getSelectTableWhere("*", "dichvu", array("status" => 1, 'tag' => $tag), "ordering", "DESC");
    }

    /**
     * @todo Get about Us
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_tintuc() {
        return $this->function->getMulSelectTableWhere("*", "news_category", array("status" => 1, "parent" => 0), "ordering", "DESC");
    }


    public function countNewsCategory($tag){
        if($tag == ''){
            
            $cate = $this->function->getSelectTableWhere("id", "news_category", array("status" => 1, "parent" => 0), "ordering", "DESC");

            $count = $this->function->getMulSelectTableWhere("*", "news", array("status" => 1, 'id_news_category' => $cate['id']), "ordering", "DESC");
             return count($count);
        }
        else{
            
            $cate = $this->function->getSelectTableWhere("id", "news_category", array("status" => 1, "parent" => 0, 'tag' => $tag), "ordering", "DESC");

            $count =  $this->function->getMulSelectTableWhere("*", "news", array("status" => 1, 'id_news_category' => $cate['id']), "ordering", "DESC");
             return count($count);
        }
        
    }

    /**
     * @todo Get about Us
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_detail_category_tintuc($tag, $num = 10, $offset = 0) {
        if($tag == ''){
            
            $cate = $this->function->getSelectTableWhere("id", "news_category", array("status" => 1, "parent" => 0), "ordering", "DESC");
            // return$cate;
            $this->db->limit($num, $offset);
            return $this->function->getMulSelectTableWhere("*", "news", array("status" => 1, 'id_news_category' => $cate['id']), "ordering", "DESC");
        }
        else{
            
            $cate = $this->function->getSelectTableWhere("id", "news_category", array("status" => 1, "parent" => 0, 'tag' => $tag), "ordering", "DESC");
            // return$cate;
            $this->db->limit($num, $offset);
            return $this->function->getMulSelectTableWhere("*", "news", array("status" => 1, 'id_news_category' => $cate['id']), "ordering", "DESC");
        }
    }
    /**
     * @todo Get about Us
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_title_category_tintuc($tag) {
        if($tag == ''){
            $cate = $this->function->getSelectTableWhere("*", "news_category", array("status" => 1, "parent" => 0), "ordering", "DESC");
            return $cate;
        }
        else{
            $cate = $this->function->getSelectTableWhere("*", "news_category", array("status" => 1, "parent" => 0, 'tag' => $tag), "ordering", "DESC");
            return $cate;
        }
    }

    public function get_detail_tintuc($tag = ''){
        return $this->function->getSelectTableWhere("*", "news", array("status" => 1, "tag" => $tag), "ordering", "DESC");
    }

    public function get_title_category_tintuc_detail($id){
        return $this->function->getSelectTableWhere("*", "news_category", array("status" => 1, "id" => $id), "ordering", "DESC");
    }

    public function get_tinlienquan($tag = ''){
        $temp = $this->function->getSelectTableWhere("*", "news", array("status" => 1, "tag" => $tag), "ordering", "DESC");
        $this->db->limit(4,0);
        $this->db->where('id !=', $temp['id']);
        return $this->function->getMulSelectTableWhere("*", "news", array("status" => 1, "id_news_category" => $temp['id_news_category']), "ordering", "DESC");
    }
    

    /**
     * @todo Get Tuyen ding
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_tuyendung() {
        return $this->function->getMulSelectTableWhere("*", "tuyendung_category", array("status" => 1, "parent" => 0), "ordering", "DESC");
    }

    /**
     * @todo Get tuyen dung
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_detail_category_tuyendung($tag, $num = 10, $offset = 0) {
        if($tag == ''){
            
            $cate = $this->function->getSelectTableWhere("id", "tuyendung_category", array("status" => 1, "parent" => 0), "ordering", "DESC");
            // return$cate;
            $this->db->limit($num, $offset);
            return $this->function->getMulSelectTableWhere("*", "tuyendung", array("status" => 1, 'cat_id' => $cate['id']), "ordering", "DESC");
        }
        else{
            
            $cate = $this->function->getSelectTableWhere("id", "tuyendung_category", array("status" => 1, "parent" => 0, 'tag' => $tag), "ordering", "DESC");
            // return$cate;
            $this->db->limit($num, $offset);
            return $this->function->getMulSelectTableWhere("*", "tuyendung", array("status" => 1, 'cat_id' => $cate['id']), "ordering", "DESC");
        }
    }

    /**
     * @todo Get about Us
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_title_category_tuyendung($tag) {
        if($tag == ''){
            $cate = $this->function->getSelectTableWhere("*", "tuyendung_category", array("status" => 1, "parent" => 0), "ordering", "DESC");
            return $cate;
        }
        else{
            $cate = $this->function->getSelectTableWhere("*", "tuyendung_category", array("status" => 1, "parent" => 0, 'tag' => $tag), "ordering", "DESC");
            return $cate;
        }
    }


    public function countHoiDap(){
        $where = array("status" => 1);
        return $this->function->total_rows("hoidap", $where);
    }

    public function get_hoidap(){
        return $this->function->getMulSelectTableWhere("*", "hoidap", array("status" => 1), "ordering", "DESC");
    }

    //Thu Vien
    /**
     * @todo Get about Us
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_thuvien() {
        return $this->function->getMulSelectTableWhere("*", "albums", array("status" => 1), "ordering", "ASC");
    }

    /**
     * @todo Get about Us
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_title_thuvien($tag) {
        if($tag == ''){
            $cate = $this->function->getSelectTableWhere("*", "albums", array("status" => 1), "ordering", "ASC");
            return $cate;
        }
        else{
            $cate = $this->function->getSelectTableWhere("*", "albums", array("status" => 1, 'tag' => $tag), "ordering", "ASC");
            return $cate;
        }
    }

    /**
     * @todo Get thu vien anh
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_detail_thuvien($tag) {
        if($tag == ''){
            
            $cate = $this->function->getSelectTableWhere("id", "albums", array("status" => 1), "ordering", "ASC");

            return $this->function->getMulSelectTableWhere("*", "albums_image", array('id_albums' => $cate['id']), "ordering", "ASC");
        }
        else{
            
            $cate = $this->function->getSelectTableWhere("id", "albums", array("status" => 1, 'tag' => $tag), "ordering", "ASC");

            return $this->function->getMulSelectTableWhere("*", "albums_image", array('id_albums' => $cate['id']), "ordering", "ASC");
        }
    }

    //Thu Vien
    /**
     * @todo Get about Us
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_doitac() {
        return $this->function->getMulSelectTableWhere("*", "doitac_category", array("status" => 1), "ordering", "DESC");
    }

    /**
     * @todo Get about Us
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_title_doitac($tag) {
        if($tag == ''){
            $cate = $this->function->getSelectTableWhere("*", "doitac_category", array("status" => 1), "ordering", "DESC");
            return $cate;
        }
        else{
            $cate = $this->function->getSelectTableWhere("*", "doitac_category", array("status" => 1, 'tag' => $tag), "ordering", "DESC");
            return $cate;
        }
    }

    /**
     * @todo Get thu vien anh
     * @param strong $lang Ngôn ngữ hiện tại
     * @since 20150721
     */
    public function get_detail_doitac($tag) {
        if($tag == ''){
            
            $cate = $this->function->getSelectTableWhere("id", "doitac_category", array("status" => 1), "ordering", "DESC");

            return $this->function->getMulSelectTableWhere("*", "doitac", array('cat_id' => $cate['id']), "ordering", "DESC");
        }
        else{
            
            $cate = $this->function->getSelectTableWhere("id", "doitac_category", array("status" => 1, 'tag' => $tag), "ordering", "DESC");

            return $this->function->getMulSelectTableWhere("*", "doitac", array('cat_id' => $cate['id']), "ordering", "DESC");
        }
    }


    /**
     * Lưu thông tin đặt hàng
     * @author : Huỳnh Văn Được
     * @copyright : fromm
     */
    public function insertUngTuyen() {
        $temp = $this->input->post();
        $params['hoten'] = $temp['hoten'];
        $params['dienthoai'] = $temp['dienthoai'];
        $params['email'] = $temp['email'];
        $params['diachi'] = $temp['diachi'];
        $params['add_date'] = date("Y-m-d H:i:s");
        $params['vitri_tuyen'] = $temp['id_vitri'];
        $cv = $this->duocmaster->uploadFileNew('upload/files/','userfile');
        if($cv){
            $params['cv'] = $cv;
        }
        $this->db->insert("ungtuyen", $params);
        if($cv){
            return $cv;
        }
    }

    public function getWork($id){
        return $this->function->getSelectTableWhere("*", "tuyendung", array("status" => 1, 'id' => $id), "ordering", "DESC");
    }


}
