<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron extends CI_Controller {

    private $domain = "http://namyangi.com.vn/";
    private $id_cat = 186;

    public function __construct() {
        parent::__construct();
        $this->load->library("duocmaster");
    }

    public function index() {
        require_once 'simple_html_dom.php';

            echo '<META HTTP-EQUIV="refresh" CONTENT="2">';
            $link = "http://namyangi.com.vn/chu-de/me---be/42/day-con?p=360";
            $html = file_get_html($link);
            foreach ($html->find('div.info') as $article) {
                $href = $this->domain . $article->find('a', 0)->href;
                $this->_detail($href);
            }
        
        echo "OK!";
    }

    public function _detail($link) {
        $html = file_get_html($link);
        foreach ($html->find('div#right-main') as $article) {
            $title = trim($article->find('h1', 0)->innertext);

            // Format day
            $date = trim($article->find('i', 0)->innertext);
            $date_original = trim(substr($date, 13, -2));

            $date_strip = explode(" - ", $date_original);
            $date = $this->function->changeFormatDate($date_strip[0]) . " " . $date_strip[1];

            $detail = trim($article->find('div#detail', 0)->innertext);
            $pos_begin = strpos($detail, '<div style="border-bottom: 1px solid #ddd;">&nbsp;</div>');
            $pos_end = strpos($detail, '<div class="clearfix"></div>                                                      <div style="color:#222222; font-size:12px;font-style: italic;" class="tag">');

            $detail = trim(substr($detail, $pos_begin + 60, $pos_end - $pos_begin));

            preg_match_all('/<img[^>]+>/i', $detail, $imgTags);
            for ($i = 0; $i < count($imgTags[0]); $i++) {
                preg_match('/src="([^"]+)/i', $imgTags[0][$i], $imgage);
                $src = str_ireplace('src="', '', $imgage[0]);
                if (!$this->isValidURL($src)) {
                    $detail = str_replace($src, $this->domain . $src, $detail);
                }
            }

            // Chuyển nội dung thành thuộc tính html
            $htmlGetIMG = str_get_html($detail);

            // Lấy hình đầu tiên làm avartar
            $avatar = isset($htmlGetIMG->find('img', 0)->src) ? explode(";", trim($htmlGetIMG->find('img', 0)->src)) : "";

            $tag = $this->function->convertHTML($title);
            $count = $this->function->total_rows("news", array("tag" => $tag));
            if ($count == 0) {
                $avatar = $this->saveImage($avatar[0]);
                $detail = stripcslashes(trim($this->removeTag_a($detail)));

                $data = array(
                    'title' => trim($title),
                    'tag' => $tag,
                    'sumary' => $this->function->cut_string(strip_tags($detail), 200),
                    'id_cat' => $this->id_cat,
                    'avatar' => "upload/news/lagre/" . $avatar,
                    'detail' => $detail,
                    'time' => $date,
                    'status' => 1
                );
                $this->load->database();
                $this->db->insert("news", $data);
            }
        }
    }

    public function removeTag_a($string) {
        $string = preg_replace("/<a[^>]+\>/i", "", $string);
        return $string;
    }

    public function saveImage($linkImg) {
        try {
            $file = $this->function->convertHTML(basename($linkImg));
            $this->duocmaster->saveImage($linkImg, "upload/news/lagre/" . $file);
            $this->duocmaster->resizeIMGv3("upload/news/lagre/" . $file, "upload/news/lagre/" . $file, 500, 400);
            $this->duocmaster->resizeIMGv3("upload/news/lagre/" . $file, "upload/news/thumb/" . $file, 246, 246);
            return $file;
        } catch (Exception $exc) {
            return false;
        }
    }

    function isValidURL($url) {
        return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
    }

}
