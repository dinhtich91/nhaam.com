<?php
if (!defined('BASEPATH'))  exit('No direct script access allowed');
class Slider_position extends CI_Controller {

    /**
     * Tên controller = tên thư mục(gồm form.php, list.php)
     */
    private $Controller = "slider_position";    
    private $TYPE       = "homepage";
    private $task;    
    
    public function __construct() {
        parent::__construct();    
        if(!$this->session->userdata('idAdmin')) redirect(PATH_FOLDER_ADMIN.'/login');
        $this->load->model(PATH_FOLDER_ADMIN.'/slider_position_model', 'slider_position');    
        $this->load->model(PATH_FOLDER_ADMIN.'/category_model', 'category'); 
        $this->load->model(PATH_FOLDER_ADMIN.'/user_model', 'user');
        $this->task=$this->task();  
        
        if ($this->session->userdata('levelAdmin')!=3) {
            exit('No direct script access allowed');
        }
    }

    /**
     * Nạp link task thêm,sửa,xóa,danh sách,tình trạng ẩn hiện,submit form (Xóa chọn, sắp sếp nhanh)
     * Dạng folderadmin/controller/method
     */
    public function task(){
        $data['task_add']      = PATH_FOLDER_ADMIN."/".$this->Controller."/add";
        $data['task_edit']     = PATH_FOLDER_ADMIN."/".$this->Controller."/edit";
        $data['task_del']      = PATH_FOLDER_ADMIN."/".$this->Controller."/del";
        $data['task_list']     = PATH_FOLDER_ADMIN."/".$this->Controller;
        $data['task_status']   = PATH_FOLDER_ADMIN."/".$this->Controller."/status";
        $data['action_form']   = PATH_FOLDER_ADMIN."/".$this->Controller."/action";
        $data['page']          = PATH_FOLDER_ADMIN."/".$this->Controller."/p";
        $data['task_serach']   = PATH_FOLDER_ADMIN."/".$this->Controller."/search";
        return $data;
    }       
    
    public function index() { 
        $this->p(0);
    }
    
    public function p($page=0){       
        $data = $this->task;    
        
        $data['title_header'] = "Vị trí Slider";
        $this->load->view(PATH_FOLDER_ADMIN.'/view.header.php',$data);
        /* #### */
        
                
        #Phân trang
        $config['base_url']    = $data['page'];
        $config['total_rows']  = $this->slider_position->total_rows($this->TYPE);
        $config['per_page']    = ADMIN_PER_PAGE; 
        $config['num_links']   = ADMIN_NUM_LINKS;
        $config['cur_page']    = $page;
        $this->pagination->initialize($config); 
        $data['total_rows']    = $config['total_rows'];
        $data['list']          = $this->slider_position->display($config['per_page'],$page,$this->TYPE);     
        
        /* #### */
        $this->load->view(PATH_FOLDER_ADMIN.'/'.$this->Controller.'/list',$data);
        $this->load->view(PATH_FOLDER_ADMIN.'/view.footer.php'); 
    }
    
    /**
     * Addtion
     */
    public function add(){        
        $data = $this->task;        
        if ($this->input->post()) {
            if($this->slider_position->add($this->TYPE)) $this->messages->add(MSG_ADD_SUCCESS, 'success');
            else $this->messages->add(MSG_ADD_ERROR, 'warning');
            redirect($data['task_list']);
        }
                
        $data['title_header']  = "Thêm mới - Nhóm quyền";
        $this->load->view(PATH_FOLDER_ADMIN.'/view.header.php',$data);
        $this->load->view(PATH_FOLDER_ADMIN.'/'.$this->Controller.'/form',$data);
        $this->load->view(PATH_FOLDER_ADMIN.'/view.footer.php'); 
    }
    
    public function edit($id){
        $data = $this->task;        
        if ($this->input->post()) {
            $this->slider_position->update($id,$this->TYPE);
            $this->messages->add(MSG_EDIT_SUCCESS, 'success');
            redirect($data['task_list']);
        }
        
        $data['title_header']   = "Chỉnh sửa - Nhóm quyền";
        $this->load->view(PATH_FOLDER_ADMIN.'/view.header.php',$data);
        /* #### */
        
        $data['detail']         = $this->slider_position->getList((int)$id);       
        
        /* #### */
        $this->load->view(PATH_FOLDER_ADMIN.'/'.$this->Controller.'/form',$data);
        $this->load->view(PATH_FOLDER_ADMIN.'/view.footer.php'); 
    }
    
    /**
     * Chức năng : Xóa bằng href
     * @author : Huỳnh Văn Được - 20121123
     */
    public function del($id){ 
        $data = $this->task;
        $this->slider_position->del($id);
        $this->messages->add(MSG_DEL_SUCCESS, 'success');
        redirect($data['task_list']);
    }
    
    /**
     * Chức năng : Ajax Hiện/Ẩn nhanh
     * @author : Huỳnh Văn Được - 20121123
     */
    public function status($id=0,$status=0,$field='status'){
        echo $this->slider_position->status($id,$status,$field);
    }
    /**
     * Chức năng : Xóa nhiều & Sắp xếp nhanh
     * @author : Huỳnh Văn Được - 20121123
     */
    public function action(){
        $data = $this->task;
        if($this->input->post("del")){
            $this->slider_position->del_all();     
            $this->messages->add(MSG_DEL_SUCCESS, 'success');
        }else if($this->input->post("ordering")){
            $this->slider_position->ordering_all();     
            $this->messages->add(MSG_EDIT_SUCCESS, 'success');
        }
        redirect($data['task_list']);
    }   
    
    /**
     * Trang tìm kiếm
     */
    public function search(){
        
        $filter_name = $this->input->get("filter_name");
        
        
        $data = $this->task;
        $page = 0;
        $data['title_header'] = "Tìm kiếm - ";
        $this->load->view(PATH_FOLDER_ADMIN.'/view.header.php',$data);
        /* #### */
        
        $data['list']          = $this->slider_position->displaySearch($filter_name);     
        $data['total_rows']    = count($data['list']);
        $data['filter_name']   = $filter_name;   
        
        /* #### */
        $this->load->view(PATH_FOLDER_ADMIN.'/'.$this->Controller.'/list',$data);
        $this->load->view(PATH_FOLDER_ADMIN.'/view.footer.php'); 
    }
}

?>
