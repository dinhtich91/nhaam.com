<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Common extends CI_Controller {

    private $Controller = "ajax";

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('idAdmin'))
            redirect(PATH_FOLDER_ADMIN . '/login');
        $this->load->library("duocmaster");
        $this->load->model(PATH_FOLDER_ADMIN . '/user_model', 'user');
    }

    /**
     * @todo Ajax upload hình đại diện
     * @author Huỳnh Văn Được 20130329 <duoc.huynh@dpassion.com>
     * @copyright (c) 2013, Dpassion
     */
    public function uploadLogo() {
        $image = $this->duocmaster->uploadImage("upload/images/large/", "uploadfile");
        if ($image) {
            $this->duocmaster->resizeIMGv2($image, "upload/images/" . basename($image), 600, 355);
            $this->duocmaster->resizeIMGv2($image, "upload/images/large/" . basename($image), 600, 355);            
            echo "upload/images/" . basename($image);
        } else {
            echo "error";
        }
    }

    public function uploadAvatarAlbums() {
        $image = $this->duocmaster->uploadImage("upload/album/lagre/", "uploadfile");
        if ($image) {
            $this->duocmaster->resizeIMGv3($image, "upload/album/thumb/" . basename($image), 304, 188);
            echo "upload/album/thumb/" . basename($image);
        } else {
            echo "error";
        }
    }

    /**
     * @todo Ajax thêm hình
     * @author Huỳnh Văn Được 20130329 <duoc.huynh@dpassion.com>
     * @copyright (c) 2013, Dpassion
     */
    public function addPhoto() {
        $data['cache'] = $this->input->get();
        //$data['listTypeFranchise']  = $this->type_franchise->displayAll();
        $this->load->view(PATH_FOLDER_ADMIN . '/' . $this->Controller . '/ajax.add_photo.php', $data);
    }

    public function addAlbum() {
        $data['cache'] = $this->input->get();
        $this->load->view(PATH_FOLDER_ADMIN . '/' . $this->Controller . '/ajax.addalbum.php', $data);
    }
    public function addDanDung() {
        $data['cache'] = $this->input->get();
        $this->load->view(PATH_FOLDER_ADMIN . '/' . $this->Controller . '/ajax.adddandung.php', $data);
    }

    /**
     * @todo Xóa hình
     * @author Huỳnh Văn Được 20130329 <duoc.huynh@dpassion.com>
     * @copyright (c) 2013, Dpassion
     */
    public function delPhoto() {
        $img = basename($this->input->POST("img"));
        @unlink(PATH_NORMAL . "/" . $img);
        @unlink(PATH_LARGE . "/" . $img);
        @unlink(PATH_THUMB . "/" . $img);
        @unlink(PATH_SMALL . "/" . $img);
    }

    /**
     * @todo Upload ajax hình phụ
     * @author Huỳnh Văn Được 20130401 <duoc.huynh@dpassion.com>
     */
    public function uploadPhoto() {
        $image = $this->duocmaster->uploadResize("upload/images/", 295, 445, TRUE, "uploadfile");
        if ($image) {
            echo $image;
        } else {
            echo "error";
        }
    }

    /**
     * @todo Upload ajax hình phụ
     * @author Huỳnh Văn Được 20130401 <duoc.huynh@dpassion.com>
     */
    public function uploadPhotoThumb() {
        $image = $this->duocmaster->uploadResize("upload/images/", 650, 500, TRUE, "uploadfile");
        if ($image) {
            echo $image;
        } else {
            echo "error";
        }
    }

    public function uploadPhotoAlbum() {
        $image = $this->duocmaster->uploadImage("upload/album/lagre/", "uploadfile");
        if ($image) {
            $this->duocmaster->resizeIMGv3($image, "upload/album/thumb/" . basename($image), 304, 188);
            echo "upload/album/thumb/" . basename($image);
        } else {
            echo "error";
        }
    }

    /**
     * @todo Upload ajax photo Franchise
     * @author Huỳnh Văn Được 20130401 <duoc.huynh@dpassion.com>
     */
    public function uploadAvatar() {
        $file = PATH_UPLOAD_AVATAR . "avatar." . time() . $this->function->getExtension($_FILES['uploadfile']['name']);
        if (move_uploaded_file($_FILES['uploadfile'] ['tmp_name'], $file)) {
            $this->duocmaster->resizeIMGv3($file, $file, 100, 100);
            echo $file;
        } else {
            echo "error";
        }
    }

}

?>
