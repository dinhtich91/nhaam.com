<?php
if (!defined('BASEPATH'))  exit('No direct script access allowed');
class Image extends CI_Controller {
    private $FOLDER ="/image/";    
    /**
     * @param : Tên controller
     * @todo : controller image
     */
    private $CONTROLLER = "/image";
    
    /**
     * @todo : hình intro
     */
    private $TYPE = "intro";
    
    public function __construct() {
        parent::__construct();       
        $this->load->model(PATH_FOLDER_ADMIN.'/user_model', 'user');    
        $this->load->model(PATH_FOLDER_ADMIN.'/image_model', 'image');
        $this->load->model(PATH_FOLDER_ADMIN.'/menu_model', 'menu');
        if (!$this->session->userdata('idAdmin'))  redirect(PATH_FOLDER_ADMIN.'/login'); 
        $this->load->model(PATH_FOLDER_ADMIN.'/user_model', 'user');
        if ($this->user->checkUserPermission($this->session->userdata('idAdmin'),$this->Controller) == 0) {
            exit('No direct script access allowed');
        }
    }
    /**
     * @author : Huỳnh Văn Được
     * @method : Hiển thị image
     * @filesource : image.php
     * @copyright : D'passion
     */
    public function index(){
        $data['headerCategory'] = $this->loadCategoryHeader();
        $data['title_header']   = 'Hình intro';
        $data['list']           = $this->image->display($this->TYPE);        
        $this->load->view(PATH_FOLDER_ADMIN.'/view.header.php', $data);
        $this->load->view(PATH_FOLDER_ADMIN.$this->FOLDER.'list.php', $data);
        $this->load->view(PATH_FOLDER_ADMIN.'/view.footer.php');
    }    
    public function loadCategoryHeader() {
        $this->load->model(PATH_FOLDER_ADMIN.'/category_model', 'category'); 
        return $this->category->display(0, 0, FALSE);
    }

    /**
     * @todo   : Chỉnh sửa image
     * @param  : id image
     * @author : Huỳnh Văn Được - 20121018
     */
    public function edit($id=0) {        
        $data['headerCategory'] = $this->loadCategoryHeader();        
        if ($this->input->post()) {            
            $this->image->update($id,$this->TYPE);     
            $this->messages->add('Chỉnh sửa thành công !', 'success');
            redirect(PATH_FOLDER_ADMIN.$this->CONTROLLER);
        }        
        $data['detail']         = $this->image->getList($id,$this->TYPE);
        $data['title_header']   = 'Sửa hình intro';       
        $data['vitriMax']       = $this->image->orderingMax($this->TYPE);      

        $this->load->view(PATH_FOLDER_ADMIN.'/view.header.php', $data);
        $this->load->view(PATH_FOLDER_ADMIN.$this->FOLDER.'/form.php', $data);
        $this->load->view(PATH_FOLDER_ADMIN.'/view.footer.php');
    }
    
    /**
     * @author : Huỳnh Văn Được
     * @todo :  Hàm thêm image
     * @filesource : image.php
     * @copyright : D'passion
     */
    public function add() {        
        $data['headerCategory']  = $this->loadCategoryHeader();
        $data['title_header']    = 'Thêm hình intro (Đối đa là 4 hình)';
        if ($this->input->post()) {            
            $this->image->add($this->TYPE);  
            $this->messages->add('Thêm thành công !', 'success');
            redirect(PATH_FOLDER_ADMIN.$this->CONTROLLER);
        }  
        $data['vitriMax']       = $this->image->orderingMax($this->TYPE);
   
        $this->load->view(PATH_FOLDER_ADMIN.'/view.header.php', $data);
        $this->load->view(PATH_FOLDER_ADMIN.$this->FOLDER.'form.php', $data);
        $this->load->view(PATH_FOLDER_ADMIN.'/view.footer.php');        
    }
    /**
     * @todo : Xóa image  
     * @param : id image
     * @author : Huỳnh Văn Được
     */
    public function del($id){
        $this->image->del($id);  
        $this->messages->add('Xóa thành công !', 'success');
        redirect(PATH_FOLDER_ADMIN.$this->CONTROLLER);
    }
    
    /**
     * @todo : Xóa theo chọn checked
     * @param : list các check box được chọn
     * @author : Huỳnh Văn Được
     */
    public function delAll(){
        if ($this->input->post()) {   
            $lists = $this->input->post("list");
            foreach($lists as $id){
                $this->image->del($id);   
            }
        }
        redirect(PATH_FOLDER_ADMIN.$this->CONTROLLER);
    }

}

?>
