<?php
if (!defined('BASEPATH'))  exit('No direct script access allowed');
class Tuyendung extends CI_Controller {

    /**
     * Tên controller = tên thư mục(gồm form.php, list.php)
     */
    private $Controller = "tuyendung";    
    private $task;    
    
    public function __construct() {
        parent::__construct();    
        if(!$this->session->userdata('idAdmin')) redirect(PATH_FOLDER_ADMIN.'/login');
        $this->load->model(PATH_FOLDER_ADMIN.'/tuyendung_model', 'tuyendung');    
        $this->load->model(PATH_FOLDER_ADMIN.'/age_model', 'age'); 
        $this->load->model(PATH_FOLDER_ADMIN.'/tuyendung_category_model', 'tuyendung_category'); 
        $this->load->model(PATH_FOLDER_ADMIN.'/thuonghieu_model', 'thuonghieu'); 
        $this->load->model(PATH_FOLDER_ADMIN.'/user_model', 'user');
        $this->task=$this->task();  
        
        //if ($this->user->checkUserPermission($this->session->userdata('idAdmin'),$this->Controller) == 0) {
            //exit('No direct script access allowed');
        //}
    }

    /**
     * Nạp link task thêm,sửa,xóa,danh sách,tình trạng ẩn hiện,submit form (Xóa chọn, sắp sếp nhanh)
     * Dạng folderadmin/controller/method
     */
    public function task(){
        $data['task_add']      = PATH_FOLDER_ADMIN."/".$this->Controller."/add";
        $data['task_edit']     = PATH_FOLDER_ADMIN."/".$this->Controller."/edit";
        $data['task_del']      = PATH_FOLDER_ADMIN."/".$this->Controller."/del";
        $data['task_list']     = PATH_FOLDER_ADMIN."/".$this->Controller;
        $data['task_status']   = PATH_FOLDER_ADMIN."/".$this->Controller."/status";
        $data['action_form']   = PATH_FOLDER_ADMIN."/".$this->Controller."/action";
        $data['page']          = PATH_FOLDER_ADMIN."/".$this->Controller."/p";
        $data['task_serach']   = PATH_FOLDER_ADMIN."/".$this->Controller."/search";
        return $data;
    }       
    
    public function index() { 
        $this->p(0);
    }
    
    public function p($page=0){       
        $data = $this->task;
        $data['title_header'] = "Tuyển dụng";
        $this->load->view(PATH_FOLDER_ADMIN.'/view.header.php',$data);
        /* #### */
        
                
        #Phân trang
        $config['base_url']    = $data['page'];
        $config['total_rows']  = $this->tuyendung->total_rows();
        $config['per_page']    = ADMIN_PER_PAGE; 
        $config['num_links']   = ADMIN_NUM_LINKS;
        $config['cur_page']    = $page;
        $this->pagination->initialize($config); 
        $data['total_rows']    = $config['total_rows'];
        $data['list']          = $this->tuyendung->display($config['per_page'],$page);     
        
        /* #### */
        $this->load->view(PATH_FOLDER_ADMIN.'/'.$this->Controller.'/list',$data);
        $this->load->view(PATH_FOLDER_ADMIN.'/view.footer.php'); 
    }
    
    /**
     * Addtion
     */
    public function add(){        
        $data = $this->task;        
        if ($this->input->post()) {
            
            $this->tuyendung->add();
            $this->messages->add(MSG_ADD_SUCCESS, 'success');
            redirect($data['task_list']);
        }
                
        $data['title_header'] = "Thêm mới";
        $this->load->view(PATH_FOLDER_ADMIN.'/view.header.php',$data);
        /* #### */
        
        $data['orderingMax']    = $this->tuyendung->orderingMax();
        $data['dequytuyendung_category']  = $this->tuyendung_category->dequytuyendung_category(0);
        
        /* #### */
        $this->load->view(PATH_FOLDER_ADMIN.'/'.$this->Controller.'/form',$data);
        $this->load->view(PATH_FOLDER_ADMIN.'/view.footer.php'); 
    }
    
    public function edit($id){
        $data = $this->task;        
        if ($this->input->post()) {
            $this->tuyendung->update($id);
            $this->messages->add(MSG_EDIT_SUCCESS, 'success');
            redirect($data['task_list']);
        }
        
        $data['title_header']   = "Chỉnh sửa";
        $this->load->view(PATH_FOLDER_ADMIN.'/view.header.php',$data);
        /* #### */
        
        $data['detail']         = $this->tuyendung->getList((int)$id);       
        $data['dequytuyendung_category']  = $this->tuyendung_category->dequytuyendung_category(0);        
        // $data['getImgThumb']    = $this->tuyendung->getImgThumb($id);
        
        /* #### */
        $this->load->view(PATH_FOLDER_ADMIN.'/'.$this->Controller.'/form',$data);
        $this->load->view(PATH_FOLDER_ADMIN.'/view.footer.php'); 
    }
    
    /**
     * Chức năng : Xóa bằng href
     * @author : Huỳnh Văn Được - 20121123
     */
    public function del($id){ 
        $data = $this->task;
        $this->tuyendung->del($id);
        $this->messages->add(MSG_DEL_SUCCESS, 'success');
        redirect($data['task_list']);
    }
    
    /**
     * Chức năng : Ajax Hiện/Ẩn nhanh
     * @author : Huỳnh Văn Được - 20121123
     */
    public function status($id=0,$status=0,$field='status'){
        echo $this->tuyendung->status($id,$status,$field);
    }
    /**
     * Chức năng : Xóa nhiều & Sắp xếp nhanh
     * @author : Huỳnh Văn Được - 20121123
     */
    public function action(){
        $data = $this->task;
        if($this->input->post("del")){
            $this->tuyendung->del_all();     
            $this->messages->add(MSG_DEL_SUCCESS, 'success');
        }else if($this->input->post("ordering")){
            $this->tuyendung->ordering_all();     
            $this->messages->add(MSG_EDIT_SUCCESS, 'success');
        }
        redirect($data['task_list']);
    }   
    
    /**
     * Trang tìm kiếm
     */
    public function search(){        
        $filter_name = $this->input->get("filter_name");        
        
        $data = $this->task;
        $page = 0;
        $data['title_header']  = "Tìm kiếm - ";
        $this->load->view(PATH_FOLDER_ADMIN.'/view.header.php',$data);
        /* #### */
        
        $data['list']          = $this->tuyendung->displaySearch($filter_name);     
        $data['total_rows']    = count($data['list']);
        $data['filter_name']   = $filter_name;   
        
        /* #### */
        $this->load->view(PATH_FOLDER_ADMIN.'/'.$this->Controller.'/list',$data);
        $this->load->view(PATH_FOLDER_ADMIN.'/view.footer.php');
    }
    /**
     * @todo Ajax get thể tích và giá theo sản phẩm
     * @author HVD 20130910 <duoc.huynh@dpassion.com>
     * @copyright (c) Gevi, Dpassion
     */
    public function ajax_VolumePrice($id_product=0){
        $data['volume_price'] = $this->tuyendung->getVolumePriceProduct($id_product);
        $this->load->view(PATH_FOLDER_ADMIN.'/ajax/ajax.volume_price.php',$data);
    }
    /**
     * @todo Ajax get thể tích và giá theo sản phẩm
     * @author HVD 20130910 <duoc.huynh@dpassion.com>
     * @copyright (c) Gevi, Dpassion
     */
    public function getImgThumb($id_product=0){
        $data['thumb'] = $this->tuyendung->getImgThumb($id_product);
        $this->load->view(PATH_FOLDER_ADMIN.'/ajax/ajax.img.thumb.php',$data);
    }
    /**
     * @todo Ajax get thương hiệu theo danh mục
     * @author HVD 20130910 <duoc.huynh@dpassion.com>
     * @copyright (c) Gevi, Dpassion
     */
    public function ajaxThuongHieu($id_cat=0,$id_trademark=0){
        $data                = $this->tuyendung_category->getList((int)$id_cat);
        $data['result']      = array();    
        $data['selected']    = $id_trademark;
        if($data){
            $data['result'] = $this->thuonghieu->getTrademarkByCate($data['trademark']);
        }
        $this->load->view(PATH_FOLDER_ADMIN.'/ajax/ajax.trademark.php',$data);
    }
    
    
}

?>
