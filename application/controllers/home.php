<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->Mylang = $this->getLang();
        $this->lang->load($this->Mylang, "lang");

        $this->load->model('config_model', 'myconfig');
        $this->load->model('fromm_model', 'fromm');
        $this->load->model('staticbaiviet_model', 'staticbaiviet');
        $this->load->library('breadcrumb');
    }

    /**
     * @todo Index
     */
    public function index() {
        $data['active_menu'] = "home";
        $data['lang'] = $this->Mylang;

        $data['active_menu'] = "home";
        $data['class_body'] = "home";

        // $data['listCateParent'] = $this->fromm->listCategoryByParent(0);
        // $data['recentProducts'] = $this->fromm->listRecentProduct(15);
        // $data['sideBox'] = $this->fromm->listCategorySide(1);

        $this->load->view('index.header.php', $data);
        $this->load->view('home/index', $data);
        $this->load->view('index.footer.php');
    }

    /**
     * @todo Trang giời thiệu
     * @author Huynh Van Duoc <suport@saigondomain.com>
     * @since 20150917
     */
    public function aboutus($id_gioithieu = '') {
        $data['detail'] = $this->staticbaiviet->getList("gioi-thieu");
        $data['title_header'] = "Giới thiệu về FROMM";
        $data['active_menu'] = "gioithieu";
        $data['lang'] = $this->Mylang;
        $data['class_body'] = "intro";
        $data['list'] = $this->fromm->get_aboutus();
               
        $data['detail_gioithieu'] = $this->fromm->get_detail_aboutus($id_gioithieu);
        $data['title_cate'] = $this->fromm->get_title_aboutus($id_gioithieu);

        $this->load->view('index.header.php', $data);
        $this->load->view('page/page.aboutus.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    /**
     * @todo Trang Sản Phẩm
     * @author Huynh Van Duoc <suport@saigondomain.com>
     * @since 20150917
     */
    public function sanpham($tag = 0) {

        if ($this->input->post()) {
            $data = $this->input->post();
            $name = trim($data['hoten']);           
            $address = trim($data['diachi']);
            $email = trim($data['email']);
            $phone = trim($data['dienthoai']);
            $quality = (int) trim($data['soluong']);
            $comment = trim($data['tinnhan']);
            if ($name != "" && $email != "" && $phone != "" && $address != "") {
                if (!$this->function->valid_email($email))
                    echo "<script> alert('Email không đúng định dạng!') </script>";
                else if ($quality == 0)
                    echo "<script> alert('Vui lòng chọn số lượng!') </script>";
                else {
                    $this->fromm->insertOrder();
                    $body = "<h3>Thông tin đặt hàng -------------</h3>";
                    $body.="<p><strong>Họ tên:</strong> " . $name . "</p>";
                    $body.="<p><strong>Địa chỉ:</strong> " . $address . "</p>";
                    $body.="<p><strong>Email:</strong> " . $email . "</p>";
                    $body.="<p><strong>Điện thoại:</strong> " . $phone . "</p>";
                    $body.="<p><strong>Nội dung:</strong> " . $comment . "</p>";
                    $body.="<h3>Thông tin sản phẩm -------------</h3>";
                    $body.="<p><strong>Sản phẩm:</strong> " . $data['name_product'] . "</p>";
                    $body.="<p><strong>Link sản phẩm:</strong> " . $data['link_product'] . "</p>";
                    $body.="<p><strong>Số lượng:</strong> " . $data['soluong'] . "</p>";
                    $body.="<p><em>Email này được gửi từ website của FROMM</em></p>";
                    $subject = "[FROMM THÔNG TIN ĐẶT HÀNG]";
                    if ($this->send("Đặt hàng từ website - " . $name, $email, $body, $subject, $email)) {
                        echo "<script> alert('Đặt hàng thành công!') </script>";
                    } else {
                       echo "<script> alert('Xảy ra lỗi!') </script>";
                    }
                }
            } else {
                echo "<script> alert('Vui lòng nhập đủ thông tin!') </script>";
            }
        }

        $data['title_header'] = "Giới thiệu về FROMM";
        $data['active_menu'] = "sanpham";
        $data['lang'] = $this->Mylang;
        $data['class_body'] = "product";

        $data['list'] = $this->fromm->get_sanpham();       
        $data['detail_sanpham'] = $this->fromm->get_detail_sanpham($tag);

        $this->load->view('index.header.php', $data);
        $this->load->view('product/product.detail.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    /**
     * @todo Trang Sản Phẩm
     * @author Huynh Van Duoc <suport@saigondomain.com>
     * @since 20150917
     */
    public function dichvu($tag = '') {

        $data['title_header'] = "Dịch vụ FROMM";
        $data['active_menu'] = "dichvu";
        $data['lang'] = $this->Mylang;
        $data['class_body'] = "product";
        
        $data['list'] = $this->fromm->get_dichvu();       
        $data['detail_sanpham'] = $this->fromm->get_detail_dichvu($tag);
        $data['title_cate'] = $this->fromm->get_title_dichvu($tag);

        $this->load->view('index.header.php', $data);
        $this->load->view('dichvu/dichvu.detail.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    /**
     * @todo Trang Tin Tuc
     * @author Huynh Van Duoc <suport@saigondomain.com>
     * @since 20150917
     */
    public function tintuc($tag = '', $page = 0) {

        $data['title_header'] = "Tin tức FROMM";
        $data['active_menu'] = "tintuc";
        $data['lang'] = $this->Mylang;
        $data['class_body'] = "product";

        $data['title_cate'] = $this->fromm->get_title_category_tintuc($tag);
        $tag = $data['title_cate']['tag'];

        $config['total_rows'] = $this->fromm->countNewsCategory($tag);

        $config['base_url'] = base_url('tin-tuc/'.$tag."/");
        $config['first_url'] = base_url('tin-tuc/'.$tag."/");
        $config['cur_page'] = $page;
        $config['per_page'] = 5;
        $config['num_links'] = 5;
        $this->pagination->initialize($config);
        
        $data['list'] = $this->fromm->get_tintuc();       
        $data['list_new_cate'] = $this->fromm->get_detail_category_tintuc($tag, $config['per_page'], $page);
        $data['title_cate'] = $this->fromm->get_title_category_tintuc($tag);

        $this->load->view('index.header.php', $data);
        $this->load->view('news/news.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    /**
     * @todo Trang Tin Tuc
     * @author Huynh Van Duoc <suport@saigondomain.com>
     * @since 20150917
     */
    public function tuyendung($tag = '', $page = 0) {

        if ($this->input->post()) {
            $data['lang'] = $this->Mylang;
            $params = $this->input->post();
            $name = trim($params['hoten']);
            $address = trim($params['diachi']);
            $email = trim($params['email']);
            $phone = trim($params['dienthoai']);

            if ($name != "" && $email != "" && $phone != "" && $address != "") {
                if (!$this->function->valid_email($email))
                    echo "<script> alert('Email không đúng định dạng!') </script>";
                else {
                    $temp = $this->fromm->insertUngTuyen();

                    $adminConfig = $this->myconfig->getConfig();
                    
                    $body = "<h3>Thông tin ứng tuyển</h3>";
                    $body.="<p><strong>Họ tên:</strong> " . $name . "</p>";
                    $body.="<p><strong>Địa chỉ:</strong> " . $address . "</p>";
                    $body.="<p><strong>Email:</strong> " . $email . "</p>";
                    $body.="<p><strong>Điện thoại:</strong> " . $phone . "</p>";
                    $body.="<p><strong>Địa chỉ:</strong> " . $address . "</p>";
                    $body.="<p><strong>CV Ứng viên:</strong> " . base_url() .'/'.$temp. "</p>";


                    $subject = "[fromm]";
                    if ($this->send("Ứng tuyển - " . $name, $email, $body, $subject, $email)) {
                        echo "<script> alert('Gửi thành công!') </script>";
                    } else {
                        echo "<script> alert('Xảy ra lỗi!') </script>";
                    }
                }
            } else {
                echo "<script> alert('Vui lòng nhập đủ thông tin!') </script>";
            }
        }

        $data['title_header'] = "Tin tức FROMM";
        $data['active_menu'] = "tuyendung";
        $data['lang'] = $this->Mylang;
        $data['class_body'] = "product";

        $data['list'] = $this->fromm->get_tuyendung();       
        $data['list_new_cate'] = $this->fromm->get_detail_category_tuyendung($tag);
        $data['title_cate'] = $this->fromm->get_title_category_tuyendung($tag);

        $this->load->view('index.header.php', $data);
        $this->load->view('tuyendung/tuyendung.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    /**
     * @todo Trang Sản Phẩm
     * @author Huynh Van Duoc <suport@saigondomain.com>
     * @since 20150917
     */
    public function tintucdetail($tag = '') {

        $data['title_header'] = "Tin tức FROMM";
        $data['active_menu'] = "tintuc";
        $data['lang'] = $this->Mylang;
        $data['class_body'] = "product";


        $data['list'] = $this->fromm->get_tintuc();       
        $data['list_new_cate'] = $this->fromm->get_detail_tintuc($tag);
        if(empty($data['list_new_cate'])){
            redirect(base_url());
        }
        $data['title_cate'] = $this->fromm->get_title_category_tintuc_detail($data['list_new_cate']['id_news_category']);
        $data['tinlienquan'] = $this->fromm->get_tinlienquan($tag);

        $this->load->view('index.header.php', $data);
        $this->load->view('news/news.detail.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    /**
     * @todo Trang Tin Tuc
     * @author Huynh Van Duoc <suport@saigondomain.com>
     * @since 20150917
     */
    public function hoidap($page = 0) {

        $data['title_header'] = "Tin tức FROMM";
        $data['active_menu'] = "hoidap";
        $data['lang'] = $this->Mylang;
        $data['class_body'] = "product";

        $config['total_rows'] = $this->fromm->countHoiDap();

        $config['base_url'] = base_url('hoi-dap/');
        $config['first_url'] = base_url('hoi-dap/');
        $config['cur_page'] = $page;
        $config['per_page'] = 1;
        $config['num_links'] = 5;
        $this->pagination->initialize($config);
        
        $data['list'] = $this->fromm->get_hoidap();       

        $this->load->view('index.header.php', $data);
        $this->load->view('hoidap/hoidap.php', $data);
        $this->load->view('index.footer.php', $data);
    }

        /**
     * @todo Trang Tin Tuc
     * @author Huynh Van Duoc <suport@saigondomain.com>
     * @since 20150917
     */
    public function thuvien($tag = '') {

        $data['title_header'] = "Thư viện FROMM";
        $data['active_menu'] = "thuvien";
        $data['lang'] = $this->Mylang;
        $data['class_body'] = "product";

        
        $data['list'] = $this->fromm->get_thuvien();       
        $data['list_new_cate'] = $this->fromm->get_detail_thuvien($tag);
        $data['title_cate'] = $this->fromm->get_title_thuvien($tag);

        $this->load->view('index.header.php', $data);
        $this->load->view('thuvien/thuvien.php', $data);
        $this->load->view('index.footer.php', $data);
    }

        /**
     * @todo Trang Tin Tuc
     * @author Huynh Van Duoc <suport@saigondomain.com>
     * @since 20150917
     */
    public function doitac($tag = '') {

        $data['title_header'] = "Đối tác FROMM";
        $data['active_menu'] = "doitac";
        $data['lang'] = $this->Mylang;
        $data['class_body'] = "product";

        
        $data['list'] = $this->fromm->get_doitac();       
        $data['list_new_cate'] = $this->fromm->get_detail_doitac($tag);
        $data['title_cate'] = $this->fromm->get_title_doitac($tag);

        $this->load->view('index.header.php', $data);
        $this->load->view('doitac/doitac.php', $data);
        $this->load->view('index.footer.php', $data);
    }

        /**
     * @todo Trang Tin Tuc
     * @author Huynh Van Duoc <suport@saigondomain.com>
     * @since 20150917
     */
    public function getWork($id = 0) {

        $data['title_header'] = "Work FROMM";
        $data['active_menu'] = "doitac";
        $data['lang'] = $this->Mylang;
        $data['class_body'] = "product";
        
        $data['work'] = $this->fromm->getWork($id);       

        $this->load->view('tuyendung/work.php', $data);
    }

    /**
     * @todo Liên hệ
     * @author Huynh Van Duoc  <support@saigondomain.com>
     * @since 20150610
     */
    public function contact() {
        $data['title_header'] = "Liên hệ fromm";
        $data['active_menu'] = "lienhe";
        $data['lang'] = $this->Mylang;

        if ($this->input->post()) {
            $data['lang'] = $this->Mylang;
            $params = $this->input->post();
            $name = trim($params['hoten']);
            $address = trim($params['diachi']);
            $email = trim($params['email']);
            $phone = trim($params['dienthoai']);
            $comment = trim($params['tinnhan']);
            if ($name != "" && $email != "" && $phone != "" && $comment != "" && $address != "") {
                if (!$this->function->valid_email($email))
                    echo "<script> alert('Email không đúng định dạng!') </script>";
                else if ($comment == "")
                    echo "<script> alert('Vui lòng nội dung liên hệ!') </script>";
                else {
                    $this->fromm->insertContact();
                    $adminConfig = $this->myconfig->getConfig();
                    
                    $body = "<h3>Thông tin liên hệ</h3>";
                    $body.="<p><strong>Họ tên:</strong> " . $name . "</p>";
                    $body.="<p><strong>Địa chỉ:</strong> " . $address . "</p>";
                    $body.="<p><strong>Email:</strong> " . $email . "</p>";
                    $body.="<p><strong>Điện thoại:</strong> " . $phone . "</p>";
                    $body.="<p><strong>Địa chỉ:</strong> " . $address . "</p>";
                    $body.="<p><strong>Nội dung:</strong> " . $comment . "</p>";

                    $subject = "[fromm]";
                    if ($this->send("Liên hệ - " . $name, $email, $body, $subject)) {
                        echo "<script> alert('Gửi thành công!') </script>";
                    } else {
                        echo "<script> alert('Xảy ra lỗi!') </script>";
                    }
                }
            } else {
                echo "<script> alert('Vui lòng nhập đủ thông tin!') </script>";
            }
        }

        // breadcrumb
        $data['class_body'] = "contact";

        $this->load->view('index.header.php', $data);
        $this->load->view('page/contact.php', $data);
        $this->load->view('index.footer.php', $data);
    }


    /**
     * @todo Gửi email liên hệ
     * @author Huynh Van Duoc <support@saigondomain.com>
     * @since 20150615
     */
    public function sendEmail() {
        $json = array();
        if ($this->input->post()) {
            $data = $this->input->post();
            $name = trim($data['name']);
            $address = trim($data['address']);
            $email = trim($data['email']);
            $phone = trim($data['phone']);
            $comment = trim($data['comment']);
            if ($name != "" && $email != "" && $phone != "" && $comment != "" && $address != "") {
                if (!$this->function->valid_email($email))
                    $json['email_valid'] = "Email không đúng định dạng";
                else if ($comment == "")
                    $json['contact_require'] = "Vui lòng nội dung liên hệ.";
                else {
                    $this->fromm->insertContact();
                    $adminConfig = $this->myconfig->getConfig();
//                $to = $adminConfig['email'];
//                $subject = 'Liên hệ từ Website';
//                $headers = "MIME-Version: 1.0" . "\r\n";
//                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
//                $headers .= 'From: <info@fromm.com.vn>' . "\r\n";
                    
                    $body = "<h3>Thông tin liên hệ</h3>";
                    $body.="<p><strong>Họ tên:</strong> " . $name . "</p>";
                    $body.="<p><strong>Địa chỉ:</strong> " . $address . "</p>";
                    $body.="<p><strong>Email:</strong> " . $email . "</p>";
                    $body.="<p><strong>Điện thoại:</strong> " . $phone . "</p>";
                    $body.="<p><strong>Địa chỉ:</strong> " . $address . "</p>";
                    $body.="<p><strong>Nội dung:</strong> " . $comment . "</p>";
                    $body.="<p><em>Email này được gửi từ website fromm247.com</em></p>";
                    $subject = "[fromm247]";
                    if ($this->send("Liên hệ - " . $name, $email, $body, $subject)) {
                        $json['success'] = "Gửi thành công, chúng tôi sẽ phản hồi bạn trong thời gian sớm nhất.";
                    } else {
                        $json['success'] = "Gửi thất bại, vui lòng gọi số hotline: 0909.306.952 - 0909.300.952";
                    }
                }
            } else {
                $json['error_require'] = "Vui lòng nhập đủ thông tin";
            }
        }
        echo json_encode($json);
    }


    /**
     * @todo List Cat by parent
     * @author Huynh Van Duoc <support@saigondomain.com>
     * @since 20150918
     */
    public function productCatList($tag = "", $page = 0) {

        $detailCat = $this->fromm->detailCatByTag($tag);
        $tag_parent = $tag;
        $big_father_id = $detailCat['parent'];
        $data['detail'] = $this->fromm->detailCatByTag($tag);
        if (!$detailCat) {
            redirect(base_url());
        }
        $data['title_header'] = $detailCat['title'];
        $data['active_menu'] = $tag;
        $data['class_body'] = "list-products";
        
        $list_cat_child = $this->fromm->listCatByParent($detailCat['id']);
        
        if (count($list_cat_child) == 0) {
            $cat_id = $detailCat['id'];
            $parent_detcat = $detailCat['parent'];
            $parent_for_b = $this->fromm->findParentByID($parent_detcat);
            
            $config['total_rows'] = $this->fromm->countProduct($cat_id);
            $this->breadcrumb->append_crumb('Trang chủ', base_url());//Samsung
            $this->breadcrumb->append_crumb($parent_for_b[0]['title'], $parent_for_b[0]['tag']);
            $this->breadcrumb->append_crumb($detailCat['title'], current_url());
            $config['base_url'] = base_url($tag_parent . "/");
            $config['first_url'] = site_url($tag_parent . "/");
            $config['cur_page'] = $page;
            $config['per_page'] = CLIENT_PER_PAGE;
            $config['num_links'] = 5;
            $this->pagination->initialize($config);
            $data['list'] = $this->fromm->listProductByCatID($config['per_page'], $page, $cat_id);
            $data['listCateParent'] = $this->fromm->listCategoryByParent(0);

            $data['tag_current'] = $tag;
            $this->load->view('index.header.php', $data);
            $this->load->view('product-cat/product-cat.list.php', $data);
            $this->load->view('index.footer.php', $data);
            return false;
        }
        foreach ($list_cat_child as $array_con) {
            $mot_chieu [] = $array_con['id'];
        }
        
        $parent_con = $this->fromm->listCategoryByParentArray($mot_chieu);
        if (count($parent_con) == 0) {
            $parent_con = $mot_chieu;
            $parent_for_life = $this->fromm->findParentByID($big_father_id);

            $mot_chieu[] = $detailCat['id'];
            $config['total_rows'] = $this->fromm->countProductArray($mot_chieu);
            $this->breadcrumb->append_crumb('Trang chủ', base_url());
            $this->breadcrumb->append_crumb($parent_for_life[0]['title'], $parent_for_life[0]['tag']);
            $this->breadcrumb->append_crumb($detailCat['title'], current_url());
            $config['base_url'] = base_url($tag_parent . "/");
            $config['first_url'] = site_url($tag_parent . "/");
            $config['cur_page'] = $page;
            $config['per_page'] = CLIENT_PER_PAGE;
            $config['num_links'] = 5;
            $this->pagination->initialize($config);
            $data['list'] = $this->fromm->listProductByArrayCatID($config['per_page'], $page, $mot_chieu);
            $data['listCateParent'] = $this->fromm->listCategoryByParent(0);

            $data['tag_current'] = $tag;
            $this->load->view('index.header.php', $data);
            $this->load->view('product-cat/product-cat.list.php', $data);
            $this->load->view('index.footer.php', $data);
            return false;
        }
        foreach ($parent_con as $parent_cuoi) {
            $mot_chieu_2[] = $parent_cuoi['id'];
        }
        $mot_chieu_2[] = $detailCat['id'];
        $data['listCateParent'] = $this->fromm->listCategoryByParent(0);
        $config['total_rows'] = $this->fromm->countProductArray($mot_chieu_2);
        

        $config['base_url'] = base_url($tag_parent . "/");
        $config['first_url'] = site_url($tag_parent . "/");
        $config['cur_page'] = $page;
        $config['per_page'] = CLIENT_PER_PAGE;
        $config['num_links'] = 5;
        $this->pagination->initialize($config);
        $data['list'] = $this->fromm->listProductByArrayCatID($config['per_page'], $page, $mot_chieu_2);
        $this->breadcrumb->append_crumb('Trang chủ', base_url()); //Samsung
        $this->breadcrumb->append_crumb($detailCat['title'], current_url());
        $data['tag_current'] = $tag;
        $this->load->view('index.header.php', $data);
        $this->load->view('product-cat/product-cat.list.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    /**
     * @todo List product by category
     * @author Huynh Van Duoc <support@saigondomain.com>
     * @since 20150918
     */
    public function productList($tag_parent, $tag_child, $page = 0) {
        $detailCat = $this->fromm->detailCatByTag($tag_child);
        if (!$detailCat) {
            redirect(base_url());
        }
        $this->session->set_userdata(array("CALLBACK_URL" => current_url()));

        // Chi tiết danh mục gốc
        $detailCatParent = $this->fromm->detailCatByTag($tag_parent);

        $data['title_header'] = $detailCat['title'];
        $data['active_menu'] = $tag_parent;
        $data['class_body'] = "list-products";

        // breadcrumb
        $this->breadcrumb->append_crumb('Trang chủ', base_url());
        $this->breadcrumb->append_crumb($detailCatParent['title'], site_url($detailCatParent['tag']));
        $this->breadcrumb->append_crumb($detailCat['title'], current_url());

        // Danh sách sản phẩm có phân trang
        $config['base_url'] = base_url($tag_parent . "/" . $tag_child);
        $config['first_url'] = site_url($tag_parent . "/" . $tag_child);
        $config['total_rows'] = $this->fromm->countProduct($detailCat['id']);
        $config['cur_page'] = $page;
        $config['per_page'] = CLIENT_PER_PAGE;
        $config['num_links'] = 5;
        $this->pagination->initialize($config);
        $data['list'] = $this->fromm->listProductByCatID($config['per_page'], $page, $detailCat['id']);

        $data['tag_parent'] = $tag_parent;
        $data['tag_child'] = $tag_child;

        $this->load->view('index.header.php', $data);
        $this->load->view('product/product.list.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    public function productDetail($tag = "" , $page ="", $id = "") {
        
        $detailPro = $this->fromm->detailProductByID($id);
        
        if (!$detailPro) {
            redirect(base_url());
        }
        $tag_parent_id = $detailPro['cat_id'];
        // Chi tiết danh mục gốc
        $detailCatParent = $this->fromm->findParentByID($tag_parent_id);
        
        if (!$detailCatParent) {
            redirect(base_url());
        }
        
        $detailCatGran = $this->fromm->findParentByID($detailCatParent[0]['parent']);
        $detailCatGrangran = $this->fromm->findParentByID($detailCatGran[0]['parent']);
        $data['title_header'] = $detailPro['title'];
        $data['active_menu'] = $detailCatGrangran[0]['title'];
        $data['class_body'] = "product-details";
        $data['detailProduct'] = $detailPro;
        
        // breadcrumb
        $this->breadcrumb->append_crumb('Trang chủ', base_url());
        $this->breadcrumb->append_crumb($detailCatGrangran[0]['title'], site_url($detailCatGrangran[0]['tag']));
        $this->breadcrumb->append_crumb($detailCatGran[0]['title'], site_url($detailCatGran[0]['tag']));
        $this->breadcrumb->append_crumb($detailCatParent[0]['title'], site_url($detailCatParent[0]['tag']));
        $this->breadcrumb->append_crumb($detailPro['title'], current_url());
        $data['listCateParent'] = $this->fromm->listCategoryByParent(0);
        
        //other product by Parent
        $product_f = array();
        $son_of_grangran = $this->fromm->listCatByParent($detailCatGrangran[0]['id']);
        
        foreach($son_of_grangran as $son_grand){
            $product_f[] = $son_grand['id'];
        }
        $product_cat_fu = $this->fromm->listCategoryByParentArray($product_f);
        $product_cc = array();
        foreach($product_cat_fu as $product_cuoi){
            $product_cc[] = $product_cuoi['id'];
        }        
        $other_product = $this->fromm->listProductByArrayCatID(10,0,$product_cc);
        $data['other_product'] = $other_product;
        //Product image album
        $data['listImg'] = $this->fromm->imgProduct($detailPro['id']);


        $this->load->view('index.header.php', $data);
        $this->load->view('product/product.detail.php', $data);
        $this->load->view('index.footer.php', $data);
    }
    public function search($page=0) {
        if($this->input->get('per_page')){
            $page = $this->input->get('per_page');
        }
        else{
            $page=0;
        }

        $s = $this->input->get("search_field") ? $this->input->get("search_field") : "";
        $data['title_header'] = "Kết quả tìm kiếm";
        $config['base_url'] = base_url("tim-kiem?search_field=" . $s . "&per_page=" . $page);

        $config['total_rows'] = $this->fromm->countSearch($s); // đếm tổng số sản phẩm
        $data['result_count'] = $config['total_rows'];
        $config['cur_page'] = $page;
        $config['per_page'] = 15; 
        $config['num_links'] = CLIENT_NUM_LINKS;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);
        $data['list'] = $this->fromm->listSearch($config['per_page'], $page, $s);
        $this->breadcrumb->append_crumb('Trang chủ', base_url());
        $this->breadcrumb->append_crumb('Kết quả tìm kiếm', base_url('tim-kiem'));
        
        $data['listCateParent'] = $this->fromm->listCategoryByParent(0);

        $data['class_body'] = "news-page";
        $this->load->view('index.header.php', $data);
        $this->load->view('news/search.list.php', $data);
        $this->load->view('index.footer.php', $data);
    }
    /**
     * @todo Gửi email đặt hàng
     * @author Huynh Van Duoc <support@saigondomain.com>
     * @since 20150615
     */
    public function order() {
        $json = array();
        if ($this->input->post()) {
            $data = $this->input->post();
            $name = trim($data['name']);           
            $address = trim($data['address']);
            $email = trim($data['email']);
            $phone = trim($data['phone']);
            $quality = (int) trim($data['quality']);
            $comment = trim($data['comment']);
            if ($name != "" && $email != "" && $phone != "" && $address != "") {
                if (!$this->function->valid_email($email))
                    $json['email_valid'] = "Email không đúng định dạng";
                else if ($quality == 0)
                    $json['contact_require'] = "Vui lòng chọn số lượng.";
                else {
                    $this->fromm->insertOrder();
                    $body = "<h3>Thông tin đặt hàng -------------</h3>";
                    $body.="<p><strong>Họ tên:</strong> " . $name . "</p>";
                    $body.="<p><strong>Địa chỉ:</strong> " . $address . "</p>";
                    $body.="<p><strong>Email:</strong> " . $email . "</p>";
                    $body.="<p><strong>Điện thoại:</strong> " . $phone . "</p>";
                    $body.="<p><strong>Nội dung:</strong> " . $comment . "</p>";
                    $body.="<h3>Thông tin sản phẩm -------------</h3>";
                    $body.="<p><strong>Sản phẩm:</strong> " . $data['name_product'] . "</p>";
                    $body.="<p><strong>Link sản phẩm:</strong> " . $data['link_product'] . "</p>";
                    $body.="<p><strong>Số lượng:</strong> " . $data['quality'] . "</p>";
                    $body.="<p><em>Email này được gửi từ website fromm247.com.vn</em></p>";
                    $subject = "[fromm247 THÔNG TIN ĐẶT HÀNG]";
                    if ($this->send("Đặt hàng từ website - " . $name, $email, $body, $subject, $email)) {
                        $json['success'] = "Cảm ơn bạn, chúng tôi đã nhận được thông tin đặt hàng.";
                    } else {
                        $json['success'] = "Gửi thất bại, vui lòng gọi số hotline: 0969.39.3333";
                    }
                }
            } else {
                $json['error_require'] = "Vui lòng nhập đủ thông tin";
            }
        }
        echo json_encode($json);
    }
    
    
    
    /**
     * @todo Danh sách tin
     * 
     */
    public function newsList($page = 0) {
        $data['title_header'] = "Tin tức";
        $data['class_body'] = "news-page";
        $data['active_menu'] = "tin-tuc";
        $config['base_url'] = base_url("tin-tuc");
        $config['first_url'] = base_url("tin-tuc");
        $config['total_rows'] = $this->fromm->countNews();
        $config['cur_page'] = $page;
        $config['per_page'] = CLIENT_PER_PAGE;
        $config['num_links'] = 5;
        $this->pagination->initialize($config);
        $data['list'] = $this->fromm->listNewsByCategory($config['per_page'], $page);
        $this->load->view('index.header.php', $data);
        $this->load->view('news/news.list.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    public function news_detail($tag = "") {
        $data['detail'] = $this->fromm->detailNews($tag);
        if (!$data['detail']) {
            redirect(base_url());
        }
        $data['title_header'] = $data['detail']['title'];
        $data['class_body'] = "news-details";
        $data['active_menu'] = "tin-tuc";

        // breadcrumb
        $this->breadcrumb->append_crumb('Trang chủ', base_url());
        $this->breadcrumb->append_crumb("Tin tức", site_url("tin-tuc"));

        $this->load->view('index.header.php', $data);
        $this->load->view('news/news.detail.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    /**
     * @todo Thư viên
     * @author Huynh Van Duoc  <support@saigondomain.com>
     * @since 20150610
     */
    public function library() {
        $data['title_header'] = "Thư viện";
        $data['active_menu'] = "thu-vien";
        $data['class_body'] = "albums";

        // breadcrumb
        $this->breadcrumb->append_crumb('Trang chủ', base_url());
        $this->breadcrumb->append_crumb("Thư viện", site_url("thu-vien"));

        $data['album_hinhkhac'] = $this->fromm->listAlbum(1);
        $data['album_tieccuoi'] = $this->fromm->listAlbum(0);

        $this->load->view('index.header.php', $data);
        $this->load->view('library/library.album.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    /**
     * @todo Thư viên
     * @author Huynh Van Duoc  <support@saigondomain.com>
     * @since 20150610
     */
    public function libraryDetail($slug, $id = 0) {
        $detailAlbum = $this->fromm->detailAlbum($slug, $id);
        if (!$detailAlbum) {
            redirect(base_url());
        }
        $data['title_header'] = "Thư viện: " . $detailAlbum['title'];
        $data['active_menu'] = "thu-vien";
        $data['class_body'] = "albums";

        $data['detailAlbum'] = $detailAlbum;

        // breadcrumb
        $this->breadcrumb->append_crumb('Trang chủ', base_url());
        $this->breadcrumb->append_crumb("Thư viện", site_url("thu-vien"));
        $this->load->view('index.header.php', $data);
        $this->load->view('library/library.detail.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    

    /**
     * @todo Thư viên Video
     * @author Huynh Van Duoc  <support@saigondomain.com>
     * @since 20150610
     */
    public function libraryVideo() {
        $data['title_header'] = "Video";
        $data['active_menu'] = "thu-vien";
        $this->load->view('index.header.php', $data);
        $this->load->view('library/library.video.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    /**
     * @todo History Page
     * @author Huynh Van Duoc  <support@saigondomain.com>
     * @since 20150720
     */
    public function history() {
        $data['title_header'] = "Liên hệ";
        $data['active_menu'] = "gioi-thieu";
        $this->load->view('index.header.php', $data);
        $this->load->view('page/history.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    public function faq() {
        $data['title_header'] = "Hỏi đáp";
        $data['active_menu'] = "";
        $this->load->view('index.header.php', $data);
        $this->load->view('faq/faq.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    public function submitComment() {
        $out = "";
        if ($this->input->post()) {
            $data = $this->input->post();
            if ($data['name'] !== "" && $data['email'] !== "" && $data['subject'] !== "" && $data['message'] !== "") {
                $this->fromm->submitComment();
                $out = "Cảm ơn bạn, bình luận của bạn sẽ đươc xét duyệt trước khi đăng.";
            } else {
                $out = "Vui lòng nhập đủ các thông tin.";
            }
        }
        echo $out;
    }

    /**
     * @todo Chi tiết album
     * @param string $tag Tag hình ảnh
     * @author Huynh Van Duoc <support@saigondomain.com>
     * @since 20150624
     */
    public function detailAlbum($tag = "") {
        $data['detailAlbum'] = $this->fromm->detailAlbum($tag);
        if (!$data['detailAlbum']) {
            redirect(base_url());
        }
        $data['title_header'] = $data['detailAlbum']['title'];
        $data['lists'] = $this->fromm->listGalleryByAlbum($data['detailAlbum']['id']);
        $this->load->view('index.header.php', $data);
        $this->load->view('albums/albums.detail.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    

    /**
     * @todo Gửi FAQ
     * @author Huynh Van Duoc  <support@saigondomain.com>
     * @since 20150610
     */
    public function sendFAQ() {
        $out = "";
        if ($this->input->post()) {
            $data = $this->input->post();
            if ($data['name'] !== "" && $data['address'] !== "" && $data['email'] != "" && $data['phone_number'] != "" && $data['message']) {
                $adminConfig = $this->myconfig->getConfig();
                $to = $adminConfig['email'];
                $subject = 'Liên hệ từ Website';
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: <info@fromm.com.vn>' . "\r\n";
                $message = '<p>Họ tên:' . $data['name'] . '</p>';
                $message.= '<p>Địa chỉ:' . $data['address'] . '</p>';
                $message.= '<p>Email:' . $data['email'] . '</p>';
                $message.= '<p>Số điện thoại:' . $data['title_question'] . '</p>';
                $message.= '<p>Tiêu đề:' . $data['phone_number'] . '</p>';
                $message.= '<p>Nội dung câu hỏi:' . $data['message'] . '</p>';
                if (@mail($to, $subject, $message, $headers)) {
                    $out = "Gửi câu hỏi, chúng tôi sẽ trả lời bạn trong thời gian sớm nhất!";
                } else {
                    $out = "Gửi thất bại, vui lòng liên hệ Hotline: 19007169!";
                }
            } else {
                $out = "Vui lòng đủ các thông tin";
            }
        }
        echo $out;
    }

    /**
     * @todo : Gửi mail
     * @author : Huỳnh Văn Được - 20121105
     * @copyright : Dpassion
     */
    private function send($name, $from, $body, $subject, $client_mail = FALSE, $bcc = TRUE) {
        $adminConfig = $this->myconfig->getConfig();
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => $adminConfig['smtp_user'],
            'smtp_pass' => $adminConfig['smtp_pass'],
            'mailtype' => 'html'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($from, $name);
        $this->email->to($adminConfig['email']);
        if($client_mail != FALSE){
            $this->email->cc($client_mail);
        }
        if ($bcc) {
            $this->email->bcc($bcc);
        }
        $this->email->subject($subject);
        $this->email->message($body);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }
    
    public function successTuVan() {
        $data['title_header'] = "Thông tin đã được gửi thành công. Cám ơn bạn";
        $this->load->view('index.header.php', $data);
        $this->load->view('tuvan/tuvan.success.php', $data);
        $this->load->view('index.footer.php', $data);
    }

    /**
     * @todo Gửi send TuVan
     * @author Huynh Van Duoc  <support@saigondomain.com>
     * @since 20150908
     */
    public function sendTuVan() {
        $out = "";
        if ($this->input->post()) {
            $data = $this->input->post();
            if ($data['name'] !== "" && $data['address'] !== "" && $data['email'] != "" && $data['phone_number'] != "" && $data['question']) {
                $adminConfig = $this->myconfig->getConfig();
                $to = $adminConfig['email'];
                $subject = 'Gui tu van tu website';
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: <info@fromm.com.vn>' . "\r\n";
                $message = '<p>Họ tên:' . $data['name'] . '</p>';
                $message.= '<p>Địa chỉ:' . $data['address'] . '</p>';
                $message.= '<p>Email:' . $data['email'] . '</p>';
                $message.= '<p>Số điện thoại:' . $data['phone_number'] . '</p>';
                $message.= '<p>Bạn đang mang thai tháng thứ mấy:' . $data['question'] . '</p>';
                if (@mail($to, $subject, $message, $headers)) {
                    $out = "Thông tin đã được gửi thành công. Cám ơn bạn!";
                } else {
                    $out = "Gửi thất bại, vui lòng liên hệ Hotline: 19007169!";
                }
            } else {
                $out = "Vui lòng đủ các thông tin";
            }
        }
        echo $out;
    }


    /**
     * @todo : Default langue
     * @return : en/vn
     * @author : TICH
     * @copyright : Dpassion
     */
    private function getLang() {
        $l = "vn";
        if (!$this->session->userdata('lang')) {
            $l = "vn";
        } else {
            $l = $this->session->userdata('lang');
        }
        return $l;
    }

    public function set_lang($lang) {
        // $this->session->set_userdata(array(
        //     'lang' => $lang
        // ));
        $temp = base_url();
        echo "<script>
            alert('Updating ...');
            window.location.href = '".$temp."';
         </script>";
        

        // redirect($_SERVER['HTTP_REFERER']);
        // exit();
    }

}

/* Location: ./application/controllers/home.php */