<?php

$lang['trangchu'] = "Trang chủ";

$lang['lienhe'] = "Liên hệ";
$lang['gioithieu'] = "Giới thiệu";
$lang['sanpham'] = "Sản phẩm";
$lang['dichvu'] = "Dịch vụ";
$lang['tintuc'] = "Tin tức";
$lang['khachhang_doitac'] = "khách hàng & đối tác";
$lang['thuvien'] = "Thư viện";
$lang['tuyendung'] = "Career";
$lang['hoidap'] = "Hỏi đáp";

// Trang Lien he
$lang['hoten'] = "Họ tên";
$lang['diachi'] = "Địa chỉ";
$lang['dienthoai'] = "Số điện thoại";
$lang['email'] = "Email";
$lang['tinnhan'] = "Nội dung";
$lang['gui'] = "Gửi";

// Trang San Pham
$lang['thongtinsanpham'] = "Thông tin sản phẩm";
$lang['thongso'] = "Thông số";
$lang['guiyeucau'] = "Gửi yêu cầu";
$lang['giaban'] = "Giá bán";
$lang['soluong'] = "Số lượng";
$lang['tiente']  =" đ";

// Trang Tin tức
$lang['xemchitiet'] = "Xem chi tiết";
$lang['tinlienquan'] = "Tin liên quan";
$lang['ungtuyen'] = "Ứng tuyển";




