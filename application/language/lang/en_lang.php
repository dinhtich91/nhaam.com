<?php

$lang['trangchu'] = "Home";
$lang['lienhe'] = "Contact us";
$lang['gioithieu'] = "About us";
$lang['sanpham'] = "Products";
$lang['dichvu'] = "Services";
$lang['tintuc'] = "News";
$lang['khachhang_doitac'] = "Clients & Partners";
$lang['thuvien'] = "Gallery";
$lang['tuyendung'] = "Career";
$lang['hoidap'] = "faqs";

// Trang Lien he
$lang['hoten'] = "Your name";
$lang['diachi'] = "Address";
$lang['dienthoai'] = "Phone number";
$lang['email'] = "Email";
$lang['tinnhan'] = "Your message";
$lang['gui'] = "Send";

// Trang San Pham
$lang['thongtinsanpham'] = "Infomation";
$lang['thongso'] = "Specifications";
$lang['guiyeucau'] = "Send Contact";
$lang['giaban'] = "Price";
$lang['soluong'] = "Quantity";
$lang['tiente']  =" USD";

// Trang Tin tức
$lang['xemchitiet'] = "Read more";
$lang['tinlienquan'] = "Related News";
$lang['ungtuyen'] = "Apply";

$lang['dangcapnhat'] = "Updating ...";
$lang['lienhechungtoi'] = "Contact";
