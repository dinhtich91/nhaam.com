<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/**
 * author: Huỳnh Văn Được <br/>
 * date:20121017
 * Chức năng: Trả về đường dẫn của thư mục admin
 */ 
define('PATH_FOLDER_ADMIN','admincp');        // Thư mục admin    
define('PATH_EDITOR','/from/');               // phải có dấu "/" phía cuối
define('FOLDER_UPLOAD',"upload");                   // Thư mục upload dữ liệu
define('FOLDER_RESIZE','resize');                   // Thư mục rezise ex: upload/resize
define('PATH_SITE',dirname($_SERVER['PHP_SELF']));


/* TEXT*/
define('UPDATING','Đang cập nhật');                 // Text đang cập nhật
define('MSG_ADD_SUCCESS','Thêm thành công !');      // Text thêm thành công
define('MSG_ADD_ERROR','Thêm thất bại !');          // Text thêm thất bại
define('MSG_DEL_SUCCESS','Xóa thành công !');       // Text xóa thành công
define('MSG_EDIT_SUCCESS','Sửa thành công !');      // Text thêm thành công


define('IMG_VN', '<img src="access/image/flags/vn.png"/>');
define('IMG_EN', '<img src="access/image/flags/gb.png"/>');
define('IMG_DE', '<img src="access/image/flags/de.png"/>');

define('STATUS_1','access/image/1.png');                        // Trạng thái bật
define('STATUS_0','access/image/0.png');                        // Trạng thái ẩn
define('IMG_LOADING','access/image/loading.gif');           // Trạng thái ẩn
define('ICON_UPLOAD', 'access/image/filemanager/folder.png');   // icon upload
define('ICON_DEL', 'access/image/b_drop.png'); // icon xóa
define('ICON_ADD', 'access/image/filemanager/upload.png');      // icon thêm
define('ICON_UPLOAD_PHOTO', 'access/image/filemanager/upload_photo.png');      // icon thêm



/* UPLOAD */
define('PATH_NORMAL', 'upload/photo/normal');                 // hình bình thường
define('PATH_LARGE', 'upload/photo/large');                   // hình bình thường
define('PATH_THUMB', 'upload/photo/thumb');                   // hình bình thường
define('PATH_SMALL', 'upload/photo/small');                   // hình bình thường
define('ICON_ADD_NEWS', 'access/image/filemanager/list_add.png');      // icon thêm
define('PATH_LOGO', 'upload/logo');      // icon thêm

// pagination
define('ADMIN_PER_PAGE','25');                                    // Phân trang admin 25 rows/trang
define('ADMIN_NUM_LINKS','100');                                  // Hiển thị 100 link

// pagination client
define('CLIENT_PER_PAGE','10');                                    // Phân trang admin 25 rows/trang
define('CLIENT_NUM_LINKS','100');                                  // Hiển thị 100 link


// image large
define('W_IMG_LARGE','980');   
define('H_IMG_LARGE','408');

// image large
define('W_IMG_PRODUCT_AVATAR','170');   
define('H_IMG_PRODUCT_AVATAR','110');

// image large
define('W_IMG_PRODUCT_DETAIL','250');   
define('H_IMG_PRODUCT_DETAIL','327');

// image large
define('W_IMG_COMMUNICATION','656');   
define('H_IMG_COMMUNICATION','450');

//Sophie vá các bạn
define('W_IMG_SLIDE_THUMB','75');   
define('H_IMG_SLIDE_THUMB','116');

//Đồ chơi
define('W_IMG_SLIDE_DOCHOI','140');   
define('H_IMG_SLIDE_DOCHOI','105');

// MAX số lượng đặt
define('MAX_NUM','1000');

/* End of file constants.php */
/* Location: ./application/config/constants.php */
