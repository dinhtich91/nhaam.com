<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */


$route['default_controller'] = "home";
$route[PATH_FOLDER_ADMIN] = PATH_FOLDER_ADMIN . '/homepage';
$route[PATH_FOLDER_ADMIN . "/(:any)"] = PATH_FOLDER_ADMIN . '/$1';
$route[PATH_FOLDER_ADMIN . "/(:any)/(:any)"] = PATH_FOLDER_ADMIN . '/$1/$2';
$route[PATH_FOLDER_ADMIN . '/login'] = PATH_FOLDER_ADMIN . '/homepage/login';
$route[PATH_FOLDER_ADMIN . '/logout'] = PATH_FOLDER_ADMIN . '/homepage/logout';


$route['en']="home/set_lang/en";
$route['vn']="home/set_lang/vn";
$route['de']="home/set_lang/de";


$route['trang-chu'] = 'home/index';
$route['lien-he'] = 'home/contact';
$route['gioi-thieu'] = 'home/aboutus';
$route['gioi-thieu/(:any)'] = 'home/aboutus/$1';

$route['san-pham'] = 'home/sanpham';
$route['san-pham/(:any)'] = 'home/sanpham/$1';

$route['dich-vu'] = 'home/dichvu';
$route['dich-vu/(:any)'] = 'home/dichvu/$1';

$route['tin-tuc'] = 'home/tintuc';
$route['tin-tuc/(:any)'] = 'home/tintuc/$1';
$route['tin-tuc/(:any)/(:num)'] = 'home/tintuc/$1/$2';
$route['tin-tuc-chi-tiet/(:any)'] = 'home/tintucdetail/$1';

$route['tuyen-dung'] = 'home/tuyendung';
$route['tuyen-dung/(:any)'] = 'home/tuyendung/$1';

$route['hoi-dap'] = 'home/hoidap';
$route['hoi-dap/(:num)'] = 'home/hoidap/$1';

$route['thu-vien'] = 'home/thuvien';
$route['thu-vien/(:any)'] = 'home/thuvien/$1';

$route['doi-tac'] = 'home/doitac';
$route['doi-tac/(:any)'] = 'home/doitac/$1';

$route['getWork/(:num)'] = 'home/getWork/$1';


// $route['phuong-thuc-giao-hang'] = 'home/phuongThucGiaoHang';
// $route['phuong-thuc-thanh-toan'] = 'home/phuongThucThanhToan';

// $route['tin-tuc/(:num)'] = 'home/newsList/$1';
// $route['tin-tuc'] = 'home/newsList';
// $route['tin-tuc/(:any)'] = 'home/news_detail/$1';


// $route['(:any)/(:any)/(:any)'] = 'home/productDetail/$1/$2/$3';
// $route['gio-hang'] = 'cart/index';
// $route['xoa-san-pham/(:any)'] = 'cart/delete/$1';
// $route['them-vao-gio-hang/(:num)'] = 'cart/addToCart/$1';
// $route['mua-ngay/(:num)'] = 'cart/addQuickCart/$1';
// $route['cart/update'] = 'cart/update';
// $route['cart/(:any)'] = 'cart/$1';
// $route['change-city/(:num)/(:num)'] = 'cart/changeCity/$1/$2';
// $route['tai-khoan'] = 'cart/profile';
// $route['login'] = 'cart/login';
// $route['kiem-tra-don-hang'] = 'cart/history';
// $route['checkorder'] = 'cart/checkorder';
// $route['forgetpassword'] = 'cart/forgetPassword';

// $route['home/sendEmail'] = 'home/sendEmail';
// $route['home/order'] = 'home/order';

// $route['thu-vien'] = 'home/library';
// $route['thu-vien-chi-tiet/(:any)-(:num)'] = 'home/libraryDetail/$1/$2';

// $route['(:any)/(:num)'] = 'home/productCatList/$1/$2';


// $route['(:any)'] = 'home/productCatList/$1';

// $route['thu-vien/video'] = 'home/libraryVideo';
// $route['thu-vien-hinh/(:any)'] = 'home/detailAlbum/$1';
// $route['tim-kiem'] = 'home/search/$1';
// $route['submit-comment'] = 'home/submitComment';

// $route['loai-san-pham/(:any)'] = 'home/listCatProduct/$1';
// $route['san-pham'] = 'home/product';
// $route['home/filter'] = 'home/filter';
// $route['send-contact'] = 'home/sendContact';
// $route['home/sendFAQ'] = 'home/sendFAQ';
// $route['home/sendTuVan'] = 'home/sendTuVan';
// $route['gui-tu-van-thanh-cong'] = 'home/successTuVan';
// $route['san-pham/(:any)/(:any)'] = 'home/detailProduct/$1/$2';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
